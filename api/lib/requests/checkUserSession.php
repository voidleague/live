<?php

use ClassLib\Auth;

class checkUserSession extends apiBase {

    protected function process()
    {
        if(!empty($_COOKIE[Auth::$av])) {
            $user_data = [];
            $result = Auth::checkToken($_COOKIE[Auth::$av], $user_data);
            if($result == 7) {
                return true;
            }
        }
        return false;
    }

}