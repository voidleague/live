<?php

class ApiCall {
    /**
     * @var array
     */
    private $allowed_requests = [
        'checkUserSession'
    ];

    /**
     * @var string
     */
    private $requests_folder = REQ;

    /**
     * @var string
     */
    private $request = '';

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @throws ErrorException
     */
    public function call()
    {
        $args = explode('/', $_SERVER['REQUEST_URI']);

        if (!in_array($args[2], $this -> allowed_requests)) {
            throw new ErrorException('Illegal API request');
        }

        $requestFile = $this -> requests_folder . $args[2] . '.php';
        if (!file_exists( $requestFile )) {
            throw new ErrorException('Request not found');
        }

        require_once($this -> requests_folder . '/apiBase.php');
        require_once($requestFile);

        $requestCall = new $args[2];
        $requestCall -> request = $args[2];
        if (!empty($args[3])) {
            $requestCall -> params = $args[3];
        }

        $this -> response = $requestCall -> process();

    }

}