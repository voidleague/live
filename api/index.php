<?php
/**
 * Api v1.0 for www.voidleague.com
 * By marislazda@gmail.com
 * 08.06.2015
 */

define("APP", (dirname(dirname(__FILE__))).'/application/');
define("REQ", (dirname(__FILE__)).'/lib/requests/');

// Config
require APP.'controllers/classes/idiorm.php';
require APP.'controllers/config.php';

// Autoloads
require APP.'vendor/autoload.php';
require APP.'void.classes/autoload.php';
require_once('lib/ApiCall.php');

try {
    $apiCall = new ApiCall();
    $apiCall -> call();
    $output = [
        'status' => 1,
        'response' => $apiCall->response
    ];
} catch (ErrorException $error) {
    $output = [
        'status' => -1,
        'error' => $error->getMessage()
    ];
}

header('Content-Type: application/json');
echo json_encode($output);



