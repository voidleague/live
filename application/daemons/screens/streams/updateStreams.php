<?php
#SLEEP-TIMEOUT 30

define("APP", dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/application/');
// Autoloads
require APP . 'vendor/autoload.php';
require APP . 'void.classes/autoload.php';
include APP . "controllers/classes/idiorm.php";
include APP . "controllers/config.php";


use ClassLib\Streams\Twitch;

$getStreams = ORM::for_table('streams')->find_array();

foreach($getStreams AS $stream) {

    $twitch_username = end(explode('/', $stream['link']));
    $stream_data = Twitch::getChannelStream($twitch_username);

    $stream = ORM::for_table('streams')->find_one($stream['id']);

    if(!empty($stream_data['stream'])) {

        $stream_data = $stream_data['stream'];
        $stream->image = $stream_data['preview']['medium'];
        $stream->viewers = $stream_data['viewers'];
        $stream->save();

    } else {

        if(empty($stream->official)) {
            $stream->delete();
        } else {
            $stream->viewers = 0;
            $stream->image = '';

            $stream->save();
        }

    }

}