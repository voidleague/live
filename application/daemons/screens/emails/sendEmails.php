<?php
#SLEEP-TIMEOUT 5

define("APP", dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/application/');
// Autoloads
require APP . 'vendor/autoload.php';
require APP . 'void.classes/autoload.php';
include APP . "controllers/classes/idiorm.php";
include APP . "controllers/config.php";

// get unset emails
$getEmails = ORM::for_table('emails')
    ->where('sent', 'N')
    ->order_by_asc('id')
    ->find_array();

if(!empty($getEmails)) {
    // Init phpmailer
    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@voidleague.com';
    $mail->Password = 'voidleague_pr0';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->isHTML(true);

    foreach($getEmails AS $email) {
        $mail->From = $email['from'];
        $mail->FromName = (!empty($email['from_name']) ? $email['from_name'] : 'VoidLeague');
        $mail->addAddress($email['to']);
        $mail->Subject = $email['subject'];
        $mail->Body = $email['body'];

        if($mail->send()) {
            $email = ORM::for_table('emails')->find_one($email['id']);
            $email->sent = 'Y';
            $email->set_expr('sent_gmt', 'NOW()');
            $email->save();
        }
    }
}

$fp = fopen('/var/www/email_screen', 'w');
fwrite($fp, date('Y-m-d H:i:s')."\n");
fclose($fp);
