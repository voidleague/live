<?php
#SLEEP-TIMEOUT 30

define("APP", dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/application/');
// Autoloads
require APP . 'vendor/autoload.php';
require APP . 'void.classes/autoload.php';
include APP . "controllers/classes/idiorm.php";
include APP . "controllers/config.php";

use ClassLib\Cups;
use ClassLib\Riot;
use ClassLib\Teams;


$log_location = APP.'daemons/screens/cups/log/bracketsUpdate.txt';

// Get active cups
$cups = ORM::for_table('cups')
    ->where('status', 1)
    ->where('automatic', 'Y')
    ->where('other_cup', 'N')
    ->find_array();

if(!empty($cups)) {
    foreach($cups AS $cup) {

        // Get only pairs who have both teams and empty match_id
        $brackets = ORM::for_table('cups_tree')
            ->table_alias('t1')
            ->select_many([
                'entity1' => 't2.entity',
                'entity2' => 't1.entity',
                'pair' => 't1.pair'
            ])
            ->join('cups_tree', 't2.cid = t1.cid AND t1.pair = t2.pair AND t1.id != t2.id', 't2')
            ->join('cups', 'c.id = t1.cid', 'c')
            ->where('t1.cid', $cup['id'])
            ->where_not_equal('t1.entity', 0)
            ->where_not_equal('t2.entity', 0)
            ->where('t1.match_id', 0)
            ->group_by('t1.pair')
            ->order_by_asc('t1.row')
            ->order_by_asc('t1.pair')
            ->find_array();

        if(!empty($brackets)) {

            $file = fopen($log_location,"w");
            fwrite($file,"===================\n");
            fwrite($file,"==== Starting to sync: ".$cup['name']." ====\n");
            fwrite($file,"===================\n");

            foreach($brackets AS $pair) {

                // Try to check only if 30 seconds have passed
                if(empty($pair['last_sync']) || ((time()-30) > strtotime($pair['last_sync']))) {

                    fwrite($file, "---\n");
                    fwrite($file, "Syncing pair: [".$pair['pair']."]\n");
                    fwrite($file, "---\n");

                    $team1_roster = Teams::getTeamRoster($pair['entity1']);
                    $team2_roster = Teams::getTeamRoster($pair['entity2']);



                    fwrite($file, "Team rosters found!\n");
                    fwrite($file, "---\n");

                    // Get last game
                    $sum_id = array_values($team1_roster)[0]['game_id'];

                    fwrite($file, "Looking for last match [by id: ".$sum_id."]...\n");
                    fwrite($file, "---\n");

                    $lastMatchID = Riot::getSummonerLastMatchID($sum_id, $cup['region']);
                    if(!empty($lastMatchID)) {

                        fwrite($file, "Match found! [ID: ".$lastMatchID."]\n");
                        fwrite($file, "---\n");

                        // Get match data
                        fwrite($file, "Gathering match data...\n");
                        fwrite($file, "---\n");

                        echo $lastMatchID;
                        die;

                        $lastMatch = Riot::getMatchInfo($lastMatchID, $cup['region']);
                        if(!empty($lastMatch)) {

                            fwrite($file, "Match data gathered!\n");
                            fwrite($file, "---\n");

                            // Check if rosters have match

                            fwrite($file, "Checking match roster...\n");
                            fwrite($file, "---\n");

                            $validRoster = Riot::checkRosters($team1_roster, $team2_roster, $lastMatch, $sum_id);
                            print '<pre>'.print_r($validRoster, true).'</pre>';
                            die;
                            if($validRoster['status'] == 'valid') {

                                fwrite($file, "Rosters valid!\n");
                                fwrite($file, "---\n");



                                if($validRoster['winner'] == 100) {
                                    $pairWinner = $pair['entity1'];
                                    $pairLoser = $pair['entity2'];
                                } else {
                                    $pairWinner = $pair['entity2'];
                                    $pairLoser = $pair['entity1'];
                                }


                                // Update brackets

                                fwrite($file, "Updating pair......----OK!\n");
                                fwrite($file, "---\n");

                                echo 'googo';
                                die;
                                Cups::updatePair($cup['id'], $pairWinner, $pairLoser, $pair['pair']);

                                die('updated');

                                fwrite($file, "Pair updated----OK!\n");
                                fwrite($file, "---\n");


                            } else {

                                fwrite($file, "Rosters doesn't match!\n");
                                fwrite($file, "---\n");
                                fwrite($file, "<pre>".print_r($validRoster, true)."</pre>");
                                fwrite($file, "---\n");

                            }

                        }

                    } else {
                        fwrite($file, "Match not found!\n");
                        fwrite($file, "---\n");
                    }

                }
            }



            fclose($file);

        }

    }
}
