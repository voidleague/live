<?php
#SLEEP-TIMEOUT 5

date_default_timezone_set("CET");

define("APP", dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/application/');
// Autoloads
require APP . 'vendor/autoload.php';
require APP . 'void.classes/autoload.php';
include APP . "controllers/classes/idiorm.php";
include APP . "controllers/config.php";

use ClassLib\Cups;

$min_teams = 8;

$getCups = ORM::for_table('cups')
    ->table_alias('c')
    ->select([
        'c.id',
        'c.name',
        'c.teams',
        'c.start_time'
    ])
    ->select_expr('(SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = c.id AND cups_teams.status = 1) AS teams_registered')
    ->where('c.status', 0)
    ->where('c.published', 1)
    ->find_array();

if(!empty($getCups)) {
    foreach($getCups AS $cup) {
        $teams = $cup['teams'];
        // Process only if have at least min teams count
        if($cup['teams_registered'] >= $min_teams && $cup['start_time'] <= date('Y-m-d H:i:s')) {
            // Change teams count if registered teams are less then needed
            while(true) {
                if($cup['teams_registered'] <= $teams && $cup['teams_registered'] > ($teams/2)) {
                    break;
                }
                $teams = $teams/2;
            }

            // Update cup teams if doesn't match
            if($teams != $cup['teams']) {
                ORM::raw_execute("UPDATE `cups` SET `teams` = :teams WHERE `id` = :id", [
                   'teams' => $teams,
                   'id' => $cup['id'],
                ]);
            }

            Cups::startCup($cup['id']);
        }
    }
}