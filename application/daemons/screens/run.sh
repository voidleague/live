#!/bin/sh
# check if any argument is passed
if [ -z $1 ]
then
	echo "No file to run. Usage run.sh /path/to/php/file.php";
	exit
fi

# check if file exists and is a normal file
if  [ ! -f $1 ]
then
	echo "File not exists!"
	exit
fi

# check if sleep timeout is found

let "timeout = `cat $1 | grep SLEEP-TIMEOUT | grep -i -E -o -m 1 '[0-9]*' | awk 'BEGIN {secs=0} {secs = \$1} END{print secs}'`";
if [ $timeout -le 0 ]
then
	echo "The script $1 WAS EXECUTED ONCE, because a valid SLEEP-TIMEOUT was not found in the script.";
	php $1 $2 $3
	sleep 15
	exit
fi


# main loop
until [ 1 -eq 2 ]
do
	php $1 $2 $3
	sleep `cat $1 | grep SLEEP-TIMEOUT | grep -i -E -o -m 1 '[0-9]*' | awk 'BEGIN {secs=0} {secs = \$1} END{print secs}'`
done