#!php
<?

exec('whoami', $who);

$screensToLaunch = [
    'emails' => 'emails/sendEmails.php',
    'startcups' => 'cups/startCups.php',
    'streams' => 'streams/updateStreams.php'
];

exec("screen -wipe");
$lines = array();
$status = null;
$str = exec("screen -ls", $lines, $status);
$screenNames = array();
foreach ($lines as $line) {
    if (!empty($line)) {
        $line = trim($line);
        if (preg_match('/[0-9]+\.([a-zA-Z0-9\_]+)/', $line, $matches)) {
            $screenNames[] = $matches[1];
        }
    }
}
foreach ($screensToLaunch as $screen => $path) {
    $path = __DIR__ . '/' . $path;
    if (!is_file($path)) {
        echo "[" . date("Y-m-d H:i:s") . "] FILE NOT EXISTS ($path).\n";
    } else {
        if (!in_array($screen, $screenNames)) {
            if (isset($argv[1]) && $argv[1] == 'check') {
                echo "[" . date("Y-m-d H:i:s") . "] MISSING $screen ($path).\n";
            } else {
                echo "[" . date("Y-m-d H:i:s") . "] Starting screen " . $screen . " (" . $path . ").\n";
                exec("screen -dmS " . escapeshellarg($screen) . " ".__DIR__."/run.sh " . $path);
            }
        } else {
            echo "[" . date("Y-m-d H:i:s") . "] Screen $screen OK.\n";
        }
    }
}