<?php

namespace ClassLib;
use \ORM;

error_reporting(E_ALL);
ini_set("display_errors", 1);

class Comments {

    protected $permission;

    protected $type;

    protected $entity_id;

    protected $params = [];

    protected $userID = false;

    function __construct($type, $entity_id, $params = []) {

        $this->permission = 'manage comments';
        $this->type = $type;
        $this->entity_id = $entity_id;
        $this->params = $params;
    }

    protected function form() {

        if(!$this->userID) {
            return '<div class="bx info">'.t('You need to !login_link to comment', [
                '!login_link'=>'<a href="/login/">'.t('login').'</a>'
            ]).'</div>';
        }

        if(!empty($_POST['comment']) && !empty($this->userID)) {
            $this->addComment();
        }


        $form = [
            'comment' => [
                '#type' => 'textarea',
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Comment')
            ]
        ];
        $output = render_form($form);
        return $output;

    }

    /**
     * Save comment
     */
    protected function addComment() {
        $new_comment = ORM::for_table(PREFIX.'comments')->create();
        $new_comment->nid = $this->entity_id;
        $new_comment->uid = $this->userID;
        $new_comment->type = $this->type;
        $new_comment->date = time();
        $new_comment->comment = $_POST['comment'];
        $new_comment->save();
    }


    protected function delete($id) {
        if($this->permission) {
            ORM::for_table('comments')->find_one($id)->delete();
        }
    }

    protected function loadComments() {
        $comments = ORM::for_table(PREFIX.'comments')
            ->table_alias('c')
            ->select_many([
                'c.id',
                'c.date',
                'c.uid',
                'c.comment',
                'username' => 'u.lietotajvards'
            ])
            ->left_outer_join(PREFIX.'lietotaji','u.id = c.uid', 'u')
            ->where([
                'c.nid' => $this->entity_id,
                'c.type' => $this->type
            ])
            ->order_by_asc('c.id')
            ->find_array();
        return $comments;
    }


    protected function listComments() {
        // Load comments for this article
        $comments = $this->loadComments();
        $output = '';
        $comment_nr = 1;
        if(!empty($comments)) {
            foreach($comments AS $comment) {
                $output .= '<div class="clear"></div>';
                $output .= '<div class="comment">';
                $output .= '<span class="com-username">'.getMember($comment['username']).'</span>';
                $output .= '<span class="com-nr">#'.$comment_nr.'</span> ';
                $output .= '<span class="com-date">'.laikaParveide($comment['date']).'</span>';

                // If admin (delete operation)

                $output .= '<div class="com-text">'.htmlspecialchars($comment['comment']).'</div>';
                $output .= '</div>';
                $output .= '<div class="clear"></div>';

                $comment_nr++;
            }
        }
        return $output;
    }

    public function show($userID) {
        $this->userID = $userID;

        $output = '<div id="comments_list">';
        $output .= '<div id="comments-header"><h3>Discuss</h3></div>';
        if($this->userID) {
            $output .= $this->form();
        }
        $output .= $this->listComments();
        $output .= '</div>';
        return $output;
    }



}