<?php
namespace ClassLib;

use \ORM;
class Cups {
    /**
     * @var array
     */
    private static $pos_letters = [];

    /**
     * @var int
     */
    private static $pos_letters_i = 0;

    /**
     * @var loaded cup
     */
    private static $cup;

    /**
     * Get list of available game servers/regions
     *
     * @param $game
     * @return mixed
     */
    public static function getRegions($game, $prepare = false) {
        $data =  ORM::for_table('gamesaccounts')->where('game',$game)->find_array();
        if($prepare) {
            $prepare_data = [];
            foreach($data AS $d) {
                $prepare_data[$d['value']] = $d['name'];
            }
            return $prepare_data;
        }
        return $data;
    }

    public static function load($cup_id) {
        return ORM::for_table('cups')
            ->table_alias('c')
            ->select('c.*')
            ->select_expr('(SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = c.id AND cups_teams.status = 1) AS teams_registered')
            ->find_one($cup_id);
    }

    public static function processMatch($team1_id, $team2_id, $cup_id) {
        // get teams rosters
        $team1_roster = Teams::getTeamRoster($team1_id);
        $team2_roster = Teams::getTeamRoster($team2_id);
        // For now we only use team1_captain
        $team1_captain = current($team1_roster)['game_id'];
        $cup = self::load($cup_id);
        $lastMatchId = Riot::getSummonerLastMatchId($team1_captain, $cup->region);
        if(!empty($lastMatchId)) {
            $lastMatch = Riot::getMatchInfo($lastMatchId, $cup->region);
            dsm($lastMatch);
            die;
        }
    }

    public static function startCup($cup_id) {
        ORM::raw_execute("DELETE FROM `cups_tree` WHERE `cid` = :id", [
            'id' => $cup_id
        ]);

        self::createTree($cup_id);
        self::updateTree($cup_id);

        ORM::raw_execute("UPDATE `cups` SET `status` = 1, `started_time` = NOW() WHERE `id` = :id", [
            'id' => $cup_id
        ]);
    }

    /**
     * Create array with alphabet range (AA-ZZ)
     */
    private static function createPairLetters() {
        $pos_letters = [];
        foreach(range('A', 'Z') as $letter) {
            $pos_letters[] = $letter;
        }
        foreach(range('A', 'Z') as $letter1) {
            foreach(range('A', 'Z') as $letter2) {
                $pos_letters[] = $letter1.$letter2;
            }
        }

        self::$pos_letters = $pos_letters;
        self::$pos_letters_i = 1;
    }


    private static function createTree($cup_id) {
        self::createPairLetters();

        // Load cup
        $cup = self::load($cup_id);

        if(empty($cup->double_elimination)) {

            $max_row = floor(log($cup->teams,2));
            $side_max = $cup->teams;
            $row = 1;
            while($row <= ($max_row+1)) {
                self::createTreeMake($cup,$row,$side_max);
                $row++;
            }

            if($cup->place3) {
                ORM::raw_execute("INSERT INTO `cups_tree` (`cid`,`pos`,`row`) VALUES ('".$cup->id."','1','_3')");
                ORM::raw_execute("INSERT INTO `cups_tree` (`cid`,`pos`,`row`) VALUES ('".$cup->id."','2','_3')");
            }

        } else { // double elimination

            // Winners bracket
            $max_row = floor(log($cup->teams,2))+1;

            $side_max = $cup->teams;
            $row = 1;
            $double_pair = true;
            while($row <= ($max_row)) {
                self::createTreeMake($cup,$row,$side_max, 0, $double_pair);
                $row++;
            }

            // Loosers bracket
            $side_max = ($cup->teams/2);
            $double_pair = true;
            $row = 1;

            $max_row--;
            //$max_row--;
            if($cup->teams == 16) {
                $max_row++;
            }
            if($cup->teams == 32) {
                $max_row += 2;
            }
            if($cup->teams == 64) {
                $max_row += 3;
            }

            while($row <= ($max_row)) {
                self::createTreeMake($cup,$row,$side_max, 1, $double_pair);
                $row++;
            }
        }
    }

    private static function createTreeMake($cup, $row, &$side_max, $double = 0, &$double_pair = true) {

        $side_i = 1;
        $max_row_raw = $max_row = floor(log($cup->teams,2));
        if($cup->double_elimination) {
            $max_row_raw += 1;
        }

        while($side_i <= $side_max) {
            $pair = (int)round(self::$pos_letters_i/2,0)-1;
            $pair = self::$pos_letters[$pair];
            if($row != ($max_row_raw) && empty($cup->double_elimination) || (!empty($cup->double_elimination) && empty($double) && $row != $max_row_raw || !empty($cup->double_elimination) && !empty($double))) {
                self::$pos_letters_i++;
            } else {
                $pair = ''; // reset letter for winner
            }

            ORM::raw_execute("INSERT INTO `cups_tree` (`cid`,`pos`,`row`,`double`,`pair`) VALUES ('".$cup->id."','".$side_i."','".$row."','".$double."','".$pair."')");

            $side_i++;
        }

        if(empty($cup->double_elimination)) {
            $side_max = $side_max/2;
        } else { // double elimination
            if(empty($double)) {
                $side_max = $side_max/2;
            } else if($double) {
                if($double_pair && (($side_max != 3 && $cup->teams == 8) || $cup->teams != 8)) {
                    $double_pair = false;
                } else {
                    $double_pair = true;
                    $side_max = $side_max/2;
                }
            }
        }
    }


    private static function updateTree($cup_id) {

        $cup = self::load($cup_id);

        $free_slots = ($cup->teams-$cup->teams_registered);

        $participants = array();
        $entities = ORM::for_table('cups_teams')
            ->where('cid', $cup->id)
            ->where('status', 1)
            ->order_by_expr('RAND()')
            ->find_array();
        $type = ($cup->type == 'players') ? 'user' : 'team';
        foreach($entities AS $entity) {
            $rating = ORM::for_table('rank')
                ->where('entity_id', $entity['entity_id'])
                ->where('game', $cup->game)
                ->where('type', $type)
                ->find_one();

            if(!empty($rating)) {
                $entity['rating'] = $rating['rating'];
            }

            if(empty($entity['rating'])) {
                $entity['rating'] = '-'.$entity['entity_id'];
            }
            $participants[] = (array)$entity;

            // Add free slot
            if($free_slots > 0) {
                $participants[] = array(
                    'entity_id'=>0,
                    'rating' => '-'.$entity['rating'],
                );
                $free_slots--;
            }
        }

        // Order by rating
        /*
        $participants = array_sort($participants, 'rating', SORT_DESC);

        // Tournament bracket order
        $count = count($participants);
        // Order entities.
        for ($i = 0; $i < log($count / 2, 2); $i++) {
            $out = array();
            foreach ($participants as $player) {
                $splice = pow(2, $i);
                $out = array_merge($out, array_splice($participants, 0, $splice));
                $out = array_merge($out, array_splice($participants, -$splice));
            }
            $participants = $out;
        }
      */

        // Update database
        $pos = 1;
        foreach($participants AS $key=>$participant) {
            $participant = (object)$participant;
            ORM::raw_execute("UPDATE `cups_tree` SET `entity` = '".$participant->entity_id."' WHERE cid = '".$cup->id."' AND `pos` = '".$pos."' AND `row` = 1 AND `double` = 0");

            // Next row if have free slot
            if($participant->entity_id == 0) {
                $new_pos = round($pos/2,0);
                ORM::raw_execute("UPDATE `cups_tree` SET `entity` = '".$participants[$key-1]['entity_id']."' WHERE cid = '".$cup->id."' AND `pos` = '".$new_pos."' AND `row` = 2 AND `double` = 0");
            }
            $pos++;
        }
    }

    /**
     * @param $cup_id
     * @param $winner
     * @param $loser
     * @param $pair
     * @throws \Exception
     */
    public static function updatePair($cup_id, $winner, $loser, $pair)
    {
        $cup = self::load($cup_id);

        // Get entities data
        $winner = ORM::for_table('cups_tree')
            ->where('cid', $cup->id)
            ->where('entity', $winner)
            ->where('pair', $pair)
            ->find_one();

        $loser = ORM::for_table('cups_tree')
            ->where('cid', $cup->id)
            ->where('entity', $loser)
            ->where('pair', $pair)
            ->find_one();

        // Prepare winner for next row
        $new_pos = round($winner->pos/2);
        $new_row = $winner->row+1;

        // Load teams
        $winner_team = Teams::load($winner->entity,[
            'ranking' => true
        ]);
        $loser_team = Teams::load($loser->entity,[
            'ranking' => true
        ]);

        // Add match
        $match = Match::addMatch($cup_id, [
           'winner' => [
               'id' => $winner_team->id,
               'rating' => $winner_team->rating,
               'score' => 1
           ],
           'loser' => [
               'id' => $loser_team->id,
               'rating' => $loser_team->rating,
               'score' => 0
           ]
        ]);

        if(empty($match)) {
            throw new \Exception('Match not created');
        }


        // Update brackets (now only single)
        ORM::raw_execute("UPDATE
            `cups_tree`
            SET
            `match_id` = :match_id
            WHERE
            `cid` = :cid AND
            `pair` = :pair
        ", [
            'match_id' => $match->id,
            'cid' => $cup->id,
            'pair' => $pair
        ]);

        // Move forward winner if it's not 3/4 match
        if($winner->row != '_3') {

            ORM::raw_execute("UPDATE
                `cups_tree`
                SET
                `entity` = :winner
                WHERE
                `cid` = :cid AND
                `pos` = :pos AND
                `row` = :row
            ", [
                'winner' => $winner_team->id,
                'cid' => $cup->id,
                'pos' => $new_pos,
                'row' => $new_row
            ]);

        }

    }

}