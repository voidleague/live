<?php
namespace ClassLib;

use ORM;
class GameAccounts {

    public static function loadAll($uid) {

        $gameaccounts = [];
        $data =  ORM::for_table('users_gameaccount')
            ->where('uid', $uid)
            ->order_by_asc('value')
            ->find_array();

        foreach($data AS $d) {
            $gameaccounts[$d['id']] = $d;
        }

        return $gameaccounts;

    }

}