<?php

namespace ClassLib;

class Riot {

    /**
     * Unlimited API key from developer.riotgames.com [MarisL account]
     * @var string
     */
    protected static $api_key = '96583bdc-513b-4ee4-9565-33e4d290e3b6';

    /**
     * @var string
     */
    protected static $api_version = 'v1.4';

    /**
     * API version for /league callbacks
     * @var string
     */
    protected static $api_version_league = 'v2.5';

    /**
     * Get summoner ID from name
     *
     * @param $value
     * @param $region
     * @return array
     */
    public static function getSummonerByName($value, $region) {
        $summonerId_url = 'http://'.$region.'.api.pvp.net/api/lol/'.$region.'/'.self::$api_version.'/summoner/by-name/'.rawurlencode($value).'?api_key='.self::$api_key;
        self::log($summonerId_url);
        $summoner_response = @file_get_contents($summonerId_url);
        $summoner_response = json_decode($summoner_response);

        if($summoner_response != false){
            $return = [
                'status' => true,
                'data' => $summoner_response
            ];
        } else {
            $return = [
                'status' => false
            ];
        }
        return $return;
    }

    /**
     * Get summoner league data (league, division)
     *
     * @param $value
     * @param $region
     * @return array
     */
    public static function getSummonerLeague($value, $region) {
        $url = 'https://'.$region.'.api.pvp.net/api/lol/'.$region.'/'.self::$api_version_league.'/league/by-summoner/'.$value.'?api_key='.self::$api_key;
        self::log($url);
        $data = file_get_contents($url);
        $data = json_decode($data);

        if($data != false){
            $return = [
                'status' => true,
                'data' => $data
            ];
        } else {
            $return = [
                'status' => false
            ];
        }
        return $return;
    }


    public static function getSummonerLastMatchID($summonerId, $region) {
        $url = 'https://'.$region.'.api.pvp.net/api/lol/'.$region.'/v1.3/game/by-summoner/'.$summonerId.'/recent?api_key='.self::$api_key;
        self::log($url);
        $data = file_get_contents($url);
        $data = json_decode($data);

        if(empty($data)) {
            return false;
        }

        $match = $data->games[0];
        if(!empty($match->gameId)) {
            return $match->gameId;
        }
        return false;
    }

    public static function getMatchInfo($matchId, $region) {
        $url = 'https://'.$region.'.api.pvp.net/api/lol/'.$region.'/v2.2/match/'.$matchId.'?api_key='.self::$api_key;
        self::log($url);
        $data = file_get_contents($url);
        if(empty($data)) {
            return false;
        }
        $data = json_decode($data);
        return $data;
    }

    /**
     * @param $url
     */
    private static function log($url) {
        \ORM::raw_execute("INSERT INTO `riotgames_requests` SET `url` = :url, `gmt` = NOW()", [
           'url' => $url
        ]);
    }

    /**
     * Check if we have legit roster
     *
     * @param $team1_roster
     * @param $team2_roster
     * @param $match
     */
    public static function checkRosters($team1_roster, $team2_roster, $match, $sum_id) {
        $rosters = self::getMatchRoster($match);
        $entity_team = (isset($rosters[100][$sum_id])) ? 100 : 200;

        $wrong_players = [];
        foreach($rosters AS $team => $roster) {
            foreach($roster AS $ros) {
                if($team == $entity_team) {
                    if(!isset($team1_roster[$ros->summonerId])) {
                        $wrong_players[] = $ros;
                    }
                } else {
                    if(!isset($team2_roster[$ros->summonerId])) {
                        $wrong_players[] = $ros;
                    }
                }
            }
        }

        if(empty($wrong_players)) {
            return [
                'status' => 'valid',
                'winner' => $entity_team
            ];
        }

        return [
            'status' => 'error',
            'rosters' => $wrong_players
        ];
    }

    public static function getMatchRoster($match) {
        $tmp_teams = [];
        foreach($match->participants AS $participant) {
            $tmp_teams[$participant->participantId] = $participant->teamId;
        }

        print '<pre>'.print_r($match, true).'</pre>';
        die;

        $rosters = [];
        foreach($match->participantIdentities AS $participant) {
            $rosters[$tmp_teams[$participant->participantId]][$participant->player->summonerId] = $participant->player;
        }
        return $rosters;
    }
}