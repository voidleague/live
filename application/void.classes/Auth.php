<?php
namespace ClassLib;

class Auth {
    /**
     * Cookie domain name
     * @var
     */
    protected static $domain_name;

    /**
     * Cookie remember time in seconds
     * @var
     */
    private static $remember = '2628000';

    /**
     * Cookie path
     * @var string
     */
    private static $cookie_path = '/';

    /**
     * Cookie name
     * @var string
     */
    public static $av = 'AUTH';

    /**
     * @var string
     */
    private static $user_table = 'bliezamv3_lietotaji';

    /**
     * Check if params have been set
     * @var bool
     */
    private static $init = false;

    private static function init() {
        $domain_name = explode(".",$_SERVER['SERVER_NAME']);
        if(isset($domain_name[2])) {
            self::$domain_name = $domain_name[0].'.'.$domain_name[1].'.'.$domain_name[2];
        }
        self::$remember = 60*60*24*29.5*12*24; // 1 year
        self::$init = true;
    }

    /**
     * Generate secret user password with salt
     * @param $plain_password
     * @return string
     */
    public static function generatePassword($plain_password) {
        return md5(md5($plain_password) . self::cryptPass($plain_password));
    }

    private static function cryptPass($pass, $salt=NULL) {
        $salt_len=7;
        $algo='sha512';
        if (!$salt||strlen($salt)<$salt_len) {
            $salt=uniqid($pass, TRUE);
        }
        $salt=substr($salt, 0, $salt_len);
        $hashed=hash($algo, $salt.$pass);
        return $salt.$hashed;
    }

    private static function getSalt($userID) {
        $salt = \ORM::for_table(self::$user_table)
            ->where('deleted', 'N')
            ->find_one($userID)->salt;
        return $salt;
    }

    private static function getUserID($email) {
        $userID = \ORM::for_table(self::$user_table)
            ->where('deleted', 'N')
            ->where('epasts', $email)
            ->find_one();
        if(!empty($userID)) {
            return $userID->id;
        }
        return false;
    }

    public static function makeSalt() {
        $salt='';
        for($x=0;$x<64;$x++){
            $salt.=chr(mt_rand(0,255)); //ģenerējam sāli ar garumu 512b
        }
        return $salt;
    }

    public static function checkSubmittedPassword($email, $password, $autorenew) {
        $authenticated = false;
        $userID = self::getUserID($email);
        if(!empty($userID)) {
            $crypted_password = self::generatePassword($password);
            if(self::checkPassword($userID, $crypted_password)) {
                $authenticated = self::makeToken($userID, $crypted_password ,$autorenew,time()+self::$remember,'');
            }

        }
        return $authenticated;
    }


    private static function makeToken($userID, $password, $autorenew, $expire, $info){
        $key = pack('H*','86b6de905bbee18b3425f7e5974c1cad7e24ec06a707a2766c5988c20ebc7337');
        $hmac_key = pack('H*','8412789462918462123861789234612378964123763412781273467252347564735');
        $td = mcrypt_module_open('rijndael-256', '', 'cbc', '');
        srand();
        $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $key, $iv);
        $data = $userID.','.bin2hex($password) .','.intval($autorenew).','.intval($expire). ','.ip2long($_SERVER['REMOTE_ADDR']) . ',' . bin2hex($info);//info - papildus mainīgie utt, kas jāsaglabā no vienas lapas uz otru
        $data = $data.',' . hash_hmac('sha512',$data,$hmac_key,false);
        $data = mcrypt_generic($td, $data);
        $data = bin2hex($iv) . ',' . bin2hex($data);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return $data;
    }

    public static function checkToken($data,&$mas){
        $key = pack('H*','86b6de905bbee18b3425f7e5974c1cad7e24ec06a707a2766c5988c20ebc7337');
        $hmac_key = pack('H*','8412789462918462123861789234612378964123763412781273467252347564735');
        $td = mcrypt_module_open('rijndael-256', '', 'cbc', '');
        srand();
        $mm = explode(',',$data);
        if(count($mm) !=2 ){
            return 0;
        }
        $iv = pack('H*',$mm[0]);
        mcrypt_generic_init($td, $key, $iv);
        $data=rtrim(mdecrypt_generic($td,pack('H*', $mm[1])), "\0");
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $mas = explode(',',$data);
        if(count($mas)!=7){
            return 1; // Too much fields
        }
        if($mas[3]<time()){
            return 2; // Session timeout
        }
        if(hash_hmac('sha512',$mas[0].','.$mas[1].','.$mas[2].','.$mas[3].','.$mas[4] . ',' . $mas[5],$hmac_key,false)!=$mas[6]){
            return 3; // Data changed
        }
        if(!($salt = self::getSalt($mas[0]))){
            return 4; // UserID not found
        }
        if(!self::checkPassword($mas[0],pack('H*',$mas[1]))){
            return 5; // Wrong password - Changed?
        }
        $mas[5]=pack('H*',$mas[5]);
        if(ip2long($_SERVER['REMOTE_ADDR']) != $mas[4]){
            return 6; // Data OK - IP changed
        }
        return 7; // OK
    }

    private static function checkPassword($userID, $hash){
        $getUserID = \ORM::for_table(self::$user_table)
            ->where('parole', $hash)
            ->where('deleted', 'N')
            ->find_one($userID);
        if(!empty($getUserID) && $userID == $getUserID->id) {
            return true;
        } else {
            return false;
        }
    }

    public static function logout() {
        setcookie(self::$av,'','0','/');
    }
}
