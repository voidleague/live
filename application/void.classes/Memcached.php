<?php
namespace ClassLib;

class Memcached {

    protected static $mc;

    protected static function connect() {

        if (!self::$mc) {
            self::$mc = new \Memcache();
            return self::$mc->connect(GLOBAL_MEMCACHED_HOST, GLOBAL_MEMCACHED_PORT);
        }

    }

    public static function get($key) {
        self::connect();
        return self::$mc->get($key);
    }

    public static function set($key, $var, $timeout = 0) {
        self::connect();
        return self::$mc->set($key, $var, 0, $timeout);
    }

    public static function replace($key, $var, $timeout = 0) {
        self::connect();
        return self::$mc->replace($key, $var, 0, $timeout);
    }

    public static function delete($key) {
        self::connect();
        return self::$mc->delete($key);
    }

}