<?php

namespace ClassLib;

use \ORM;

class Ranking {

    /**
     * @var int
     */
    private static $default_rating = 1500;

    /**
     * @param $game
     * @param string $type
     * @param string $region
     * @param string $format
     */
    public function reorder($game, $type='user', $region = '',$format = '')
    {
        if($type == 'user') {
            ORM::raw_execute("UPDATE `rank` SET `old` = `now` WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format." AND `status` = 1'");
            ORM::raw_execute("SET @a = 0");
            ORM::raw_execute("UPDATE `rank` SET `now` = @a:=@a+1 WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1 ORDER BY `rating` DESC, `entity_id` ASC");
        }
        else {
            ORM::raw_execute("UPDATE `rank` SET `old` = `now` WHERE `game` = '".$game."'AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1");
            ORM::raw_execute("SET @a = 0");
            ORM::raw_execute("UPDATE `rank` SET `now` = @a:=@a+1 WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1 ORDER BY `rating` DESC, `entity_id` ASC");
        }
    }

    /**
     * @param $S1
     * @param $S2
     * @param $R1
     * @param $R2
     * @return mixed
     */
    private static function calculate($S1,$S2,$R1,$R2)
    {
        if ($S1 != $S2) {
            if ($S1 > $S2) {
                $E = 60 - round(1 / (1 + pow(10, ( ($R2 - $R1) / 400 ))) * 60);
                $R['R3'] = $R1 + $E;
                $R['R4'] = $R2 - $E;
            } else {
                $E = 60 - round(1 / (1 + pow(10, ( ($R1 - $R2) / 400 ))) * 60);
                $R['R3'] = $R1 - $E;
                $R['R4'] = $R2 + $E;
            }
        } else {
            if ($R1 == $R2) {
                $R['R3'] = $R1;
                $R['R4'] = $R2;
            } else {
                if($R1 > $R2) {
                    $E = 60 - round(1 / (1 + pow(10,( ($R1 - $R2) / 400 ))) * 60) - (60 - round(1 / (1 + pow(10, ( ($R2-$R1)/400 ))) * 60));
                    $R['R3'] = $R1 - $E;
                    $R['R4'] = $R2 + $E;
                } else {
                    $E = 60 - round(1 / (1 + pow(10, ( ($R2-$R1) / 400 ))) * 60) - (60 - round(1 / (1 + pow(10, ( ($R1-$R2) / 400))) * 60));
                    $R['R3'] = $R1 + $E;
                    $R['R4'] = $R2-$E;
                }
            }
        }

        $R['S1'] = $S1;
        $R['S2'] = $S2;
        $R['R1'] = $R1;
        $R['R2'] = $R2;

        $R['P1'] = (($R['R3'] - $R['R1']) > 0) ? "+".($R['R3'] - $R['R1']) : ($R['R3'] - $R['R1']);
        $R['P2'] = (($R['R4'] - $R['R2']) > 0) ? "+".($R['R4'] - $R['R2']) : ($R['R4'] - $R['R2']);

        $R['P1'] = round($R['P1'] / 2, 0);
        $R['P2'] = round($R['P2'] / 2, 0);

        if($R['P1'] == 0 || $R['P2'] == 0) {
            $R['P1'] = 1;
            $R['P2'] = -1;
        }
        $R['P2'] = abs($R['P2']);


        return $R;
    }

    /**
     * @param $entity_id
     * @param $game
     * @param string $type
     * @return bool
     */
    private static function check($entity_id, $game, $type='user')
    {
        $check = ORM::for_table('rank')
            ->where('game', $game)
            ->where('entity_id', $entity_id)
            ->where('type', $type)
            ->count();
        if($check) {
            return true;
        }
        return false;
    }

    /**
     * @param $entity_id
     * @param $game
     * @param string $type
     */
    private static function addFirstTime($entity_id,$game,$type='user')
    {
        // add region
        if($type == 'team') {
            $team = ORM::for_table('users_teams')->find_one($entity_id);
            if(!empty($team)) {
                $region = $team->region;
                $format = $team->format;
                $status = ($team->uid > 0) ? 1 : 0;
            }
            else {
                $region = '';
                $format = '';
                $status = '';
            }
        }
        else {
            $region = ORM::for_table('users_gameaccount')->find_one($entity_id)->region;
            $format = '1on1';
            $status = 1;
        }

        $rank = ORM::for_table('rank')->create();
        $rank->status = $status;
        $rank->format = $format;
        $rank->region = $region;
        $rank->entity_id = $entity_id;
        $rank->game = $game;
        $rank->rating = self::$default_rating;
        $rank->type = $type;
        $rank->save();

    }

    /**
     * @param $entity_id
     * @param $game
     * @param string $type
     * @return bool|ORM
     */
    private static function getAllData($entity_id, $game, $type='user')
    {
        return ORM::for_table('rank')
            ->where('game', $game)
            ->where('entity_id', $entity_id)
            ->where('type', $type)
            ->find_one();
    }

    /**
     * @param $entity_id
     * @param $entity_id_score
     * @param $entity_id2
     * @param $entity_id2_score
     * @param $game
     * @param string $type
     * @return bool|ORM|string
     */
    private static function p10($entity_id, $entity_id_score, $entity_id2, $entity_id2_score, $game, $type='user')
    {
        $status = '';
        if($entity_id_score > $entity_id2_score) {
            $status = 'W';
        }
        elseif($entity_id_score < $entity_id2_score) {
            $status = 'L';
        }

        $rank_p10 = ORM::for_table('rank_p10')->create();
        $rank_p10->entity_id = $entity_id;
        $rank_p10->game = $game;
        $rank_p10->status = $status;
        $rank_p10->type = $type;
        $rank_p10->save();

        // count p10
        $p10 = ORM::for_table('rank_p10')->raw_query("SELECT count(*),
								(SELECT count(*) FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `status` = 'W' AND `type` = '".$type."') AS win,
								(SELECT count(*) FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `status` = 'L' AND `type` = '".$type."') AS lost
								FROM `rank_p10` WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."'")->find_one();
        $status = 1;
        if($type == 'team') {
            $uid = ORM::for_table('users_teams')->find_one($entity_id)->uid;
            if(empty($uid)) {
                $status = 0;
            }
        }
        ORM::raw_execute("UPDATE `rank` SET `status` = '".$status."', `p10` = '".$p10->win."-".$p10->lost."' WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."'");
        $p10 = $p10->win.'-'.$p10->lost;

        return $p10;
    }

    /**
     * @param $entity_id
     * @param $game
     * @param string $type
     * @return array|null|string
     */
    public function now($entity_id,$game,$type='user')
    {
        $rank = ORM::for_table('rank')
            ->where('game', $game)
            ->where('entity_id', $entity_id)
            ->where('type', $type)
            ->find_one();

        if(isset($rank->now)) {
            return $rank->now;
        }
        else {
            return '-';
        }
    }

    /**
     * @param $entity_id
     * @param $entity_id_rating
     * @param $entity_id2
     * @param $entity_id2_rating
     * @param $game
     * @param string $type
     */
    public function remove($entity_id, $entity_id_rating, $entity_id2, $entity_id2_rating, $game, $type='user')
    {
        $entity_id_rating = '+'.$entity_id_rating;
        ORM::raw_execute("DELETE FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."' ORDER BY id DESC LIMIT 1");
        ORM::raw_execute("DELETE FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id2."' AND `type` = '".$type."' ORDER BY id DESC LIMIT 1");
        ORM::raw_execute("UPDATE `rank` SET `rating` = `rating`".$entity_id2_rating." WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."' AND `type` = '".$type."'");
        ORM::raw_execute("UPDATE `rank` SET `rating` = `rating`".$entity_id_rating." WHERE `entity_id` = '".$entity_id2."' AND `game` = '".$game."' AND `type` = '".$type."'");
    }

    /**
     * @param $entity_id
     * @param $entity_id_score
     * @param $entity_id2
     * @param $entity_id2_score
     * @param $game
     * @param string $type
     * @param bool $created
     * @param $double
     * @return array
     */
    public static function add($entity_id, $entity_id_score, $entity_id2, $entity_id2_score, $game,$type='user', $created=false, $double = 0) {
        $rating = [];
        $data = new \stdClass();
        $data->entity_id1 = new \stdClass();
        $data->entity_id2 = new \stdClass();

        if(!self::check($entity_id, $game, $type)) {
            self::addFirstTime($entity_id, $game, $type);
        }
        if(!self::check($entity_id2, $game, $type)) {
            self::addFirstTime($entity_id2,$game,$type);
        }

        $data->entity_id1 = self::getAllData($entity_id, $game, $type);
        $data->entity_id2 = self::getAllData($entity_id2, $game, $type);

        // p10
        $data->entity_id1->p10 = self::p10($entity_id, $entity_id_score, $entity_id2, $entity_id2_score, $game, $type);
        $data->entity_id1->p10 = self::p10($entity_id2, $entity_id2_score, $entity_id, $entity_id_score, $game, $type);
        // draw
        if($entity_id_score == $entity_id2_score) {
            $data->entity_id1->draw = $data->entity_id1->draw+1;
            $data->entity_id2->draw = $data->entity_id2->draw+1;
        }
        else {
            $data->entity_id1->win = $data->entity_id1->win+1;
            $data->entity_id2->lost = $data->entity_id2->lost+1;

            $rating = self::calculate($entity_id_score, $entity_id2_score, $data->entity_id1->rating, $data->entity_id2->rating);

            // Loser bracket gets 50% off elo
            if(empty($rating['P1'])) {
                $rating['P1'] = 0;
            }
            if(empty($rating['P2'])) {
                $rating['P2'] = 0;
            }

            if ($double) {
                $rating['P1'] = round($rating['P1'] / 2, 0);
                $rating['P2'] = round($rating['P2'] / 2, 0);
            }
            $data->entity_id1->rating = intval($data->entity_id1->rating)+$rating['P1'];
            $data->entity_id2->rating = intval($data->entity_id2->rating)-$rating['P2'];

        }

        $last_played = (!empty($created) ? $created : time());
        ORM::raw_execute("UPDATE `rank` SET `rating` = '".$data->entity_id1->rating."', `last_played` = '".$last_played."', `win` = '".$data->entity_id1->win."',`lost` = '".$data->entity_id1->lost."',`draw` = '".$data->entity_id1->draw."' WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."' AND `type` = '".$type."'");
        ORM::raw_execute("UPDATE `rank` SET `rating` = '".$data->entity_id2->rating."', `last_played` = '".$last_played."', `win` = '".$data->entity_id2->win."',`lost` = '".$data->entity_id2->lost."',`draw` = '".$data->entity_id2->draw."' WHERE `entity_id` = '".$entity_id2."' AND `game` = '".$game."' AND `type` = '".$type."'");

        return [
            'P1'=>'+'.$rating['P1'],
            'P2'=>'-'.$rating['P2'],
        ];
    }


    /**
     * @param $entity_id
     * @param $game
     * @param string $type
     * @return int
     */
    public function getRating($entity_id, $game, $type='user')
    {
        $rating =  ORM::for_table('rank')
            ->where('game', $game)
            ->where('entity_id', $entity_id)
            ->where('type', $type)
            ->find_one()->rating;

        if(empty($rating)) {
            $rating = 0;
        }

        return $rating;
    }

}