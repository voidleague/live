<?php
namespace ClassLib;

use \ORM;
class Badges {

    public static function getBadges($entity_id, $type='team') {
        return ORM::for_table('badges_nodes')
            ->table_alias('bn')
            ->select_many([
                'bn.entity_id',
                'bn.badge_id',
                'b.title',
                'b.icon'
            ])
            ->join('badges','b.id = bn.badge_id','b')
            ->where('b.deleted','N')
            ->where('bn.type', $type)
            ->where('bn.entity_id', $entity_id)
            ->find_array();
    }

    public static function entityForm($entity_id, $type='team') {
        $existingBadges = self::getBadges($entity_id, $type);

        $output = '<div class="badgesForm">';
        // select
        $output .= '<select style="width:300px;" name="badge" data-entity_id="'.$entity_id.'" data-type="'.$type.'">';
        $badgeOptions = self::badgesOptions();
        foreach($badgeOptions AS $val => $option) {
            $output .= '<option value="'.$val.'">'.$option.'</option>';
        }
        $output .= '</select> <input type="submit" class="addBadge" value="Add" />';


        $output .= '<div class="existingBadges">';
        foreach($existingBadges AS $badge) {
            $output .= '<span class="badge"><img width="32" src="/public/img/badges/'.$badge['icon'].'" alt="" />
            <a href="#" class="removeBadge" data-entity_id="'.$badge['entity_id'].'" data-badge_id="'.$badge['badge_id'].'" data-type="team">[x]</a></span>';
        }

        $output .= '</div>';
        return $output;
    }

    public static function removeBadge($entity_id, $badge_id, $type) {
        ORM::raw_execute("DELETE FROM `badges_nodes` WHERE `type` = :type AND `entity_id` = :entity_id AND `badge_id` = :badge_id LIMIT 1",[
            'type' => $type,
            'badge_id' => $badge_id,
            'entity_id' => $entity_id
        ]);
    }

    public static function addBadge($entity_id, $badge_id, $type, $return = true) {

        ORM::raw_execute("INSERT INTO `badges_nodes` SET `type` = :type, `entity_id` = :entity_id, `badge_id` = :badge_id, `added` = NOW()",[
            'type' => $type,
            'badge_id' => $badge_id,
            'entity_id' => $entity_id
        ]);

        if($return) {
            $badge = ORM::for_table('badges')->find_one($badge_id);
            echo '<span class="badge"><img width="32" src="/public/img/badges/'.$badge['icon'].'" alt="" />
            <a href="#" class="removeBadge" data-entity_id="'.$entity_id.'" data-badge_id="'.$badge_id.'" data-type="team">[x]</a></span>';
        }
    }

    public static function getAvailableBadges() {
        return ORM::for_table('badges')->where('deleted','N')->find_array();
    }

    protected static function badgesOptions() {
        $getBadges = self::getAvailableBadges();
        $badges = ['' => 'Choose badge'];
        foreach($getBadges AS $badge) {
            $badges[$badge['id']] = $badge['title'];
        }
        return $badges;
    }
}