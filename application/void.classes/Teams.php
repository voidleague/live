<?php

namespace ClassLib;

use ORM;

class Teams {

    /**
     * Removes gameaccount from team
     *
     * @param $team_id
     * @param $user_id
     */
    public static function leaveTeam($team_id, $user_id) {
        $member = ORM::for_table('users_teams_members')
            ->where('team', $team_id)
            ->where('uid', $user_id)
            ->find_one();
        if(!empty($member)) {

            // Check if user has in team who is playing in active tournament
            $activeCups = ORM::for_table('cups_teams')
                ->table_alias('ct')
                ->left_outer_join('cups', 'c.id = ct.cid', 'c')
                ->where('ct.status', 1)
                ->where('c.status', 1)
                ->where('ct.entity_id', $team_id)
                ->count();
            if(empty($activeCups)) {
                ORM::raw_execute("DELETE FROM `users_teams_members` WHERE `team` = :team_id AND `uid` = :user_id", [
                    'team_id' => $team_id,
                    'user_id' => $user_id
                ]);
                return '0000';
            } else {
                // Can't leave a team while it's active
                return '0001';
            }
        }
        return false;
    }

    public static function getTeamRoster($team_id) {
        $members = [];
        $getRoster = ORM::for_table('users_teams_members')
            ->select([
                'g.unique_game_id',
                'g.value',
                'g.id'
            ])
            ->table_alias('m')
            ->join('users_gameaccount', 'g.id = m.gid', 'g')
            ->where('m.team', $team_id)
            ->where('m.status', 1)
            ->order_by_asc('m.role')
            ->find_array();
        foreach($getRoster AS $roster) {
            $members[$roster['unique_game_id']] = [
                'summoner' => $roster['value'],
                'game_id' => $roster['unique_game_id'],
                'gameaccount_id' => $roster['id']
            ];
        }
        return $members;
    }

    /**
     * @param $team_id
     * @param array $additional_data
     * @return $this|bool|ORM
     */
    public static function load($team_id, $additional_data = []) {

        $team = ORM::for_table('users_teams')
            ->table_alias('t')
            ->select('t.*');

        if(!empty($additional_data['ranking'])) {
            $team->select_many([
                'now' => 'r.now',
                'rating' => 'r.rating'
            ]);
            $team->left_outer_join('rank', "r.entity_id = t.id AND r.type = 'team'", 'r');
        }

        $getTeam = $team
            ->where('t.id', $team_id)
            ->find_one();

        return $getTeam;
    }

}