<?php
namespace ClassLib;
use \ORM;

class Helper {

    public static function converted_datetime($datetime) {
        if(empty($_SESSION['user_timezone'])) {
            return $datetime;
        }
        $datetime = new \DateTime($datetime);
        $new_timezone = new \DateTimeZone($_SESSION['user_timezone']);
        $datetime->setTimezone($new_timezone);
        return $datetime->format('Y-m-d H:i:s');
    }

    /**
     * Insert email data into database (will be sent by screen)
     * @param $to
     * @param $text
     * @param $uid
     * @param bool $from\
     */
    public static function sendMail($to, $subject, $body, $uid = false, $from = false) {
        if(empty($from)) {
            $from = 'info@voidleague.com';
        }
        $email = ORM::for_table('emails')->create();
        $email->to = $to;
        $email->from = $from;
        $email->set_expr('gmt', 'NOW()');
        $email->subject = $subject;
        $email->body = $body;
        if(!empty($uid)) {
            $email->uid = $uid;
        }
        $email->save();
    }
}