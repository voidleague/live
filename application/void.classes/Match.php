<?php

namespace ClassLib;

class Match {

    public static function addMatch($cup_id, $data=[])
    {

        if(empty($data['winner']['id']) || empty($data['loser']['id'])) {
            throw new \Exception('Entity empty');
        }

        $cup = Cups::load($cup_id);
        $match_type = ($cup->type == 'players') ? 'user' : 'team';
        $R = [
            'P1' => 0,
            'P2' => 0
        ];


        if ($cup->competitive == 'Y') {
            $R = Ranking::add($data['winner']['id'], $data['winner']['score'], $data['loser']['id'], $data['loser']['score'], $cup->game, $match_type, false);
        }

        $match = \ORM::for_table('matches')->create();
        $match->match_type_id = $cup->id;
        $match->winner_entity = $data['winner']['id'];
        $match->looser_entity = $data['loser']['id'];
        $match->winner_score = $data['winner']['score'];
        $match->looser_score = $data['loser']['score'];
        $match->winner_rating = $R['P1'];
        $match->looser_rating = $R['P2'];
        $match->entity_type = $match_type;
        $match->set_expr('created', 'NOW()');
        if (!empty($data['uid'])) {
            $match->uid = $data['uid'];
        }
        $match->save();

        return $match;
    }
}