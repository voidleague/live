<?php
use \ORM;
use ClassLib\Cups;
$regions = Cups::getRegions('LOL', true);

	include 'controllers/front_controllers.php';
    include ROOT.'/application/plugins/find/controllers/find_controller.php';

    if(!empty($front_stream)) {
        print $front_stream;
    }

?>


<div id="slider">
    <div id="navigation">
        <?php
        //$hot_news = db_query("SELECT `nid`,`image`,`title`,`desc` FROM ".PREFIX."news WHERE `hot` = 1 AND language = '' OR language = '".$language."' AND status = 1 ORDER BY nid DESC LIMIT 4");
        $hot_news = db_query("SELECT `nid`,`image`,`title`,`desc`,`slug` FROM ".PREFIX."news WHERE `status` = 1 ORDER BY `updated` DESC LIMIT 2");
        $new_i=1;
        while($hot_new = db_fetch_object($hot_news)) {
            $class = '';
            if($new_i == 1) {
                $hotNew = $hot_new;
                $class = ' active';
            }
            print '<div class="item hot-new new-'.$new_i . $class.'">
				  <div class="image"><img src="/public/img/news/thumb/'.$hot_new->image.'" alt="'.$hot_new->title.'" /></div>
				  <h2>'.$hot_new->title.'</h2>
				  <p class="front-new-desc">'.$hot_new->desc.'</p>
				  <p class="front-new-id">'.$hot_new->slug.'-'.$hot_new->nid.'</p>
				  <p class="front-new-image">'.$hot_new->image.'</p>
				 </div>';
            $new_i++;
        }

            if (!empty($getLastFindteam)) {
                echo '<div id="findteam_front"><ul class="find-slider">';
                $findteam_i = 1;
                foreach ($getLastFindteam AS $findteam) {
                    $findController = new FindController();
                    $find = $findController->load($findteam['id']);
                    $class = ($findteam_i == 1) ? 'active_find' : '';
                    echo '<li onclick="window.location=\'/user/'.$findteam['username'].'/\'">
				  <div class="image"><img height="40px" src="/public/img/lol_icons/tier/'.$findteam['tier'].'.png" alt="'.$findteam['tier'].'" /></div>
				  <div class="front-find-info">
				  <h2>'.$findteam['value'].' '.t('is looking for team').'</h2>
				  <span class="front-find-region">'.$regions[$findteam['region']].'</span>';
                    echo '<div class="find-front-roles">';
                    if(!empty($find['roles'])) {
                        foreach ($find['roles'] AS $role) {
                            echo '<span class="find-role">
                                <img src="/public/img/icons/find/' . $role . '.png" alt="" width="15px"/>
                                </span>';
                        }
                    }
				    echo '</div>
                    <div class="find-front-desc">'.htmlspecialchars($findteam['about']).'</div>
                    </div>
                    <div class="clear"></div>
                    </li>';
                    $findteam_i++;
                }
                echo '</ul></div>';

        }
        ?>
    </div>
    <script>
        $(document).ready(function(){
            $('#findteam_front .find-slider').bxSlider({
                auto:false,
                pager:false
            });
        });
    </script>
    <div id="featured_mark"></div>
    <div id="image"><img src="/public/img/news/full/<?php print $hotNew->image;?>" alt=""/></div>
    <div id="about">
        <h1><a href="/news/read/<?php print $hotNew->slug;?>-<?php print $hotNew->nid;?>/"><?php print $hotNew->title;?></a></h1>
        <p><?php print $hotNew->desc;?></p>
    </div>
</div>
<!-- / slider -->


	<div class="clear"></div>
	<div class="content index_content">
		<div id="left">
            <div id="front-streams">
               <div class="main-title">
                    <h3><?=t('Streams');?></h3>
                    <a href="/stream" class="right-link"><?=t('View all');?></a>
               </div>
                <div class="main-content">
                    <?php if(!empty($streams)):?>
                        <?php foreach($streams AS $stream): ?>
                            <div class="stream">
                                <div class="stream-featured">
                                    <a href="/stream/watch/<?=$stream['slug'];?>-<?=$stream['id'];?>">
                                        <div class="stream-image">
                                            <img src="<?=str_replace('http', 'https', $stream['image']);?>" />
                                        </div>
                                        <span class="streamer">
                                            <?=htmlspecialchars($stream['streamer']);?> (<?=$stream['viewers'];?>)
                                        </span>
                                    </a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div> <!-- Front streams -->
			<div class="clear"></div>
			<div id="front_videos">
                <div class="main-title" style="margin-top:25px;">
                    <h3><?=t('Videos');?></h3>
                    <a href="/video" class="right-link"><?=t('View all');?></a>
                </div>
				<?php foreach($videos AS $video): ?>
                    <?php $video['title'] = htmlspecialchars($video['title']); ?>
					<div class="front-video">
						<div style="background-image: url('<?php print $video['img'];?>');"><a href="/video/i/<?php print $video['vid'];?>/"><img src="/public/images/vid_button.png" alt="" /></a></div>
						<a href="/video/i/<?php print $video['vid'];?>/" class="front-video-title"><?php print $video['title'];?></a>
					</div>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>
            <div id="front-ranking">
                <div class="main-title" style="margin-top:25px;">
                    <h3><?=t('TOP ranking');?></h3>
                    <a href="/rank/LOL" class="right-link"><?=t('View all');?></a>
                </div>
                <?php print $top_teams_data;?>
            </div>
			<div id="latest_comments">
                <div class="main-title" style="margin-top:25px;">
                    <h3><?=t('Latest comments');?></h3>
                </div>

				<?php
				$comments_db = db_query("SELECT ".PREFIX."comments.id,".PREFIX."news.title,".PREFIX."videos.title AS video_title, polls.question, ".PREFIX."comments.nid, ".PREFIX."comments.date,".PREFIX."comments.uid, ".PREFIX."comments.comment,".PREFIX."comments.type, ".PREFIX."lietotaji.lietotajvards
                            FROM ".PREFIX."comments
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."comments.uid=".PREFIX."lietotaji.id
                            LEFT JOIN ".PREFIX."news ON ".PREFIX."news.nid=".PREFIX."comments.nid
                            LEFT JOIN `polls` ON polls.id=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."videos ON ".PREFIX."videos.vid=".PREFIX."comments.nid
                            ORDER BY ".PREFIX."comments.id DESC LIMIT 3");

    $comments_i=0;
    while($comment = db_fetch_object($comments_db))
    {

      $comments_i++;
      print '<div class="comment">';
      if($comment->type=='n') {
        $page = 'news/read';
        $title = $comment->title;
      }
      elseif($comment->type=='v') {
        $title = $comment->video_title;
        $page='video/i';
      }
      else {
        $page = 'polls/results';
        $title = $comment->question;
      }

      print '<p>'.getMember($comment->lietotajvards).': '.htmlspecialchars($comment->comment).'</p>';
      print '<p class="about">'.t('Added').' <span>'.laikaParveide($comment->date).'</span> '.t('in').' "<a href="/'.$page.'/'.$comment->nid.'/">'.htmlspecialchars($title).'</a>"</p>';
      print '</div>';
    }

    if($comments_i==0) {
      print '<div class="comment"><p>'.t('No comments...').'</p></div>';
    }
				?>
				</div>
				<div class="hide_last_sepr"></div>
				<?php if($comments_i > 2): ?>
				<div id="latest_comments2">
								<?php
				$comments_db = db_query("SELECT ".PREFIX."comments.id,".PREFIX."news.title,".PREFIX."videos.title AS video_title, polls.question, ".PREFIX."comments.nid, ".PREFIX."comments.date,".PREFIX."comments.uid, ".PREFIX."comments.comment,".PREFIX."comments.type, ".PREFIX."lietotaji.lietotajvards
                            FROM ".PREFIX."comments
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."comments.uid=".PREFIX."lietotaji.id
                            LEFT JOIN ".PREFIX."news ON ".PREFIX."news.nid=".PREFIX."comments.nid
                            LEFT JOIN `polls` ON polls.id=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."videos ON ".PREFIX."videos.vid=".PREFIX."comments.nid
                            ORDER BY ".PREFIX."comments.id DESC LIMIT 3,6");

    $comments_i=0;
    while($comment = db_fetch_object($comments_db))
    {
      $comments_i++;
      print '<div class="comment">';
      if($comment->type=='n') {
        $page = 'news/read';
        $title = $comment->title;
      }
      elseif($comment->type=='v') {
        $title = $comment->video_title;
        $page='video/i';
      }
      else {
        $page = 'poll';
        $title = $comment->jautajums;
      }

      print '<p>'.getMember($comment->lietotajvards).': '.htmlspecialchars($comment->comment).'</p>';
      print '<p class="about">'.t('Added').' <span>'.laikaParveide($comment->date).'</span> '.t('in').' "<a href="/'.$page.'/'.$comment->nid.'/">'.htmlspecialchars($title).'</a>"</p>';
      print '</div>';
    }

    ?>
				</div>
                <div class="clear"></div>
				<div class="load_more comments"><a href="#"><?php print t('More comments'); ?></a></div>
				<?php endif;?>

			<div class="clear"></div>


		</div><!-- left -->

		<div id="right">
			<div id="follow">
			<a id="twitter" href="https://www.twitter.com/VoidLeague" target="_blank"><img src="/public/img/twitter_ico.png" alt="" /></a>
			<a id="youtube" href="https://www.youtube.com/voidleaguelol" target="_blank"><img src="/public/img/youtube_ico.png" alt="" /></a>
			<a id="facebook" href="https://www.facebook.com/VoidLeague" target="_blank"><img src="/public/img/facebook_ico.png" alt="" /></a>
			<a id="twitch" href="https://www.twitch.tv/voidleague" target="_blank"><img src="/public/img/twitch_ico.png" alt="" /></a>
			</div>
            <div id="donation_button" style="clear: both; margin-bottom: 10px;">
                <a href="/donations"><img src="/public/img/donations_button3.png" alt="" /></a>
            </div>
			<div class="side side_style1 side_tournaments">
				<div class="block">
					<div class="title">
						<h3><?php print t('Opened & active cups'); ?></h3>
					</div>
					<div class="block_content">

						<?php
						$cups = db_query("SELECT *, CONVERT_TZ(`start_time`,'CET','".$_SESSION['user_timezone']."') AS `start_time`, CONVERT_TZ(`checkin_date`,'CET','".$_SESSION['user_timezone']."') AS `checkin_date` FROM `cups` WHERE `status` != 2 AND `published` = 1 AND `other_cup` = 'N' ORDER BY `start_time` ASC");
                        $cups_i = 0;
						while($cup = db_fetch_object($cups)) {
                            $cup_date = date('d.m.Y H:i',strtotime($cup->start_time));
                            if(strtotime($cup->checkin_date) < time() || time() > strtotime($cup->start_time)) {
                                $cup_check_count = 1;
                            } else {
                                $cup_check_count = 0;
                            }
                            $entities = db_result(db_query("SELECT count(*) FROM `cups_teams` WHERE `status` = ".$cup_check_count." AND `cid` = '".$cup->id."'"));
								print '<div class="side_tournament">';
								print '<div class="type"><a href="#"><img src="/public/images/games/icons/'.$cup->region.'.png" alt=""/></a></div>';
								print '<h5><span class="cup_status status-'.$cup->status.'"></span><a href="/cups/id/'.$cup->slug.'-'.$cup->id.'"> '.$cup->name.'</a></h5>';
								print '<p class="registered">'.t('Cup').' <span>'.$cup_date.'</span></p>';
								print '<span class="user-count">'.t(ucfirst($cup->type)).': '.$entities.'</span>';

								print '</div>';
                            $cups_i++;
						}

                        if(empty($cups_i)) {
                            print '<p>'.t('There are no opened or active cups...').'</p><br />';
                        }
                        ?>
						<div class="hide_last_sepr"></div>
					</div>
				</div>
			</div>
<br />

            <?php if(!empty($otherCups)): ?>
                <div class="side side_style1 side_tournaments">
                    <div class="block">
                        <div class="title">
                            <h3><?php print t('Other cups'); ?></h3>
                        </div>
                        <div class="block_content">

                            <?php
                            foreach($otherCups AS $cup) {
                                $cup = (object)$cup;
                                $cup_date = date('d.m.Y H:i',strtotime($cup->start_time));
                                if(strtotime($cup->checkin_date) < time() || time() > strtotime($cup->start_time)) {
                                    $cup_check_count = 1;
                                } else {
                                    $cup_check_count = 0;
                                }
                                $entities = db_result(db_query("SELECT count(*) FROM `cups_teams` WHERE `status` = ".$cup_check_count." AND `cid` = '".$cup->id."'"));
                                print '<div class="side_tournament">';
                                print '<div class="type"><a href="#"><img src="/public/images/games/icons/'.$cup->region.'.png" alt=""/></a></div>';
                                print '<h5><span class="cup_status status-'.$cup->status.'"></span><a href="/cups/id/'.$cup->slug.'-'.$cup->id.'"> '.$cup->name.'</a></h5>';
                                print '<p class="registered">'.t('Cup').' <span>'.$cup_date.'</span></p>';
                                print '<span class="user-count">'.t(ucfirst($cup->type)).': '.$entities.'/'.$cup->teams.'</span>';

                                print '</div>';
                                $cups_i++;
                            }

                            ?>
                            <div class="hide_last_sepr"></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(!empty($_SESSION['user_country']) && $_SESSION['user_country'] == 'LV'): ?>
                <div id="strike_lv" style="width:350px; height:390px; clear:both;">
                    <a href="https://strike.lv" target="_blank" rel="nofollow">
                        <img width="340" height="390" src="/public/img/strike_lv.jpg" alt="" />
                    </a>
                </div>
            <br />
            <?php endif; ?>
            <?php
              print $top_teams.'<br /><br />';
            ?>

            <div class="side side_style2 side_poll">
                <div class="block">
                    <div class="title">
                        <h3><?php print t('Poll'); ?><!--a href="/archive/polls/"><?php print t('Archive'); ?></a>--></h3>
                    </div>
                    <div class="block_content">
                        <?php
                        include_once ROOT.'/application/plugins/polls/controllers/polls_controller.php';
                        $poll = new PollsController();
                        print $poll->block();
                        ?>

                    </div>
                </div>
            </div>

<?php
$statisticsFront = \ClassLib\Memcached::get('statisticsFront');
if(!$statisticsFront || 1==1) {
    $count_users = mysql_result(queryy("SELECT count(*) FROM ".PREFIX."lietotaji WHERE activated = 1"),0,0);
    $last_user = db_fetch_object(db_query("SELECT lietotajvards,country FROM ".PREFIX."lietotaji WHERE `activated` = 1 ORDER BY id DESC"));
    $count_cups = mysql_result(queryy("SELECT count(*) FROM `cups` WHERE status = 2 AND published = 1"),0);
    $count_cups2 = mysql_result(queryy("SELECT count(*) FROM `cups` WHERE status != 2 AND published = 1"),0);

    $statisticsFront = [
        'total_users' => $count_users,
        'last_user' => $last_user,
        'cups_closed' => $count_cups,
        'cups_active' => $count_cups2
    ];
    \ClassLib\Memcached::set('statisticsFront', $statisticsFront, 60);
}

    $count_users = $statisticsFront['total_users'];
    $last_user = $statisticsFront['last_user'];
    $count_cups = $statisticsFront['cups_closed'];
    $count_cups2 = $statisticsFront['cups_active'];

    $tenMinutesAgo = time()-60*10;
    $all_online = $visitors_online->count_users();
    $reg_online = mysql_result(queryy("SELECT count(*) FROM ".PREFIX."lietotaji WHERE online > '$tenMinutesAgo'"),0);
    $guests_online = round($all_online-$reg_online);
    $newest_team = db_fetch_object(db_query("SELECT `name`,`slug`,`id`,`country` FROM `users_teams` WHERE `status` = 1 ORDER BY `id` DESC LIMIT 1"));
?>


			<div class="side side_style3 side_statistic">
				<h3><?php print t('Statistic'); ?></h3>
				<div class="block">
					<ul class="tabs">
						<li class="active"><a href="#" class="members"><?php print t('Users'); ?></a></li>
						<li><a href="#" class="tournaments"><?php print t('Cups'); ?></a></li>
					</ul>
					<div class="block_content">
						<div id="members"  class="stats">

			            <p><?php print t('Users');?>: <span><?php print $count_users; ?></span></p>
			            <p><?php print t('Online');?>: <span><?php print $all_online; ?></span></p>
			            <p><?php print t('Members online');?>: <span><?php print $reg_online; ?></span></p>
			            <p><?php print t('Guests online');?>: <span><?php print $guests_online; ?></span></p>
			            <br /><br />
			            <p><?php print t('Latest registered user');?>: <span> <img src="/public/images/flags/<?php print strtolower($last_user->country); ?>.png" alt="" /> <?php print getMember($last_user->lietotajvards); ?></span></p>
                        <p><?php print t('Latest team');?>: <span><img src="/public/images/flags/<?php print $newest_team->country;?>.png" alt="" /> <a href="/team/id/<?php print $newest_team->slug;?>-<?php print $newest_team->id;?>"><?php print htmlspecialchars($newest_team->name); ?></a></span></p>
                        <p style="margin-top:10px;">
                  <h2><?php print t('Members online'); ?></h2>
								  <?php

					$online_l = db_query("SELECT ".PREFIX."lietotaji.id, ".PREFIX."lietotaji.lietotajvards,".PREFIX."lietotaji.country, r.color AS group_color
										FROM ".PREFIX."lietotaji
                                        LEFT JOIN `users_roles` ur ON ".PREFIX."lietotaji.id = ur.uid
                                        LEFT JOIN `role` r ON r.rid = ur.rid
										WHERE ".PREFIX."lietotaji.online > '$tenMinutesAgo'
										ORDER BY ".PREFIX."lietotaji.online DESC");
						$o_c = $reg_online;

                    $online_members = [];
					while($online = mysql_fetch_assoc($online_l)) {
                        $on_mem = getMember($online['lietotajvards'],['color'=>$online['group_color'],'country'=>$online['country']]);
                        $online_members[] = $on_mem;
                    }

                    echo '<div style="display:inline-block;">'.implode(',</div> <div style="display:inline-block;">',$online_members).'</div>';


								  ?>
                  </p>

						</div>
						<div id="tournaments"  class="stats">
	            <p><?php print t('Closed cups'); ?>: <span><?php print $count_cups;?></span></p>
	            <p><?php print t('Active cups'); ?>: <span><?php print $count_cups2;?></span></p>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
