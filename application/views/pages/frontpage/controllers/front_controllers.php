<?php
use \ORM;
use ClassLib\Memcached;

// Get front videos
$videos = Memcached::get('front_videos');
if(!$videos) {
    include $_SERVER['DOCUMENT_ROOT'].'/application/controllers/classes/videos.class.php';
    $y = new Youtube;
    $videos = ORM::for_table('bliezamv3_videos')
        ->order_by_desc('vid')
        ->limit(4)
        ->find_array();
    foreach($videos AS $vid => $video) {
        $y->load($video['video']);
        $videos[$vid]['img'] = $y->getThumbnail();
    }
    Memcached::set('front_videos', $videos, 300);
}


    //Top teams
    $top_teams = '<div class="team profile"><div class="profile-header"><section class="section"><header><h2 class="title">'.t('Top teams').'</h2></header><div class="table matches expanded"><div class="body">';
        $topteams = db_query("SELECT rank.rating, users_teams.* FROM `rank` LEFT JOIN `users_teams` ON users_teams.id = rank.entity_id WHERE rank.type = 'team' AND users_teams.status = 1 ORDER BY `rating` DESC LIMIT 5");
        $top_i=0;
        while($topteam = db_fetch_object($topteams)) {
            $top_teams .= '<a href="/team/id/'.$topteam->slug.'-'.$topteam->id.'" class="row"><div class="top-team">';
            if(empty($topteam->picture)) {
                $topteam->picture = 'noimage.png';
            }

            if(!empty($topteam->picture)) {
                $top_teams .= '<img src="/public/images/users_teams/'.$topteam->picture.'" alt="" class="team-avatar" width="28" />';
            }

            $top_teams .= '    <h2 class="profile-name"><img src="/public/images/flags/'.$topteam->country.'.png"  alt="" /> '.htmlspecialchars($topteam->name).'</h2>';
            $top_teams .= '</div><div class="result-inner"></div><div class="top-rating">'.$topteam->rating.'</div></a>';
            $top_i++;
        }
        if(empty($top_i)) {
            $top_teams .= "<div class='no_top_teams'>Teams haven't played any games in ".date('Y')." season</div>";
        }
    $top_teams .= '</div></div></section></div></div>';


$top_teams_data = array();
$formats = array('3on3','5on5');
//Top teams
// EAST
foreach($formats AS $format) {
    $top_teams_data['east'.$format] = '<div class="team profile"><div class="profile-header"><section class="section"><header><h2 class="title">'.t('Top EU Nordic & East '.$format.' teams').'</h2></header><div class="table matches expanded"><div class="body">';
    $topteams = db_query("SELECT rank.rating, users_teams.* FROM `rank` LEFT JOIN `users_teams` ON users_teams.id = rank.entity_id WHERE rank.type = 'team' AND users_teams.status = 1 AND users_teams.region = 'eune' AND users_teams.format = '".$format."' ORDER BY `rating` DESC LIMIT 5");
    $top_i=0;
    while($topteam = db_fetch_object($topteams)) {
        $top_teams_data['east'.$format] .= '<a href="/team/id/'.$topteam->slug.'-'.$topteam->id.'" class="row"><div class="top-team">';
        if(empty($topteam->picture)) {
            $topteam->picture = 'noimage.png';
        }

        if(!empty($topteam->picture)) {
            $top_teams_data['east'.$format] .= '<img src="/public/images/users_teams/'.$topteam->picture.'" alt="" class="team-avatar" width="28" />';
        }

        $top_teams_data['east'.$format] .= '    <h2 class="profile-name"><img src="/public/images/flags/'.$topteam->country.'.png"  alt="" /> '.htmlspecialchars($topteam->name).'</h2>';
        $top_teams_data['east'.$format] .= '</div><div class="result-inner"></div><div class="top-rating">'.$topteam->rating.'</div></a>';
        $top_i++;
    }
    if(empty($top_i)) {
        $top_teams_data['east'.$format] .= "<div class='no_top_teams'>Teams haven't played any games in ".date('Y')." season</div>";
    }
    $top_teams_data['east'.$format] .= '</div></div></section></div></div>';


//  WEST
    $top_teams_data['west'.$format] = '<div class="team profile"><div class="profile-header"><section class="section"><header><h2 class="title">'.t('Top EU West '.$format.' teams').'</h2></header><div class="table matches expanded"><div class="body">';
    $topteams = db_query("SELECT rank.rating, users_teams.* FROM `rank` LEFT JOIN `users_teams` ON users_teams.id = rank.entity_id WHERE rank.type = 'team' AND users_teams.status = 1 AND users_teams.region = 'euw' AND users_teams.format = '".$format."' ORDER BY `rating` DESC LIMIT 5");
    $top_i=0;
    while($topteam = db_fetch_object($topteams)) {
        $top_teams_data['west'.$format] .= '<a href="/team/id/'.$topteam->slug.'-'.$topteam->id.'" class="row"><div class="top-team">';
        if(empty($topteam->picture)) {
            $topteam->picture = 'noimage.png';
        }

        if(!empty($topteam->picture)) {
            $top_teams_data['west'.$format] .= '<img src="/public/images/users_teams/'.$topteam->picture.'" alt="" class="team-avatar" width="28" />';
        }

        $top_teams_data['west'.$format] .= '    <h2 class="profile-name"><img src="/public/images/flags/'.$topteam->country.'.png"  alt="" /> '.htmlspecialchars($topteam->name).'</h2>';
        $top_teams_data['west'.$format] .= '</div><div class="result-inner"></div><div class="top-rating">'.$topteam->rating.'</div></a>';
        $top_i++;
    }
    if(empty($top_i)) {
        $top_teams_data['west'.$format] .= "<div class='no_top_teams'>Teams haven't played any games in ".date('Y')." season</div>";
    }
    $top_teams_data['west'.$format] .= '</div></div></section></div></div>';

}

$top_teams_data = '<div class="top-teams"><div class="left-side">'.$top_teams_data['west5on5'] . $top_teams_data['west3on3'].'</div><div class="right-side">'.$top_teams_data['east5on5'] . $top_teams_data['east3on3'] .'</div></div>';

//front stream
$front_stream = '';
$front_stream_data = db_fetch_object(db_query("SELECT * FROM `streams` WHERE `front` = 1 AND `status` = 1 LIMIT 1"));
if(!empty($front_stream_data)) {
    $explode_twitch_username = explode('/',$front_stream_data->link);
    $twitch_username = end($explode_twitch_username);
    $front_stream_data->username = $twitch_username;


    $front_stream = '
    <object type="application/x-shockwave-flash" height="678" width="960" id="live_embed_player_flash" data="http://www.twitch.tv/widgets/live_embed_player.swf?channel='.$front_stream_data->username.'" bgcolor="#000000">
        <param name="allowFullScreen" value="true" />
        <param name="allowScriptAccess" value="always" />
        <param name="allowNetworking" value="all" />
        <param name="movie" value="http://www.twitch.tv/widgets/live_embed_player.swf" />
        <param name="flashvars" value="hostname=www.twitch.tv&channel='.$front_stream_data->username.'&auto_play=true&start_volume=25" />
    </object>';
}

$streams = [];
$getStreams = ORM::for_table('streams')
    ->order_by_desc('id')
    ->where_not_equal('image', '')
    ->limit(3)
    ->find_array();
foreach($getStreams AS $stream) {
    if(empty($stream['image'])) {
        $stream['image'] = '/public/img/nostream.jpg';
    }
    $streams[] = $stream;
}

// Get other cups ##
    $otherCups = ORM::for_table('cups')
        ->where('published', 1)
        ->where_not_equal('status', 2)
        ->where('other_cup', 'Y')
        ->find_array();



// Get last findteams
$getLastFindteam = Memcached::get('front_findteam');
if(!$getLastFindteam) {
    $getLastFindteam = ORM::for_table('findteam')
        ->table_alias('f')
        ->select('f.*')
        ->select('g.value')
        ->select('u.lietotajvards', 'username')
        ->left_outer_join('users_gameaccount', 'g.id = f.entity', 'g')
        ->left_outer_join(PREFIX . 'lietotaji', 'u.id = g.uid', 'u')
        ->limit(5)
        ->order_by_desc('f.updated')
        ->find_array();
    Memcached::set('front_find_team', $getLastFindteam, 300);
}