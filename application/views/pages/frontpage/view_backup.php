<?php
	if($lietotajs['id']) {
		//print '<iframe src="http://www.draugiem.lv/user/381584/" border="0" width="0px" height="0px"></iframe>';
	}
?>
	<div id="slider">
		<div id="navigation">
		<?php 
			//$hot_news = db_query("SELECT `nid`,`image`,`title`,`desc` FROM ".PREFIX."news WHERE `hot` = 1 AND language = '' OR language = '".$language."' AND status = 1 ORDER BY nid DESC LIMIT 4");
			$hot_news = db_query("SELECT `nid`,`image`,`title`,`desc` FROM ".PREFIX."news WHERE AND `language` = '' OR `language` = '".$language."' AND `status` = 1 ORDER BY `nid` DESC LIMIT 4");
			$new_i=1;
			while($hot_new = db_fetch_object($hot_news)) {
				$class = '';
				if($new_i == 1) {
					$hotNew = new stdClass();
					$hotNew->nid = $hot_new->nid;
					$hotNew->title = $hot_new->title;
					$hotNew->desc = $hot_new->desc;
					$hotNew->image = $hot_new->image;
					$class = ' active';
				}
			print '<div class="item new-'.$new_i . $class.'">
				  <div class="image"><img src="/thumb.php?img=public/img/news/full/'.$hot_new->image.'&max_w=79&max_h=65&crop=1" alt="'.$hot_new->title.'" /></div>
				  <h2>'.$hot_new->title.'</h2>
				  <p class="front-new-desc">'.$hot_new->desc.'</p>
				  <p class="front-new-id">'.$hot_new->nid.'</p>
				  <p class="front-new-image">'.$hot_new->image.'</p>
				 </div>'; 
				 $new_i++;
			}
		?>
		</div>
		<div id="featured_mark"></div>
		<div id="image"><img src="/public/img/news/full/<?php print $hotNew->image;?>" alt=""/></div>
		<div id="about">
			<h1><a href="/new/<?php print $hotNew->nid;?>/"><?php print $hotNew->title;?></a></h1>
			<p><?php print $hotNew->desc;?></p>
		</div>
	</div>
	<!-- / slider -->
	<div class="clear"></div>
	<div class="content index_content">
		<div id="left">
			<div id="news">
				<h3><?php print _('Visi jaunumi'); ?><a href="/archive/news/"><?php //print _('Arhīvs'); ?></a></h3>
				<div id="latest">
				<?php
				$news = db_query("SELECT t1.nid,t1.image,t1.title,t1.desc,t1.created,t2.lietotajvards
								  FROM ".PREFIX."news AS t1,".PREFIX."lietotaji AS t2
								  WHERE t1.uid = t2.id AND t1.language = '' OR t1.uid = t2.id AND t1.language = '".$language."' AND status = 1
								  ORDER BY t1.nid DESC LIMIT 3");
				$news_i=0;
				while($new = db_fetch_object($news)) {
				$news_i++;
				$comments = mysql_result(db_query("SELECT count(id) FROM ".PREFIX."comments WHERE nid = '".$new->nid."' AND type = 'n'"),0);
				print '	<div class="new">
								<div class="image">
									<a href="/new/'.$new->nid.'/" class="thumb"><span><img src="/thumb.php?img=public/img/news/full/'.$new->image.'&max_w=79&max_h=65&crop=1" alt="" /></span></a>
									<a href="/new/'.$new->nid.'/" class="comment_count"><span>'.$comments.'</span></a>
								</div>
								<h4><a href="/new/'.$new->nid.'/">'.$new->title.'</a></h4>
								<p>'.$new->desc.'</p>
								<p class="about">'._('Ievietots').' <span>'.laikaParveide($new->created).'</span> '._('Autors').' <a href="/user/'.$new->lietotajvards.'/">'.$new->lietotajvards.'</a></p>
							</div>';
				}
				?>
				</div>
				<?php if($news_i > 2): ?>
					<div id="older">
					<?php
					$news = db_query("SELECT t1.nid,t1.image,t1.title, t1.desc,t1.created,t2.lietotajvards
									  FROM ".PREFIX."news AS t1,".PREFIX."lietotaji AS t2
									  WHERE t1.uid = t2.id AND t1.language = '' OR t1.uid = t2.id AND language = '".$language."' AND status = 1
									  ORDER BY t1.nid DESC LIMIT 3,4");
					while($new = db_fetch_object($news)) {
						$news_i++;
					  print '<div class="new">';
					  print '<div class="image"><a href="/new/'.$new->nid.'/"><img src="/thumb.php?img=public/img/news/full/'.$new->image.'&max_w=46&max_h=46&crop=1"/></a></div>';
					  print '<h4><a href="/new/'.$new->nid.'/">'.htmlspecialchars($new->title).'</a></h4>';
					  print '<p class="about"><span>'.laikaParveide($new->created).'</span> '._('Autors').' '.getMember($new->lietotajvards).'</p>';
					  print '</div>';
					}
					?>
					</div>
				<?php endif;?>
				<?php if($news_i > 5): ?>
					<div id="older2">
					<?php
					$news = db_query("SELECT t1.nid,t1.image,t1.title,t1.desc,t1.created,t2.lietotajvards
									  FROM ".PREFIX."news AS t1,".PREFIX."lietotaji AS t2
									  WHERE t1.uid = t2.id AND t1.language = '' OR t1.uid = t2.id AND language = '".$language."' AND status = 1
									  ORDER BY t1.nid DESC LIMIT 7,8");
					while($new = db_fetch_object($news)) {
					  print '<div class="new">';
					  print '<div class="image"><a href="/new/'.$new->nid.'/"><img src="/thumb.php?img=public/img/news/full/'.$new->image.'&max_w=46&max_h=46&crop=1"/></a></div>';
					  print '<h4><a href="/new/'.$new->nid.'/">'.htmlspecialchars($new->title).'</a></h4>';
					  print '<p class="about"><span>'.laikaParveide($new->created).'</span> '._('Autors').' '.getMember($new->lietotajvards).'</p>';
					  print '</div>';
					}
					?>
					</div>
					<div class="load_more news"><a href="#"><?php print _('Vairāk jaunumi'); ?></a></div>
				<?php endif;?>
			</div> <!-- news -->
			<div class="clear"></div>
			
			<div id="latest_comments">
				<h3><?php print _('Jaunākie komentāri');?> <a href="/archive/comments/"><?php //print _('Arhīvs'); ?></a></h3>
				
				<?php
				$comments_db = db_query("SELECT ".PREFIX."comments.id,".PREFIX."news.title,".PREFIX."videos.title AS video_title, ".PREFIX."aptaujas.jautajums, ".PREFIX."comments.nid, ".PREFIX."comments.date,".PREFIX."comments.uid, ".PREFIX."comments.comment,".PREFIX."comments.type, ".PREFIX."lietotaji.lietotajvards
                            FROM ".PREFIX."comments
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."comments.uid=".PREFIX."lietotaji.id
                            LEFT JOIN ".PREFIX."news ON ".PREFIX."news.nid=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."aptaujas ON ".PREFIX."aptaujas.id=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."videos ON ".PREFIX."videos.vid=".PREFIX."comments.nid
                            ORDER BY ".PREFIX."comments.id DESC LIMIT 3");

    $comments_i=0;
    while($comment = db_fetch_object($comments_db))
    { 
	
      $comments_i++;
      print '<div class="comment">';
      if($comment->type=='n') {
        $page = 'new';
        $title = $comment->title;
      }
      elseif($comment->type=='v') {
        $title = $comment->video_title;
        $page='video/i';
      }
      else {
        $page = 'poll';
        $title = $comment->jautajums;
      }
      
      print '<p>'.getMember($comment->lietotajvards).': '.htmlspecialchars($comment->comment).'</p>';
      print '<p class="about">'._('Ievietots').' <span>'.laikaParveide($comment->date).'</span> '._('iekšs').' "<a href="/'.$page.'/'.$comment->nid.'/">'.htmlspecialchars($title).'</a>"</p>';
      print '</div>';
    }

    if($comments_i==0) {
      print _('<div class="comment"><p>'._('Nav komentāri...').'</p></div>');
    }
				?>
				</div>
				<div class="hide_last_sepr"></div>
				<?php if($comments_i > 2): ?>
				<div id="latest_comments2">
								<?php
				$comments_db = db_query("SELECT ".PREFIX."comments.id,".PREFIX."news.title,".PREFIX."videos.title AS video_title, ".PREFIX."aptaujas.jautajums, ".PREFIX."comments.nid, ".PREFIX."comments.date,".PREFIX."comments.uid, ".PREFIX."comments.comment,".PREFIX."comments.type, ".PREFIX."lietotaji.lietotajvards
                            FROM ".PREFIX."comments
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."comments.uid=".PREFIX."lietotaji.id
                            LEFT JOIN ".PREFIX."news ON ".PREFIX."news.nid=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."aptaujas ON ".PREFIX."aptaujas.id=".PREFIX."comments.nid
                            LEFT JOIN ".PREFIX."videos ON ".PREFIX."videos.vid=".PREFIX."comments.nid
                            ORDER BY ".PREFIX."comments.id DESC LIMIT 3,6");

    $comments_i=0;
    while($comment = db_fetch_object($comments_db))
    { 
      $comments_i++;
      print '<div class="comment">';
      if($comment->type=='n') {
        $page = 'new';
        $title = $comment->title;
      }
      elseif($comment->type=='v') {
        $title = $comment->video_title;
        $page='video/i';
      }
      else {
        $page = 'poll';
        $title = $comment->jautajums;
      }
      
      print '<p>'.getMember($comment->lietotajvards).': '.htmlspecialchars($comment->comment).'</p>';
      print '<p class="about">'._('Ievietots').' <span>'.laikaParveide($comment->date).'</span> '._('iekšs').' "<a href="/'.$page.'/'.$comment->nid.'/">'.htmlspecialchars($title).'</a>"</p>';
      print '</div>';
    }

    ?>
				</div>
        
				<div class="load_more comments"><a href="#"><?php print _('Vairāk komentāri'); ?></a></div>
				<?php endif;?>
			
			<div class="clear"></div>
			
		</div><!-- left -->
		
		<div id="right">
			<!--<div id="follow">
			<a id="youtube" href="https://www.youtube.com/user/BliezamLV" target="_blank"><img src="/public/img/youtube_ico.png" alt="" /></a>
			</div>-->
			<div class="side side_style1 side_tournaments">
				<div class="block">
					<div class="title">
						<h3><?php print _('Atvērti turnīri'); ?></h3>
					</div>
					<div class="block_content">
					
						<?php
						$tourney_i=0;
						$query = db_query("SELECT 
								* 
								FROM ".PREFIX."tournaments
								WHERE status = 'open' ORDER BY id ASC");
								while($t = db_fetch_object($query))
								{
								$tourney_i++;
								$reg_players = mysql_fetch_assoc(queryy("SELECT count(*) AS c FROM ".PREFIX."tournaments_players WHERE t_id = '".$t->id."'"));
								
								if($t->coins != 0) $vip =  '<img src="/public/images/vip.gif" alt="" /> '; else $vip='';
								$players = '<span class="players">'.$reg_players['c'].'/'.$t->players.' | '.$t->game_type.'</span>';
										print '<div class="side_tournament">
													<div class="type"><a href="#"><img src="/public/images/tournaments/'.$t->game.'/'.$t->game.'.jpg" alt=""/></a></div>
													<h5>'.$vip.'<a href="/tournament/'.$t->id.'/"><strong>'.$t->platform.'</strong> '.$t->name.'</a></h5>';
													
									$piedalisanas_parbaude = mysql_result(mysql_query("SELECT count(id) FROM ".PREFIX."tournaments_players WHERE t_id = '".$t->id."' AND user_id = '".$lietotajs['id']."'"),0,0);
									if($lietotajs['id'] AND $piedalisanas_parbaude == 1)
									{
									print '<p class="registered">'._('Reģistrējies').'</p>';
									}
									else
									{
									// nav pieteicies uz sho turniru 		
									echo '
									<a href="#" class="register" onclick="if (confirm(\''._('Esi drošs, ka vēlies pieteikties?').' '.$t->name.'? '._('FEE').': '.$t->coins.' '._('coins').'\')) document.location.href=\'/tournament/'.$t->id.'/join/\'">'._('Register').'</a>
									';
									} 

									print '<span class="user-count">'._('Spēlētāji').': '.$reg_players['c'].'/'.$t->players.'</span>';
									print '</div>';
								}

								if($tourney_i==0) {
								echo '<div style="color: #666; padding: 10px;">'._('Nav atvērti turnīri...').'</div>';
								}

						?>
						<div class="hide_last_sepr"></div>
					</div>
				</div>
			</div>
			
			
			<div class="side side_style3 side_forum">
				<h3><?php print _('Forumi');?></h3>
				<div class="block">
					<ul class="tabs">
						<li class="active"><a href="#" class="latest" class="forum-item"><?php print _('Jaunākās atbildes'); ?></a></li>
						<li><a href="#" class="hottest"><?php print _('Jaunākie temati'); ?></a></li>
					</ul>
					<div class="block_content">
						<div id="latest" class="forum-item">
					<?php $latest_posts = db_query("SELECT ".PREFIX."forums_atbildes.autors_vards AS name, ".PREFIX."forums_atbildes.teksts AS text,".PREFIX."forums_atbildes.datums AS date,
                                          ".PREFIX."forums_temati.nosaukums AS title,".PREFIX."forums_atbildes.temats AS tid
                                          FROM ".PREFIX."forums_atbildes
                                          LEFT JOIN ".PREFIX."forums_temati ON ".PREFIX."forums_temati.id = ".PREFIX."forums_atbildes.temats
                                          ORDER BY date DESC LIMIT 3");
                while($latest_post = db_fetch_object($latest_posts)) {
print '<div class="post">
         <p class="forum-text"><a href="/user/'.$latest_post->name.'/">'.$latest_post->name.':</a> '.forum_text(htmlspecialchars(strip_tags($latest_post->text))).'</p>
         <p class="about">'._('Ievietots').' <span>'.laikaParveide($latest_post->date).'</span> '._('iekšs').' "<a href="/forums/topic/'.$latest_post->tid.'/">'.$latest_post->title.'</a>"</p>
       </div>'; 
                
                }

          ?>
					</div>
					<div id="hottest" class="forum-item">
					<?php $newest_threads = db_query("SELECT t1.id,t1.nosaukums, t1.autors, t1.datums , t2.lietotajvards
                                             FROM ".PREFIX."forums_temati AS t1, ".PREFIX."lietotaji AS t2 
                                             WHERE t1.autors = t2.id
                                             ORDER BY t1.id DESC LIMIT 3");
                  while($thread = db_fetch_object($newest_threads)) {
                      print '<div class="post">';
                      print '<p><a href="/forums/topic/'.$thread->id.'/">'.h($thread->nosaukums).'</a></p>';
                      print '<p class="about">'._('Autors').'<span> '.getMember($thread->lietotajvards).' '.laikaParveide($thread->datums).'</span></p>';
                      print '</div>';
                  }

          ?>
					</div>
					<div class="hide_last_sepr"></div>
					</div>
				</div>
			</div>
			
			<div class="side side_style2 side_poll">
				<div class="block">
					<div class="title">
						<h3><?php print _('Aptauja'); ?><!--a href="/archive/polls/"><?php print _('Arhīvs'); ?></a>--></h3>
					</div>
					<div class="block_content">
						<?php include_once "poll.php"; ?>
					</div>
				</div>
			</div>
<?php
    $visitors_online = new usersOnline();
     
    $records = mysql_fetch_assoc(queryy("SELECT max_online, max_online_time FROM ".PREFIX."records"));
    $count_users = mysql_result(queryy("SELECT count(id) FROM ".PREFIX."lietotaji WHERE activated = 1"),0,0);
    $last_user = db_fetch_object(db_query("SELECT lietotajvards,country FROM ".PREFIX."lietotaji WHERE activated = 1 ORDER BY id DESC"));
    $count_tourneys = mysql_result(queryy("SELECT count(id) FROM ".PREFIX."tournaments WHERE status = 'closed'"),0);
    $count_tourneys2 = mysql_result(queryy("SELECT count(id) FROM ".PREFIX."tournaments WHERE status = 'active'"),0);
    $tenMinutesAgo = time()-60*10;
    $reg_online = mysql_result(queryy("SELECT count(id) FROM ".PREFIX."lietotaji WHERE online > '$tenMinutesAgo'"),0);
    $guests_online = round($visitors_online->count_users()-$reg_online);
    $all_online = $visitors_online->count_users();
    
    if($all_online > $records['max_online']) 
    {
        queryy("UPDATE ".PREFIX."records SET max_online = '".$all_online."', max_online_time = '".time()."'");
        $records = mysql_fetch_assoc(queryy("SELECT max_online, max_online_time FROM ".PREFIX."records"));
    }
?>
			<div class="side side_style3 side_statistic">
				<h3><?php print _('Statistika'); ?></h3>
				<div class="block">
					<ul class="tabs">
						<li class="active"><a href="#" class="members"><?php print _('Lietotāji'); ?></a></li>
						<li><a href="#" class="tournaments"><?php print _('Turnīri'); ?></a></li>
					</ul>
					<div class="block_content">
						<div id="members"  class="stats">
						<?php $count_users = mysql_result(db_query("SELECT count(id) FROM ".PREFIX."lietotaji WHERE activated = 1"),0); ?>
			            <p><?php print _('Lietotāji');?>: <span><?php print $count_users; ?></span></p>
			            <p><?php print _('Tiešsaistē');?>: <span><?php print $all_online; ?></span></p>
			            <p><?php print _('Reģistrētie tiešsaistē');?>: <span><?php print $reg_online; ?></span></p>
			            <p><?php print _('Viesi tiešsaistē');?>: <span><?php print $guests_online; ?></span></p>
			            <br /><br />
			            <p><?php print _('Pēdējais reģistrētais lietotājs');?>: <span><?php print getMember($last_user->lietotajvards); ?></span></p>
                  <p style="margin-top:10px;">
                  <h2><?php print _('Lietotāji tiešsaistē'); ?></h2>
								  <?php
								  
					$online_l = db_query("SELECT ".PREFIX."lietotaji.id, ".PREFIX."lietotaji.lietotajvards,".PREFIX."lietotaji.country, users_groups.color, users_groups.permissions AS flags
										FROM ".PREFIX."lietotaji 
										LEFT JOIN users_groups ON users_groups.gid = ".PREFIX."lietotaji.group
										WHERE ".PREFIX."lietotaji.online > '$tenMinutesAgo'
										ORDER BY ".PREFIX."lietotaji.online DESC");
						$o_c = $reg_online;
					while($online = mysql_fetch_assoc($online_l))
						{
							$strong = 'color:'.$online['color'];
						$o_c--;
						if($o_c != 0) $komats  = ', '; else $komats = '';
						echo getMember($online['lietotajvards'],$strong).$komats;
						}
						
								  ?>
                  </p>
						
						</div>
						<div id="tournaments"  class="stats">
	            <p><?php print _('Aizvērti turnīri'); ?>: <span><?php print $count_tourneys;?></span></p>
	            <p><?php print _('Aktīvi turnīri'); ?>: <span><?php print $count_tourneys2;?></span></p>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>
		