<?php global $language; ?>
<div id="left" class="article">
	<h1><?=$new->title;?> <?php if(user_access('administer news')) { print '<span><a href="/admin/news/edit/'.$new->nid.'/" >Edit</a></span>'; } ?></h1>
	<div class="info">
		<p>
            <?php if($new->uid == 1): ?>
                <strong>VoidLeague</strong>
            <?php else: ?>
              <a href="/user/<?=$new->autors;?>/"><?=$new->autors;?></a>
            <?php endif; ?>
            | <?=laikaParveide($new->created);?> | <?=_('Views');?>: <?=$element->views($new->nid,'n');?></p>
		<div class="addthis">
		<?php  $new_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$new->slug.'-'.$new->nid; ?>
<a href="http://twitter.com/share" class="twitter-share-button" data-count="none"><img src="/public/images/twitter.png" alt="" /></a>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<div id="fb-root"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://<?=$_SERVER['SERVER_NAME'].'/'.$new->slug.'-'.$new->nid;?>" send="false" layout="button_count" width="30" show_faces="no" font=""></fb:like></div>

		</div>
	</div>		
	
		<div class="new">
			<?=$new->body;?>
		</div>
		<div class="view-new">
	<?php 
	
	if($new->comments == 1) {
	  comments('n',$new->nid,'new');
	}
	else {
	  print '<div class="bx error">'._('Comments disabled').'</div>';
	}
	
	?></div>
</div>
<div id="right">

		<div class="side side_style1 lidzigi_raksti">
			<div class="block">
				<div class="title">
					<h3><?php print _('Related news'); ?></h3>
				</div>
				<div class="block_content">
					<?php
	         	    	$same_articles = queryy(" SELECT nid,title,`slug`
	                                        FROM ".PREFIX."news WHERE category = '".$new->category."' AND nid != '".$new->nid."' AND language = '' OR category = '".$new->category."' AND nid != '".$new->nid."' AND language = '".$language."'
	                                        ORDER BY nid DESC LIMIT 10");
	               		while($sa = mysql_fetch_object($same_articles))
	                		{
	                		$sim_news = 1;
	                	echo '<div class="same_cat"><a href="/news/read/'.$sa->slug.'-'.$sa->nid.'">'.cut($sa->title, 40).'</a></div>';
	                		}  
	                		if($sim_news==0) {
                        print _('Nav saistītie raksti');
	                		}                      
	              	?>
	              	<div class="hide_last_sepr"></div>
				</div>
			</div>
			<div class="clear"></div>
			<br /><br />

		</div>
</div>