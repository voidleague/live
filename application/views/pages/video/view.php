<div id="videos">
    <div id="left">
        <?php
        if(!isset($video->vid) AND !(arg(1))) { ?>
            <h3><?php print t('TOP'); ?></h3>
            <div class="clear"></div>
            <div class="yt-alert yt-alert-promo yt-rounded ">
            <?php include 'top.php'; ?>
            </div>
            <?php include 'newest.php'; ?>
        <?php 
            }
            
            elseif(arg(1) == 'add' AND $lietotajs['id'] != 0) {
                  include 'add.php';
            }
            elseif(arg(1) == 'edit' AND arg(2) AND $lietotajs['id'] != 0 AND $lietotajs['id'] == $video->uid
                OR arg(1) == 'edit' AND arg(2) AND $lietotajs['id'] != 0 AND admin('b')
                OR arg(1) == 'edit' AND arg(2) AND $lietotajs['id'] != 0 AND admin('a')) {
                  include 'edit.php';
            }
            elseif(arg(1) == 'my-videos' AND $lietotajs['id'] != 0) {
                include 'my-videos.php';
            }
            elseif(arg(2)) {
                    include 'video.php';
                 }?>
    </div>
    <div id="right">
        <?php if($lietotajs['id']): ?>
        <div class="side side_style1">
                <div class="block">
                  <div class="title">
                    <h3><?php print t('My videos'); ?></h3>
                    </div>
                    <div class="content">
                      <ul>
                        <?php
                            $videos_menu = array(
                              'add'=>t('Add video'),
                              'my-videos'=>t('My videos'),
                            );
                            $my_videos = mysql_result(db_query("SELECT count(*) FROM ".PREFIX."videos WHERE uid = '".$lietotajs['id']."'"),0);
                            foreach($videos_menu AS $menu_src=>$menu_title) {
                                $active = '';
                                $my_videos_count = '';
                              if(arg(1) == $menu_src) {
                                $active = ' active';
                              }
                              if($menu_src == 'my-videos') {
                                $my_videos_count = '('.$my_videos.')';
                              }
                                print '<li class="'.$menu_src.''.$active.'"><a href="/video/'.$menu_src.'/">'.$menu_title.' '.$my_videos_count.'</a></li>';
                              
                            }
                        ?>
                      </ul>
                    </div>
                </div>
        </div>       
        
        
        <?php endif; ?>
        <div class="side side_style1">
                <div class="block">
                  <div class="title">
                    <h3><?php print t('Random videos'); ?></h3>
                    </div>
                    <p>
                        <?php include 'random.php'; ?>
                    </p>
                    </div></div>
    </div>
</div>