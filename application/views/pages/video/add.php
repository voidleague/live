<?php

// :: controller
if(isset($_POST['add_video']) && $_POST['add_video']) { 
    $youtube = 'youtube.com/watch?v=';
    if(empty($_POST['youtube_url']) OR empty($_POST['video_title'])) {
        $video_error = t('Need to add title and Youtube link');
    }
    elseif(strpos($_POST['youtube_url'], $youtube) == false) {
        $video_error = t('Wrong youtube link');
    } 
    else {
        $video_ok = t('Video added');
        $video = mysql_real_escape_string($_POST['youtube_url']);
        $video = explode('&',$video);
        $video = $video[0];
        $title = mysql_real_escape_string($_POST['video_title']);
        $desc = mysql_real_escape_string($_POST['video_description']);
        db_query("INSERT INTO ".PREFIX."videos (`uid`,`title`,`desc`,`video`,`date`) VALUES ('".$lietotajs['id']."','".$title."','".$desc."','".$video."','".time()."')");
        
    }
}
?>


<h3><?php print t('Add video'); ?></h3>
<div class="clear"></div>
<?php if(!empty($video_error)) {
          print '<div class="bx error">'.$video_error.'</div>';
       }
?>
<?php if(!empty($video_ok)) {
          print '<div class="bx info">'.$video_ok.'</div>';
       }
?>
<?php if(empty($video_ok)): ?>

<?php
$youtube_url = (isset($_POST['youtube_url'])) ? $_POST['youtube_url'] : '';
$video_title = (isset($_POST['video_title'])) ? $_POST['video_title'] : '';
$video_description = (isset($_POST['video_description'])) ? $_POST['video_description'] : '';
?>
<form method="POST">
    <table class="add-video">
        <tr><td class="first"><?php print t('Youtube link'); ?>:</td><td><input type="text" name="youtube_url" value="<?php print $youtube_url; ?>"/> <span class="example"><?php print t('example'); ?>:</span><img src="/public/img/youtube.png" alt="" /></td></tr>
        <tr><td class="first"><?php print t('Title'); ?>:</td><td><input type="text" name="video_title" value="<?php print $video_title; ?>"/></td></tr>
        <tr><td class="first"><?php print t('Description'); ?>:</td><td><textarea name="video_description"><?php print $video_description; ?></textarea></td></tr>
        <tr><td class="first"></td><td><input type="submit" name="add_video" value="<?php print t('Add video'); ?>" /></td></tr>
    </table>
</form>
<?php endif; ?>