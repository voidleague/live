<?php
$title = t('Video');
include ROOT.'/application/controllers/classes/videos.class.php';
if(arg(2)) {
    $video = db_fetch_object(db_query("SELECT t1.*,t2.lietotajvards AS author FROM ".PREFIX."videos AS t1, ".PREFIX."lietotaji AS t2 WHERE t1.uid=t2.id AND t1.vid = '".intval(arg(2))."'"));
    if($video->vid) {
        $title = h(strip_tags($video->title));
    }
    else {
        $video = false;
    }
}

function page()
{
   global $video;
   global $lietotajs;
   global $title;
   print '<link href="/public/css/video.css" rel="stylesheet" type="text/css" />';
   if(isset($video->vid)) {
      $page_title = '<a href="/video/">'.t('Video').'</a> - <a href="/video/i/'.$video->vid.'/">'.h(strip_tags($video->title)).'</a>';
   }
   else {
      $page_title = '<a href="/video/">'.$title.'</a>';
   }
   print '<h1 class="title">'.$page_title.'</h1>';
   
   include_once 'video/index.php';
return;
}
?>

