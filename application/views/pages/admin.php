<?php
$title = 'Administrācijas panelis';
function page()
{ 
global $ielogojies;
global $lietotajs;
global $ip;


if(user_access('access administration theme')) {

    include ROOT."/administration/main.php";
}
else
{
    echo error('Permission denied');
}
return;
}
?>