<?php

use \ORM;

class DonationsController extends AppController {
    /*
     * Paypal form submit url
     */
    private $paypal_url = 'https://www.paypal.com/cgi-bin/webscr';

    /**
     * Paypal donation email
     * @var string
     */
    private $donation_email = 'info@voidleague.com';

    /**
     * Currency from IP continent
     * @var
     */
    private $currency;

    /**
     * Transaction unique ID
     * @var
     */
    private $trans_id;

    function __construct() {
        $this->title = t('Donations');
        $this->currency = $this->getCurrency();
        $this->trans_id = $this->createTransID();
    }

    public function index() {
        return $this->view('index',[
            'form' => $this->donationForm()
        ]);
    }

    protected function donationForm() {
        $form = $this->paypalForm();
        $form += [
            '#action' => $this->paypal_url,
            'name' => [
                '#type' => 'textfield',
                '#title' => t('Name'),
                '#default_value' => (User::$user->uid) ? User::$user->username : '',
                '#disabled' => (User::$user->uid) ? true : false
            ],
            'anonymous' => [
                '#type' => 'checkbox',
                '#title' => t('Stay anonymous')
            ],
            'amount' => [
                '#title' => t('Amount'),
                '#type' => 'select',
                '#options' => $this->getAmounts()
            ],
            'trans_id' => [
                '#type' => 'hidden',
                '#value' => $this->trans_id
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Donate via paypal'),
                '#attributes' => [
                    'class' => 'go donation'
                ]
            ]
        ];
        return render_form($form);
    }

    protected function getAmounts() {
        return [
            1 => '1 '.$this->currency,
            2 => '2 '.$this->currency,
            5 => '5 '.$this->currency,
            10 => '10 '.$this->currency,
            20 => '20 '.$this->currency
        ];
    }

    /**
     * Get currency from continent
     * @return string
     */
    protected function getCurrency() {
        $continent = geoip_continent_code_by_name($_SERVER['REMOTE_ADDR']);
        if($continent == 'EU') {
            return 'EUR';
        }
        return 'USD';
    }
    /**
     * Paypal hidden fields with values
     * @return array
     */
    protected function paypalForm() {
        $form = [
            'cmd' => [
                '#type' => 'hidden',
                '#value' => '_donations',
            ],
            'business' => [
                '#type' => 'hidden',
                '#value' => $this->donation_email
            ],
            'notify_url' => [
                '#type' => 'hidden',
                '#value' => 'http://www.voidleague.com/donations/ipn',
            ],
            'return' => [
                '#type' => 'hidden',
                '#value' => 'http://www.voidleague.com/donations?thank_you'
            ],
            'rm' => [
                '#type' => 'hidden',
                '#value' => 2
            ],
            'no_note' => [
                '#type' => 'hidden',
                '#value' => 1
            ],
            'cbt' => [
                '#type' => 'hidden',
                '#value' => 'Go back to VoidLeague',
            ],
            'no_shipping' => [
                '#type' => 'hidden',
                '#value' => 1
            ],
            'lc' => [
                '#type' => 'hidden',
                '#value' => $this->currency
            ],
            'currency_code' => [
                '#type' => 'hidden',
                '#value' => $this->currency
            ],
            'bn' => [
                '#type' => 'hidden',
                '#value' => 'PP-DonationsBF:btn_donate_LG.gif:NonHostedGuest'
            ],
            'custom' => [
                '#type' => 'hidden',
                '#value' => $this->trans_id
            ]
        ];
        return $form;
    }

    /**
     * Create unique trans_id
     * @return string
     */
    protected function createTransID() {
        $rand = substr(md5(microtime()),rand(0,26),5);
        return $rand . time();
    }

    /**
     * Paypal response
     */
    public function ipn() {
        if(!empty($_POST)) {
            if($_POST['receiver_email'] == $this->donation_email && $_POST['payment_status'] == 'Completed') {
                $donation = ORM::for_table('donations')->create();
                $donation->set_expr('date', 'NOW()');
                $donation->amount = $_POST['mc_gross'];
                $donation->currency = $_POST['mc_currency'];
                $donation->additonal_data = serialize($_POST);
                $donation->trans_id = $_POST['custom'];
                $donation->save();

                $uid = ORM::for_table('donations_transactions')
                    ->where('trans_id',trim($_POST['custom']))
                    ->find_one()->uid;
                if(!empty($uid)) {
                    $this->giveBadge($uid);
                }
            }
        }
        exit;
    }

    /**
     * Ajax save trans_id + user data
     */
    public function button() {
        $trans = ORM::for_table('donations_transactions')->create();
        $trans->uid = User::$user->uid;
        if(!empty($_POST['name'])) {
            $trans->name = $_POST['name'];
        }
        $trans->trans_id = $_POST['trans_id'];
        $trans->anonymous = ($_POST['anonymous'] == 'true') ? 'Y' : 'N';
        $trans->set_expr('date','NOW()');
        $trans->save();
        echo 'OK';
        exit;
    }

    /**
     * Get limited donations via ajax request
     * @return mixed
     */
    public function getDonations() {
        $offset = (!isset($_GET['offset']) || !is_numeric($_GET['offset'])) ? 0 : $_GET['offset'];
        $getDonations = ORM::for_table('donations')
            ->table_alias('d')
            ->select_many([
                'd.amount',
                'd.currency',
                'd.date',
                'username' => 'u.lietotajvards',
                't.anonymous',
                'u.photo',
                't.name'
            ])
            ->join('donations_transactions','t.trans_id = d.trans_id','t')
            ->left_outer_join(PREFIX.'lietotaji','u.id = t.uid','u')
            ->where_gte('d.amount', 1)
            ->order_by_desc('d.date')
            ->offset($offset)
            ->limit(20)
            ->find_array();

        $donations = [];

        if(!empty($getDonations)) {
            foreach($getDonations AS $donation) {
                // amount & currency
                $label = ($donation['currency'] == 'EUR' ? '€' : '$');
                $output = '<div class="row"><div class="result"><div class="result-inner2">'.$donation['amount'].''.$label.'</div></div>';

                if(empty($donation['photo']) || $donation['anonymous'] == 'Y') {
                    $donation['photo'] = 'noimage.png';
                }
                // Donater
                $output .= '<img alt="'.htmlspecialchars($donation['username']).'" class="team-avatar" src="/public/images/photo/small/'.$donation['photo'].'" width="46px" height="46" />';

                $output .= '<div class="donater'.($donation['anonymous'] == 'Y' ? ' anonymous' : '').'">';
                $output .= '<span class="don-name">';
                if($donation['anonymous'] == 'N') {
                    if(!empty($donation['username'])) {
                        $output .= getMember($donation['username']);
                    } else {
                        $output .= strip_tags($donation['name']);
                    }
                } else {
                    $output .= t('Anonymous');
                }
                $output .= '</span>';

                $output .= '</div>';
                // Date
                $output .= '<div class="date">'.laikaParveide(strtotime($donation['date'])).'</div></div>';
                $donations[] = $output;
            }
        }
        echo implode('',$donations);
        exit;
    }

    /**
     * Give badge if non-anonymous donation & logged in
     *
     * @param $uid
     * @param $amount
     */
    protected function giveBadge($uid) {
        $badge_id = 19; // donation badge
        ClassLib\Badges::addBadge($uid, $badge_id, 'user', false);
    }
}