<div id="right_300">
    <div class="team profile">
        <section class="section">
            <header>
                <h2 class="title">Donate</h2>
            </header>
            <div class="profile-header">
                <?=$form;?>
            </div>
        </section>
    </div>
</div>
<div id="left_615">
    <img src="/public/img/badges/1424024353.png" alt="" style="float:left; padding-right:10px;" />
    VoidLeague does a lot of work and has a fairly in-depth mission - to get all the summoners together and create a
    place where everyone can play and compete with each other for awesome prizes! And it is 100% FOR FREE.<br />
    Help Us to improve and continue our job to stay as awesome as always!
    <div class="clear"></div>
    <h3 style="margin-top:30px;"><?=t('Current donations');?></h3>
    <div id="donations-list">
        <span id="loading_slider"></span>
    </div>
</div>

<script>
    var offset = 0;
    $(document).ready(function() {
       $("#form-checkbox-item-anonymous").click(function() {
            if($(this).is(':checked')) {
                $(".form-field-name").hide();
            } else {
                $(".form-field-name").show();
            }
       });

        $('#right_300 form input.go').click(function(e) {
            $.post("/donations/button", {
                trans_id: $('input[name="trans_id"]').val(),
                name: $("#form-field-item-name").val(),
                anonymous: $("#form-checkbox-item-anonymous").is(':checked')
            }).complete(function(data) {
                $('#right_300 form').submit();
            });
        });


        loadDonations();

    });

    function loadDonations() {
        $.get('/donations/getDonations?offset='+offset, function(data) {
            if(data.length != 0) {
                addDonations(data);
                offset += 20;
            }
        });
    }

    /**
     * Add donatotions using mKE
     * @param data
     */
    function addDonations(data) {
        var first_load = ($('#loading_slider').is(':visible') ? true : false);
        if(first_load) {
            $('#loading_slider').remove();
        }
        $('#donations-list').append(data);


    }

</script>
<style>

    #donations-list .row {
        height: 45px;
        float: left;
        margin: 10px 20px 10px 0px;
        background: #151d23;
        width: 287px;
        position:relative;
    }

    #donations-list .row .result {
        min-width:50px;
        height:50px;
        color:#6caf00;
        font-size:34px;
        text-align:center;
        top:-4px;
        left:7px;
        position:absolute;
        background:#353542;
        display:block;
        padding:2px;
    }

    #donations-list .row .result .result-inner2 {
        padding-top:4px;
    }

    #donations-list .row img {
        position: absolute;
        right: 6px;
        top: -1px;
    }

    #donations-list .row .donater {
        text-align:center;
        padding-left: 15px;
    }

    #donations-list .row .don-name {
        color:#ccc;
    }

    #donations-list .row .date {
        text-align:center;
        color:#666;
        margin-top:3px;
        padding-left: 15px;
    }

</style>