<?php

class ContactsController extends AppController {
  public function index() {
      $args = arg();

    $params = get_query_parameters();
    $this->title = t('Contacts');
    if(isset($_POST['submit'])) {
      $data = (object)$_POST;
      $this->_validateContact($data);
    }
    $form = array(
      'subject' => array(
        '#type' => 'textfield',
        '#title'  => t('Subject'),
        '#default_value' => (!empty($args[1])) ? strtoupper(($args[1])).':' : '',
      ),
      'email' => array(
        '#type' => 'textfield',
        '#title'  => t('E-mail'),
        '#default_value' => (!empty(User::$user->uid)) ? User::$user->epasts : '',
      ),
      'message' => array(
        '#type' => 'textarea',
        '#title'  => t('Message')
      ),
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Send'),
      )
    );
    $form = render_form($form);
    return $this->view('contacts',array('form'=>$form));
  }

  private function _validateContact($data) {
    if(empty($data->subject) || empty($data->email) || empty($data->message)) {
      $this->setError(t('Please fill all fields'));
    }
    elseif(!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
      $this->setError(t('Please enter valid e-mail'));
    }
    else {
$data->message = $data->message.'<br />
--------------------------------------<br />
'.$data->email.'<br />
'.date('d.m.Y H:i');

        ClassLib\Helper::sendMail('info@voidleague.com', $data->subject, $data->message);
      $this->setMessage(t('Thanks! You have received your message and will answer as soon it will be possible'));
      $this->redirect('contacts');
    }
  }
}