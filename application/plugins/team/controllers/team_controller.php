<?php

use ClassLib\Badges;

class TeamController extends AppController{
    function __construct() {


    }

    /**
     * Load more matches via ajax
     */
    function load_matches() {
        $team = (int)$_GET['team'];
        $offset = (int)$_GET['offset'];
        $matches = self::get_matches($team, $offset);

        foreach($matches AS $match): ?>
            <a href="<?php print $match->link;?>" class="row">
                      <div class="datetime">
                        <span class="time"> <?php print date('H:i a',$match->created);?><sup>GMT+2</sup></span>
            <?php print date('d F Y',$match->created); ?>
            </div>
            <div class="result <?php print strtolower($match->status);?>">
                <div class="result-inner"><?php print $match->status;?></div>
            </div>
            <div class="opponent">
                <img alt="<?php htmlspecialchars($match->enemy->name);?>" class="team-avatar" width="56" src="/public/images/users_teams/<?=$match->enemy->picture;?>" width="46px" height="46" />
                <?php print htmlspecialchars($match->enemy->name);?>
            </div>
            <div class="rating_points <?php print strtolower($match->status);?>">
                <?=$match->status_points;?>
            </div>
            </a>
        <?php endforeach;
    }


    public static function get_matches($team_id, $offset=null) {

        $getMatches = db_query("SELECT * FROM `matches` WHERE `entity_type` = 'team' AND (`winner_entity` = '".$team_id."' OR `looser_entity` = '".$team_id."')
        ORDER BY `created` DESC
        ".(is_null($offset) ? 'LIMIT 30' : 'LIMIT '.$offset.', 10'));

        $matches = array();
        while($getMatch = db_fetch_object($getMatches)) {
            $getMatch->status = ($getMatch->winner_entity == $team_id) ? 'Won' : 'Lost';
            $getMatch->status_points = ($getMatch->winner_entity == $team_id) ? $getMatch->winner_rating : $getMatch->looser_rating;
            if($getMatch->status_points > 0) {
                $getMatch->status_points = '+'.$getMatch->status_points;
            }
            $enemy_teamId = ($getMatch->status == 'Won') ? $getMatch->looser_entity : $getMatch->winner_entity;
            $getMatch->enemy = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$enemy_teamId."'"));
            if(empty($getMatch->enemy)) {
                $getMatch->enemy = (object)array(
                    'name' => '*deleted*',
                );
            }

            if(empty($getMatch->enemy->picture)) {
                $getMatch->enemy->picture = 'noimage.png';
            }

            $getMatch->link = '#';
            if(!empty($getMatch->match_type_id) && $getMatch->match_type == 'cup') {
                $cup = db_fetch_object(db_query("SELECT `id`,`slug` FROM `cups` WHERE `id` = '".$getMatch->match_type_id."' "));
                $getMatch->link = '/cups/id/'.$cup->slug.'-'.$cup->id;
            }
            $matches[] = $getMatch;
        }

        return $matches;
    }


    function id() {

        $routes = arg();
        $getTeam = explode('-',$routes[2]);
        $getTeam = end($getTeam);
        $getTeam = db_fetch_object(db_query("SELECT users_teams.*,".PREFIX."speles.title AS game_title
                                            FROM `users_teams`
                                            LEFT JOIN ".PREFIX."speles ON ".PREFIX."speles.tag = users_teams.game
                                            WHERE users_teams.id = '".intval($getTeam)."'"));

        if(empty($getTeam->picture)) {
            $getTeam->picture = 'noimage.png';
        }
        if(empty($getTeam->id)) {
            $this->setError(t('Team not found'));
            $this->redirect('/');
        }
        $this->title = $getTeam->name;
        $this->team = $getTeam;

        $this->team->rank = db_fetch_object(db_query("SELECT *, win+lost+draw AS games_total FROM `rank` WHERE `entity_id` = '".$getTeam->id."' AND `type` = 'team'"));
        if(!empty($this->team->rank)) {
            $this->team->rank->win_ratio = @round(($this->team->rank->win+$this->team->rank->lost)/$this->team->rank->win,0);
            $this->team->rank->win_procents = @round($this->team->rank->win/$this->team->rank->games_total,2);
            $this->team->rank->win_procents = @round($this->team->rank->win_procents*100,2).'%';
            //win/(win+lost+draw
        }
        elseif(empty($this->team->rank)) {
            $this->team->rank = new stdClass();
            $this->team->rank->win_procents = 0;
            $this->team->rank->win = 0;
            $this->team->rank->lost = 0;
            $this->team->rank->now = '-';
            $this->team->rank->rating = '-';
            $this->team->rank->streak = '-';

        }

        $this->team->rank->days_old = otherDiffDate(date('y-m-d H:i:s',$this->team->created));
        // get streak
        $getStreak = db_query("SELECT * FROM `rank_p10` WHERE `entity_id` = '".$this->team->id."' AND `type` = 'team' ORDER BY id DESC");
        $streak_i = 0;
        $streak_status = true;
        while($streak = db_fetch_object($getStreak)) {
            if($streak_status == true) {
                if(isset($this->team->rank->streak_type) && $this->team->rank->streak_type != $streak->status) {
                    $this->team->rank->streak = $streak_i;
                    $streak_status = false;
                }
                else {
                    $this->team->rank->streak_type = $streak->status;
                    $streak_i++;
                    $this->team->rank->streak = $streak_i;
                }
            }
        }

        // recent matches
        $this->team->matches = self::get_matches($this->team->id);

        // Check if user has invited to this team
        $query = get_query_parameters();
        $checkInvite = db_fetch_object(db_query("SELECT * FROM `users_teams_members` WHERE status = 0 AND `team` = '".$getTeam->id."' AND `uid` = '".User::$user->uid."'"));
        if(!empty($checkInvite->uid) && !isset($query->inv)) {
            $this->setMessage(t('You have been invited  to this team! Please !accept or !decline',array('!accept'=>'<a href="?inv=1">'.t('accept').'</a>','!decline'=>'<a href="?inv=0">'.t('decline').'</a>')));
        }
        // Accept/Decline invite

        if(!empty($query->inv) && !empty($checkInvite->uid)) {
            if($query->inv == 1) {
                db_query("UPDATE `users_teams_members` SET `status` = 1, `joined` = NOW() WHERE `team` = '".$getTeam->id."' AND `uid` = '".User::$user->uid."'");
                $this->setMessage(t('You successfully joined team'));
            }
            else {
                db_query("DELETE FROM `users_teams_members` WHERE `team` = '".$getTeam->id."' AND `uid` = '".User::$user->uid."'");
                $this->setMessage(t('You successfully declined invite to team'));
            }
        }

        if(!empty($query->leave_team)) {
            $leave_team = ClassLib\Teams::leaveTeam($getTeam->id, User::$user->uid);
            if($leave_team == '0001') {
                $this->setError(t("You can't leave a team while it is in active tournament"));
            } else if($leave_team) {
                $this->setMessage(t('You left team successfully'));
            }
            $this->redirect();
        }



        // get team members

        $Members = array();
        $sql = "SELECT users_teams_members.gid, ".PREFIX."speles.id AS game_id,users_teams_members.role,
        CONVERT_TZ(users_teams_members.joined, '+02:00','".$_SESSION['user_timezone']."') AS joined,
        ".PREFIX."lietotaji.id AS uid, ".PREFIX."lietotaji.photo AS avatar, ".PREFIX."lietotaji.lietotajvards AS username, ".PREFIX."lietotaji.country
                               FROM `users_teams_members`
                               LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_teams_members.uid
                               LEFT JOIN ".PREFIX."speles ON ".PREFIX."speles.tag = '".$this->team->game."'
                               WHERE `team` = '".$getTeam->id."' AND users_teams_members.status = 1 ORDER BY `role` ASC";

        $getMembers = db_query($sql);
        while($getMember = db_fetch_object($getMembers)) {
            if(empty($getMember->avatar)) {
                $getMember->avatar = 'noimage.png';
            }
            if($getMember->role == 1) {
                $getMember->role = 'Captain';
            }
            elseif($getMember->role == 2) {
                $getMember->role = 'Co-captain';
            }
            else {
                $getMember->role = 'Player';
            }
            $nick = db_result(db_query("SELECT `value` FROM `users_gameaccount` WHERE `uid` = '".$getMember->uid."' AND `game` = '".$this->team->game."' AND `id` = '".$getMember->gid."'"));

            $getMember->nick = $nick;
            $getMember->delete = '';
            if(User::$user->uid == $getMember->uid && $getTeam->uid != User::$user->uid) {
                $getMember->delete = '<a href="?leave_team=1" class="confirm" title="'.t('Are you sure that you want to leave this team?').'"><img src="/public/images/admin/no.png" alt=""></a>';
            }
            $Members[] = $getMember;
        }
        $this->team->members = $Members;

        // region
        if(!empty($this->team->region)) {
            $this->team->region_key = $this->team->region;
            $this->team->region = db_result(db_query("SELECT `name` FROM `gamesaccounts` WHERE `value` = '".$this->team->region."'"));
        }

        // Get trophies
        $trophies = array();
        $getTrophies = db_query("SELECT trophies.* FROM `trophies` JOIN ".PREFIX."speles ON ".PREFIX."speles.tag = trophies.game  WHERE trophies.bundle = 'team' AND trophies.entity = '".$this->team->id."'");
        while($getTrophy = db_fetch_object($getTrophies)) {
            $trophies[] = $getTrophy;
        }
        $this->team->trophies = $trophies;

        // get team badges
        $this->team->badges = Badges::getBadges($this->team->id);

        // Chart
        if(count($this->team->matches) > 10 && $this->team->rank->now != '-' && User::$user->uid == 1) {
            $chart_data = [];
            $current_rating = $this->team->rank->rating;
            $match_week = null;
            $weeks = 0;

            $chart_data[] = [
                'date' => date('d.m.Y'),
                'rating' => $current_rating
            ];
            foreach($this->team->matches AS $match) {
                $match_rating = ($match->winner_entity != $this->team->id) ? $match->winner_rating : $match->looser_rating;

                $current_rating = $current_rating+$match_rating;

                if(!empty($match_week) && $match_week != date('W',$match->created)) {
                    $gendate = new DateTime();
                    $gendate->setISODate(date('Y', $match->created),$match_week, date('d', $match->created));
                    $date = $gendate->format('d.m.Y');

                    $chart_data[] = [
                        'date' => $date,
                        'rating' => $current_rating
                    ];
                    $weeks++;

                }
                $match_week = date('W',$match->created);

                if($weeks > 5) {
                    break;
                }
            }
            $this->chart_data = array_reverse($chart_data);
            $this->team_ranking_chart = $this->view('ranking_chart');
        }

        // Slice matches
        $this->team->matches = array_slice($this->team->matches,0,10);


        // Get rank history
            $this->team->rank_history = ORM::for_table('rank_history')
                ->where('entity_id', $this->team->id)
                ->where('entity_type', 'team')
                ->find_array();
        return $this->view('index');
    }
    #code
}
