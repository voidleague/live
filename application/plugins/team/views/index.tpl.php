<?php
$team = $this->team;
if(empty($team->rank->streak)) {
    $team->rank->streak = 0;
}
?>

<div class="team profile">
    <?php
    if(user_access('administer warnings')) {
        echo '<a href="/warnings/team/'.$team->id.'" class="warning_link"></a>';
    }
    ?>
<div class="profile-header">
    <?php
    if(!empty($team->picture)) {
        print '<img src="/public/images/users_teams/'.$team->picture.'" alt="" class="team-avatar" width="85px" />';
    }
    ?>
    <h2 class="profile-name">
        <img src="/public/images/flags/<?php print $team->country;?>.png"  alt="" /> <?php print htmlspecialchars($team->name);?> <img src="/public/images/games/icons/<?php print $team->game;?>.jpg" alt="" width="30px" /> <?php print htmlspecialchars($team->tag);?>
        <?php if(isset($team->region)) print '<span class="team-region">'.$team->region.' '.$team->format.'</span>'; ?>
        <span class="team-region">
            <? // Parbauda, vai valid. Ja true -> rada attiecigo pogu
            if(!empty($team->facebook) && $team->facebook != 'Not valid') echo '<a rel="nofollow" target="_blank" href="http://www.facebook.com/'.$team->facebook.'"><img src="/public/images/facebook_logo.png" alt="fblogo" width="20px" style="margin-right:10px;" /></a>';
            if(!empty($team->twitter) && $team->twitter != 'Not valid') echo '<a rel="nofollow" target="_blank" href="http://www.twitter.com/'.$team->twitter.'"><img src="/public/images/twitter_logo.png" alt="twitterlogo" width="20px" /></a>';
        ?></span>
    </h2>

    <ul class="profile-meta">
        <li class="statistic">
            <span class="number large"><?php print $team->rank->now; ?></span><?php print t('Rank');?>
        </li>
        <li class="statistic">
            <span class="number large"><?php print $team->rank->rating; ?></span><?php print t('Rating');?>
        </li>
        <li class="statistic">
                <span class="number large"><div class="streak" title=""><?php print $team->rank->streak;?><span class="<?php print strtolower($team->rank->streak_type);?>"></span></div></span><?php print t('Streak');?>

        </li>
    </ul>
    <div class="clear"></div>
</div>
<ul class="profile-extended">
    <li class="win-rate">
        <div class="ratio-bar"><div style="width:<?php print $team->rank->win_procents;?>%"></div></div>
        <div class="ratio-stats">
            <?php print t('Wins'); ?> <span class="stat"><?php print $team->rank->win;?></span>
            <?php print t('Losses'); ?> <?php print $team->rank->lost;?>
        </div>
    </li>
    <li class="badges">
        <?php if(!empty($team->badges)): ?>
            <?php foreach($team->badges AS $badge): ?>
                <span class="badge tooltip"><img width="48" style="border-radius:5px; border: 1px solid #666;" src="/public/img/badges/<?=$badge['icon'];?>" title="<?=$badge['title'];?>" alt="<?=$badge['title'];?>" /></span>
            <?php endforeach; ?>
        <?php endif; ?>
    </li>

    <li class="stats">
        <ul>
            <li><?php print t('Total Matches');?>: <span class="glow"><?php print ($team->rank->win+$team->rank->lost);?></span></li>
            <li><?php print t('Winrate');?>: <span class="glow"><?php print $team->rank->win_procents;?></span></li>
            <li><?php print t('Team Age'); ?>: <span class="glow"><?php print $team->rank->days_old; ?></span></li>
        </ul>
    </li>
    <?php if(!empty($team->rank_history)):?>
    <li class="rank_history" style="width:9%">
        <ul>
            <?php foreach($team->rank_history AS $history):?>
            <li>
                <table width="100%">
                    <tr><th colspan="2" class="history_label"><?=$history['year'];?></th></tr>
                    <tr><td>#<?=$history['rank'];?></td><td><?=$history['rating'];?></td></tr>
                </table>
            </li>
            <?php endforeach;?>
        </ul>
    </li>
    <?php endif; ?>
</ul>
    <br />

    <?php if(!empty($this->team_ranking_chart)) :?>
        <?php echo $this->team_ranking_chart; ?>
    <?php endif;?>
  <section class="section">
    <header>
      <h2 class="title">
        Recent Matches
      </h2>
    </header>
  <div class="table matches expanded">
    <div class="body">
      <?php if(count($team->matches) > 0): ?>
        <?php foreach($team->matches AS $match): ?>
        <a href="<?php print $match->link;?>" class="row">
          <div class="datetime">
            <span class="time"> <?php print date('H:i a',$match->created);?><sup>GMT+2</sup></span>
            <?php print date('d F Y',$match->created); ?>
          </div>
          <div class="result <?php print strtolower($match->status);?>">
            <div class="result-inner"><?php print $match->status;?></div>
          </div>
          <div class="opponent">
            <img alt="<?php htmlspecialchars($match->enemy->name);?>" class="team-avatar" width="56" src="/public/images/users_teams/<?=$match->enemy->picture;?>" width="46px" height="46" />
            <?php print htmlspecialchars($match->enemy->name);?>
          </div>
          <div class="rating_points <?php print strtolower($match->status);?>">
              <?=$match->status_points;?>
          </div>
        </a>
        <?php endforeach; ?>
          <div class="load_more"><a href="#" class="loadMoreMatches" data-team_id="<?=$team->id;?>" data-offset="10"><?=t('Load more matches');?></a></div>
      <?php else: ?>
        <a href="javascript:void(0);" class="row">
          <div class="empty">There are no matches history...</div>
        </a>
      <?php endif; ?>
    </div>
  </div>
    </section>

    <section class="section">
        <header>
            <h2 class="title">
                <?php print t('Team Roster'); ?>
            </h2>
        </header>
        <ul class="table roster expanded">
            <?php foreach($team->members AS $member): ?>
            <li>
                <div class="row " data-toggle="collapse">
                    <div class="image"><img width="56" src="/public/images/photo/small/<?php print $member->avatar;?>" class="user-avatar" /></div>
                    <div class="username"><?php print getMember($member->username,['country'=>$member->country]); ?> <?php print $member->delete; ?></div>
                    <div class="ingame"><?php if(!empty($member->nick)) { print 'In-game: <a target="_blank" href="http://www.lolking.net/search?name='.$member->nick.'&region='.strtoupper($team->region_key).'">'.$member->nick.'</a>'; }?></div>
                    <div class="joined"><?php if(user_access('administer '.$team->game.' cups')): ?><?php print t('member since');  ?> <?php print date('d.m.Y H:i',strtotime($member->joined)); ?> <?php endif;?></div>
                    <div class="role"><?php print $member->role;?></div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section class="section">
        <header>
            <h2 class="title">
                <?php print t('Trophies'); ?>
            </h2>
        </header>
        <ul class="table roster expanded">
            <?php if(count($team->trophies) > 0):?>
                <?php foreach($team->trophies AS $trophy): ?>
                    <li>
                        <a href="/<?=$trophy->src;?>">
                            <div class="row" data-toggle="collapse">
                                <div class="image" style="width:33px"><img src="/public/images/awards/<?=$trophy->game;?>/winner.png" class="user-avatar" /></div>
                                <div class="username" style="width:100%;"><?php print $trophy->title; ?></div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <li>
                    <div class="row" data-toggle="collapse">
                        <div><?php print t('Team does not have any trophy...');?></div>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
    </section>