<script type="text/javascript"
        src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

<script type="text/javascript">
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Rating', ''],
            <?php foreach($this->chart_data AS $chart): ?>
                ['<?=$chart['date'];?>', <?=$chart['rating'];?>],
            <?php endforeach; ?>
        ]);

        var options = {
            is3D: true,
            curveType: 'function',
            backgroundColor: {
                fill: '#28282a',
                stroke: 'white'
            },
            series: {
                0:{
                    color:'1794CF',
                    lineWidth:4
                }
            },
            legend: 'none',
            hAxis: {
                textStyle: {
                    color: 'white'
                }
            },
            vAxis: {
                textStyle: {
                    color: 'white',
                    bold: true
                }
            },
            pointSize: 7
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
    }
</script>
<section class="section">
    <header>
        <h2 class="title">
            Ranking history
        </h2>
    </header>
    <div class="table matches expanded">
        <div class="body">
            <div id="curve_chart" style="width: 930px; height: 300px; background:#28282a;"></div>
        </div>
    </div>
</section>