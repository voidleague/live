<?php

use ClassLib\Badges;

class UserController extends AppController {
    public function index() {
        $args = arg();
        $member = db_fetch_object(db_query("SELECT
                                            u.*, u.id AS uid, u.lietotajvards AS username, r.color AS group_color, r.name AS group_name
                                            FROM ".PREFIX."lietotaji u
                                            LEFT JOIN `users_roles` ur ON u.id = ur.uid
                                            LEFT JOIN `role` r ON r.rid = ur.rid
                                            WHERE u.`lietotajvards` = '".mysql_real_escape_string($args[1])."'"));
        if(empty($member)) {
            $this->setError('There are no member with this username');
            //$this->redirect('/');
        }
        $this->title = $member->username;
        $member->dzimums = ($member->dzimums <= 1) ? t('Male') : t('Female');
        // ooooooolllllllllllldddddddd
        $years_old = date('Y')-$member->dd_gads;
        if((date('n')<$member->dd_menesis) || (date('n')==$member->dd_menesis && date('j')<$member->dd_gads))
        {
            $years_old--;
            $years_old = $years_old." "._('years');
        }
        if(empty($member->dd_gads) OR empty($member->dd_diena) OR empty($member->dd_menesis))
        {
            $years_old = "-";
        }
        $member->years_old = $years_old;



        // Get gameaccounts
        $gameaccounts = array();
        $getGameAccounts = db_query("SELECT users_gameaccount.*, gamesaccounts.name
                                        FROM `users_gameaccount`
                                        LEFT JOIN `gamesaccounts` ON gamesaccounts.value = users_gameaccount.region
                                        WHERE `uid` = '".$member->uid."' AND users_gameaccount.game = gamesaccounts.game");
        while($getGameAccount = db_fetch_object($getGameAccounts)) {
            $gameaccounts[] = $getGameAccount;
        }
        $member->gameaccounts = $gameaccounts;

        // Get trophies
        $trophies = array();
        $getTrophies = db_query("
            SELECT t.*, ut.name AS team_name, ut.country AS team_country
            FROM `trophies` t
            LEFT JOIN `users_teams_members` m ON m.team = t.entity
            LEFT JOIN `users_teams` ut ON ut.id = m.team
            WHERE t.bundle = 'team' AND m.uid = '".$member->uid."' ORDER BY t.`id` DESC");

        while($getTrophy = db_fetch_object($getTrophies)) {
            $trophies[] = $getTrophy;
        }
        $member->trophies = $trophies;

        // get teams
        $teams = array();
        $getTeams = db_query("SELECT users_teams.*
                                FROM `users_teams_members`
                                LEFT JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                WHERE users_teams_members.uid = '".$member->uid."' AND `users_teams_members`.status = 1");
        while($getTeam = db_fetch_object($getTeams)) {
            if(empty($getTeam->picture)) {
                $getTeam->picture = 'noimage.png';
            }
            $teams[] = $getTeam;
        }
        $member->teams = $teams;

        $member->badges = Badges::getBadges($member->id,'user');

        return $this->view('user',array('member'=>$member));
    }
}