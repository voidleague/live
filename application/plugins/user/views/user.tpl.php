<?php
$member = $variables['member'];
?>

<div class="team profile">
    <div class="profile-header">
        <div class="profile-photo-left">
            <?php
            if(!empty($member->photo)) {
                print '<img src="/public/images/photo/'.$member->photo.'" alt="" class="team-avatar" width="85px" />';
            }
            ?>
            <?php if(!empty($member->group_color)): ?>
                <div class="clear"></div><br /><img src="/public/themes/voidleague/img/favicon.ico" alt="" /><br />
                <span style="color:#<?=$member->group_color;?>; font-weight:bold;"><?=$member->group_name;?></span>
            <?php endif; ?>
        </div>
        <h2 class="profile-name"><img src="/public/images/flags/<?php print strtolower($member->country);?>.png"  alt="" /> <?php print htmlspecialchars($member->username);?></h2>
        <ul class="stats" style="padding-left: 105px;">
            <li class="extended"><?php print t('Name');?>: <span class="glow"><?=htmlspecialchars($member->name);?></span></li>
            <li class="extended"><?php print t('Years');?>: <span class="glow"><?=$member->years_old; ?></span></li>
            <li class="extended"><?php print t('Skype');?>: <span class="glow"><?=htmlspecialchars($member->skype);?></span></li>
            <li class="extended"><?php print t('Gender');?>: <span class="glow"><?=htmlspecialchars($member->dzimums);?></span></li>
            <li class="extended"><?php print t('City');?>: <span class="glow"><?=htmlspecialchars($member->city);?></span></li>
            <?php if(admin()): ?>
                <li class="extended"><?php print t('IP Address');?>: <span class="glow"><?=$member->ip;?> (Only admin)</span></li>
            <?php endif; ?>

        </ul>
        <ul class="profile-meta">
            <?php if(User::$user->uid): ?>
                <li class="statistic">
                    <div id="send_msg"><a href="/messages/write/<?php print $member->uid;?>/"><?php print t('Send message');?></a></div>
                </li>
            <?php endif; ?>
        </ul>
        <div class="clear"></div>
    </div>
    <ul class="profile-extended">
        <li class="win-rate">

        </li>
        <li class="badges">
            <?php if(!empty($member->badges)): ?>
                <?php foreach($member->badges AS $badge): ?>
                    <span class="badge tooltip"><img width="32" src="/public/img/badges/<?=$badge['icon'];?>" title="<?=$badge['title'];?>" alt="<?=$badge['title'];?>" /></span>
                <?php endforeach; ?>
            <?php endif; ?>
        </li>
        <!--<li class="social-media"></li>-->
        <li class="stats">
            <ul>
                <li><?php print t('Last seen');?>: <span class="glow"><?php print laikaParveide($member->online); ?></span></li>
                <li><?php print t('Registered');?>: <span class="glow"><?php print laikaParveide($member->registrejies); ?> (<?php print $member->uid;?>)</span></li>
            </ul>
        </li>
    </ul>
    <br />
    <section class="section">
        <header>
            <h2 class="title">
                <?php print t('Gameaccounts'); ?>
            </h2>
        </header>
        <ul class="table roster expanded">
            <?php if(count($member->gameaccounts) > 0):?>
            <?php foreach($member->gameaccounts AS $gameaccount): ?>
                <li>
                    <div class="row " data-toggle="collapse">
                        <div class="image"><img src="/public/images/games/icons/<?php print $gameaccount->game;?>.jpg" class="user-avatar" /></div>
                        <div class="username"><?php print $gameaccount->name; ?></div>
                        <div class="ingame"><?php if(!empty($gameaccount->value)) { echo '<a target="_blank" href="http://www.lolking.net/search?name='.$gameaccount->value.'&region='.strtoupper($gameaccount->region).'">'.$gameaccount->value.'</a>'; }?></div>
                        <div class="joined"><?php print t('created');?> <?php print date('d.m.Y',strtotime($gameaccount->created)); ?></div>
                    </div>
                </li>
            <?php endforeach; ?>
            <?php else: ?>
                <li>
                    <div class="row" data-toggle="collapse">
                        <div><?php print t('User does not have gameaccount...');?></div>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
    </section>


    <section class="section user-teams">
        <header>
            <h2 class="title">
                <?php print t('Teams'); ?>
            </h2>
        </header>
        <ul class="table roster expanded">
            <?php if(count($member->teams) > 0):?>
                <?php foreach($member->teams AS $team): ?>
                    <li>
                        <a href="/team/id/<?=$team->slug;?>-<?=$team->id;?>">
                            <div class="row" data-toggle="collapse">
                                <div class="image" style="width:33px"><img width="46px" height="46px" src="/public/images/users_teams/<?=$team->picture;?>" class="user-avatar" /></div>
                                <div class="username" style="width:100%;"><h2 class="profile-name"><img src="/public/images/flags/<?=strtolower($team->country);?>.png" alt="" /> <?=htmlspecialchars($team->name);?></h2></div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <li>
                    <div class="row" data-toggle="collapse">
                        <div><?php print t('User does not have any team...');?></div>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
    </section>


    <section class="section">
        <header>
            <h2 class="title">
                <?php print t('Trophies'); ?>
            </h2>
        </header>
        <ul class="table roster expanded">
            <?php if(count($member->trophies) > 0):?>
                <?php foreach($member->trophies AS $trophy): ?>
                    <li>
                        <a href="/<?=$trophy->src;?>">
                            <div class="row" data-toggle="collapse">
                                <div class="image" style="width:33px"><img src="/public/images/awards/<?=$trophy->game;?>/winner.png" class="user-avatar" /></div>
                                <div class="username" style="width:100%;"><?php print $trophy->title; ?> [<img src="/public/images/flags/<?=strtolower($trophy->team_country);?>.png" /> <?=$trophy->team_name;?>]</div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <li>
                    <div class="row" data-toggle="collapse">
                        <div><?php print t('User does not have any trophy...');?></div>
                    </div>
                </li>
            <?php endif; ?>
        </ul>
    </section>