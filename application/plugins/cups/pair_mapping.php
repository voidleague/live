<?php
/**
 * Pair mapping for double elimination
 */
$pair_mapping = [
    8 => [
        // Winners bracket
        'A' => [
            'W' => 'E1',
            'L' => 'H1',
        ],
        'B' => [
            'W' => 'E2',
            'L' => 'H2',
        ],
        'C' => [
            'W' => 'F1',
            'L' => 'I1',
        ],
        'D' => [
            'W' => 'F2',
            'L' => 'I2',
        ],
        'E' => [
            'W' => 'G1',
            'L' => 'K1',
        ],
        'F' => [
            'W' => 'G2',
            'L' => 'J1',
        ],
        'G' => [
            'W' => 'WINNER',
        ],
        // Loser bracket
        'H' => [
            'W' => 'J2',
        ],
        'I' => [
            'W' => 'K2',
        ],
        'J' => [
            'W' => 'L1',
        ],
        'K' => [
            'W' => 'L2',
        ],
    ],
    16 => [
        // Winners bracket
        'A' => [
            'W' => 'I1',
            'L' => 'P1',
        ],
        'B' => [
            'W' => 'I2',
            'L' => 'P2',
        ],
        'C' => [
            'W' => 'J1',
            'L' => 'Q1',
        ],
        'D' => [
            'W' => 'J2',
            'L' => 'Q2',
        ],
        'E' => [
            'W' => 'K1',
            'L' => 'R1',
        ],
        'F' => [
            'W' => 'K2',
            'L' => 'R2',
        ],
        'G' => [
            'W' => 'L1',
            'L' => 'S1',
        ],
        'H' => [
            'W' => 'L2',
            'L' => 'S2',
        ],
        'I' => [
            'W' => 'M1',
            'L' => 'V1',
        ],
        'J' => [
            'W' => 'M2',
            'L' => 'W1',
        ],
        'K' => [
            'W' => 'N1',
            'L' => 'T1',
        ],
        'L' => [
            'W' => 'N2',
            'L' => 'U1',
        ],
        'M' => [
            'W' => 'O1',
            'L' => 'Z1',
        ],
        'N' => [
            'W' => 'O2',
            'L' => 'AA1',
        ],
        'O' => [
            'W' => 'WINNER'
        ],
        // Loser bracket
        'P' => [
            'W' => 'T2',
        ],
        'Q' => [
            'W' => 'U2',
        ],
        'R' => [
            'W' => 'V2',
        ],
        'S' => [
            'W' => 'W2',
        ],
        'T' => [
            'W' => 'X1',
        ],
        'U' => [
            'W' => 'X2',
        ],
        'V' => [
            'W' => 'Y1',
        ],
        'W' => [
            'W' => 'Y2',
        ],
        'X' => [
            'W' => 'Z2',
        ],
        'Y' => [
            'W' => 'AA2'
        ],
        'Z' => [
            'W' => 'AB1',
        ],
        'AA' => [
            'W' => 'AB2',
        ],
    ],
    32 => [
        // Winners bracket
        'A' => [
            'W' => 'Q1',
            'L' => 'AF1',
        ],
        'B' => [
            'W' => 'Q2',
            'L' => 'AF2',
        ],
        'C' => [
            'W' => 'R1',
            'L' => 'AG1',
        ],
        'D' => [
            'W' => 'R2',
            'L' => 'AG2',
        ],
        'E' => [
            'W' => 'S1',
            'L' => 'AH1',
        ],
        'F' => [
            'W' => 'S2',
            'L' => 'AH2',
        ],
        'G' => [
            'W' => 'T1',
            'L' => 'AI1',
        ],
        'H' => [
            'W' => 'T2',
            'L' => 'AI2',
        ],
        'I' => [
            'W' => 'U1',
            'L' => 'AJ1',
        ],
        'J' => [
            'W' => 'U2',
            'L' => 'AJ2',
        ],
        'K' => [
            'W' => 'V1',
            'L' => 'AK1',
        ],
        'L' => [
            'W' => 'V2',
            'L' => 'AK2',
        ],
        'M' => [
            'W' => 'W1',
            'L' => 'AL1',
        ],
        'N' => [
            'W' => 'W2',
            'L' => 'AL2',
        ],
        'O' => [
            'W' => 'X1',
            'L' => 'AM1',
        ],
        'P' => [
            'W' => 'X2',
            'L' => 'AM2',
        ],
        // row2
        'Q' => [
            'W' => 'Y1',
            'L' => 'AR1',
        ],
        'R' => [
            'W' => 'Y2',
            'L' => 'AS1',
        ],
        'S' => [
            'W' => 'Z1',
            'L' => 'AT1',
        ],
        'T' => [
            'W' => 'Z2',
            'L' => 'AU1',
        ],
        'U' => [
            'W' => 'AA1',
            'L' => 'AN1',
        ],
        'V' => [
            'W' => 'AA2',
            'L' => 'AO1',
        ],
        'W' => [
            'W' => 'AB1',
            'L' => 'AP1',
        ],
        'X' => [
            'W' => 'AB2',
            'L' => 'AQ1',
        ],
        // row3
        'Y' => [
            'W' => 'AC1',
            'L' => 'AZ1',
        ],
        'Z' => [
            'W' => 'AC2',
            'L' => 'BA1',
        ],
        'AA' => [
            'W' => 'AD1',
            'L' => 'BB1',
        ],
        'AB' => [
            'W' => 'AD2',
            'L' => 'BC1',
        ],
        // row4
        'AC' => [
            'W' => 'AE1',
            'L' => 'BF1',
        ],
        'AD' => [
            'W' => 'AE2',
            'L' => 'BG1',
        ],
        // row5
        'AE' => [
            'W' => 'WINNER',
        ],
        // Losers brackets
        'AF' => [
            'W' => 'AN2',
        ],
        'AG' => [
            'W' => 'AO2',
        ],
        'AH' => [
            'W' => 'AP2',
        ],
        'AI' => [
            'W' => 'AQ2',
        ],
        'AJ' => [
            'W' => 'AR2',
        ],
        'AK' => [
            'W' => 'AS2',
        ],
        'AL' => [
            'W' => 'AT2',
        ],
        'AM' => [
            'W' => 'AU2',
        ],
        // row2
        'AN' => [
            'W' => 'AV1',
        ],
        'AO' => [
            'W' => 'AV2',
        ],
        'AP' => [
            'W' => 'AW1',
        ],
        'AQ' => [
            'W' => 'AW2',
        ],
        'AR' => [
            'W' => 'AX1',
        ],
        'AS' => [
            'W' => 'AX2',
        ],
        'AT' => [
            'W' => 'AY1',
        ],
        'AU' => [
            'W' => 'AY2',
        ],
        // row3
        'AV' => [
            'W' => 'AZ2',
        ],
        'AW' => [
            'W' => 'BA2',
        ],
        'AX' => [
            'W' => 'BB2',
        ],
        'AY' => [
            'W' => 'BC2',
        ],
        // row4
        'AZ' => [
            'W' => 'BD1',
        ],
        'BA' => [
            'W' => 'BD2',
        ],
        'BB' => [
            'W' => 'BE1',
        ],
        'BC' => [
            'W' => 'BE2',
        ],
        //row5
        'BD' => [
            'W' => 'BF2',
        ],
        'BE' => [
            'W' => 'BG2',
        ],
        // row6
        'BF' => [
            'W' => 'BH1',
        ],
        'BG' => [
            'W' => 'BH2',
        ]
    ],
    // 64 teams
    64 => [
        // Winner:row1d
        'A' => [
            'W' => 'AG1',
            'L' => 'BL1',
        ],
        'B' => [
            'W' => 'AG2',
            'L' => 'BL2',
        ],
        'C' => [
            'W' => 'AH1',
            'L' => 'BM1',
        ],
        'D' => [
            'W' => 'AH2',
            'L' => 'BM2',
        ],
        'E' => [
            'W' => 'AI1',
            'L' => 'BN1',
        ],
        'F' => [
            'W' => 'AI2',
            'L' => 'BN2',
        ],
        'G' => [
            'W' => 'AJ1',
            'L' => 'BO1',
        ],
        'H' => [
            'W' => 'AJ2',
            'L' => 'BO2',
        ],
        'I' => [
            'W' => 'AK1',
            'L' => 'BP1',
        ],
        'J' => [
            'W' => 'AK2',
            'L' => 'BP2',
        ],
        'K' => [
            'W' => 'AL1',
            'L' => 'BQ1',
        ],
        'L' => [
            'W' => 'AL2',
            'L' => 'BQ2',
        ],
        'M' => [
            'W' => 'AM1',
            'L' => 'BR1',
        ],
        'N' => [
            'W' => 'AM2',
            'L' => 'BR2',
        ],
        'O' => [
            'W' => 'AN1',
            'L' => 'BS1',
        ],
        'P' => [
            'W' => 'AN2',
            'L' => 'BS2',
        ],
        'Q' => [
            'W' => 'AO1',
            'L' => 'BT1',
        ],
        'R' => [
            'W' => 'AO2',
            'L' => 'BT2',
        ],
        'S' => [
            'W' => 'AP1',
            'L' => 'BU1',
        ],
        'T' => [
            'W' => 'AP2',
            'L' => 'BU2',
        ],
        'U' => [
            'W' => 'AQ1',
            'L' => 'BV1',
        ],
        'V' => [
            'W' => 'AQ2',
            'L' => 'BV2',
        ],
        'W' => [
            'W' => 'AR1',
            'L' => 'BW1',
        ],
        'X' => [
            'W' => 'AR2',
            'L' => 'BW2',
        ],
        'Y' => [
            'W' => 'AS1',
            'L' => 'BX1',
        ],
        'Z' => [
            'W' => 'AS2',
            'L' => 'BX2',
        ],
        'AA' => [
            'W' => 'AT1',
            'L' => 'BY1',
        ],
        'AB' => [
            'W' => 'AT2',
            'L' => 'BY2',
        ],
        'AC' => [
            'W' => 'AU1',
            'L' => 'BZ1',
        ],
        'AD' => [
            'W' => 'AU2',
            'L' => 'BZ2',
        ],
        'AE' => [
            'W' => 'AV1',
            'L' => 'CA1',
        ],
        'AF' => [
            'W' => 'AV2',
            'L' => 'CA2',
        ],
        // Winner:row2
        'AG' => [
            'W' => 'AW1',
            'L' => 'CJ1',
        ],
        'AH' => [
            'W' => 'AW2',
            'L' => 'CK1',
        ],
        'AI' => [
            'W' => 'AX1',
            'L' => 'CL1',
        ],
        'AJ' => [
            'W' => 'AX2',
            'L' => 'CM1',
        ],
        'AK' => [
            'W' => 'AY1',
            'L' => 'CN1',
        ],
        'AL' => [
            'W' => 'AY2',
            'L' => 'CO1',
        ],
        'AM' => [
            'W' => 'AZ1',
            'L' => 'CP1',
        ],
        'AN' => [
            'W' => 'AZ2',
            'L' => 'CQ1',
        ],
        'AO' => [
            'W' => 'BA1',
            'L' => 'CB1',
        ],
        'AP' => [
            'W' => 'BA2',
            'L' => 'CC1',
        ],
        'AQ' => [
            'W' => 'BB1',
            'L' => 'CD1',
        ],
        'AR' => [
            'W' => 'BB2',
            'L' => 'CE1',
        ],
        'AS' => [
            'W' => 'BC1',
            'L' => 'CF1',
        ],
        'AT' => [
            'W' => 'BC2',
            'L' => 'CG1',
        ],
        'AU' => [
            'W' => 'BD1',
            'L' => 'CH1',
        ],
        'AV' => [
            'W' => 'BD2',
            'L' => 'CI1',
        ],
        //Winner:row3
        'AW' => [
            'W' => 'BE1',
            'L' => 'CZ1',
        ],
        'AX' => [
            'W' => 'BE2',
            'L' => 'DA1',
        ],
        'AY' => [
            'W' => 'BF1',
            'L' => 'DB1',
        ],
        'AZ' => [
            'W' => 'BF2',
            'L' => 'DC1',
        ],
        'BA' => [
            'W' => 'BG1',
            'L' => 'DD1',
        ],
        'BB' => [
            'W' => 'BG2',
            'L' => 'DE1',
        ],
        'BC' => [
            'W' => 'BH1',
            'L' => 'DF1',
        ],
        'BD' => [
            'W' => 'BH2',
            'L' => 'DG1',
        ],
        //Winner:row4
        'BE' => [
            'W' => 'BI1',
            'L' => 'DL1',
        ],
        'BF' => [
            'W' => 'BI2',
            'L' => 'DM1',
        ],
        'BG' => [
            'W' => 'BJ1',
            'L' => 'DN1',
        ],
        'BH' => [
            'W' => 'BJ2',
            'L' => 'DO1',
        ],
        //Winner:row5
        'BI' => [
            'W' => 'BK1',
            'L' => 'DR1',
        ],
        'BJ' => [
            'W' => 'BK2',
            'L' => 'DS1',
        ],
        //Winner:row6
        'BK' => [
            'W' => 'WINNER',
        ],
        //Loser:row1
        'BL' => [
            'W' => 'CB2',
        ],
        'BM' => [
            'W' => 'CC2',
        ],
        'BN' => [
            'W' => 'CD2',
        ],
        'BO' => [
            'W' => 'CE2',
        ],
        'BP' => [
            'W' => 'CF2',
        ],
        'BQ' => [
            'W' => 'CG2',
        ],
        'BR' => [
            'W' => 'CH2',
        ],
        'BS' => [
            'W' => 'CI2',
        ],
        'BT' => [
            'W' => 'CJ2',
        ],
        'BU' => [
            'W' => 'CK2',
        ],
        'BV' => [
            'W' => 'CL2',
        ],
        'BW' => [
            'W' => 'CM2',
        ],
        'BX' => [
            'W' => 'CN2',
        ],
        'BY' => [
            'W' => 'CO2',
        ],
        'BZ' => [
            'W' => 'CP2',
        ],
        'CA' => [
            'W' => 'CQ2',
        ],
        // Loser:row2
        'CB' => [
            'W' => 'CR1',
        ],
        'CC' => [
            'W' => 'CR2',
        ],
        'CD' => [
            'W' => 'CS1',
        ],
        'CE' => [
            'W' => 'CS2',
        ],
        'CF' => [
            'W' => 'CT1',
        ],
        'CG' => [
            'W' => 'CT2',
        ],
        'CH' => [
            'W' => 'CU1',
        ],
        'CI' => [
            'W' => 'CU2',
        ],
        'CJ' => [
            'W' => 'CV1',
        ],
        'CK' => [
            'W' => 'CV2',
        ],
        'CL' => [
            'W' => 'CW1',
        ],
        'CM' => [
            'W' => 'CW2',
        ],
        'CN' => [
            'W' => 'CX1',
        ],
        'CO' => [
            'W' => 'CX2',
        ],
        'CP' => [
            'W' => 'CY1',
        ],
        'CQ' => [
            'W' => 'CY2',
        ],
        //Loser:row3
        'CR' => [
            'W' => 'CZ2'
        ],
        'CS' => [
            'W' => 'DA2'
        ],
        'CT' => [
            'W' => 'DB2'
        ],
        'CU' => [
            'W' => 'DC2'
        ],
        'CV' => [
            'W' => 'DD2'
        ],
        'CW' => [
            'W' => 'DE2'
        ],
        'CX' => [
            'W' => 'DF2'
        ],
        'CY' => [
            'W' => 'DG2'
        ],
        // Loser:row4
        'CZ' => [
            'W' => 'DH1'
        ],
        'DA' => [
            'W' => 'DH2'
        ],
        'DB' => [
            'W' => 'DI1'
        ],
        'DC' => [
            'W' => 'DI2'
        ],
        'DD' => [
            'W' => 'DJ1'
        ],
        'DE' => [
            'W' => 'DJ2'
        ],
        'DF' => [
            'W' => 'DK1'
        ],
        'DG' => [
            'W' => 'DK2'
        ],
        // Loser:row5
        'DH' => [
            'W' => 'DL2'
        ],
        'DI' => [
            'W' => 'DM2'
        ],
        'DJ' => [
            'W' => 'DN2'
        ],
        'DK' => [
            'W' => 'DO2'
        ],
        //Loser:row6
        'DL' => [
            'W' => 'DP1'
        ],
        'DM' => [
            'W' => 'DP2'
        ],
        'DN' => [
            'W' => 'DQ1'
        ],
        'DO' => [
            'W' => 'DQ2'
        ],
        // Loser:row7
        'DP' => [
            'W' => 'DR2'
        ],
        'DQ' => [
            'W' => 'DS2'
        ],
        // Loser:row8
        'DR' => [
            'W' => 'DT1'
        ],
        'DS' => [
            'W' => 'DT2'
        ],
    ]
];

// Add odd/even and enemy pair to mapping
$number = 1;
foreach($pair_mapping AS $teams => $mapping_ar) {
    foreach($mapping_ar AS $pair => $mapping) {

        //$mapping_type = ($number % 2 == 0) ? 'even' : 'odd';
        $mapping_type = ($number % 2 == 0) ? 'even' : 'odd';
        $pair_mapping[$teams][$pair]['type'] = $mapping_type;
        if($mapping_type == 'even') {
                $pair_mapping[$teams][$pair]['enemy_pair'] = $prev_pair;
                $pair_mapping[$teams][$prev_pair]['enemy_pair'] = $pair;
        }
        if($mapping['W'] != 'WINNER') {
            $prev_pair = $pair;
            $number++;
        }

    }
}
