<?php
class CupsActiveController extends CupsController {
  public $cup;
  function __construct($cup) {
    $this->cup = $cup->cup;
    $this->homepath = 'table';
    $this->links = array(
      'table' => t('Table'),
      'rules' => t('Rules'),
      'participants' => ($this->cup->type == 'teams') ? t('Teams') : t('Players'),
    );
  }

  public function table() {
    $cup = $this->cup;


    $row_names = array(
      3 => array(
        1 => t('Quarter'),
        2 => t('Semi'),
        3 => t('Finals'),
        4 => t('Winner'),
        '_3' => t('3rd Place'),
      ),
      '3_double' => [
          1 => t('Round 1'),
          2 => t('Semi-finals'),
          3 => t('Finals'),
          4 => t('Winner'),
          5 => t('3rd Place')
      ],
      4 => array(
        1 => 'RO 16',
        2 => t('Quarter'),
        3 => t('Semi'),
        4 => t('Finals'),
        5 => t('Winner'),
        '_3' => t('3rd Place'),
      ),
      '4_double' => [
          1 => t('Round 1'),
          2 => t('Round 2'),
          3 => t('Round 3'),
          4 => t('Semi-finals'),
          5 => t('Finals'),
          6 => t('Winner'),
          7 => t('3rd Place')
      ],
      5 => array(
        1 => 'RO 32',
        2 => 'RO 16',
        3 => t('Quarter'),
        4 => t('Semi'),
        5 => t('Finals'),
        6 => t('Winner'),
        '_3' => t('3rd Place'),
      ),
      '5_double' => array(
          1 => t('Round 1'),
          2 => t('Round 2'),
          3 => t('Round 3'),
          4 => t('Round 4'),
          5 => t('Round 5'),
          6 => t('Round 6'),
          7 => t('Semi-finals'),
          8 => t('Finals'),
          9 => t('Winner'),
          10 => t('3rd Place')
      ),
      6 => array(
        1 => 'RO 64',
        2 => 'RO 32',
        3 => 'RO 16',
        4 => t('Quarter'),
        5 => t('Semi'),
        6 => t('Finals'),
        7 => t('Winner'),
        '_3' => t('3rd Place'),
      ),
      '6_double' => array(
          1 => t('Round 1'),
          2 => t('Round 2'),
          3 => t('Round 3'),
          4 => t('Round 4'),
          5 => t('Round 5'),
          6 => t('Round 6'),
          7 => t('Round 7'),
          8 => t('Round 8'),
          9 => t('Semi-finals'),
          10 => t('Finals'),
          11 => t('Winner'),
          12 => t('3rd Place')
      ),
      7 => array(
        1 => 'RO 128',
        2 => 'RO 64',
        3 => 'RO 32',
        4 => 'RO 16',
        5 => t('Quarter'),
        6 => t('Semi'),
        7 => t('Finals'),
        8 => t('Winner'),
        '_3' => t('3rd Place'),
      ),
    );


    $last = (floor(log($cup->teams,2))+1);


    $table = array();
    if($cup->type == 'players') {
      $get_tree = db_query("SELECT cups_tree.double, cups_tree.pos,cups_tree.row,cups_tree.entity,cups_tree.match_id, ".PREFIX."lietotaji.lietotajvards AS username, ".PREFIX."lietotaji.country
                                  FROM `cups_tree`
                                  LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = cups_tree.entity
                                  WHERE `cid` = '".$cup->id."' ORDER BY `row` ASC, `pos` ASC");
    }
    else {
      $get_tree = db_query("SELECT cups_tree.pair, cups_tree.double, cups_tree.double, users_teams.country, cups_tree.pos,cups_tree.row,cups_tree.entity,cups_tree.match_id, users_teams.name AS team_name, users_teams.tag AS team_tag, users_teams.id AS team_id, users_teams.slug AS team_slug
                                  FROM `cups_tree`
                                  LEFT JOIN `users_teams` ON users_teams.id = cups_tree.entity
                                  WHERE `cid` = '".$cup->id."' ORDER BY `double` ASC, `row` ASC, `pos` ASC");
    }


    if(!empty($cup->haveWinner) && $cup->status == 1 && $cup->admin) {
      $this->setMessage(t('There is a winner. !link',array('!link'=>'<a href="?close_cup=true">'.t('Close cup').'</a>')));
    }
    while($tree = db_fetch_object($get_tree)) {

        if(empty($table[$tree->double])) {
            $table[$tree->double] = [];
        }
        if(empty($table[$tree->double][$tree->row])) {
          $table[$tree->double][$tree->row] = [];
        }
      if(empty($table[$tree->row][$tree->pos])) {
        $table[$tree->double][$tree->row][$tree->pos] = new stdClass();
      }
      if($cup->type == 'players') {
        $table[$tree->double][$tree->row][$tree->pos]->label = (isset($tree->username)) ? '<img src="/public/images/flags/'.strtolower($tree->country).'.png" width="10px" alt="" /> '.getMember($tree->username) : '-';
      }
      else {
        $table[$tree->double][$tree->row][$tree->pos]->label = (isset($tree->team_name)) ? '<img src="/public/images/flags/'.strtolower($tree->country).'.png" width="10px" alt="" /> <a href="/team/id/'.$tree->team_slug.'-'.$tree->team_id.'">'.$tree->team_name.'</a>' : '-';
      }
      $table[$tree->double][$tree->row][$tree->pos]->data = $cup->id.' '.$tree->pos.' '.$tree->row.' '.$tree->double;
      $table[$tree->double][$tree->row][$tree->pos]->remove_data = $cup->id.' '.$tree->pos.' '.$tree->row.' '.$tree->double.' '.$tree->pair;
      $table[$tree->double][$tree->row][$tree->pos]->match_id = $tree->match_id;
      $table[$tree->double][$tree->row][$tree->pos]->entity_id = $tree->entity;
      $table[$tree->double][$tree->row][$tree->pos]->pair = $tree->pair;

        $match = false;
      if(!empty($tree->match_id)) {
          $match = db_fetch_object(db_query("SELECT * FROM `matches` WHERE `id` = '".intval($tree->match_id)."'"));
      }

      if(!empty($match->id)) {
        if($tree->entity == $match->winner_entity) {
          $table[$tree->double][$tree->row][$tree->pos]->score = $match->winner_score;
        }
        else {
          $table[$tree->double][$tree->row][$tree->pos]->score = $match->looser_score;
        }
      }
      else {
        $table[$tree->double][$tree->row][$tree->pos]->score = 0;
      }

      if($cup->double_elimination) {
          $double_elimination_last = $tree->row;
      }

      // Calculate if this is BO3 pair
      if(empty($tree->double) && (($tree->row >= ($last-1) && $tree->row != ($last) && !empty($cup->double_elimination)) || (empty($cup->double_elimination) && (($tree->row >= ($last-1) && $tree->row != $last) || ($tree->row == '_3'))))) {
          $table[$tree->double][$tree->row][$tree->pos]->bo3 = true;
      }
    }

    if($cup->double_elimination) {
        $last += 2;
    }


    // Delay for users
    if($cup->delay > 0 && (strtotime($cup->started_time)+($cup->delay*60) > time()) && !user_access('administer streams')) {
      //return $this->view('delay',$cup);
    }

    $current_row = 0;
    $grid = floor(log($cup->teams,2));
    $output = '<div class="cup-tree grid-'.$grid.' '.($cup->double_elimination ? 'double' : '').'"><div class="cup-tree-inbox">';

    $place3_pair = isset($table['_3']) ? $table['_3'] : array();

    $last_score = $last;

if($cup->double_elimination) {
    $last_score -=1;
}


    foreach($table AS $table_type=>$subtable) {
        if(!empty($cup->double_elimination)) {
            ksort($subtable);
        }
        // TODO::Remove this in 2025
        $teams_table_sum = 0;

        if($cup->teams == 32) {
            $teams_table_sum = 0;
        }
        if($cup->teams == 64) {
            $teams_table_sum = 6;
        }


        if(!empty($cup->double_elimination) && $cup->teams == 16) {
            $teams_table_sum++;
        }
        if(!empty($cup->double_elimination) && $cup->teams == 32) {
            $teams_table_sum = 3;
        }
        if(!empty($cup->double_elimination) && $cup->teams == 64) {
            $teams_table_sum = 4;
        }

        $remove_last = 2;

        if($cup->teams == 16) {
            $remove_last = 3;
        }
        if($cup->teams == 32) {
            //$remove_last = 2;
        }

        if($cup->teams == 64) {
            $remove_last = -1;
        }

        if($cup->teams == 64 && !empty($cup->double_elimination)) {
            $remove_last = 2;
        }
        if($cup->teams == 32 && !empty($cup->double_elimination)) {
            $remove_last = 2;
        }


        if($cup->teams == 16 && $table_type == 1) {
            $last += 1;
        }
        if($cup->teams == 32 && $table_type == 1) {
            $last += 4;
        }
        if($cup->teams == 64 && $table_type == 1) {
            $last += 5;
        }

        if($cup->teams == 64 && $table_type == 0 && !empty($cup->double_elimination)) {
            $last -= 1;
        }

        if($cup->teams == 32 && $table_type == 0 && !empty($cup->double_elimination)) {
            $last -= 1;
        }



        $output .= '<div class="clear"></div><div class="subtree'.$table_type.'">';

        foreach($subtable AS $row=>$entities) {

            //Label
            $label_type = ($cup->double_elimination) ? $grid.'_double' : $grid;
            $label_text = ($table_type == 0 && $row >= ($last-$remove_last) ? ($row+$teams_table_sum) : $row);

            if($table_type == 1 && $row == 3 && $cup->teams == 8) {
                $label_text += 2;
            }
            if($table_type == 1 && $row == 5 && $cup->teams == 16) {
                $label_text += 2;
            }
            if($table_type == 1 && $row == 7 && $cup->teams == 32) {
                $label_text += 3;
            }
            if($table_type == 1 && $row == 9 && $cup->teams == 64) {
                $label_text += 3;
            }

            $label = $row_names[$label_type][$label_text];

            //$label .= '---'.$row.'-'.($last-$remove_last).'='.($row+$teams_table_sum);

            //$label .= '---'.$row.'-'.($last-$remove_last).'-'.($row+$teams_table_sum);

                if($row == '_3') {
                  $output .= '<div class="clear"></div>';
                }
                $output .= '<div class="row row-'.$row.'">';
                $output .= '<h2>'.($table_type == 1 && $row != ($last-3) ? 'Loser' : '').' '.$label.'</h2>';

                foreach($entities AS $key=>$entity) {
                  if($key % 2 != 0 || $row==$last) {
                    $class = ($last == $row) ? 'last' : '';
                    $output .= '<div class="tree-pair '.$class.'">';

                    // Add pair letter
                    if($row != $last) {
                        $output .= '<div class="pair_letter">'.$entity->pair.'</div>';
                    }
                    // Add bo3 icon & description
                    if(!empty($entity->bo3)) {
                        $output .= '<div class="pair_bo3" title="'.t('Best of 3. (Allowed to agree for Best of 1)').'"><img src="/public/img/bo3.png" alt="" /></div>';
                    }
                  }

                  $score = ($row != $last_score || $table_type == 1) ? '<div class="team-score">'.$entity->score.'</div>' : '';
                  $enemy = ($key % 2 != 0) ? $key+1 : $key-1;
                  $admin_link_next = ($cup->status == 1 && isset($table[$table_type][$row][$enemy]->label) && $table[$table_type][$row][$enemy]->label != '-' && $cup->admin && $entity->label != '-' && empty($entity->match_id)) ? '<a href="#" class="cup-add-result '.$entity->data.'"></a>' : '';
                  $admin_link_prev = ($cup->status == 1 && $cup->admin && $entity->label != '-' && empty($entity->match_id) && $row != 1 && $row != '_3') ? '<a href="#" title="'.t('Are you sure want to remove this pair?').'" class="cup-remove-result cup-'.$cup->id.' row-'.$row.' pos-'.$key.' confirm '.$entity->remove_data.'"></a>' : '';

                  // Don't allow to go back if there is free slot
                  if($row == 2 AND $entity->entity_id != 0) {
                    $row1_pos = db_result(db_query("SELECT `pos` FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `entity` = '".$entity->entity_id."' AND `row` = 1 AND `double` = '".$table_type."'"));
                    if($table_type) {
                        $row1_pos--;
                    } else {
                      $row1_pos++;
                    }
                    $checkFreeSlot = db_result(db_query("SELECT count(*) FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `entity` = 0 AND `row` = 1 AND `pos` = '".$row1_pos."' AND `double` = '".$table_type."'"));
                    if($checkFreeSlot) {
                      $admin_link_prev = '';
                    }
                  }

                  $output .= '<div class="team">'.$admin_link_prev.' '.$entity->label.' '.$score.' '.$admin_link_next.'</div>';
                  if($key % 2 == 0 || $row==$last) {
                    $output .= '</div>';
                  }
                }
                $output .= '</div>';


        }
        $output .= '</div>';
    }
    $output .= '</div></div>';
    return $output;
  }

  public function matches() {
    return $this->view('matches',array('cup'=>$this->cup));
  }

  public function room() {
    if(!$this->cup->participate && !$this->cup->admin) {
      $this->redirect('/');
    }
    $form = array(
      'text' => array(
        '#type' => 'textfield',
      ),
      'submit' => array(
        '#type' => 'submit',
        '#value' => t('Add'),
      ),
      'cup_id' => array(
        '#type' => 'hidden',
        '#value' => $this->cup->id,
      ),
      'room_entity' => array(
        '#type' => 'hidden',
        '#value' => User::$user->uid,
      ),
    );
    $form = render_form($form);
    return $this->view('room',array('cup'=>$this->cup,'form'=>$form));
  }
}