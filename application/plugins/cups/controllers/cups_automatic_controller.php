<?php
class CupsAutomaticController extends CupsController {
    private $api_key;
    private $version;

    function __construct() {
        $this->api_key = '96583bdc-513b-4ee4-9565-33e4d290e3b6'; //unlimited
        $this->version = 'v1.3';
    }

    private function SyncSummonerNames() {
        $api_key = 'bcaa7471-e051-49da-874d-1693b750f5a9';
        $api_version = 'v1.4';
        $summoners = db_query("SELECT * FROM `users_gameaccount` WHERE `unique_game_id` = '1337'  AND `not_found` = 0 LIMIT 20");
        while($summoner = db_fetch_object($summoners)) {
            $summonerId_url = 'http://prod.api.pvp.net/api/lol/'.$summoner->region.'/'.$api_version.'/summoner/by-name/'.rawurlencode($summoner->value).'?api_key='.$api_key;
            $summoner_response = @file_get_contents($summonerId_url);
            $summoner_response = end(json_decode($summoner_response));
            if(!empty($summoner_response->id)) {
                db_query("UPDATE `users_gameaccount` SET `unique_game_id` = '".$summoner_response->id."' WHERE `id` = '".$summoner->id."'");
            } else {
                db_query("UPDATE `users_gameaccount` SET `not_found` = 1 WHERE `id` = '".$summoner->id."'");
            }

        }

        print 'finished';


        die;
    }

    public function SyncResults() {

        $this->SyncSummonerNames();

        Log::debug('automatic_system','RIOT SYSTEM::STARTED');
        $activeTournaments = $this->getSyncData();
        if(!empty($activeTournaments)) {
            foreach($activeTournaments AS $tournament) {
                Log::debug('automatic_system','Tournament sync: '.$tournament->name);
                // Sync only if pairs are not empty
                if(!empty($tournament->pairs)) {
                    foreach($tournament->pairs AS $pair) {
                        $pair[0]->members = $this->getTeamMembers($pair[0]->entity);
                        $pair[1]->members = $this->getTeamMembers($pair[1]->entity);
                        // If we found a game in a RIOT history

                        if($history = $this->_checkMatchHistory($pair,$tournament)) {
                            print '<pre>'.print_r($history,true).'</pre>';
                            die('googog');
                            $this->_addMatchResult($history);
                        }

                        die;

                    }
                }
            }
        }
        Log::debug('automatic_system','RIOT SYSTEM::ENDED');
    }

    /**
     * Get active tournaments and pairs what we need to sync
     * match_id = 0
     * entity1 & entity2 != empty
     */
    private function getSyncData() {
        $tournaments = array();
        $getTournaments = db_query("SELECT `id`,`region`,`name`, `format` FROM `cups` WHERE `status` = 1 AND `id` = 196");
        while($getTournament = db_fetch_object($getTournaments)) {
            $tournament = $getTournament;
            // Get pairs
            $pairs = array();
            $pairs_i = 1;
            $getTree = db_query("SELECT * FROM `cups_tree` WHERE `cid` = '".$tournament->id."' AND `match_id` = 0 AND `entity` != 0 ORDER BY `row` ASC, `pos` ASC");
            while($tree = db_fetch_object($getTree)) {
                $tree->entity_name = db_result(db_query("SELECT `name` FROM `users_teams` WHERE `id` = '".$tree->entity."'"));


                if(!empty($last_tree) && $tree->pos == ($last_tree->pos+1) && ($tree->pos++%2==0)) {
                    $i = round($pairs_i/2,0);
                    $pairs[$i][] = $last_tree;
                    $pairs[$i][] = $tree;
                }

                $last_tree = $tree;
                $pairs_i++;
            }
            $tournament->pairs = $pairs;
            $tournaments[] = $tournament;
        }
        return $tournaments;
    }

    /**
     * Get team roster from gameaccounts
     */
    private function getTeamMembers($entity_id) {
        $members = array();
        $getMembers = db_query("SELECT users_gameaccount.unique_game_id, users_gameaccount.value, users_teams_members.role
                                FROM `users_teams_members`
                                INNER JOIN `users_gameaccount` ON users_gameaccount.id = users_teams_members.gid
                                WHERE users_teams_members.team = '".$entity_id."'
                                ORDER BY `role` ASC");
        while($getMember = db_fetch_object($getMembers)) {
            $members[] = $getMember;
        }
        return $members;
    }

    /**
     * Check if there is finished last game in history between team1 & team2
     */
    private function _checkMatchHistory($data,$cup) {

        Log::debug('automatic_system','_checkMatchHistory ['.$data[0]->entity_name.' vs '.$data[1]->entity_name.']');
        $all_team1_members = array();
        foreach($data[0]->members AS $member) {
            $all_team1_members[] = $member->unique_game_id;
        }
        $all_team2_members = array();
        foreach($data[0]->members AS $member) {
            $all_team2_members[] = $member->unique_game_id;
        }
        $captain_id = $data[0]->members[0]->unique_game_id;

        $url = 'https://prod.api.pvp.net/api/lol/'.$cup->region.'/'.$this->version.'/game/by-summoner/'.$captain_id.'/recent?api_key='.$this->api_key;
        $response = json_decode(file_get_contents($url));
        $last_game = $response->games[0];

        print '<pre>'.print_r($data,true).'</pre>';
        print '<pre>'.print_r($last_game,true).'</pre>';

        // If last game have been a CUSTOM GAME
        if(!empty($last_game) && $last_game->gameType == 'CUSTOM_GAME' && $last_game->subType == 'NONE') {

            Log::debug('automatic_system','gamefound');
            // Count how many players from team crew was at game
            $team1_members = 0;
            $team2_members = 0;
            foreach($last_game->fellowPlayers AS $fellowPlayer) {
                if(in_array($fellowPlayer->summonerId, $all_team1_members)) {
                    $team1_members++;
                }
            }
            foreach($last_game->fellowPlayers AS $fellowPlayer) {
                if(in_array($fellowPlayer->summonerId, $all_team2_members)) {
                    $team2_members++;
                }
            }
            /**
             * If have at least 2 members than from both teams
             */
            print $team1_members.'<br />'.$team2_members;
            die;
            if($team1_members >= 2 && $team2_members >= 2) {
                return array('game'=>$last_game,'cup'=>$cup,'data'=>$data);
            }
        }
        return false;
    }

    /**
     * Add match result
     */
    private function _addMatchResult($data) {
        $win = $data['game']->stats->win;
        $cup = $data['cup'];
        if($win) {
            $winner = $data['data'][0];
            $looser = $data['data'][1];
        }
        else {
            $winner = $data['data'][1];
            $looser = $data['data'][0];
        }

        // Get new place to go
        $new_pos = round($winner->pos/2);
        $new_row = $winner->row+1;


        $winner->p = 1;
        $looser->p = 0;
        $rank = new Ranking;
        $match_type = ($cup->type == 'players') ? 'user' : 'team';
        $R = $rank->add($winner->entity,$winner->p,$looser->entity,$looser->p,$cup->game,$match_type);
        $winner->rating = $R['P1'];
        $looser->rating = $R['P2'];
        db_query("INSERT INTO `matches` (`match_type_id`,`winner_entity`,`looser_entity`,`winner_score`,`looser_score`,`entity_type`,`match_type`,`created`,`uid`,`winner_rating`,`looser_rating`) VALUES ('".$cup->id."','".$winner->entity."','".$looser->entity."','".$winner->p."','".$looser->p."','".$match_type."','cup','".time()."','".User::$user->uid."','".$winner->rating."','".$looser->rating."')");
        $matchId = db_result(db_query("SELECT `id` FROM `matches` ORDER BY `id` DESC LIMIT 1"));
        db_query("UPDATE `cups_tree` SET `match_id` = '".$matchId."' WHERE `cid` = '".$cup->id."' AND (`pos` = '".$winner->pos."' AND `row` = '".$winner->row."' OR `pos` = '".$looser->pos."' AND `row` = '".$looser->row."')");

        if($data->row != '_3') {
            db_query("UPDATE `cups_tree` SET `entity` = '".$winner->entity."' WHERE `cid` = '".$cup->id."' AND `pos` = '".$new_pos."' AND `row` = '".$new_row."'");

        }

        if($cup->place3) {
            $total_rows = floor(log($cup->teams,2));
            $semifinal_row = $total_rows-1;
            if($data->row == $semifinal_row) {
                $pos = round($looser->pos/2,0);
                db_query("UPDATE `cups_tree` SET `entity` = '".$looser->entity."' WHERE `cid` = '".$cup->id."' AND `pos` = '".$pos."' AND `row` = '_3'");
            }
        }
    }

}