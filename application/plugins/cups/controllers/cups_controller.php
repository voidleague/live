<?php
use \ORM;
use ClassLib\Riot;
use ClassLib\Cups;

class CupsController extends AppController{
    public $cup;
    private $region;
    private $regions = [];

    function __construct() {
        $this->title = t('Cups');
        // Attach js
        $this->attached['js'][] = 'cups.js';
        $this->attached['css'][] = 'cups.css';
        $games = array();
        $getGames = db_query("SELECT * FROM ".PREFIX."speles ORDER BY `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $games[$getGame->tag] = $getGame;
        }
        $this->games = $games;

        $this->homepath = 'active';
        $this->links = array(
            'active' => t('Active'),
            'closed' => t('Closed'),
        );

        // Include mapping for double elimination
        require_once APP.'/plugins/cups/pair_mapping.php';
        $this->pair_mapping = $pair_mapping;

        // Regions - atkartojas ar FIND TEAMMATES funkciju no find_controller
        $getRegions = Cups::getRegions('LOL');
           $this->regions[''] = '-';
           foreach($getRegions AS $getRegion) {
                    $this->regions[$getRegion['value']] = $getRegion['name'];
           }
    }



    public function index() {
        // create games menu
        $games_menu = array('all'=>t('All'));
        $getGames = db_query("SELECT * FROM ".PREFIX."speles ORDER BY `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $games_menu[$getGame->tag] = '<img src="/public/images/games/icons/'.$getGame->tag.'.jpg" alt="" width="22px"/>'.$getGame->title;
            $games = $getGame;
        }
        $games_menu = render_links($games_menu,'game');
        $args = arg();
        if(isset($args[1]) && $args[1] == 'closed') {
            $cups_list = $this->cups_list_closed();
        }
        else {
            $cups_list = $this->cups_list();
        }
        if(!isset($this->pager)) {
            $this->pager = false;
        }
        $filterForm = $this->cupFilterForm();
        return $this->view('cups_list',[
                        'games_menu'=>$games_menu,
                        'cups_list'=>$cups_list,
                        'pager'=>$this->pager,
                        'filterForm'=> render_form($filterForm, false, [
                                    'method' => 'GET'
                                    ])
                        ]);
    }

    private function cupFilterFormFormats() {
        $formats = [
            '' => '-',
            '3on3' => '3on3',
            '5on5' => '5on5'
        ];
        return $formats;
    }

    private function cupFilterForm()
    {
        $query = get_query_parameters();
        $form = [
            'region' => [
                '#type' => 'select',
                '#title' => t('Region'),
                '#options' => $this->regions,
                '#default_value' => (!empty($query->region) ? $query->region : '')
            ],
            'format' => [
                '#type' => 'select',
                '#title' => t('Format'),
                '#options' => $this->cupFilterFormFormats(),
                '#default_value' => (!empty($query->format) ? $query->format : '')
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Search')
            ]

        ];
        return $form;
    }

    /**
     * @return string returns array with cup admins
     */
    private function cup_admins($cid) {
        $admins = array();
        $getAdmins = db_query("SELECT `uid` FROM `cups_admins` WHERE `cid` = '".intval($cid)."'");
        while($getAdmin = db_result($getAdmins)) {
            $admins[$getAdmin] = $getAdmin;
        }
        return $admins;
    }

    public function cups_list() {
        $query = get_query_parameters();
        $output = '';
        if(!empty($query->game) && !empty($this->games[$query->game])) { // 3 vienadi un atkartojas ieksh cups_list_closed - var saisinat?
            $game_filter = "AND `game` = '".$query->game."'";
        }
        else {
            $game_filter = '';
        }
        if(!empty($query->format)) {
            $game_format = " AND `format` = '".$query->format."'";
        }
        else {
            $game_format = '';
        }
        if(!empty($query->region)) {
            $game_region = " AND `region` = '".$query->region."'";
        }
        else {
            $game_region = '';
        }
        $getCups = db_query("SELECT cups.*,
        CONVERT_TZ(start_time, 'CET','".$_SESSION['user_timezone']."') AS start_time,
        CONVERT_TZ(checkin_date, 'CET','".$_SESSION['user_timezone']."') AS checkin_date,
        (SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = cups.id AND cups_teams.status = 1) AS teams_registered
                            FROM `cups`
                            WHERE `status` != 2 ".$game_filter." ".$game_region." ".$game_format." ORDER BY  `start_time` ASC");
        $col = 0;
        $count_cups = 0;
        while($cup = db_fetch_object($getCups)) {
            $col++;
            if($col % 2 == 0) $cup->col = 'odd'; else $cup->col = 'even';
            // Check if can join to cup
            $join = '';
            if($cup->type == 'teams') {
                $checkTeam = db_result(db_query("SELECT users_teams.id
                                    FROM `users_teams_members`
                                    JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                    WHERE users_teams_members.role IN(1,2) AND users_teams.game = '".$cup->game."' AND users_teams_members.uid = '".User::$user->uid."' AND users_teams.region = '".$cup->region."' AND users_teams.format = '".$cup->format."'"));
            }
            else {
                $checkTeam = User::$user->uid;
            }

            if($checkTeam && $cup->status == 0 && $cup->registration == 1) {
                // check if registered
                $checkRegistered = db_result(db_query("SELECT count(*) FROM `cups_teams` WHERE `entity_id` = '".$checkTeam."' AND `cid` = '".$cup->id."'"));
                if(!$checkRegistered) {
                    $join = '<a class="submit" href="/cups/id/'.$cup->slug.'-'.$cup->id.'?join=true">'.t('Join').'</a>';
                }
            }


            $cup->date = date('d.m.Y H:i',strtotime($cup->start_time));
            // calculate teams_registered procents
            $registered_procents = intval($cup->teams_registered/$cup->teams * 100);
            if(!empty($cup->uid) && User::$user->uid == $cup->uid || admin()) {
                $cup->admin = TRUE;
            }
            else {
                $cup->admin = FALSE;
            }
            if($cup->published || $cup->admin) {
                $cup_class = ($cup->published) ? 'published' : 'unpublished';
                $coins = ($cup->coins > 0) ? '<img src="/public/images/vip.gif" alt="" />' : '';

                $alert = '';
                if(user_access('administer '.$cup->game.' tournaments') && empty($cup->riot_event)) {
                    $alert = '<img src="/public/img/warning_icon.png" alt="" width="16px" />';
                }
                $output .= '<li '.($cup->id > 440 ? 'style="font-weight:bold !important;"' : '').' class="col-'.$cup->col.' '.$cup_class.'">
                <div class="cups-list-game"><img src="/public/images/games/icons/'.$cup->game.'.jpg" alt="" width="18px" /></div>
                <div class="cups-list-name">'.$alert.' '.$coins.' <a href="/cups/id/'.$cup->slug.'-'.$cup->id.'">'.$cup->name.'</a></div>
                <div class="cups-list-format">'.$cup->format.'</div>
                <div class="cups-list-teams"> <div class="teams-registered-bar"><div class="teams-registered-loading procents-'.$registered_procents.'" style="width:'.$registered_procents.'%;"></div></div><span class="teams-registered">'.$cup->teams_registered.'</span></div>
                <div class="cups-list-date">'.$cup->date.'</div>
                <div class="cups-list-join">'.$join.'</div>
                <div class="clear"></div>
                </li>';
                $count_cups++;
            }

        }
        if($count_cups == 0) {
            // empty
            $output .= '<li class="empty">'.t('There are no opened or active cups').'...</li>';
        }
        if(!empty($query->ajax)) {
            print $output;
            die;
        }
        else {
            return $output;
        }
    }


    public function cups_list_closed() {
        $query = get_query_parameters();
        $output = '';
        if(!empty($query->game) && !empty($this->games[$query->game])) {
            $game_filter = "AND `game` = '".$query->game."'";
        }
        else {
            $game_filter = '';
        }
        if(!empty($query->format)) {
            $game_format = " AND `format` = '".$query->format."'";
        }
        else {
            $game_format = '';
        }
        if(!empty($query->region)) {
            $game_region = " AND `region` = '".$query->region."'";
        }
        else {
            $game_region = '';
        }
        $getCupsCount = db_result(db_query("SELECT count(*)
                            FROM `cups`
                            WHERE `status` = 2 ".$game_filter." ".$game_region." ".$game_format." ORDER BY `status` ASC"));
        $pager = $this->pager($getCupsCount,15);
        $getCups = db_query("SELECT cups.*,
                CONVERT_TZ(start_time, 'CET','".$_SESSION['user_timezone']."') AS start_time,
                CONVERT_TZ(checkin_date, 'CET','".$_SESSION['user_timezone']."') AS checkin_date,
                (SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = cups.id AND cups_teams.status = 1) AS teams_registered
                            FROM `cups`
                            WHERE `status` = 2 ".$game_filter." ".$game_region." ".$game_format." ORDER BY `start_time` DESC LIMIT ".$this->pager->from.",".$this->pager->perPage."");

        $col = 0;
        while($cup = db_fetch_object($getCups)) {
            $col++;
            if($col % 2 == 0) $cup->col = 'odd'; else $cup->col = 'even';
            $cup->date = date('d.m.Y H:i',strtotime($cup->start_time));
            // calculate teams_registered procents
            $registered_procents = intval($cup->teams_registered/$cup->teams * 100);

            $coins = ($cup->coins > 0) ? '<img src="/public/images/vip.gif" alt="" />' : '';
            $output .= '<li class="col-'.$cup->col.'">
            <div class="cups-list-game"><img src="/public/images/games/icons/'.$cup->game.'.jpg" alt="" width="18px" /></div>
            <div class="cups-list-name">'.$coins.' <a href="/cups/id/'.$cup->slug.'-'.$cup->id.'">'.$cup->name.'</a></div>
            <div class="cups-list-format">'.$cup->format.'</div>
            <div class="cups-list-teams"> <div class="teams-registered-bar"><div class="teams-registered-loading procents-'.$registered_procents.'" style="width:'.$registered_procents.'%;"></div></div><span class="teams-registered">'.$cup->teams_registered.'</span></div>
            <div class="cups-list-date">'.$cup->date.'</div>
            <div class="clear"></div>
            </li>';
        }
        $this->pager = $pager;
        if($col == 0) {
            // empty
            $output .= '<li class="empty">'.t('There are no closed cups').'...</li>';
        }
        if(!empty($query->ajax)) {
            print $output;
            die;
        }
        else {
            return $output;
        }
    }

    public function id() {
        $routes = Routes::$routes;
        $cup = explode('-',$routes[2]);

        $sql = "SELECT cups.*, ".PREFIX."lietotaji.lietotajvards AS admin_username, a.name AS region_label, game_type,
                start_time AS default_start_time,
                CONVERT_TZ(start_time, 'CET','".$_SESSION['user_timezone']."') AS start_time,
                CONVERT_TZ(checkin_date, 'CET','".$_SESSION['user_timezone']."') AS checkin_date,
                                        (SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = cups.id AND cups_teams.status = 1) AS teams_registered
                                        FROM `cups`
                                        LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = cups.uid
                                        LEFT JOIN `gamesaccounts` a ON a.value = cups.region
                                        WHERE cups.id = '".end($cup)."'";

        $cup = db_query($sql);
        $cup = db_fetch_object($cup);

        $this->cup = $cup;
        if(empty($cup->id)) {
            $this->redirect('/');
        }


        $this->title = $cup->name;
        $query = get_query_parameters();

        if($cup->status == 0) {
            $cup_status = 'open';
        }
        else {
            $cup_status = 'active';
        }

        /**
         * Check if user & team participate into cup and add TRUE/FALSE
         */
        $cup->participate = FALSE;
        if($cup->type == 'players') {
            $partipicateEntity = User::$user->uid;
        }
        else {
            $partipicateEntity = ORM::for_table('users_teams_members')
                ->select('t.*')
                ->table_alias('m')
                ->join('users_teams', 't.id = m.team', 't')
                ->where('t.game', $cup->game)
                ->where('t.region', $cup->region)
                ->where('t.format', $cup->format)
                ->where('m.uid', User::$user->uid)
                ->where_in('m.role', [1, 2])
                ->where('t.deleted', 'N')
                ->find_one();
        }
        if(!empty($partipicateEntity)) {
            $participate = ORM::for_table('cups_teams')
                ->where('cid', $cup->id)
                ->where('entity_id', $partipicateEntity->id)
                ->where('status', 1)
                ->count();
            if ($participate) {
                $cup->participate = TRUE;
                $cup->participate_entity = $partipicateEntity;
            }
        }

        $cup->cup_admins = $this->cup_admins($cup->id);
        if(isset($cup->cup_admins[User::$user->uid]) || admin()) {
            $cup->admin = TRUE;
        }
        else {
            $cup->admin = FALSE;
        }
        /**
         * Close cup
         */

        // Check if has winner
        if($cup->admin) {
            if($cup->double_elimination) {
                $cup->haveWinner = db_result(db_query("SELECT COUNT(*) FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `pair` = '' AND `entity` != 0 LIMIT 1"));
            } else {
                $ddd = db_fetch_object(db_query("SELECT `entity` FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `row` != '_3' ORDER BY `row` DESC LIMIT 1"));
                if(!empty($ddd->entity)) {
                    $cup->haveWinner = 1;
                }
            }
        }
        $query = (object)get_query_parameters();
        if(isset($query->close_cup) && $query->close_cup == true && $cup->admin && $cup->status == 1) {
            $this->close_cup($cup);
        }

        $cupsClass = 'Cups'.ucfirst($cup_status).'Controller';
        include ROOT.'/application/plugins/cups/controllers/cups_'.$cup_status.'_controller.php';
        $cupsClass = new $cupsClass($this);
        $query = get_query_parameters();

        if(empty($query->tab)) {
            if($cup->status == 0) {
                $query->tab = 'info';
            }
            else {
                $query->tab = 'table';
            }
        }

        $output = $this->getInfo($cup);

        // Captains info update div
        if($query->tab == 'table' && $cup->participate) {
            $this->attached['css'][] = 'cups.css';
            $output .= $this->view('team_match_info', [
                'team' => $cup->participate_entity,
                'cup_id' => $cup->id
            ]);
        }

        if(isset($cupsClass->links)) {
            $output .= render_links($cupsClass->links);
        }
        if(method_exists($cupsClass,$query->tab)) {
            $output .= $cupsClass->{$query->tab}();
        }
        else {
            $output .= $this->{$query->tab}();
        }
        return $output;

    }

    public function getInfo($cup) {

        if($cup->status == 0) {
            $join = '';
            $checkin = false;
            if($cup->type == 'teams') {
                $checkTeam = db_result(db_query("SELECT users_teams.id
                                    FROM `users_teams_members`
                                    JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                    WHERE users_teams_members.role IN(1,2) AND users_teams.game = '".$cup->game."' AND users_teams_members.uid = '".User::$user->uid."' AND users_teams.region = '".$cup->region."' AND users_teams.format = '".$cup->format."'"));
            }
            else {
                $checkTeam = User::$user->uid;
            }

            if($checkTeam && $cup->only_invites == 'N') {
                // check if registered
                $checkRegistered = db_fetch_object(db_query("SELECT * FROM `cups_teams` WHERE `entity_id` = '".$checkTeam."' AND `cid` = '".$cup->id."'"));
                if(!isset($checkRegistered->entity_id)) {
                    $join = '<a class="submit" href="/cups/id/'.$cup->slug.'-'.$cup->id.'?join=true">'.t('Join').'</a>';
                }
                else {
                    if($checkRegistered->status == 0 && strtotime($cup->checkin_date) < time() && strtotime($checkRegistered->checkin) < strtotime($cup->checkin_date)) {
                        $checkin = '<a class="checkin-button submit submit-2" href="/cups/id/'.$cup->slug.'-'.$cup->id.'?checkin=true">'.t('Check-in').'</a>';
                    }
                    elseif($checkRegistered->status == 1 && strtotime($cup->checkin_date) < time() && strtotime($checkRegistered->checkin) < strtotime($cup->checkin_date)) {
                        $checkin = '<a class="checkin-button submit submit-2" href="/cups/id/'.$cup->slug.'-'.$cup->id.'?checkin=false">'.t('Cancel check-in').'</a>';
                    } else {
                        $checkin = '<a class="checkin-button submit submit-2 checkin-countdown" href="/cups/id/'.$cup->slug.'-'.$cup->id.'?checkin=true">'.$cup->checkin_date.'</a>';
                    }
                }
            }
            $cup->join = $join;
            $cup->checkin = $checkin;


            if($cup->admin) {
                $cup->edit_link = '<a class="cup-edit-link" href="/admin/cups/edit/'.$cup->id.'">'.t('Edit').'</a>';
            }

        }
        $CupAdmins = array();
        $cup->cup_admins = $this->cup_admins($cup->id);
        if(!empty($cup->cup_admins)) {
            $getAllAdmins = db_query("SELECT lietotajvards AS username FROM ".PREFIX."lietotaji WHERE `id` IN(".implode(',',$cup->cup_admins).")");
            while($getAllAdmin = db_result($getAllAdmins)) {
                $CupAdmins[] = getMember($getAllAdmin);
            }
            $cup->rendered_admins = implode(', ',$CupAdmins);
        } else {
            $cup->rendered_admins = t('none');
        }
        return $this->view('cup_info',$cup);
    }

    public function rules() {
        $rules = $this->get_rules($this->cup);
        return $this->view('rules',array('rules'=>$rules));
    }

    public function participants() {
        if($this->cup->type == 'teams') {
            $entities = db_query("SELECT DISTINCT users_teams.*, rank.*,users_teams.id AS team_id,
                                 (win/(win+lost+draw)) AS win_procents, (win+lost+draw) AS games_played
                                 FROM `cups_teams`
                                 JOIN `users_teams` ON users_teams.id = cups_teams.entity_id
                                 LEFT JOIN `rank` ON rank.entity_id = cups_teams.entity_id
                                 WHERE `cid` = '".$this->cup->id."' AND cups_teams.status = 1 AND rank.type = 'team' GROUP BY users_teams.id");
        }
        else {
            $entities = db_query("SELECT DISTINCT ".PREFIX."lietotaji.id AS uid, rank.*, ".PREFIX."lietotaji.lietotajvards AS username, rank.*,
                                 (win/(win+lost+draw)) AS win_procents, (win+lost+draw) AS games_played
                                 FROM `cups_teams`
                                 JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = cups_teams.entity_id
                                 LEFT JOIN `rank` ON rank.entity_id = cups_teams.entity_id
                                 WHERE `cid` = '".$this->cup->id."' AND cups_teams.status = 1 AND rank.type = 'user' GROUP BY `uid`");
        }

        $participants = array();
        $col = 0;
        while($entity = db_fetch_object($entities)) {
            $col++;
            if($col % 2 == 0) $entity->col = 'odd'; else $entity->col = 'even';
            if($entity->now > $entity->old) {
                $entity->rank_status = 'rank_down';
            }
            elseif($entity->now < $entity->old) {
                $entity->rank_status = 'rank_up';
            }
            else {
                $entity->rank_status = 'rank_neutral';
            }

            if($this->cup->type == 'teams') {
                $entity->label = '<img src="/public/images/flags/'.strtolower($entity->country).'.png" alt="" /> <a href="/team/id/'.$entity->slug.'-'.$entity->team_id.'">'.$entity->name.'</a>';
            }
            else {
                $entity->label = getMember($entity->username);
            }
            $entity->win_procents = round($entity->win_procents*100,2).'%';
            $participants[] = $entity;
        }
        return $this->view('participants',array('participants'=>$participants));
    }

    /**
     * Add result popup
     */
    public function add_result() {
        $query = get_query_parameters();
        if(empty($query->c) || empty($query->p) || empty($query->r) || !isset($query->double)) {
            die;
        }
        $cup = db_fetch_object(db_query("SELECT * FROM `cups` WHERE `id` = '".intval($query->c)."'"));
        $cup->cup_admins = $this->cup_admins($cup->id);
        if(isset($cup->cup_admins[User::$user->uid]) || admin()) {
            $cup->admin = TRUE;
        }
        else {
            $cup->admin = FALSE;
        }
        if(!$cup->admin) {
            die;
        }

        // Get entities data
        $winner = db_fetch_object(db_query("SELECT * FROM `cups_tree` WHERE `double` = '".(int)$query->double."' AND `cid` = '".$cup->id."' AND `pos` = '".intval($query->p)."' AND `row` = '".mysql_real_escape_string($query->r)."'"));
        if($winner->pos % 2 != 0) {
            $pos = $winner->pos+1;
        }
        else {
            $pos = $winner->pos-1;
        }
        $looser = db_fetch_object(db_query("SELECT * FROM `cups_tree` WHERE  `double` = '".(int)$query->double."' AND `cid` = '".$cup->id."' AND `row` = '".$winner->row."' AND `pos` = ".$pos." ORDER BY `row` ASC"));

        if($cup->type == 'players') {
            $winner_entity = db_result(db_query("SELECT `lietotajvards` FROM ".PREFIX."lietotaji WHERE `id` = '".$winner->entity."'"));
            $looser_entity = db_result(db_query("SELECT `lietotajvards` FROM ".PREFIX."lietotaji WHERE `id` = '".$looser->entity."'"));
            $winner_label = getMember($winner_entity);
            $looser_label = getMember($looser_entity);
        }
        else {
            $winner_entity = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$winner->entity."'"));
            $looser_entity = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$looser->entity."'"));
            $winner_label = '<a href="/team/id/'.$winner_entity->slug.'-'.$winner_entity->id.'">'.$winner_entity->name.'</a>';
            $looser_label = '<a href="/team/id/'.$looser_entity->slug.'-'.$looser_entity->id.'">'.$looser_entity->name.'</a>';
            // Teams entities
        }

        $output = '<div id="cup-add-result-box"><a href="#" class="close-cup-add-result-box"></a><form autocomplete="off" method="POST" id="cup-accept-result-form"><table class="no-style">';
        $output .= '<tr><td class="left">W:'.$winner_label.'</td><td><input type="text" class="result-input" name="winner_p" /></td></tr>';
        $output .= '<tr><td class="left">L:'.$looser_label.'</td><td><input type="text" class="result-input" name="looser_p" /></td></tr>';
        $output .= '<tr><td>
        <input type="hidden" value="'.$winner->pos.'" name="winner_pos"/>
        <input type="hidden" value="'.$looser->pos.'" name="looser_pos"/>
        <input type="hidden" value="'.$winner->row.'" name="row"/>
        <input type="hidden" value="'.(int)$query->double.'" name="double"/>
        <input type="hidden" value="'.$cup->id.'" name="cup_id"/>
        <input type="submit" class="cup-accept-result" value="'.t('Add').'" /></td><td><a href="#" class="close-add-result-box"><img src="/public/images/admin/no.png" alt=""></a></td></tr></table></form></div>';

        print $output;
        exit;
    }


    public function add_result_submit() {
        $data = $this->security($_POST);
        try {
            $this->add_result_submit_validate($data);
            $this->add_result_confirmed($data);
            print 'ok';
        }
        catch (Error $e) {
            print $e->getMessage();
        }
        exit;
    }


    private function add_result_submit_validate(&$data) {
        if(!isset($data->double) || empty($data->cup_id) || empty($data->winner_p) || !isset($data->looser_p) || empty($data->winner_pos) || empty($data->looser_pos) || empty($data->row)) {
            throw new Error(t('Some data is missing'));
        }
        $cup = db_fetch_object(db_query("SELECT * FROM `cups` WHERE `id` = '".intval($data->cup_id)."'"));
        $cup->cup_admins = $this->cup_admins($cup->id);
        if(isset($cup->cup_admins[User::$user->uid]) || admin()) {
            $cup->admin = TRUE;
        }
        else {
            $cup->admin = FALSE;
        }
        if(!$cup->admin) {
            throw new error(t('Permission denied'));
        }
        if($data->winner_p <= $data->looser_p) {
            //throw new Error(t('Winner need to be bigger score'));
        }

        // Get entities data

        $winner = db_fetch_object(db_query("SELECT * FROM `cups_tree` WHERE `double` = '".(int)$data->double."' AND `cid` = '".$cup->id."' AND `pos` = '".intval($data->winner_pos)."' AND `row` = '".mysql_real_escape_string($data->row)."'"));
        if($winner->pos % 2 != 0) {
            $pos = $winner->pos+1;
        }
        else {
            $pos = $winner->pos-1;
        }
        $looser = db_fetch_object(db_query("SELECT * FROM `cups_tree` WHERE `double` = '".(int)$data->double."' AND `cid` = '".$cup->id."' AND `match_id` = ''  AND `row` = '".$data->row."' AND `pos` = ".$pos." ORDER BY `row` ASC"));
        if(empty($winner) || empty($looser)) {
            throw new Error(t('Need both entities'));
        }
        $data->winner = $winner;
        $data->looser = $looser;
        $data->cup = $cup;

    }

    /**
     * Add result to database
     */

    private function add_result_confirmed($data) {
        $final = floor(log($data->cup->teams,2))+1;
        // Get new place to go
        $new_pos = round($data->winner->pos/2);
        $new_row = $data->winner->row+1;
        $match_type = ($data->cup->type == 'players') ? 'user' : 'team';

        if($data->cup->competitive == 'Y') {
            $rank = new Ranking;
            $R = $rank->add($data->winner->entity, $data->winner_p, $data->looser->entity, $data->looser_p, $data->cup->game, $match_type, false, $data->double);

            $data->winner->rating = $R['P1'];
            $data->looser->rating = $R['P2'];

        } else {
            $data->winner->rating = 0;
            $data->looser->rating = 0;
        }


        db_query("INSERT INTO `matches` (`match_type_id`,`winner_entity`,`looser_entity`,`winner_score`,`looser_score`,`entity_type`,`match_type`,`created`,`uid`,`winner_rating`,`looser_rating`) VALUES ('".$data->cup_id."','".$data->winner->entity."','".$data->looser->entity."','".$data->winner_p."','".$data->looser_p."','".$match_type."','cup','".time()."','".User::$user->uid."','".$data->winner->rating."','".$data->looser->rating."')");
        $matchId = db_result(db_query("SELECT `id` FROM `matches` ORDER BY `id` DESC LIMIT 1"));
        $data->match_id = $matchId;

        if(empty($data->cup->double_elimination)) {
            db_query("UPDATE `cups_tree` SET `match_id` = '".$matchId."' WHERE `cid` = '".$data->cup->id."' AND (`pos` = '".$data->winner->pos."' AND `row` = '".$data->winner->row."' OR `pos` = '".$data->looser->pos."' AND `row` = '".$data->looser->row."')");
            if($data->row != '_3') {
                db_query("UPDATE `cups_tree` SET `entity` = '".$data->winner->entity."' WHERE `cid` = '".$data->cup->id."' AND `pos` = '".$new_pos."' AND `row` = '".$new_row."'");
            }

            if($data->cup->place3) {
                $total_rows = floor(log($data->cup->teams,2));
                $semifinal_row = $total_rows-1;
                if($data->row == $semifinal_row) {
                    $pos = round($data->looser->pos/2,0);
                    $sql = "UPDATE `cups_tree` SET `entity` = '".$data->looser->entity."' WHERE `cid` = '".$data->cup->id."' AND `pos` = '".$pos."' AND `row` = '_3'";
                    db_query($sql);

                }
            }
        } else {
            // Double elimination tree update
            if(!empty($this->pair_mapping[$data->cup->teams])) {
                $this->addDoubleEliminationResult($data);
            } else {
                throw new Error(t('No mapping for this cup format'));
            }

        }
    }



    /**
     * Add double elimination result to database
     * Using ../pair_mapping.php system
     * $data - object of result data
     */
    private function addDoubleEliminationResult($data) {
        $mapping = $this->pair_mapping[$data->cup->teams];


        // Update match id
        $bracket = ORM::for_table('cups_tree')->where(['cid'=>$data->cup->id,'pair'=>$data->winner->pair])->find_many();
        foreach($bracket AS $brack) {
            $brack->match_id = $data->match_id;
            $brack->save();
        }

        // move further winner team
        if(isset($mapping[$data->winner->pair]['W'])) {
            // Final game
            if($mapping[$data->winner->pair]['W'] == 'WINNER') {

                $winner = ORM::for_table('cups_tree')->where([
                    'cid'=>$data->cup->id,
                    'pair'=>''
                ])->find_one();

            } else { // Random game

                $w_pair = substr($mapping[$data->winner->pair]['W'],0,-1);
                $desc = (substr($mapping[$data->winner->pair]['W'],-1) == 1) ? 'ASC' : 'DESC';

                $w_pos = ORM::for_table('cups_tree')->raw_query("SELECT `pos` FROM `cups_tree` WHERE `cid` = :cid AND `pair` = :pair ORDER BY `pos` $desc",[
                    'cid' => $data->cup->id,
                    'pair' => $w_pair
                ])->find_one();

                $winner = ORM::for_table('cups_tree')->where(['cid'=>$data->cup->id,'pair'=>$w_pair,'pos'=>$w_pos->pos])->find_one();
            }

            $winner->entity = $data->winner->entity;
            $winner->save();
        }

        // Move further loser team
        if(isset($mapping[$data->winner->pair]['L'])) {

            $l_pair = substr($mapping[$data->winner->pair]['L'],0,-1);
            $desc = (substr($mapping[$data->winner->pair]['L'],-1) == 1) ? 'ASC' : 'DESC';

            $l_pos = ORM::for_table('cups_tree')->raw_query("SELECT `pos` FROM `cups_tree` WHERE `cid` = :cid AND `pair` = :pair ORDER BY `pos` $desc",[
                'cid' => $data->cup->id,
                'pair' => $l_pair
            ])->find_one();

            $loser = ORM::for_table('cups_tree')->where([
                'cid' => $data->cup->id,
                'pair' => $l_pair,
                'pos' => $l_pos->pos
            ])->find_one();
            $loser->entity = $data->looser->entity;
            $loser->save();

            // Check if have enemy pair teams (Only for row 1)
            if($data->row == 1 && empty($data->double)) {
                $enemyPair = $mapping[$data->winner->pair]['enemy_pair'];
                $freePair = ORM::for_table('cups_tree')
                            ->where([
                                'entity' => 0,
                                'cid' => $data->cup->id,
                                'pair' => $enemyPair
                            ])->count();

                if($freePair) { // Enemy pair didn't had both teams

                    $next_pair = substr($mapping[substr($mapping[$data->winner->pair]['L'],0,-1)]['W'],0,-1);
                    $next_pos = substr($mapping[substr($mapping[$data->winner->pair]['L'],0,-1)]['W'],-1);
                    $next_pos = ($next_pos % 2 != 0) ? 1 : 0;

                    ORM::raw_execute("UPDATE `cups_tree` SET `entity` = :entity WHERE `cid` = :cid AND `pair` = :pair AND `pos`%2=:pos",[
                            'entity' => $data->looser->entity,
                            'cid' => $data->cup->id,
                            'pair' => $next_pair,
                            'pos' => $next_pos
                        ]);

                }
            }

            // Check if have enemy pair teams (Only for row 2)
            if($data->row == 2 && empty($data->double)) {
                $prev_pair = substr($mapping[$data->winner->pair]['L'],0,-1).'2';
                $prev_pair = $this->previous_pair($data->cup->teams,$prev_pair);

                // get more previous pairs and check if have at least 3 teams
                $prevprev1 = $this->previous_pair($data->cup->teams, $prev_pair.'1','L');
                $prevprev2 = $this->previous_pair($data->cup->teams, $prev_pair.'2','L');

                $freePair = ORM::for_table('cups_tree')
                            ->where([
                                'entity'=> 0,
                                'cid' => $data->cup->id,
                            ])
                            ->where_in('pair',[$prevprev1,$prevprev2])
                            ->count();

                if($freePair >= 2) {
                    $next_pair = substr($mapping[$l_pair]['W'],0,-1);
                    $next_pos = (substr($mapping[$l_pair]['W'],-1) % 2 != 0) ? 1 : 0;


                    ORM::raw_execute("UPDATE `cups_tree` SET `entity` = :entity WHERE `cid` = :cid AND `pair` = :pair AND `pos`%2=:pos",[
                        'entity' => $data->looser->entity,
                        'cid' => $data->cup->id,
                        'pair' => $next_pair,
                        'pos' => $next_pos
                    ]);
                }

            }

        }

    }


    /**
     * Remove result
     */

    public function remove_result_submit() {
        $data = $this->security($_POST);
        try {
            $this->remove_result_submit_validate($data);
            $this->remove_result_confirmed($data);
            print 'ok';
        }
        catch (Error $e) {
            print $e->getMessage();
        }
        exit;
    }

    private function remove_result_submit_validate(&$data) {
        if(empty($data->cup_id) || empty($data->row) || empty($data->pos)) {
            throw new Error(t('Some data is missing'));
        }
        $cup = db_fetch_object(db_query("SELECT * FROM `cups` WHERE `id` = '".intval($data->cup_id)."'"));
        $cup->cup_admins = $this->cup_admins($cup->id);
        if(isset($cup->cup_admins[User::$user->uid]) || admin()) {
            $cup->admin = TRUE;
        }
        else {
            $cup->admin = FALSE;
        }
        if(!$cup->admin) {
            throw new error(t('Permission denied'));
        }

        if($data->row == 1) {
            throw new Error(t('This is first row'));
        }
        $data->cup = $cup;

        $data->row = $data->row-1;
        $data->old_pos = $data->pos;
        $data->pos = round($data->pos*2,0);
    }

    private function remove_result_confirmed($data) {
        if(empty($data->cup->double_elimination)) {
            $data->enemy_pos = ($data->pos % 2 != 0) ? $data->pos+1 : $data->pos-1;
            $data->enemy_old_pos = ($data->old_pos % 2 != 0) ? $data->old_pos+1 : $data->old_pos-1;
            $data->match_id = db_result(db_query("SELECT `match_id` FROM `cups_tree` WHERE `cid` = '".$data->cup_id."' AND `row` = '".$data->row."' AND `pos` = '".$data->pos."'"));
            $data->enemy = db_result(db_query("SELECT `entity` FROM `cups_tree` WHERE `cid` = '".$data->cup_id."' AND `row` = '".$data->row."' AND `pos` = '".$data->enemy_pos."'"));
            $data->entity = db_result(db_query("SELECT `entity` FROM `cups_tree` WHERE `cid` = '".$data->cup_id."' AND `row` = '".$data->row."' AND `pos` = '".$data->pos."'"));


            // Remove from 3/4 bracket
            if($data->cup->place3) {
                $total_rows = floor(log($data->cup->teams,2));
                $semifinal_row = $total_rows-1;

                if($data->row == $semifinal_row) {
                    $match_id = db_result(db_query("SELECT `match_id` FROM `cups_tree` WHERE `cid` = '".$data->cup->id."' AND `entity` = '".$data->entity."' AND `row` = '_3'"));
                    if(!empty($match_id)) {
                        $match = db_fetch_object(db_query("SELECT * FROM `matches` WHERE `id` = '".$match_id."'"));
                        $type = ($data->cup->type == 'teams') ? 'team' : 'user';
                        // Remove ELO
                        if($data->cup->competitive == 'Y') {
                            $rank = new Ranking;
                            $rank->remove($match->winner_entity, $match->winner_rating, $match->looser_entity, $match->looser_rating, $data->cup->game, $type);
                        }
                        // delete match
                        db_query("DELETE FROM `matches` WHERE `id` = '".$match_id."'");
                        db_query("DELETE FROM `rank_p10` WHERE `entity_id` = '".$data->winner_entity."' AND `type` = '".$type."' ORDER BY `id` DESC LIMIT 1");
                        db_query("DELETE FROM `rank_p10` WHERE `entity_id` = '".$data->looser_entity."' AND `type` = '".$type."' ORDER BY `id` DESC LIMIT 1");
                        db_query("UPDATE `cups_tree` SET `match_id` = 0 WHERE `row` = '_3'");

                    }
                    db_query("UPDATE `cups_tree` SET `entity` = '0', `match_id` = 0 WHERE `cid` = '".$data->cup->id."' AND `entity` = '".$data->entity."' AND `row` = '_3'");
                }
            }


            if(!empty($data->match_id) && !empty($data->enemy)) {
                $data->match = db_fetch_object(db_query("SELECT * FROM `matches` WHERE `id` = '".$data->match_id."'"));
                if(!empty($data->match)) {
                    $type = ($data->cup->type == 'teams') ? 'team' : 'user';
                    // Remove ELO
                    if($data->cup->competitive == 'Y') {
                        $rank = new Ranking;
                        $rank->remove($data->match->winner_entity, $data->match->winner_rating, $data->match->looser_entity, $data->match->looser_rating, $data->cup->game, $type);
                    }
                    // delete match
                    db_query("DELETE FROM `matches` WHERE `id` = '".$data->match_id."'");
                    db_query("DELETE FROM `rank_p10` WHERE `entity_id` = '".$data->match->winner_entity."' AND `type` = '".$type."' ORDER BY `id` DESC LIMIT 1");
                    db_query("DELETE FROM `rank_p10` WHERE `entity_id` = '".$data->match->looser_entity."' AND `type` = '".$type."' ORDER BY `id` DESC LIMIT 1");
                }
            }


            // Remove from tree
            db_query("UPDATE `cups_tree` SET `match_id` = 0 WHERE `match_id` = '".$data->match_id."'");
            $data->row = $data->row+1; //++
            db_query("UPDATE `cups_tree` SET `entity` = 0, `match_id` = 0 WHERE `cid` = '".$data->cup_id."' AND `row` = '".$data->row."' AND `pos` = ".$data->old_pos);

        } else {

            $this->removeDoubleEliminationResult($data);

        }
    }

    /**
     * Remove pair result
     *
     * @param $data
     * @param $match
     */
    private function removePairResult($data, $match) {

        // Remove current pair rating & delete match and reset brackets
        if($data->cup->competitive == 'Y') {
            $rank = new Ranking;
            $rank->remove($data->match->winner_entity, $data->match->winner_rating, $data->match->looser_entity, $data->match->looser_rating, $data->cup->game, $data->type);
        }
        $matches = ORM::for_table('cups_tree')
            ->where([
                'cid' => $data->cup_id,
                'match_id' => $data->match->id
            ])->find_many();
        foreach ($matches AS $m) {
            $m->match_id = 0;
            $m->save();
        }
        $match->delete();
    }
    /**
     * Add double elimination result to database
     * Using ../pair_mapping.php system
     * $data - object of result data
     */
    private function removeDoubleEliminationResult($data) {
        $current_pair = $data->pair . ($data->old_pos % 2 != 0 ? 1 : 2);
        if(empty($data->pair)) {
            $current_pair = 'WINNER';
        }

        $data->type = ($data->cup->type == 'teams') ? 'team' : 'user';

        $prev_pair = $this->find_mapping($data->cup->teams,$current_pair);
        $prev_pair_key = current(array_keys($prev_pair));
        $prev_pair = current($prev_pair);

        if(!empty($prev_pair['W'])) {

            // Remove current team for bracket
            $bracket = ORM::for_table('cups_tree')
                ->where([
                    'cid' => $data->cup_id,
                    'pair' => $data->pair,
                    'pos' => $data->old_pos
                ])->find_one();

            $bracket->entity = 0;
            $bracket->save();

            // Remove last pair match
            $pair_match = ORM::for_table('cups_tree')
                     ->where([
                         'cid' => $data->cup->id,
                         'pair' => $prev_pair_key
                     ])->find_one();
            $data->match_id = $pair_match->match_id;

            $match = ORM::for_table('matches')->find_one($data->match_id);
            $data->match = $match;
            if(!empty($match)) {

                $this->removePairResult($data, $match);

                // Remove pair result from loser brackets
                if (!empty($prev_pair['L'])) {

                    $loser_pair = substr($prev_pair['L'], 0, -1);
                    $loser_pos = (substr($prev_pair['L'], 1, 1) % 2 != 0 ? 1 : 0);

                    $pair_match = ORM::for_table('cups_tree')
                                  ->where([
                                      'cid' => $data->cup_id,
                                      'pair' => $loser_pair
                                  ])->find_one();

                    if(!empty($pair_match->match_id)) {
                        $data->match_id = $match->id;

                        $match = ORM::for_table('matches')->find_one($data->match_id);

                        $data->match = $match;
                        $this->removePairResult($data, $match);
                    }

                    ORM::raw_execute("UPDATE `cups_tree` SET `entity` = 0 WHERE `cid` = :cid AND `pair` = :pair AND `pos`%2=:pos",[
                        'cid' => $data->cup->id,
                        'pair' => $loser_pair,
                        'pos' => $loser_pos
                    ]);

                    $bracket->entity = 0;
                    $bracket->save();

                    // Remove next loser pair
                    $next_loser_pair = $this->pair_mapping[$data->cup->teams][$loser_pair]['W'];
                    $next_loser_pair = substr($next_loser_pair,0,-1);
                    $next_loser_pos = (substr($next_loser_pair, 1, 1) % 2 != 0 ? 0 : 1);
                    
                    ORM::raw_execute("UPDATE `cups_tree` SET `entity` = 0 WHERE `cid` = :cid AND `pair` = :pair AND `pos`%2=:pos",[
                        'cid' => $data->cup->id,
                        'pair' => $next_loser_pair,
                        'pos' => $next_loser_pos
                    ]);


                }
            }
        }

    }


    private function find_mapping($teams,$pair) {
        if(!empty($this->pair_mapping[$teams])) {
            foreach($this->pair_mapping[$teams] AS $mapping_key => $mapping) {
                if((!empty($mapping['W']) && $mapping['W'] == $pair) || (!empty($mapping['L']) && $mapping['L'] == $pair)) {
                    return [$mapping_key=>$this->pair_mapping[$teams][$mapping_key]];
                }
            }
        }
        return [];
    }

    private function close_cup($cup) {
        if(empty($cup->double_elimination)) {
            $entity = db_result(db_query("SELECT `entity` FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `row` != '_3' ORDER BY `row` DESC LIMIT 1"));
        } else {
            $entity = db_result(db_query("SELECT `entity` FROM `cups_tree` WHERE `cid` = '".$cup->id."' AND `pair` = ''"));
        }
        if($entity) {
            $bundle = ($cup->type == 'teams') ? 'team' : 'user';
            $src = 'cups/id/'.$cup->slug.'-'.$cup->id;
            $this->addTrophy($entity, $bundle, 'cup', $cup->game, $src, $cup->name);
            db_query("UPDATE `cups` SET `status` = 2 WHERE `id` = '".$cup->id."'");

            if($cup->competitive == 'Y') {
                $rank = new Ranking;
                $rank->_reorder('LOL', 'team', $cup->region, $cup->format);
            }

            $this->setMessage(t('Cup successfully closed'));
            $this->redirect('cups');
        }
    }

    private function addTrophy($entity, $bundle, $type, $game,$src,$title) {
        $title = mysql_real_escape_string($title);
        db_query("INSERT INTO `trophies` (`entity`,`bundle`,`type`,`game`,`src`,`title`) VALUES ('".$entity."','".$bundle."', '".$type."', '".$game."','".$src."','".$title."')");
    }

    public function cups_closed_list() {
        $query = get_query_parameters();
        $output = '';
        if(!empty($query->tab) && !empty($this->games[$query->tab])) {
            $game_filter = "AND `game` = '".$query->tab."'";
        }
        else {
            $game_filter = '';
        }
        $getCups = db_query("SELECT cups.*, (SELECT COUNT(*) FROM `cups_teams` WHERE cups_teams.cid = cups.id AND cups_teams.status = 1) AS teams_registered
                            FROM `cups`
                            WHERE `status` = 2 ".$game_filter." ORDER BY `status` ASC");
        $col = 0;
        while($cup = db_fetch_object($getCups)) {
            $col++;
            if($col % 2 == 0) $cup->col = 'odd'; else $cup->col = 'even';
            $cup->date = date('d.m.Y H:i',strtotime($cup->start_time));
            // calculate teams_registered procents
            $registered_procents = intval($cup->teams_registered/$cup->teams * 100);

            $coins = ($cup->coins > 0) ? '<img src="/public/images/vip.gif" alt="" />' : '';
            $output .= '<li class="col-'.$cup->col.'">
            <div class="cups-list-game"><img src="/public/images/games/icons/'.$cup->game.'.jpg" alt="" width="18px" /></div>
            <div class="cups-list-name">'.$coins.' <a href="/cups/id/'.$cup->slug.'-'.$cup->id.'">'.$cup->name.'</a></div>
            <div class="cups-list-format">'.$cup->format.'</div>
            <div class="cups-list-teams"> <div class="teams-registered-bar"><div class="teams-registered-loading procents-'.$registered_procents.'" style="width:'.$registered_procents.'%;"></div></div><span class="teams-registered">'.$cup->teams_registered.'</span>/<span class="teams-total">'.$cup->teams.'</span></div>
            <div class="cups-list-date">'.$cup->date.'</div>
            <div class="clear"></div>
            </li>';
        }
        if($col == 0) {
            // empty
            print '<li class="empty">'.t('There are no closed cups').'...</li>';
        }
        if(!empty($query->ajax)) {
            print $output;
            die;
        }
        else {
            return $output;
        }
    }


    public function cups_auto() {
        include ROOT.'/application/plugins/cups/controllers/cups_automatic_controller.php';
        $cupsClass = new CupsAutomaticController;
        $cupsClass->SyncResults();
        die;
    }

    /**
     *  get mapping previous pair
     */
    private function previous_pair($teams, $pair, $type='W') {
        foreach($this->pair_mapping[$teams] AS $prev_pair => $mapping) {
            if(!empty($mapping[$type]) && $mapping[$type] == $pair) {
                return $prev_pair;
            }
        }
        return false;
    }

    protected function get_rules($cup) {

        if(!empty($cup->custom_rules)) {
            return $cup->custom_rules;
        }

        $rules = ORM::for_table('cups_rules_templates')->where(['game_type'=>$cup->game_type,'game'=>13])->find_one();
        // replacements
        $rules = preg_replace('/%tournament_date/',date('d.m.Y',strtotime($cup->start_time)),$rules->rules);
        $rules = preg_replace('/%tournament_time/',date('H:i',strtotime($cup->start_time)),$rules);
        $rules = preg_replace('/%tournament_default_time/',date('H:i',strtotime($cup->default_start_time)),$rules);
        $rules = preg_replace('/%tournament_datetime/',date('d.m.Y H:i',strtotime($cup->start_time)),$rules);
        $rules = preg_replace('/%region/',$cup->region_label,$rules);
        $rules = preg_replace('/%timezone/',$cup->timezone,$rules);
        return $rules;
    }

    public function load($cup_id) {
        return ORM::for_table('cups')->find_one($cup_id);
    }

    public function getTeamInfo() {

        $params = get_query_parameters();
        if(empty($params->team_id) || empty($params->cup_id) || empty($params->hash)) {
            die;
        }
        $hash = md5('void'.$params->cup_id.'_'.$params->team_id);
        if($params->hash != $hash) {
            die;
        }
        $teamPair = ORM::for_table('cups_tree')
            ->where('cid', $params->cup_id)
            ->where('entity', $params->team_id)
            ->order_by_desc('row')
            ->find_one()->pair;

        $enemyTeam = ORM::for_table('cups_tree')
            ->table_alias('t')
            ->join('users_teams', 'ut.id = t.entity', 'ut')
            ->where('t.pair', $teamPair)
            ->where_not_equal('t.entity', $params->team_id)
            ->where('t.cid', $params->cup_id)
            ->find_one();

        if(empty($enemyTeam)) {
            $enemyTeam = [
                'name' => '(wait for next enemy)',
            ];
            $startTime = '-';
            $enemyCaptain = '-';
        } else {
            // Get nemy team captain
            $enemyCaptain = ORM::for_table('users_teams_members')
                ->select('g.value')
                ->table_alias('m')
                ->join('users_gameaccount', 'g.id = m.gid', 'g')
                ->where('m.role', 1)
                ->where('m.status', 1)
                ->where('m.team', $enemyTeam['entity'])
                ->find_one()->value;

            if($enemyTeam['row'] == 1) {
                $cup = $this->load($params->cup_id);
                $cup->start_time = ClassLib\Helper::converted_datetime($cup->start_time);
                $startTime = date('H:i', strtotime($cup->start_time) + 600).'-'.date('H:i', strtotime($cup->start_time) + 1200);
            } else {
                $lastMatch = ORM::for_table('matches')
                    ->where_in('winner_entity', [$params->team_id, $enemyTeam['entity']])
                    ->order_by_desc('created')
                    ->limit(1)
                    ->find_one();

                $starTime = ClassLib\Helper::converted_datetime(date('Y-m-d H:i:s',$lastMatch->created));
                $startTime = date('H:i', strtotime($starTime) + 600).'-'.date('H:i', strtotime($starTime) + 1200);
            }

        }

        $return  = [
            'enemy_team' => $enemyTeam['name'],
            'enemy_captain' => $enemyCaptain,
            'pair' => $teamPair,
            'start_time' => $startTime
        ];

        $new_data = md5(serialize($return));
        if(!empty($_SESSION['cup'.$params->cup_id]) && $new_data != $_SESSION['cup'.$params->cup_id] && $params->ft == 0 || $params->ft == 1) {
            $_SESSION['cup'.$params->cup_id] = $new_data;

            // Add notification
            if($params->ft == 0) {
                $return['sound_alert'] = 1;
            }
            echo json_encode($return);
        }
        die;
    }

    public function gameFinished() {
        $team_id = 5809;
        $enemy_team_id = 5806;
        $cup_id = 261;

        $processed =  Cups::processMatch($team_id, $enemy_team_id, $cup_id);
    }


}
