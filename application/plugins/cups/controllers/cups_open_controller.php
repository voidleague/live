<?php
use ClassLib\Cups;

class CupsOpenController extends CupsController {
    public $cup;
    function __construct($cup) {
        $this->cup = $cup->cup;
    }

    protected function checkin($cid, $entity_id) {
        $sql = "UPDATE `cups_teams` SET `checkin` = NOW(), `status` = 1 WHERE `cid` = '" . $cid . "' AND `entity_id` = '" . $entity_id . "'";
        db_query($sql);
    }

    public function info() {
        $query = get_query_parameters();
        $cup = $this->cup;

        // Join to cup
        if(isset($query->join) || isset($query->checkin)) {
            if($cup->type == 'teams') {
                $checkTeam = db_result(db_query("SELECT users_teams.id
                                    FROM `users_teams_members`
                                    JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                    WHERE users_teams_members.role IN(1,2) AND users_teams.game = '".$cup->game."' AND users_teams_members.uid = '".User::$user->uid."' AND users_teams.region = '".$cup->region."' AND users_teams.format = '".$cup->format."'"));
            }
            else {
                $checkTeam = User::$user->uid;
            }
            

            
            if($checkTeam && $cup->registration == 1) {
                // check if registered
                $checkRegistered = db_result(db_query("SELECT count(*) FROM `cups_teams` WHERE `entity_id` = '".$checkTeam."' AND `cid` = '".$cup->id."'"));
                
                if(isset($query->join) && $query->join == 'true') {
                    if(!$checkRegistered) {
                        if(User::$user->coins < $cup->coins) {
                            $this->setError(t('You need more coins'));
                        }
                        elseif($cup->only_invites == 'Y' && md5('cup'.$cup->id) != $query->invite) {
                            $this->setError(t('Only with invites'));
                        }
                        else {
                            $this->_register($this->cup,$checkTeam);
                            // Auto checkin on invite
                            if($cup->only_invites == 'Y') {
                                $this->checkin($cup->id, $checkTeam);
                            }
                            $this->setMessage(t('You have successfully registered to cup'));
                            $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);

                        }
                    }
                    else {
                        $this->setError(t('You have already registered to this cup'));
                    }
                }
                elseif(isset($query->join) && $query->join == 'false') {
                    if($checkRegistered) {
                        $this->_unregister($this->cup,$checkTeam);
                    }
                }
                if(isset($query->checkin) && strtotime($cup->checkin_date) < time()) {
                    if($checkRegistered) {
                        if($query->checkin == 'true') {
                            if($cup->teams_registered < $cup->teams) {
                                $this->checkin($cup->id, $checkTeam);
                                $this->setMessage(t('You have successfully checked in!'));
                            } else {
                                $this->setError(t('Teams limit reached! Try next tournament'));
                            }

                        } else {
                            db_query("UPDATE `cups_teams` SET `checkin` = NOW(), `status` = 0 WHERE `cid` = '".$cup->id."' AND `entity_id` = '".$checkTeam."'");
                            $this->setMessage(t('You have successfully unchecked!'));
                        }
                        $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);
                    }
                }
            }
            else {
                $this->setError(t('You can not register to this cup'));
            }
        }
        
        if(isset($query->decline) && $cup->admin) {
            db_query("UPDATE `cups_teams` SET `status` = 0 WHERE `entity_id` = '".intval($query->decline)."' AND `cid` = '".$cup->id."'");
            $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);
        }
        if(isset($query->accept) && $cup->admin) {
            db_query("UPDATE `cups_teams` SET `status` = 1 WHERE `entity_id` = '".intval($query->accept)."' AND `cid` = '".$cup->id."'");
            $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);
        }
        
        // get registered & accepted entities
        $registered = array();
        $accepted = array();
        $registered_count = 0;
        $accepted_count = 0;
        $entities = db_query("SELECT * FROM `cups_teams` WHERE `cid` = '".$cup->id."'");
        while($entity = db_fetch_object($entities)) {
            $admin_link = '';
            if($entity->status == 0) {

                if(strtotime($cup->checkin_date) < time() && strtotime($entity->checkin) < strtotime($cup->checkin_date)) {
                    $class = 'need-checkin'; 
                }
                elseif(strtotime($cup->checkin_date) < time()) {
                    $class = 'checked-in';
                }
                else {
                    $class = '';
                }

                if($cup->admin && $cup->teams > $cup->teams_registered) {
                    $admin_link = '<a href="?accept='.$entity->entity_id.'"><img src="/public/images/admin/ok.png" alt="" /></a>';
                }
                if($entity->entity_type == 'team') {
                    $team = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `status` = 1 AND `id` = '".$entity->entity_id."'"));
                    if(isset($team->id)) {
                    $registered_count++;
                    $registered[] = '<img src="/public/images/flags/'.$team->country.'.png" alt="" /> <a class="'.$class.'" href="/team/id/'.$team->slug.'-'.$team->id.'">'.$team->name.'</a> '.$admin_link;
                    }
                }
                else {
                    $registered_count++;
                    $user = db_fetch_object(db_query("SELECT lietotajvards, `country` FROM ".PREFIX."lietotaji WHERE `id` = '".$entity->entity_id."'"));
                    //$registered[] = getMember($user->lietotajvards).' '.$admin_link;
                    $registered[] = '<img src="/public/images/flags/'.strtolower($user->country).'.png" alt="" /> <a href="/user/'.$user->lietotajvards.'/" class="'.$class.'">'.$user->lietotajvards.'</a> '.$admin_link;
                }
            }
            else {
                $accepted_count++;
                if($cup->admin) {
                    $admin_link = '<a href="?decline='.$entity->entity_id.'"><img src="/public/images/admin/no.png" alt="" /></a>';
                }
                if($entity->entity_type == 'team') {
                    $team = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$entity->entity_id."'"));

                    $accepted[] = '<img src="/public/images/flags/'.$team->country.'.png" alt="" /> <a href="/team/id/'.$team->slug.'-'.$team->id.'">'.$team->name.'</a> '.$admin_link;
                }
                else {
                    $user = db_fetch_object(db_query("SELECT lietotajvards,`country` FROM ".PREFIX."lietotaji WHERE `id` = '".$entity->entity_id."'"));
                    $accepted[] = '<img src="/public/images/flags/'.strtolower($user->country).'.png" alt="" /> '.getMember($user->lietotajvards).' '.$admin_link;
                }
            }
        }
        if(isset($query->start) && $query->start == 'true' && $cup->admin && $cup->teams_registered <= $cup->teams) {
            $this->_startcup($cup);
        }
        
        if($cup->admin && $cup->teams_registered > ($cup->teams/2) && $cup->teams >= $cup->teams_registered) {
            $this->setMessage('<a href="?start=true" class="confirm" title="'.t('Are you really want to start cup?').'">'.t('Start cup').'</a>');
        }
        $rules = $this->get_rules($cup);


        $timeleft = strtotime($cup->start_time)-time();
        $cup->timeleft = time_left($timeleft,true);
        if($cup->timeleft < 0) {
            $cup->timeleft = 'some moment';
        }
        return $this->view('info',array('registered_count'=>$registered_count,'accepted_count'=>$accepted_count,'registered'=>$registered,'accepted'=>$accepted,'rules'=>$rules,'cup'=>(array)$cup));
    }
    
    private function _register($cup,$entity_id) {
        $entity_type = ($cup->type == 'teams') ? 'team' : 'player';
        if($entity_type == 'team') {

            $count_players = substr($cup->format,0,1);
            $members_count = db_result(db_query("SELECT count(*) FROM `users_teams_members` WHERE `status` = 1 AND `team` = '".$entity_id."'"));
            $team = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$entity_id."' AND `region` = '".$cup->region."' AND `format` = '".$cup->format."'"));

            if($count_players > $members_count && User::$user->uid != 1) {
                $this->setError(t('You need at least !count members in your team',array('!count'=>$count_players)));
                return false;
            }
            // Check if team has region
            $regions = db_result(db_query("SELECT count(*) FROM `gamesaccounts` WHERE `game` = '".$cup->game."'"));
            if($regions > 0 && empty($team->region)) {
                $this->setError(t('You need to add team region. Please go to <a href="/myteam/edit/'.$entity_id.'">myteam edit page</a> and edit it.'));
                return false;
            }
            if(empty($team->format)) {
                $this->setError(t('You need to add team format. Please go to <a href="/myteam/edit/'.$entity_id.'">myteam edit page</a> and edit it.'));
                return false;
            }

            if(!empty($cup->region) && $cup->region != $team->region) {
                $this->setError(t('You do not have team in this region!'));
                return false;
            }
            if($cup->format != $team->format) {
                $this->setError(t('You do not have team in this format!'));
                return false;
            }
        }

        db_query("INSERT INTO `cups_teams` (`cid`,`entity_id`,`entity_type`) VALUES ('".$cup->id."','".$entity_id."','".$entity_type."')");
    }
    
    private function _unregister($cup,$entity_id) {
        $entity_type = ($cup->type == 'teams') ? 'team' : 'player';
        db_query("DELETE FROM `cups_teams` WHERE `cid` = '".$cup->id."' AND `entity_id` = '".$entity_id."'");
        $this->setMessage(t('You have successfully unregistered from cup'));
        $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);
    }
    
    private function _startcup($cup) {
        Cups::startCup($cup->id);
        $this->setMessage(t('Cup successfully started'));
        $this->redirect('cups/id/'.$cup->slug.'-'.$cup->id);
    }

    private function _tree_update($cup) {
        $free_slots = ($cup->teams-$cup->teams_registered);
        $participants = array();
        $entities = db_query("SELECT * FROM `cups_teams` WHERE `cid` = '".$cup->id."' AND `status` = 1 ORDER BY RAND()");
        $type = ($cup->type == 'players') ? 'user' : 'team';
        while($entity = db_fetch_object($entities)) {
            $entity->rating = db_result(db_query("SELECT `rating` FROM `rank` WHERE `entity_id` = '".$entity->entity_id."' AND `game` = '".$cup->game."' AND `type` = '".$type."'"));
            if(empty($entity->rating)) {
                $entity->rating = '-'.$entity->entity_id;
            }
            $participants[] = (array)$entity;

            // Add free slot
            if($free_slots > 0) {
                $participants[] = array(
                    'entity_id'=>0,
                    'rating' => '-'.$entity->rating,
                );
                $free_slots--;
            }
        }

        // Order by rating
        /*
        $participants = array_sort($participants, 'rating', SORT_DESC);

        // Tournament bracket order
        $count = count($participants);
        // Order entities.
        for ($i = 0; $i < log($count / 2, 2); $i++) {
            $out = array();
            foreach ($participants as $player) {
                $splice = pow(2, $i);
                $out = array_merge($out, array_splice($participants, 0, $splice));
                $out = array_merge($out, array_splice($participants, -$splice));
            }
            $participants = $out;
        }
      */


        // Update database
        $pos = 1;
        foreach($participants AS $key=>$participant) {
            $participant = (object)$participant;
            db_query("UPDATE `cups_tree` SET `entity` = '".$participant->entity_id."' WHERE cid = '".$cup->id."' AND `pos` = '".$pos."' AND `row` = 1 AND `double` = 0");

            // Next row if have free slot
            if($participant->entity_id == 0) {
                $new_pos = round($pos/2,0);
                db_query("UPDATE `cups_tree` SET `entity` = '".$participants[$key-1]['entity_id']."' WHERE cid = '".$cup->id."' AND `pos` = '".$new_pos."' AND `row` = 2 AND `double` = 0");
            }
            $pos++;
        }
    }
}
