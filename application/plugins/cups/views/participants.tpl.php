<?php $game = (object)$variables; ?>
<table class="ipbtable rank">
	<thead>
		<tr class="head">
			<td class="rank-rating"><?php print t('Rating'); ?></td>
			<td class="rank-user"></td>
			<td class="rank-last_played"><?php print t('Last game'); ?></td>
			<td class="rank-games_played"><?php print t('Total games'); ?></td>
			<td class="rank-win"><?php print t('Win'); ?></td>
			<td class="rank-lost"><?php print t('Lost'); ?></td>
			<?php if(!empty($game->can_draw)):?>
				<td class="rank-draw"><?php print t('Draw'); ?></td>
			<?php endif;?>
			<td class="rank-win_procentes"><?php print t('Win'); ?> %</td>
			<td class="rank-p10">P10</td>
		</tr>	
	<thead>
	<tbody>
		<?php foreach($game->participants AS $member): ?>
			<tr class="rank-member <?=$member->col;?>">
				<td class="rank-rating"><?=$member->rating;?></td>
				<td class="rank-user"><?=$member->label; ?></td>
				<td class="rank-last_played"><?=date('d.m.Y',$member->last_played);?></td>
				<td class="rank-games_played"><?=$member->games_played;?></td>
				<td class="rank-win"><?=$member->win;?></td>
				<td class="rank-lost"><?=$member->lost;?></td>
				<?php if(!empty($game->can_draw)):?>
				<td class="rank-draw"><?=$member->draw;?></td>
				<?php endif; ?>
				<td class="rank-win_procentes"><?=$member->win_procents;?></td>
				<td class="rank-p10"><?=$member->p10;?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

