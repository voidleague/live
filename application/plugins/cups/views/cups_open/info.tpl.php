<div id="cup-id-registered">
    <div id="cup-id-registered-unaccepted">
        <h3><?php print t('Registered'); ?> (<?php print $variables['registered_count'];?>)</h3>
        <?php foreach($variables['registered'] AS $registered) :?>
            <?php print $registered; ?><br />
        <?php endforeach; ?>
    </div>
    <div id="cup-id-registered-accepted">
        <h3><?php print t('Accepted'); ?> (<?php print $variables['accepted_count'];?>)</h3>
        <?php foreach($variables['accepted'] AS $accepted) :?>
            <?php print $accepted; ?><br />
        <?php endforeach; ?>
    </div>
</div>
<div id="cup-right">
<div id="cup-id-dates">
     <h3><?php print t('Check-in Info'); ?></h3>
     <table>
        <tr><td class="label"><?php print t('Check-in opens at'); ?>:</td><td> <?php print date('d.m.Y H:i',strtotime($variables['cup']['checkin_date'])); ?> <?php print t("o'clock"); ?></td></tr>
        <tr><td class="label"><?php print t('Cup begins at'); ?>:</td><td> <?php print date('d.m.Y H:i',strtotime($variables['cup']['start_time'])); ?> <?php print t("o'clock"); ?></td></tr>
         <tr><td class="label"><?php print t('Cup will start after');?>:</td><td> <?php print $variables['cup']['timeleft']; ?></td></tr>
     </table>
     <div class="cup-id-dates-icon"></div>
     <div class="clear"></div>
</div>
<div id="cup-id-rules">
    <h3><?php print t('Rules'); ?></h3>
    <?php print $variables['rules']; ?>
</div>
</div>

