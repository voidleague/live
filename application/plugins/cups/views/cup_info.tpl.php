<div style="width: 728px;
text-align: center;
margin-left: 100px;
margin-bottom: 13px;">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Brackets advert -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:728px;height:90px"
         data-ad-client="ca-pub-4127556941188052"
         data-ad-slot="9511787322"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<?php $cup = $variables; ?>
<div class="cup-info">
    <div class="cup-info-game"><img src="/public/images/games/icons/<?php print $cup->game;?>.jpg" alt="" /></div>
    <div class="cup-info-info">
        <?php print t('Admins'); ?>: <?php print $cup->rendered_admins; ?><br />
        <?php print t('Starts');?>: <?php print date('d.m.Y H:i',strtotime($cup->start_time)); ?><br />
        <?php if($cup->type == 'teams') print t('Teams'); else print t('Players'); ?>: <span class="teams-registered"><?php print $cup->teams_registered; ?></span>/<span class="teams-slots"><?php print $cup->teams; ?></span>
        <?php if($cup->competitive == 'Y'):?>
            <div class="competitive tooltip" title="<?=t('Teams will lose/gain points for every game');?>"><?=t('Competitive*');?></div>
        <?php endif; ?>
    </div>
    <?php if(!empty($cup->join)) print $cup->join; ?>
    <?php if(!empty($cup->checkin)) print $cup->checkin; ?>
    <?php if(!empty($cup->edit_link)) print $cup->edit_link; ?>

    <div class="clear"></div>
</div>
<?php if($cup->only_invites == 'Y' && $cup->status == 0): ?>
    <div class="bx info">
        <strong>This tournament is special event and can be joined only by invites.</strong><br />
    </div>
<?php endif;?>
<?php if($cup->double_elimination): ?>
    <div class="bx info">
        <strong>This tournament is running "VoidLeague edition Double Eliminations bracket".</strong><br />
        If your team loses one match, you get placed in the "Losers bracket" from where you can still get 3rd or 4th place. Losers bracket can be found on the bottom of the bracket by simply scrolling down. To get eliminated from the tournament you need to lose once in "Losers bracket".    </div>
<?php endif;?>


