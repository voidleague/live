<div id="cup-team-info">
    <div class="cup-team-info-inner-container-box">
        <div class="cup-team-teams-label">
            <span class="own-team"><?=$team['name'];?></span>
            <span class="vs">VS</span>
            <span class="enemy-team"></span>
        </div>
        <div class="cup-team-info">
            <div class="time">Start time: <span></span></div>
            <div class="pair">Pair: <span></span></div>
            <div class="enemy-captain">Enemy captain: <span></span></div>
        </div>
        <a href="#" id="submitGameResult" style="display:none;"><?=t('Game finished');?></a>
    </div>
</div>
<script>
    $(document).ready(function() {
       Cups.updateTeamInfo(<?=$cup_id;?>, <?=$team['id'];?>, '<?=md5('void'.$cup_id.'_'.$team['id']);?>');
    });
</script>