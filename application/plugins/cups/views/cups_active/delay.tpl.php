<?php
$cup = $variables;
$startdate = date('YmdHis',strtotime($cup->started_time));
$enddate = date('YmdHis',(strtotime($cup->started_time)+($cup->delay*60)));

?>
<link rel="stylesheet" href="/public/js/cooloader/cooloader.css" type="text/css" charset="utf-8" />
    <script type="text/javascript" charset="utf-8" src="/public/js/cooloader/jquery.cooloader-src.js"></script>

    <script type="text/javascript">
        $(window).ready(function(){

            var t = new Date();

            var time = {

                y : t.getFullYear(),
                m : t.getMonth() + 1,
                d : t.getDate(),
                h : t.getHours(),
                i : t.getMinutes(),
                s : t.getSeconds()

            };

            var mStart = time.m - 1;
            if(mStart < 10) mStart = '0' + mStart;

            var mEnd = time.m + 1;
            if(mEnd < 10) mEnd = '0' + mEnd;
            $('#cooloader').cooloader({

                direction	: 'up',
                duration	: 3,
                effect		: 'easeOutQuint',
                tStart		: '<?=$startdate;?>',
                tEnd		: '<?=$enddate;?>',
                pStart		: 0,
                pEnd		: 100,
                imgB		: '/public/js/cooloader/img/delay_gray.png',
                imgH		: '/public/js/cooloader/img/delay.png',
                cW			: 250,
                cH			: 1,
                cMl			: -140,
                cMt			: -90,
                cC			: '#00C2F5',
                cLive		: true,
                tTextD		: '<span style="font-weight: bold;">brackets ready in</span>&nbsp;<br /><div style="clear:both;"></div>',
                tTextI		: '%I:',
                tTextS		: '%S',
                tTextDesc     : '<br /><br />Participants need to join LoL client chat-room "Voidleague"',
                tGMT		: +2,

                loadLink	: '',

                callback	: function(){ //window.location = '?';
                 }

            });

        });

    </script>


        <div class="cooloader-center-content">
            <div id="cooloader"></div>
        </div>

        <div style="height: 40px;"></div>