<div class="bx cupf">
    <?=$filterForm;?>
</div>
<?php print render_links($this->links,'cups',true, '', true); ?>
<div id="cups">
    <div class="choose-game">
        <?php print $variables['games_menu']; ?>
    </div>
    <div class="cups-list">
        <ul><?php print $variables['cups_list']; ?></ul>
    </div>
    <?php if(!empty($variables['pager'])) print $variables['pager']; ?>
</div>