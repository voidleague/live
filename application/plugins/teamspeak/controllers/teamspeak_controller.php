<?php
class TeamspeakController extends AppController {
    public function __construct() {
        $this->title  = t('Teamspeak');
    }

    public function index() {
        return $this->view('index');
    }
}