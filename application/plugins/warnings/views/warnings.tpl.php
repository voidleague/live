<?php
if(!empty($variables['form'])) {
    echo $variables['form'];
}
?>

<h2><a href="/team/id/<?=$this->team->slug;?>-<?=$this->team->id;?>"> Go back to <?=$this->team->name;?> page</a>
</h2>

<a class="submit22" href="?do=add"><?=t('Add warning');?></a>
<div class="clear"></div>

<table style="margin-top:10px;">
    <?php if(!empty($variables['warnings'])): ?>
        <tr>
            <th><?=t('Date');?></th>
            <th><?=t('Added by');?></th>
            <th><?=t('Reason');?></th>
            <th><?=t('Operations');?></th>
        </tr>
        <?php foreach($variables['warnings'] AS $warning): ?>
            <tr>
                <td class="left"><?=date('d.m.Y H:i',strtotime($warning['created']));?></td>
                <td class="left"><?=$warning['reason'];?></td>
                <td class="left"><?=getMember($warning['lietotajvards'],['country'=>$warning['country']]);?></td>
                <td class="left">
                    <a href="?do=edit&wid=<?=$warning['wid'];?>"><?=t('edit');?></a> |
                    <a class="confirm" href="?do=delete&wid=<?=$warning['wid'];?>"><?=t('delete');?></a>
                </td>
            </tr>
        <?php endforeach;?>
    <?php else: ?>
        <tr><td><?=t('This team have not any warnings...');?></td></tr>
    <?php endif; ?>
</table>