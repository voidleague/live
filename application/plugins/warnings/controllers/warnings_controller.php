<?php
use ORM;

class WarningsController extends AppController {
    /**
     * Team object
     * @var
     */
    public $team;


    public function __construct() {
        $args = arg();
        $this->team = ORM::for_table('users_teams')->find_one($args[2]);
        if(empty($this->team)) die;
        $this->title = $this->team->name.' '.t('warnings');
        $this->permission = 'administer warnings';
    }

    /**
     * @return mixed
     */
    public function index() {
        $query = get_query_parameters();
        $args = arg();
        $form = '';

        if(!empty($query->do) && $query->do == 'delete') {
            $this->delete($query->wid);
        }

        if(!empty($query->do) && ($query->do == 'edit' || $query->do == 'add')) {
            $form = (!empty($query->wid) ? $this->form($query->wid) : $this->form());
        }

        if(!empty($_POST)) {
            $data = $this->security($_POST);
            $this->save_warning($data);
        }

        $warnings = $this->getWarnings($args[2], $args[1]);
        return $this->view('warnings',['warnings'=>$warnings,'form'=>$form]);
    }

    /**
     * Add warning to team (form)
     *
     * @return rendered form
     */
    public function form($wid=false) {
        $form = [
            'reason' => [
                '#type' => 'textarea',
                '#title' => t('Reason'),
                '#default_value' => (!empty($warning) ? $warning->reason : '')
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Save')
            ]
        ];

        if($wid) {
            $form['wid'] = [
                '#type' => 'hidden',
                '#value' => $wid
            ];
        }
        return render_form($form);
    }

    public function delete($wid) {
        $args = arg();
        ORM::for_table('warnings')->find_one($wid)->delete();
        $this->setMessage(t('Warning successfully deleted'));
        $this->redirect('warnings/'.$args[1].'/'.$args[2]);
    }

    /**
     * Return warnings of given entity & type
     *
     * @param $entity
     * @param $type
     * @return mixed
     */
    private function getWarnings($entity, $type) {
        return ORM::for_table('warnings')->select('*')->select_expr('warnings.id','wid')
            ->left_outer_join(PREFIX.'lietotaji', [PREFIX.'lietotaji.id','=','warnings.uid'])
            ->where(['type'=>$type,'entity'=>$entity])
            ->find_array();
    }

    /**
     * Create or edit warning
     *
     * @param $data
     */
    private function save_warning($data) {
        $args = arg();
        if(!empty($data->wid)) {
            $warning = ORM::for_table('warnings')->find_one($data->wid);
        } else {
            $warning = ORM::for_table('warnings')->create();
        }

        $warning->reason = $data->reason;
        $warning->uid = User::$user->uid;
        $warning->set_expr('created', 'NOW()');
        $warning->entity = $args[2];
        $warning->type = $args[1];
        $warning->save();

        $this->setMessage(t('Warning successfully saved'));
        $this->redirect('warnings/'.$args[1].'/'.$args[2]);
    }
}