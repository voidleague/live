<?php $stream = $variables['stream']; ?>
<div id="stream-video" style="width:960px">
    <object type="application/x-shockwave-flash" height="640" width="930" id="live_embed_player_flash" data="https://www-cdn.jtvnw.net/swflibs/TwitchPlayer.swf?channel=<?=$stream->username;?>" bgcolor="#000000">
        <param name="allowFullScreen" value="true" />
        <param name="allowScriptAccess" value="always" />
        <param name="allowNetworking" value="all" />
        <param name="movie" value="https://www.twitch.tv/widgets/live_embed_player.swf" />
        <param name="flashvars" value="hostname=www.twitch.tv&channel=<?=$stream->username;?>&auto_play=true&start_volume=25" />
    </object>
</div>
<div class="clear"></div>
