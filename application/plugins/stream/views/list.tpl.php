<?php if(count($variables['streams']) > 0): ?>
    <?php foreach($variables['streams'] AS $stream) :?>
       <div class="stream">
           <div class="stream-image"><?php print $stream->marked; ?><a href="/stream/watch/<?php print $stream->slug;?>-<?php print $stream->id;?>"><img src="<?php print $stream->image; ?>" alt="" height="200px" width="320px" /></a></div>
           <div class="stream-title"><a href="/stream/watch/<?php print $stream->slug;?>-<?php print $stream->id;?>"><?php print $stream->name; ?></a></div>
           <div class="stream-view"><?php print $stream->viewers; ?> <?php print t('viewers'); ?></div>
           <!--<div class="stream-date"><?php print t('Streamer'); ?>: <?php print htmlspecialchars($stream->streamer); ?><br /><?php print t('Live from');?>: <?php print date('d.m.Y H:i',strtotime($stream->stream_date));?></div>-->
           <div class="clear"></div>
       </div>
    <?php endforeach; ?>
<?php else: ?>
    <?php print t('There are no streams at this moment...'); ?>
<?php endif; ?>