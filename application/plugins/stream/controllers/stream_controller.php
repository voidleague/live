<?php
include $_SERVER['DOCUMENT_ROOT'].'/application/plugins/stream/controllers/Twitch.php';

class StreamController extends AppController {
    public function index() {
        $this->title = t('Streams');
        $streams = array();
        $getStreams = db_query("SELECT * FROM `streams` ORDER BY `id` DESC");
        while($getStream = db_fetch_object($getStreams)) {
            if(empty($getStream->image)) {
                $getStream->image = '/public/images/stream_offline.png';
            } else {
                $getStream->image = str_replace('http://','https://', $getStream->image);
            }
            $getStream->marked = ($getStream->official) ? '<div class="featured_mark"></div>' : '';
            $streams[] = $getStream;
        }
        return $this->view('list',array('streams'=>$streams));
    }

    public function watch() {
        $twitch = new Twitch();

        $args = arg();
        if(isset($args[2])) {
            $explode_streamId = explode('-',$args[2]);
            $streamId = end($explode_streamId);
        }

        if(!is_numeric($streamId)) {
            $this->redirect('stream');
        }

        $stream = db_fetch_object(db_query("SELECT * FROM `streams` WHERE `id` = '".$streamId."'"));
        $this->title = $stream->name;

        $explode_twitch_username = explode('/',$stream->link);
        $twitch_username = end($explode_twitch_username);

        $stream_data = $twitch->getChannelStream($twitch_username);
        $stream->username = $twitch_username;
        if(!empty($stream_data['stream'])) {
            $stream->data = $stream_data['stream'];
        }
        else {
            return 'NOT LIVE*';
        }
        return $this->view('watch',array('stream'=>$stream));
    }
}