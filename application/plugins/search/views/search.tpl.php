<table class="search">
    <tr>
        <td><?php print render_form($this->form);?></td>
    </tr>
</table>
<br />
<div class="search-results">
<?php if(!empty($variables['query']->what) && !empty($variables['query']->keyword)): ?>
    <table>
        <tr><td><strong><?php print count($variables['results']); ?></strong> <?php print t('results found'); ?></td></tr>
    </table><br />
<?php endif; ?>
</div>
<?php
if(count($variables['results']) > 0) {
    
    print '<ul class="search">';
    foreach($variables['results'] AS $result) {
        print '<li><a href="/'.$result->link.'">'.$result->title.'</a></li>';
    }
    print '</ul>';
}
elseif(!empty($variables['query']->what) && !empty($variables['query']->keyword) && count($variables['results']) == 0) {
    print t('Nothing found...');
}
