<?php
class searchController extends AppController{
    
    function index() {
        $this->title = t('Search');
        $query = get_query_parameters();

        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(empty($data->keyword)) {
                $this->setError(t('Please enter keyword to search'));
            }
            else {
                $this->redirect('search/?what='.$data->where.'&keyword='.$data->keyword);
            }
        }
        

        $results = array();
        if(!empty($query->keyword) && !empty($query->what)) {

            if(is_array($query->keyword)) {
                $query->keyword = end($query->keyword);
            }
            if($query->what == 'users') {
                $getResults = db_query("SELECT `lietotajvards` AS username FROM ".PREFIX."lietotaji WHERE `lietotajvards` LIKE '%".mysql_real_escape_string($query->keyword)."%' AND `activated` = 1");
            }
            else {
                $getResults = db_query("SELECT `title`, `nid` ,`slug` FROM ".PREFIX."news WHERE `title` LIKE '%".mysql_real_escape_string($query->keyword)."%'");
            }
            
            while($getResult = db_fetch_object($getResults)) {
                $link = ($query->what == 'news') ? 'new/'.$getResult->slug.'-'.$getResult->nid : 'user/'.$getResult->username;
                $title = ($query->what == 'news') ? $getResult->title : $getResult->username;
                $results[] = (object)array('link'=>$link,'title'=>$this->_highlight($title,$query->keyword));
            }
        }
        
        $this->form = array(
            'keyword' => array(
                '#type' => 'textfield',
                '#title' => t('Keyword'),
                '#default_value' => (!empty($query->keyword)) ? $query->keyword : '',
            ),
            'where' => array(
                '#type' => 'select',
                '#options' => array('users'=>t('Users'),'news'=>t('News')),
                '#default_value' => (!empty($query->what)) ? $query->what : '',
                '#title' => t('Search in'),
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Search'),
            )
        );
        return $this->view('search',array('results'=>$results,'query'=>$query));
    }
    
    
    private function _highlight($string,$highlight) {
        return preg_replace('!('.$highlight.')!i','<span class="highlight">$1</span>',$string);
    }
}
