<div id="left">
    <?php print $this->links(); ?>
    <div class="content sendMessage">
        <table>
            <tr><td><?php print render_form($this->write_form); ?></td></tr>
            <tr><th>"<?php print htmlspecialchars($this->message->message); ?>"</th></tr>
        </table>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>

<script type="application/x-javascript">
    $('.tabs li:nth-child(1)').addClass('active');
</script>