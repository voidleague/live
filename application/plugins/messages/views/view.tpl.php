<div id="left">
    <?php print $this->links(); ?>
    <div class="content view-message">
        <table>
            <tr>
                <th><strong><?php print t('From');?></strong>: <?php print $this->message->username;?>, <?php print laikaParveide($this->message->date); ?><br /><strong><?php print t('Subject'); ?></strong>: <?php print htmlspecialchars($this->message->title); ?></td>
            </tr>
            <tr>
                <td><?php print $this->message->message;?></td>
            </tr>
        </table>
        <br />
        <?php if($this->answer_button): ?>
            <a class="button" href="/messages/reply/<?php print $this->message->id;?>"><?php print t('Answer'); ?></a>
        <?php endif; ?>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>

<script type="application/x-javascript">
    <?php if($this->answer_button): ?>
        $('.tabs li:nth-child(2)').addClass('active');
    <?php else: ?>
         $('.tabs li:nth-child(3)').addClass('active');
    <?php endif; ?>
</script>