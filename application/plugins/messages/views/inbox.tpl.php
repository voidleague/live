<div id="left">
    <?php print $this->links(); ?>
    <div class="content">
        <?php if(!empty($this->messages)): ?>
        <form method="POST">
            <input type="submit" value="<?php print t('Delete checked messages');?>" class="deleteCheckedMessages" /><br /><br />
            <table id="messages">
                <tr>
                    <th class="center nostyle"><input type="checkbox" id="checkAllMessages" /></th>
                    <th></th>
                    <th><?php print t('Subject'); ?></th>
                    <th class="center"><?php print t('Date'); ?></th>
                    <th class="center"><?php print t('From'); ?></th>
                </tr>
                <?php foreach($this->messages AS $message): ?>
                    <tr class="<?php print $message->message_status;?>">
                        <td class="center message_checkbox"><input type="checkbox" name="messages[<?php print $message->id;?>]" class="messages_delete" /></td>
                        <td class="message_status"><img src="/public/img/v2/messages/<?php print $message->message_status;?>.png" alt="" /></td>
                        <td class="message_title"><a href="/messages/id/<?php print $message->id;?>/inbox"><?php print $message->title; ?></a></td>
                        <td><?php print laikaParveide($message->date);?></td>
                        <td><?php if($message->sender_username != 'Bliezam.lv') { print getMember($message->sender_username); } else { print $message->sender_username; }?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </form>
        <?php print $variables['pager']; ?>
        <?php else: ?>
            <table><tr><td><?php print t('There are no messages...'); ?></td></tr></table>
        <?php endif; ?>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>