<div class="side side_style1">
    <div class="block">
        <div class="title"><h3><?php print t('Mailbox info'); ?></h3></div>
            <div class="content">
                <div class="mailboxfull">
                    <div class="mailboxfull_bar" style="width:<?php print $variables['messagesPercents'];?>%;"></div>
                </div>
                <?php print t('Your mailbox is !procents full',array('!procents'=>$variables['messagesCount'].'%')); ?>
            </div>
    </div>
</div>