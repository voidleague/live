<?php
class MessagesController extends AppController{
    function __construct() {
        $this->title = t('Messages');
        $this->homepath = 'inbox';
        
        if(empty(User::$user->uid)) {
            $this->redirect('login');
        }
    }
    
    function links() {
        $this->links = array(
            'write' => t('New message'),
            'inbox' => t('Inbox'),
            'outbox' => t('Outbox'),
        );
        return parent::links();
    }
    
    function index() {
       return $this->inbox();
    }
    
    function inbox() {
        // Delete checked messages
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(!empty($data->messages)) {
                $messages_ids = array();
                foreach($data->messages AS $message_id=>$val) {
                    $messages_ids[] = intval($message_id);
                }
                db_query("UPDATE `messages` SET `reciever_deleted` = 1 WHERE `id` IN(".implode(',',$messages_ids).")");
            }
        }
        $getMessagesCount = db_result(db_query("SELECT count(*)
                                FROM `messages`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = messages.from
                                WHERE `to` = '".User::$user->uid."' AND `reciever_deleted` = 0"));
        // create pager
        $pager = $this->pager($getMessagesCount,30);
        
        $getMessages = db_query("SELECT messages.*, ".PREFIX."lietotaji.lietotajvards AS sender_username, ".PREFIX."lietotaji.id AS sender_uid
                                FROM `messages`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = messages.from
                                WHERE `to` = '".User::$user->uid."' AND `reciever_deleted` = 0
                                ORDER BY `date` DESC
                                LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $this->messages = array();
        while($getMessage = db_fetch_object($getMessages)) {
            // xss
            $getMessage->title = htmlspecialchars(strip_tags($getMessage->title));
            // message status
            if($getMessage->status == 0) {
                $getMessage->message_status = 'unread';
            }
            elseif($getMessage->status == 1) {
                $getMessage->message_status = 'read';
            }
            elseif($getMessage->status == 2) {
                $getMessage->message_status = 'reply';
            }
            
            if($getMessage->from == 0) {
                $getMessage->sender_username = 'Voidleague.com';
            }
            $this->messages[] = $getMessage;
        }

        return $this->view('inbox',array('pager'=>$pager));
    }
    
    function outbox() {
        // Delete checked messages
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(!empty($data->messages)) {
                $messages_ids = array();
                foreach($data->messages AS $message_id=>$val) {
                    $messages_ids[] = $message_id;
                }
                db_query("UPDATE `messages` SET `sender_deleted` = 1 WHERE `id` IN(".implode(',',$messages_ids).")");
            }
        }
        $getMessagesCount = db_result(db_query("SELECT count(*)
                                FROM `messages`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = messages.from
                                WHERE `from` = '".User::$user->uid."' AND `sender_deleted` = 0"));
        // create pager
        $pager = $this->pager($getMessagesCount,30);
        
        $getMessages = db_query("SELECT messages.*, ".PREFIX."lietotaji.lietotajvards AS sender_username, ".PREFIX."lietotaji.id AS sender_uid
                                FROM `messages`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = messages.from
                                WHERE `from` = '".User::$user->uid."' AND `sender_deleted` = 0
                                ORDER BY `date` DESC
                                LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $this->messages = array();
        while($getMessage = db_fetch_object($getMessages)) {
            // xss
            $getMessage->title = htmlspecialchars(strip_tags($getMessage->title));
            // message status
            if($getMessage->status == 0) {
                $getMessage->message_status = 'unread';
            }
            elseif($getMessage->status == 1) {
                $getMessage->message_status = 'read';
            }
            elseif($getMessage->status == 2) {
                $getMessage->message_status = 'reply';
            }
            $this->messages[] = $getMessage;
        }
        return $this->view('outbox',array('pager'=>$pager));
    }
    
    function write() {
        $routes = arg();
        $to = '';
        if(!empty($routes[2])) {
            $to = db_result(db_query("SELECT `lietotajvards` FROM ".PREFIX."lietotaji WHERE `id` = '".intval($routes[2])."'"));
            $this->title = t('Write message to').' '.$to;
        }
        
        // send mail
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(empty($data->subject) || empty($data->message)) {
                $this->setError(t('Please fill all fields'));
            }
            else {
                if(!empty($routes[2])) {
                    $data->to = $to;
                }
                $get_uid = db_result(db_query("SELECT `id` FROM ".PREFIX."lietotaji WHERE `lietotajvards` = '".$data->to."'"));
                if(empty($get_uid)) {
                    $this->setError(t("Username doesn't exist"));
                }
                else {
                    // send mail
                    db_query("INSERT INTO `messages` (`from`, `to`,`title`,`date`,`message`) VALUES ('".User::$user->uid."','".$get_uid."','".$data->subject."','".time()."','".$data->message."')");
                    $this->redirect('messages/outbox');
                }
            }
        }
        
        $this->write_form = array(
            'to' => array(
                '#type' => 'textfield',
                '#default_value'=> $to,
                '#title' => t('To'),
                '#disabled' => (!empty($to)) ? TRUE : FALSE,
            ),
            'subject' => array(
                '#type' => 'textfield',
                '#title' => t('Subject'),
            ),
            'message' => array(
                '#type' => 'textarea',
                '#title' => t('Message'),
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Send'),
            )
        );
        
        if(empty($to)) {
            $this->write_form['to']['#ajax'] = 'getUsername';
        }
        return $this->view('write');
    }
     
    function id() {
        $routes = arg();
        if(empty($routes[2]) && empty($routes[3])) {
           $this->redirect('messages'); 
        }

        $this->message_type = ($routes[3] == 'inbox') ? 'to' : 'from';
        $message_filter = ($routes[3] == 'inbox') ? 'from' : 'to';
        
        $this->answer_button  = ($routes[3] == 'inbox') ? TRUE : FALSE;
        
        
        $this->message = db_fetch_object(db_query("SELECT `messages`.*,".PREFIX."lietotaji.lietotajvards AS username
                                                  FROM `messages`
                                                  LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = `messages`.".$message_filter."
                                                  WHERE `".$this->message_type."` = '".User::$user->uid."' AND `messages`.`id` = '".$routes[2]."'"));
        if(empty($this->message->id)) {
            $this->redirect('messages'); 
        }
        if($this->message->status == 0) {
            db_query("UPDATE `messages` SET `status` = 1 WHERE `id` = '".$this->message->id."'");
        }
        if($this->message->from == 0) {
            $this->message->username = 'Voidleague.com';
            $this->answer_button = false;
        }
        return $this->view('view');
    }
    
    
    function reply() {
        $routes = arg();
        if(empty($routes[2])) {
           $this->redirect('messages'); 
        }
        
        $this->message = db_fetch_object(db_query("SELECT `messages`.*,".PREFIX."lietotaji.lietotajvards AS username
                                                  FROM `messages`
                                                  LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = `messages`.from
                                                  WHERE `to` = '".User::$user->uid."' AND `messages`.`id` = '".$routes[2]."'"));
        if(empty($this->message->id)) {
            $this->redirect('messages');
        }
        $this->message->replies = $this->message->replies+1;

        $RE = $this->message->replies+1;
        $title = 'Re:['.$this->message->replies.'] '.$this->message->title;
        
        $this->write_form = array(
            'to' => array(
                '#type' => 'textfield',
                '#default_value'=> $this->message->username,
                '#title' => t('To'),
                '#disabled' => TRUE
            ),
            'subject' => array(
                '#type' => 'textfield',
                '#title' => t('Subject'),
                '#default_value' => $title,
                '#disabled' => TRUE,
            ),
            'message' => array(
                '#type' => 'textarea',
                '#title' => t('Message'),
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Send'),
            )
        );
        
        
        
        // send mail
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(empty($data->message)) {
                $this->setError(t('Please fill all fields'));
            }
            else {
                // status
                db_query("UPDATE `messages` SET `status` = 2 WHERE `id` = '".$this->message->id."'");
                // send mail
                $data->subject = $this->message->title;
                db_query("INSERT INTO `messages` (`replies`,`from`, `to`,`title`,`date`,`message`) VALUES ('".$this->message->replies."','".User::$user->uid."','".$this->message->from."','".$data->subject."','".time()."','".$data->message."')");
                $this->redirect('messages/outbox');
            }
        }
        
        
        return $this->view('reply');
    }
    
    function sidebar() {
        $MessagesCount = db_result(db_query("SELECT count(*) FROM `messages` WHERE `to` = '".User::$user->uid."' AND `reciever_deleted` = 0"));

        //233
        // Calculate messages precents
        $precents = round($MessagesCount/233*100);
        if($precents > 100) {
            $precents = 100;
        }
        return $this->view('sidebar',array('messagesCount'=>$precents, 'messagesPercents'=>$precents));
    }
}
