<?php
class passwordController extends AppController{
    
    function __construct() {
        $this->title = t('Recover password');
    }
    
    function index() {
        if(!empty(User::$user->uid)) {
            $this->redirect('?');
        }
        if(isset($_POST['recover_passoword']) && $_POST['recover_passoword']) {
            $data = $this->security($_POST);
            if(empty($data->email)) {
                $this->setError(t('Please fill all fields'));
            }
            elseif(!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
                $this->setError(t('Invalid e-mail'));
            }
            else {
                $getUserSalt = db_result(db_query("SELECT `salt` FROM ".PREFIX."lietotaji WHERE `epasts`= '".$data->email."'"));
                if(empty($getUserSalt)) {
                    $this->setError(t('There are no user with this e-mail'));
                }
                else {
                    db_query("INSERT INTO `password_recovery` (`email`,`key`,`date`,`ip`) VALUES ('".$data->email."','".$getUserSalt."',NOW(),'".ip_address()."')");
                    $this->sendMail($data->email,t('Password recovery'),t('We have recieved a request to recover your password. Please copy link: !link to renew your password',array('!link'=>'http://www.voidleague.com/password/change/'.$getUserSalt)));
                    $this->setMessage(t('Futher instructions sent to your e-mail'));
                }
            }
        }
        
        $this->form = array(
            'email' => array(
                '#type' => 'textfield',
                '#title' => t('E-mail'),
            ),
            'recover_passoword' => array(
                '#type' => 'submit',
                '#value' => t('Recover password!'),
            ),
        );
        
        return $this->view('password');
    }
    
    
    function change() {

        $routes = arg();
        if(empty($routes[2])) {
            $this->redirect('/');
        }

        $recoverPassword = db_result(db_query("SELECT count(*) FROM `password_recovery` WHERE `key` = '".mysql_real_escape_string($routes[2])."'"));
        if(empty($recoverPassword)) {
            $this->redirect('/');
        }
        
        $getUser = db_result(db_query("SELECT `id` FROM ".PREFIX."lietotaji WHERE `salt` = '".mysql_real_escape_string($routes[2])."'"));
        if(empty($getUser)) {
            $this->redirect('/');
        }


      if(isset($_POST['recover_password']) && $_POST['recover_password']) {

            $data = $this->security($_POST);

            if(empty($data->password) || empty($data->password2)) {
                $this->setError(t('Please fill all fields'));
            }
            elseif($data->password != $data->password2) {
                $this->setError(t("Passwords didn't match"));
            }
            else {
                db_query("UPDATE ".PREFIX."lietotaji SET `parole` = '".pw($data->password)."' WHERE `id` = '".$getUser."'");
                db_query("DELETE FROM `password_recovery` WHERE `key` = '".mysql_real_escape_string($routes[2])."'");
                $this->setMessage(t('Password successfully changed'));
            }
        }
        
        $this->form = array(
            'password' => array(
                '#type' => 'password',
                '#title' => t('Password'),
            ),
            'password2' => array(
                '#type' => 'password',
                '#title' => t('Repeat password'),
            ),
            'recover_password' => array(
                '#type' => 'submit',
                '#value' => t('Change password'),
            ),
        );
        
        return $this->view('change');
    }
}
