<?php
class ForumController extends AppController {
    public function __construct() {
        if(isset($_GET['forum_reset'])) {
            db_query("DELETE FROM `forum_replies`");
            db_query("DELETE FROM `forum_topics`");
            db_query("UPDATE `forum_categories` SET `topics` = 0, `replies` = 0,`last_post` = '',`last_poster_id` = '',`last_poster_name` = ''");
            dsm('done');
        }
        $this->title = t('Forum');
        $this->path = $_SERVER['REQUEST_URI'];
        // Attach css
        $this->attached['css'][] = 'forum.css';

        // Settings
        $this->replies_per_page = 10;
        $this->topics_per_page = 10;

        // Breadcrumb
        $this->breadcrumb = array();
        $this->breadcrumb[] =  array('forum'=>t('Forum'));

        // admin
        if(user_access('moderate forum') || user_access('administer forum') || admin()) {
            $this->admin = true;
        }
        else {
            $this->admin = false;
        }
    }

    public function index() {
        $categories = array();
        $getCategories = db_query("SELECT forum_categories.*
                                   FROM `forum_categories`
                                   ORDER BY forum_categories.order ASC");
        while($getCategory = db_fetch_object($getCategories)) {
            $catId = (empty($getCategory->parent)) ? $getCategory->id : $getCategory->parent;
            $getCategory->name = htmlspecialchars($getCategory->name);
            $getCategory->icon = '<img src="/public/img/forum/icons/'.$getCategory->icon.'" alt="" />';
            $getCategory->link = '/forum/cat/'.$getCategory->slug.'-'.$getCategory->id;

            // last activity
            if(!empty($getCategory->last_poster_name)) {
                $getCategory->last_activity = getMember($getCategory->last_poster_name).' '.laikaParveide($getCategory->last_post).'<br />'.t('In').': <a href="'.$getCategory->last_src.'">'.mb_substr($getCategory->last_title,0,35).'</a>';
            }
            else {
                $getCategory->last_activity = t('empty...');
            }

            if($catId != $getCategory->id) {
                if(!isset($categories[$catId]->childs)) {
                    $categories[$catId]->childs = array();
                }
                $categories[$catId]->childs[$getCategory->id] = $getCategory;
            }
            else {
                $categories[$catId] = $getCategory;
            }
        }

        return $this->view('categories',array('categories'=>$categories));
    }

    public function cat() {
        $args = arg();
        $query = get_query_parameters();
        if(empty($args[2])) {
            $this->redirect('forum');
        }
        $explode_category_link = explode('-',$args[2]);
        $cat_id = end($explode_category_link);
        if(!is_numeric($cat_id)) {
            $this->redirect('forum');
        }

        $category = db_fetch_object(db_query("SELECT * FROM `forum_categories` WHERE `id` = '".$cat_id."'"));
        if(empty($category)) {
            $this->redirect('forum');
        }
        $category->name = htmlspecialchars($category->name);
        $this->path = '/forum/cat/'.$category->slug.'-'.$category->id;

        $this->breadcrumb[] = array('forum/cat/'.$category->slug.'-'.$category->id=>$category->name);

        if(isset($args[3]) && $args[3] == 'add_thread') {
            return $this->add_thread($category);
        }
        elseif(!empty($args[3])) {
            $explode_thread_id = explode('-',$args[3]);
            $thread_id = end($explode_thread_id);
            $thread = db_fetch_object(db_query("SELECT * FROM `forum_topics` WHERE `id` = '".intval($thread_id)."' AND `category` = '".$category->id."'"));
            if(!empty($thread)) {
                $thread->category = $category;
                $this->breadcrumb[] = array('forum/cat/'.$category->slug.'-'.$category->id.'/'.$thread->slug.'-'.$thread->id => $thread->title);
                // Reply
                if(!empty($args[4]) && $args[4] == 'reply') {
                    return $this->thread_reply($thread);
                }
                // Edit reply
                if(!empty($query->edit_reply) && is_numeric($query->edit_reply)) {
                    return $this->thread_reply($thread,$query->edit_reply);
                }

                // View
                return $this->thread_view($thread);
            }
        }

        // get topics
        $topics = array();
        $getTopicsCount = db_result(db_query("SELECT count(*) FROM `forum_topics` WHERE `category` = '".$cat_id."'"));
        $pager = $this->pager($getTopicsCount,$this->topics_per_page);
        $getTopics = db_query("SELECT forum_topics.*,".PREFIX."lietotaji.lietotajvards AS username
                               FROM `forum_topics`
                               JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = forum_topics.uid
                               WHERE forum_topics.category = '".$cat_id."'
                               ORDER BY forum_topics.pinned DESC, `forum_topics`.last_post DESC
                               LIMIT ".$this->pager->from.",".$this->pager->perPage."");

        while($getTopic = db_fetch_object($getTopics)) {
            $getTopic->title = htmlspecialchars($getTopic->title);
            $getTopic->last_activity = getMember($getTopic->last_poster_name).'<br />'.laikaParveide($getTopic->last_post);

            $getTopic->link = '/forum/cat/'.$category->slug.'-'.$category->id.'/'.$getTopic->slug.'-'.$getTopic->id;

            $getTopic->icon_class = 'thread-status ';
            if($getTopic->closed == 1) {
                $getTopic->icon_class = $getTopic->icon_class.' closed ';
            }
            if($getTopic->pinned) {
                $getTopic->pinned = '<img src="/public/img/forum/sticky.gif" alt="" />';
            }
            else {
                $getTopic->pinned = '';
            }

            $read = $this->forum_read($getTopic->id);
            $getTopic->icon_class .= ($read) ? 'read' : 'unread';
            $topics[] = $getTopic;
        }


        $data = $category;
        $data->topics = $topics;
        return $this->view('category',array('data'=>$data,'pager'=>$pager));
    }


    public function add_thread($category,$thread_id=null) {
        if(isset($_POST['submit'])) {
            $data = $this->security($_POST);

            if(empty($data->name)) {
                $this->setError(t('Please enter topic name'));
            }
            elseif(preg_match('/[^a-zA-Z0-9.,()@#!?]/', $data->name)) {
                $this->setError(t('Illegal charracters in topic name'));
            }
            elseif(empty($data->text)) {
                $this->setError(t('Please enter thread text'));
            }
            else {
                if($thread_id) {
                    $_POST['thread_id'] = intval($thread_id);
                }
                $_POST['category'] = $category;
                $this->_thread_save($_POST);
            }
        }
        $form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('Title'),
            ),
            'text' => array(
                '#type' => 'textarea',
                '#format' => 'filtered_html',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Submit new thread'),
            ),
        );
        $form = render_form($form);
        return $this->view('add_thread',array('data'=>$category,'form'=>$form));
    }

    private function _thread_save($data,$thread=false) {
        $data = $this->security($data);
        $data->text = strip_tags($data->text,'<span><a><img>');
        db_query("UPDATE ".PREFIX."lietotajis SET `xp` = xp+5 WHERE `id` = '".User::$user->uid."'");
        if(empty($data->thread_id)) {

            $data->name = strip_tags(mysql_real_escape_string($data->name));
            $data->slug = slug($data->name);
            $url = str_replace('add_thread','',$_SERVER['REQUEST_URI']);
            db_query("INSERT INTO `forum_topics` (`uid`,`title`,`created`,`category`,`last_post`,`last_poster_id`,`last_poster_name`,`slug`) VALUES ('".User::$user->uid."','".$data->name."','".time()."','".$data->category->id."','".time()."','".User::$user->uid."','".User::$user->username."','".$data->slug."')");
            $topic_id = db_result(db_query("SELECT `id` FROM `forum_topics` WHERE `category` = '".$data->category->id."' ORDER BY `id` DESC LIMIT 1"));
            dsm("INSERT INTO `forum_replies` (`ip`,`topic_id`,`category`,`title`,`text`,`created`,`updated`,`uid`) VALUES ('".ip_address()."','".$topic_id."','".$data->category->id."','".$data->name."','".$data->text."','".time()."','".time()."','".User::$user->uid."')");
            die;
            db_query("INSERT INTO `forum_replies` (`ip`,`topic_id`,`category`,`title`,`text`,`created`,`updated`,`uid`) VALUES ('".ip_address()."','".$topic_id."','".$data->category->id."','".$data->name."','".$data->text."','".time()."','".time()."','".User::$user->uid."')");

            $thread->path = substr($url,1) . $data->slug.'-'.$topic_id;
            db_query("UPDATE `forum_categories` SET `topics` = topics+1, `last_post` = '".time()."', `last_poster_id` = '".User::$user->uid."', `last_poster_name` = '".User::$user->username."', `last_title` = '".$data->name."', `last_src` = '".$thread->path."' WHERE `id` = '".$data->category->id."'");

            $this->setMessage(t('Topic successfully created'));

        }
        else {
            $nr = db_result(db_query("SELECT `nr` FROM `forum_replies` WHERE `topic_id` = '".$thread->id."' ORDER BY `id` DESC"));
            $nr++;
            db_query("INSERT INTO `forum_replies` (`nr`,`ip`,`topic_id`,`category`,`text`,`created`,`updated`,`uid`) VALUES ('".$nr."','".ip_address()."','".$thread->id."', '".$thread->category->id."','".$data->text."','".time()."','".time()."','".User::$user->uid."')");
            db_query("UPDATE `forum_topics` SET `replies` = replies+1, `last_post` = '".time()."', `last_poster_id` = '".User::$user->uid."', `last_poster_name` = '".User::$user->username."' WHERE `id` = '".$thread->id."'");
            db_query("UPDATE `forum_categories` SET `replies` = replies+1, `last_post` = '".time()."', `last_poster_id` = '".User::$user->uid."', `last_poster_name` = '".User::$user->username."',`last_src` = '".$thread->path."' WHERE `id` = '".$thread->category->id."'");

            // read tracking
            db_query("DELETE FROM `forum_tracking` WHERE `topic_id` = '".$thread->id."' AND `uid` != '".User::$user->uid."'");
            $this->setMessage(t('Reply successfully added'));

        }

        $this->redirect($thread->path);
    }


    private function _thread_edit($data,$thread,$reply) {
        $data = $this->security($data);
        db_query("UPDATE `forum_replies` SET `text` = '".$data->text."',`updated` = '".time()."' WHERE `id` = '".$reply->id."'");
        $this->setMessage(t('Reply successfully updated'));
        $this->redirect($thread->category->path.'/'.$thread->slug.'-'.$thread->id);
    }

    // Mark as read
    private function forum_read($topic_id,$insert=false) {
        $readed = db_result(db_query("SELECT COUNT(*) FROM `forum_tracking` WHERE `uid` = '".User::$user->uid."' AND `topic_id` = '".$topic_id."'"));
        if(empty($readed) && $insert) {
            db_query("INSERT INTO `forum_tracking` (`uid`,`topic_id`) VALUES ('".User::$user->uid."','".$topic_id."')");
        }
        return $readed;
    }

    private function thread_view($thread) {

        // security
        $thread->title = htmlspecialchars($thread->title);
        $thread->category->path = 'forum/cat/'.$thread->category->slug.'-'.$thread->category->id;

        // read
        $this->forum_read($thread->id,true);

        $query = get_query_parameters();
        if(!empty($query->delete_reply) && $this->admin) {
            $reply = db_fetch_object(db_query("SELECT * FROM `forum_replies` WHERE `id` = '".intval($query->delete_reply)."' AND `topic_id` = '".$thread->id."'"));
            if(!empty($reply)) {
                db_query("DELETE FROM `forum_replies` WHERE `id` = '".$reply->id."'");
                db_query("UPDATE `forum_topics` SET replies=replies-1 WHERE `id` = '".$thread->id."'");
                db_query("UPDATE `forum_categories` SET replies=replies-1 WHERE `id` = '".$thread->category->id."'");
                $this->setMessage(t('Reply successfully deleted'));
                $this->redirect($thread->category->path.'/'.$thread->slug.'-'.$thread->id);
            }
        }
        if(!empty($query->method) && $this->admin) {
            switch($query->method) {
                case 'pin':
                    db_query("UPDATE `forum_topics` SET `pinned` = 1 WHERE `id` = '".$thread->id."'");
                break;
                case 'unpin':
                    db_query("UPDATE `forum_topics` SET `pinned` = 0 WHERE `id` = '".$thread->id."'");
                break;
                case 'close':
                    db_query("UPDATE `forum_topics` SET `closed` = 1 WHERE `id` = '".$thread->id."'");
                break;
                case 'open':
                    db_query("UPDATE `forum_topics` SET `closed` = 0 WHERE `id` = '".$thread->id."'");
                break;
                case 'delete':
                    $replies = db_result(db_query("SELECT COUNT(*) FROM `forum_replies` WHERE `topic_id` = '".$thread->id."'"));
                    $replies--; // -1 for topic reply
                    db_query("DELETE FROM `forum_tracking` WHERE `topic_id` = '".$thread->id."'");
                    db_query("UPDATE `forum_categories` SET `topics` = topics-1, `replies` = replies-".$replies." WHERE `id` = '".$thread->category->id."'");
                    db_query("DELETE FROM `forum_topics` WHERE `id` = '".$thread->id."'");
                break;
            }

            $this->setMessage('Topic successfully updated');
            $this->redirect($thread->category->path);
        }



        // add view
        if($thread->last_view != ip_address()) {
            db_query("UPDATE `forum_topics` SET `views` = views+1, `last_view` = '".ip_address()."' WHERE `id` = '".$thread->id."'");
        }

        $replies = array();
        $getRepliesCount = db_result(db_query("SELECT count(*) FROM `forum_replies` WHERE `topic_id` = '".$thread->id."'"));
        $pager = $this->pager($getRepliesCount,$this->replies_per_page);
        $getReplies = db_query("SELECT forum_replies.*, lietotajvards AS username, xp, ".PREFIX."lietotaji.id AS uid,".PREFIX."lietotaji.photo, ".PREFIX."lietotaji.country
                                FROM `forum_replies`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = forum_replies.uid
                                WHERE forum_replies.topic_id = '".$thread->id."'
                                ORDER BY forum_replies.id ASC
                                LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        while($getReply = db_fetch_object($getReplies)) {
            if(empty($getReply->photo)) {
                $getReply->photo = 'noimage.png';
            }

            $getReply->operations = '';
            if($getReply->uid == User::$user->uid || $this->admin) {
                $getReply->operations = '<a href="?edit_reply='.$getReply->id.'">'.t('edit').'</a>';
            }
            if($this->admin && $getReply->nr != 1) {
                $getReply->operations .= ' | <a class="confirm" href="?delete_reply='.$getReply->id.'">'.t('delete').'</a>';
            }

            $getReply->photo = '<img src="/public/images/photo/small/'.$getReply->photo.'" alt="" class="forum-avatar team-avatar" width="75px" />';
            $getReply->xp_level = user_lvl($getReply->uid,$getReply->xp);
            $getReply->text = strip_tags($getReply->text,'<span><a><img>');

            $getReply->updated_info = '';
            if($getReply->updated != $getReply->created) {
                $getReply->updated_info = '<div class="reply-updated">'.t('Last edited').': '.laikaParveide($getReply->updated).'</div>';
            }
            $replies[] = $getReply;
        }
        $thread->replies = $replies;

        // reply
        if($thread->closed == 0 && !empty(User::$user->uid)) {
            $thread->reply_post = '<span class="forum-button reply"><a href="/'.$thread->category->path.'/'.$thread->slug.'-'.$thread->id.'/reply">'.t('Add reply').'</a></span>';
        }
        elseif($thread->closed == 1) {
            $thread->reply_post = '<span class="forum-button closed">'.t('Closed').'</span>';
        }
        else {
            $thread->reply_post = '';
        }

        // Moderate operations
        $moderate = '';
        if($this->admin) {
            $operations = array(''=>t('- Moderate topic -'));
            if($thread->pinned) {
                $operations['unpin'] = t('Unpin topic');
            }
            else {
                $operations['pin'] = t('Pin topic');
            }

            if($thread->closed) {
                $operations['open'] = t('Open topic');
            }
            else {
                $operations['close'] = t('Close topic');
            }
            $operations['delete'] = t('Delete topic');

            $form = array(
                'method' => array(
                    '#type' => 'select',
                    '#options' => $operations
                )
            );
            $moderate = render_form($form);
            $moderate = '<div id="moderate-forum">'.$moderate.'</div>';
        }
        return $this->view('view_thread',array('data'=>(array)$thread,'pager'=>$pager,'moderate'=>$moderate));
    }

    private function thread_reply($thread,$reply_id=false) {
        $thread->category->path = 'forum/cat/'.$thread->category->slug.'-'.$thread->category->id;
        $thread->path = $thread->category->path.'/'.$thread->slug.'-'.$thread->id;

        if($thread->closed == 1) {
            $this->setError(t('This topic is closed'));
            return '';
        }

        if($reply_id) {
            $reply = db_fetch_object(db_query("SELECT * FROM `forum_replies` WHERE `topic_id` = '".$thread->id."' AND `id` = '".$reply_id."'"));
            if(!$reply->uid != User::$user->uid && !$this->admin || empty($reply)) {
                $this->setError(t('Access denied'));
                return '';
            }
        }
        if(isset($_POST['submit'])) {
            $data = $this->security($_POST);

            if(empty($data->text)) {
                $this->setError(t('Please enter thread text'));
            }
            elseif(empty($reply)) {
                $_POST['thread_id'] = $thread->id;
                $this->_thread_save($_POST,$thread);
            }
            else {
                $_POST['thread_id'] = $thread->id;
                $this->_thread_edit($_POST,$thread,$reply);
            }
        }
        $form = array(
            'text' => array(
                '#type' => 'textarea',
                '#format' => 'filtered_html',
                '#default_value' => ($reply_id) ? $reply->text : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => ($reply_id) ? t('Edit reply') : t('Submit reply'),
            ),
        );
        $form = render_form($form);


        // get last replies
        $last_replies = array();
        $getLastReplies = db_query("SELECT forum_replies.*, lietotajvards AS username, xp, ".PREFIX."lietotaji.id AS uid,".PREFIX."lietotaji.photo, ".PREFIX."lietotaji.country
                                FROM `forum_replies`
                                LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = forum_replies.uid
                                WHERE forum_replies.topic_id = '".$thread->id."'
                                ORDER BY `created` DESC
                                LIMIT 5");
        while($getLastReply = db_fetch_object($getLastReplies)) {
            if(empty($getLastReply->photo)) {
                $getLastReply->photo = 'noimage.png';
            }
            $getLastReply->text = strip_tags($getLastReply->text,'<span><a><img>');
            $getLastReply->photo = '<img src="/public/images/photo/small/'.$getLastReply->photo.'" alt="" class="forum-avatar team-avatar" width="75px" />';
            $getLastReply->xp_level = user_lvl($getLastReply->uid,$getLastReply->xp);
            $last_replies[] = $getLastReply;
        }

        $thread->last_replies = $last_replies;
        return $this->view('reply_thread',array('thread'=>$thread,'form'=>$form));
    }
}