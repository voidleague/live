<?php $data = (object)$variables['data'];?>
<?php print $variables['pager'];?>
<?=$data->reply_post;?>
<div class="team profile">
    <div class="profile-header forums-categories">
        <section class="section">
            <header>
                <h2 class="title"><a href="/<?php print $data->category->path;?>"><?php print $data->category->name;?></a> :: <?php print $data->title;?></h2>
                <?php print $variables['moderate']; ?>
            </header>
            <table class="data open-topic">
                <tbody>
                <?php foreach($data->replies AS $reply): ?>
                    <tr>
                        <td class="forum-left center">
                            <img src="/public/images/flags/<?php print strtolower($reply->country);?>.png" alt="" /> <?php print getMember($reply->username);?>
                            <br />
                            <a href="/user/<?=$reply->username;?>"><?=$reply->photo;?></a><br />
                            <div class="user-level"><?=$reply->xp_level;?></div>
                        </td>
                        <td class="forum-right left">
                            <span class="forum-created"><?=laikaParveide($reply->created);?></span>
                            <span class="forum-user-operations"><?=$reply->operations;?></span>
                            <hr class="subtle-divider" />
                            <?=$reply->text;?>
                            <?=$reply->updated_info;?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </section>
    </div>
</div>
<?=$data->reply_post_bottom;?>
<?php print $variables['pager'];?>