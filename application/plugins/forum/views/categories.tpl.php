<div class="team profile">
    <div class="profile-header forums-categories categories">
        <?php foreach($variables['categories'] AS $category):?>
            <?php if(!empty($category->childs)): ?>
                <section class="section">
                    <header>
                        <h2 class="title"><?php print $category->name;?></h2>
                    </header>
                    <table class="data"><tbody>
                        <tr class="row">
                            <th></th>
                            <th><?php print t('Forum'); ?></th>
                            <th class="center"><?php print t('Topics'); ?></th>
                            <th class="center"><?php print t('Replies'); ?></th>
                            <th><?php print t('Last activity'); ?></th>
                        </tr>
                        <?php foreach($category->childs AS $child): ?>
                            <tr data-link="<?php print $child->link;?>">
                                <td class="icon"><?php print $child->icon;?></td>
                                <td class="left name"><a href="<?php print $child->link;?>"><?php print $child->name;?></a><br /><span class="description"><?php print $child->description;?></span></td>
                                <td class="topics"><?php print $child->topics?></td>
                                <td class="replies"><?php print $child->replies;?></td>
                                <td class="left last_activity"><?php print $child->last_activity;?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody></table>
                </section>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>