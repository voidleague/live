<?php $category = $variables['data']; ?>
<?php if(User::$user->uid):?>
  <span class="forum-button add-thread top"><a href="<?=$this->path;?>/add_thread"><?php print t('Add topic');?></a></span>
<?php endif;?>
<div class="team profile">
    <div class="profile-header forums-categories category">
            <?php print $variables['pager'];?>
         <section class="section">
             <header>
                 <h2 class="title"><a href="<?=$this->path;?>"><?php print $category->name;?></a></h2>
             </header>
             <table class="data"><tbody>
                 <tr class="row">
                     <th><?php print t('Topic'); ?></th>
                     <th><?php print t('Topic Starter'); ?></th>
                     <th class="center"><?php print t('Replies'); ?></th>
                     <th><?php print t('Last activity'); ?></th>
                     <th><?php print t('Views'); ?></th>
                 </tr>
                 <?php if(!empty($category->topics)):?>
                     <?php foreach($category->topics AS $topic): ?>
                         <tr data-link="<?php print $topic->link;?>">
                             <td class="left name"><a class="<?php print $topic->icon_class;?>" href="<?php print $topic->link;?>"><?php print $topic->pinned; ?> <?php print $topic->title;?></a></td>
                             <td class="left author"><?php print getMember($topic->username);?></td>
                             <td class="center replies"><?php print $topic->replies;?></td>
                             <td class="left last_activity"><?php print $topic->last_activity;?></td>
                             <td class="center views"><?php print $topic->views;?></td>
                         </tr>
                     <?php endforeach; ?>
                 <?php else: ?>
                    <tr>
                       <td colspan="5"><span class="empty"><?php print t('This forum is empty...');?></span></td>
                    </tr>
                 <?php endif; ?>
                 </tbody></table>
         </section>
            <?php print $variables['pager'];?>
    </div>
</div>