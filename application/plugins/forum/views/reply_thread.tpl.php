<?php
$query = get_query_parameters();
$thread = $variables['thread'];
$form = $variables['form'];
$label = isset($query->edit_reply) ? t('Edit reply') :  t('Add reply');
?>
<?php if(User::$user->uid):?>
    <span class="forum-button top add-thread"><a href="<?=$this->path;?>/add_thread"><?php print t('Add topic');?></a></span>
<?php endif;?>
<div class="team profile">
    <div class="profile-header forums-categories">
        <section class="section">
            <header>
                <h2 class="title"><a href="<?=$this->path;?>"><?=$thread->category->name;?></a> :: <a href="<?=$this->path;?>"><?php print $thread->title;?></a> :: <?php print $label; ?></h2>
            </header>
            <table class="data">
                <tbody>
                <tr>
                    <td class="left">
                        <?php print $variables['form'];?>
                    </td>
                </tr>
                </tbody>
            </table>
            <?php if(!empty($thread->last_replies)):?>
                <table class="data open-topic">
                    <?php foreach($thread->last_replies AS $reply): ?>
                        <tr>
                            <td class="forum-left center">
                                <img src="/public/images/flags/<?php print strtolower($reply->country);?>.png" alt="" /> <?php print getMember($reply->username);?>
                                <br />
                                <a href="/user/<?=$reply->username;?>"><?=$reply->photo;?></a><br />
                                <div class="user-level"><?=$reply->xp_level;?></div>
                            </td>
                            <td class="forum-right left">
                                <span class="forum-created"><?=laikaParveide($reply->created);?></span>
                                <hr class="subtle-divider" />
                                <?=$reply->text;?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </section>
    </div>
</div>