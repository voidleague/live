<?php $category = $variables['data']; ?>
<div class="team profile">
    <div class="profile-header forums-categories">
        <section class="section">
            <header>
                <h2 class="title"><a href="<?=$this->path;?>"><?php print $category->name;?></a> :: <?php print t('Add topic'); ?></h2>
            </header>
            <table class="data">
                <tbody>
                    <tr>
                        <td class="left">
                            <?php print $variables['form'];?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </section>
    </div>
</div>