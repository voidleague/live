<?php

use \ORM;
use ClassLib\Cups;
use ClassLib\GameAccounts;
use ClassLib\Riot;

class FindController extends AppController {

    /**
     * @array with region key/name
     */
    private $regions = [];

    /**
     * @var array
     */
    private $find_applications = [];

    private $region;
    private $league;
    private $role;
    private $type;

    private $my = false;

    private $unused = [];

    private $sub;
    private $id;

    function __construct() {
        $this->title = t('Find team');

        // Regions
        $getRegions = Cups::getRegions('LOL');
        $this->regions[''] = '-';
        foreach($getRegions AS $getRegion) {
            $this->regions[$getRegion['value']] = $getRegion['name'];
        }
    }

    public function index() {
        $param = get_query_parameters();
        $this->region = (!empty($param->region)) ? $param->region : '';
        $this->tier = (!empty($param->tier)) ? $param->tier : '';
        $this->division = (!empty($param->division)) ? $param->division : '';
        $this->sub = (!empty($param->sub)) ? $param->sub : '';
        $this->role = (!empty($param->role)) ? $param->role : '';
        $this->id = (!empty($param->id)) ? $param->id : '';
        $this->type = (!empty($param->type)) ? $param->type : '';

        $this->my = (!empty($param->sub) && $param->sub == 'my') ? true : false;

        if(!empty($_POST['save_app'])) {
            $this->save($_POST);
        }

        $data = $this->getData();
        $prepared_data = [];
        foreach($data AS $key => $find) {
                $data[$key]['image'] = '<img src="/public/img/lol_icons/tier/'.strtolower($find['tier']).'.png" alt="'.$find['tier'].'" width="120" />';
        }

        // Filter form
        $form = self::filterForm();
        $form_title = t('Search');

        if($this->sub == 'delete' && !empty($this->id)) {
            $this->delete();
        }

        if($this->sub == 'add' || $this->sub == 'edit') {
            $this->getMyGameaccounts(User::$user->uid, 'player');
        }

        if($this->sub && $this->sub == 'add') {
            $form = $this->findForm();
            $form_title = t('Add application');
        }

        if($this->sub && $this->sub == 'addteam') {
            $form = $this->findTeamForm();
            $form_title = t('Add team application');
        }

        if($this->sub && $this->sub == 'edit' && is_numeric($this->id)) {
            $form = $this->findForm($this->id);
            $form_title = t('Edit application');
        }

        if($this->sub && $this->sub == 'editteam' && is_numeric($this->id)) {
            $form = $this->findTeamForm($this->id);
            $form_title = t('Edit application');
        }


        return $this->view('list',[
            'data'=>$data,
            'filter_form'=>$form,
            'form_title' => $form_title
        ]);

    }

    protected function save($data) {

        if($this->id) {

            $finder = $this->load($this->id);

            if(empty($finder)) {
                $this->setError('Access denied');
                return false;
            }
            $gameaccount = $finder['finder']; // TODO:: remove, not used (?)
            $finder = ORM::for_table('findteam')->find_one($this->id);

            // Delete old roles
            ORM::for_table('findteam_roles')->raw_execute("DELETE FROM `findteam_roles` WHERE `fid` = :fid",[
                'fid' => $this->id
            ]);

        } else {

            $finder = ORM::for_table('findteam')->create();
            $finder->about = $data['about'];
            $finder->type = $data['type'];
            if($finder->type == 'player'){
                $finder->entity = $data['gameaccount'];
            } else {
                $finder->entity = $data['team'];
            }

            $finder->set_expr('created', 'NOW()');
            $finder->set_expr('updated', 'NOW()');

            if($finder->type == 'player') {
                $gameaccount = ORM::for_table('users_gameaccount')->find_one($data['gameaccount']);
                $finder->region = $gameaccount->region;
            } else if($finder->type == 'team') {
                $teamid = ORM::for_table('users_teams')->find_one($data['team']);
                $finder->region = $teamid->region;
            }

            if($finder->type == 'player' && empty($gameaccount)) {
                $this->setError('Please add gameaccount');
                return false;
            }
            if($finder->type == 'team' && empty($teamid)) {
                $this->setError('Please add a team');
                return false;
            }
            $isTeamInDB = ORM::for_table('findteam')->where('entity',$data['team'])->find_many();
            if(!empty($isTeamInDB)) {
                $this->setError('This team already has an application!');
                return false;
            }

        }

        $finder->about = $data['about'];

        /*
        $riotdata = Riot::getSummonerLeague($gameaccount->unique_game_id, $gameaccount->region);
        if(empty($riotdata)) {
            $this->setError('Could not retrieve data from RIOT. Please try again');
            return false;
        }
        $finder->additional_data = serialize($riotdata);
        */

        $finder->tier = $data['tier'];

        $finder->type = $data['type'];

        if(in_array($data['tier'],['unranked','master','challenger'])) {
            $data['division'] = '';
        }
        if(in_array($data['type'],['team'])) {
            $data['division'] = '';
        }
        $finder->division = $data['division'];
        $finder->save();

        foreach ($data['roles'] AS $key=>$role) {
            ORM::for_table('findteam_roles')->raw_execute("INSERT INTO
                              `findteam_roles`
                            SET
                              `role` = :role,
                              `fid` = :fid", [
                'role' => $key,
                'fid' => $finder->id
            ]);
        }

        $this->id = $finder->id;

        $this->setMessage(t('Application saved!'));
        $this->redirect('find');


    }

    /**
     * Delete find application
     *
     * @param $id
     */
    protected function delete() {

        $finder = $this->load($this->id);
        if(!empty($finder)) {
            // Load only without joins
            $finder = ORM::for_table('findteam')->find_one($this->id);
            $finder->delete();
            $this->setMessage(t('Application deleted'));
            $this->redirect('find');
        }

    }

    protected function bumb()
    {
        $finder = $this->load($this->id);
        if(!empty($finder)) {
            $finder = ORM::for_table('findteam')->find_one($this->id);
            $finder->set_expr('updated','NOW()');
            $finder->save();
            $this->setMessage(t('Application bumbed'));
            $this->redirect('find');
        }
    }

    protected function getMyGameaccounts($uid, $type) {

        $used = [];
        $data = ORM::for_table('findteam')
                ->table_alias('f')
                ->select('g.*')
                ->left_outer_join('users_gameaccount', 'g.id = f.entity', 'g')
                ->left_outer_join(PREFIX.'lietotaji', 'u.id = g.uid', 'u')
                ->where('f.type', $type)
                ->find_array();

        foreach($data AS $d) {
            $used[$d['id']] = $d;
        }

        $unused = [];
        $allGameaccounts = GameAccounts::loadAll($uid);

        foreach($allGameaccounts AS $allGameaccount) {

            if(!isset($used[$allGameaccount['id']])) {
                $unused[$allGameaccount['id']] = $allGameaccount;
                $this->unused[$allGameaccount['id']] = $allGameaccount['value'].' ('.strtoupper($allGameaccount['region']).')';
            }
        }

        return [
            'used' => $used,
            'unused' => $unused
        ];

    }

    /**
     * return array, convert teamID -> teamNAME
     */
    protected function getMyTeams ()
    {
        $userTeamList = [];
        $usersTeams = ORM::for_table('users_teams_members')
            ->select_many([
                't.id', 't.name'
            ])
            ->table_alias('m')
            ->left_outer_join('users_teams', 't.id = m.team', 't')
            ->where('uid', User::$user->uid)
            ->where('role', 1)
            ->find_array();

        foreach($usersTeams AS $team) {
            $userTeamList[$team['id']] = $team['name'];
        }
        return $userTeamList;
    }
    /**
     * Get data from `findteam` and add roles
     *
     * @return array
     */
    protected function getData() {
        $return_data = [];


        $data = ORM::for_table('findteam') // PLAYER+TEAM
            ->table_alias('f')
            ->select('f.*')
            ->select_many([
                'g.uid',
                'g.value',
                'ut.uid',
                'ut.name'
            ])
            ->select('u.lietotajvards','username')
            ->left_outer_join('users_gameaccount','g.id = f.entity','g')
            ->left_outer_join('users_teams','ut.id = f.entity','ut')
            ->left_outer_join(PREFIX.'lietotaji','u.id = g.uid','u');

        if(!empty($this->region)) {
            $data->where('f.region',$this->region);
        }
        if(!empty($this->tier)) {
            $data->where('f.tier',$this->tier);
        }
        if(!empty($this->division)) {
            $data->where('f.division',$this->division);
        }
        if(!empty($this->role)) {
            $data->left_outer_join('findteam_roles','r.fid = f.id','r');
            $data->where('r.role',$this->role);
        }
        if(!empty($this->type)) {
            $data->where('f.type',$this->type);
        }

        if(!empty($this->my)) {
            //$data->where('g.uid',User::$user->uid); APVIENOT
            //$data->where('ut.uid',User::$user->uid);
        }

        $data->order_by_desc('f.updated');
        $data = $data->find_array();

        foreach($data AS $find) {
            // get roles
            $find['roles'] = ORM::for_table('findteam_roles')->where('fid', $find['id'])->find_array();

            // Get assigned gameaccount or team
            //$assign_table = ($find['type'] == 'player') ? 'gameaccount' : 'teams';
            if($find->type == 'player') $find['entity'] = ORM::for_table('users_gameaccount')->join(PREFIX.'lietotaji',PREFIX.'lietotaji.id = users_gameaccount.uid')->find_one($find['entity']);
            if($find->type == 'team') $find['entity'] = ORM::for_table('users_teams')->select('users_teams.id')->where(PREFIX.'lietotaji.id = users_teams.uid')->find_one($find['entity']);
            $find['region'] = $this->regions[$find['region']];
            $return_data[] = $find;

        }
        return $return_data;
    }


    public function load($id) {

        $finder = ORM::for_table('findteam') // PLAYER + TEAM
            ->table_alias('f')
            ->select_many([
                'f.*',
                'g.uid',
                'g.region',
                'g.value',
                'g.unique_game_id',
                'ut.uid',
                'ut.region',
                'ut.id',
                'ut.name'
            ])
            ->left_outer_join('users_gameaccount','g.id = f.entity','g')
            ->left_outer_join('users_teams','ut.id = f.entity','ut')
            ->where('f.id',$id)
            ->find_one();


        if(empty($finder) || (!empty($finder) && $finder->uid != User::$user->uid) && !user_access('administer findteam')) {
            return false;
        }


        $find_roles = [];
        $findRoles = ORM::for_table('findteam_roles')
            ->where('fid', $id)
            ->find_array();
        foreach($findRoles AS $findRole) {
            $find_roles[$findRole['role']] = $findRole['role'];
        }

        return [
            'finder' => $finder,
            'roles' => $find_roles
        ];

    }

    protected function findForm($id=false) {

        $form = [];
        $roles = [];
        if($id) {
            $roles = [];
        }

        $html = '';

        if($id) {
            $finder = $this->load($id);

            if(empty($finder)) {
                $this->setError(t('Access denied'));
                return false;
            }

            $roles = $finder['roles'];
            $finder = $finder['finder'];

        }

        if($id) {
            $html .= '<h3>' . $finder->value . ' (' . $finder->region . ')</h3>';
        } else {

            $form += [
                'gameaccount' => [
                    '#type' => 'select',
                    '#title' => t('Gameaccount'),
                    '#options' => $this->unused
                ]
            ];
        }

        $form += [
            'tier' => [
                '#title' => t('Tier'),
                '#type' => 'select',
                '#options' => $this->getTiers(false),
                '#default_value' => (!empty($finder->tier)) ? $finder->tier : ''
            ],
            'division' => [
                '#title' => t('Division'),
                '#type' => 'select',
                '#options' => $this->getDivisions(false),
                '#default_value' => (!empty($finder->division)) ? $finder->division : ''
            ],
            'roles' => [
                '#title' => t('Roles'),
                '#type' => 'checkboxes',
                '#options' => self::getRoles(true),
                '#default_value' => (!empty($roles) ? $roles : [])
            ],
            'about' => [
                '#type' => 'textarea',
                '#title' => t('Description'),
                '#default_value' => (!empty($finder->about) ? $finder->about : '')
            ],
            'type' => [
                '#type' => 'hidden',
                '#value' => 'player'
            ],
            'save_app' => [
                '#type' => 'submit',
                '#value' => ($id) ? t('Save') : t('Add')
            ]

        ];

        if($id) {
            // Add delete button
        }

        return $html . render_form($form);

    }

    /**
     * Team Application
     */
    protected function findTeamForm ($id=false) {

        $form = [];
        $roles = [];
        $isPart = $this->getMyTeams();

        if($id) {
            $finder = $this->load($id);

            if(empty($finder)) {
                $this->setError(t('Access denied'));
                return false;
            }
            $roles = $finder['roles'];
            $finder = $finder['finder'];
        }

        if(!empty($isPart)) {
            $form += [
                'team' => [
                    '#title' => t('Team'),
                    '#type' => 'select',
                    '#options' => $this->getMyTeams(),
                    '#default_value' => ''
                ],
                'tier' => [
                    '#title' => t('Team Tier'),
                    '#type' => 'select',
                    '#options' => self::getTiers(),
                    '#default_value' => (!empty($finder->tier)) ? $finder->tier : ''
                ],
                'roles' => [
                    '#title' => t('Roles'),
                    '#type' => 'checkboxes',
                    '#options' => self::getRoles(true),
                    '#default_value' => (!empty($roles) ? $roles : [])
                ],
                'about' => [
                    '#type' => 'textarea',
                    '#title' => t('Description'),
                    '#default_value' => (!empty($finder->about) ? $finder->about : '')
                ],
                'type' => [
                    '#type' => 'hidden',
                    '#value' => 'team',
                ],
                'save_app' => [
                    '#type' => 'submit',
                    '#value' => t('Submit')
                ]
            ];
        }
        else
        {
            //setError no access
            $this->setError(t('Access denied (no teams/not logged in)'));
        }
        return render_form($form);
    }
    /**
     * Render filter form
     *
     * @return mixed
     */
    protected function filterForm() {

        $form = [
            'type' => [
                '#type' => 'select',
                '#title' => t('Type'),
                '#options' => self::getTypes(),
                '#default_value' => $this->type
            ],
            'region' => [
                '#type' => 'select',
                '#title' => t('Region'),
                '#options' => $this->regions,
                '#default_value' => $this->region
            ],
            'tier' => [
                '#type' => 'select',
                '#title' => t('Tier'),
                '#options' => self::getTiers(),
                '#default_value' => $this->tier
            ],
            'division' => [
                '#type' => 'select',
                '#title' => t('Division'),
                '#options' => self::getDivisions(),
                '#default_value' => $this->division
            ],
            'role' => [
                '#type' => 'select',
                '#title' => t('Role'),
                '#options' => self::getRoles(true, true),
                '#default_value' => $this->role
            ],
            '' => [
                '#type' => 'submit',
                '#value' => t('Search')
            ]

        ];

        // Show add button
        if(User::$user->uid) {

            $form['button1'] = [
                '#type' => 'markup',
                '#markup' => '<a href="?sub=add" class="find-my add"><span></span>'.t('Add application').'</a>'
            ];
            $form['button3'] = [
                '#type' => 'markup',
                '#markup' => '<a href="?sub=addteam" class="find-my team"><span></span>'.t('Add team app').'</a>'
            ];
            $form['button2'] = [
                '#type' => 'markup',
                '#markup' => '<a href="?sub=my" class="find-my my"><span></span>'.t('Your applications').'</a>'
            ];

        }

        return render_form($form, false, [
            'method'=>'GET'
        ]);

    }


    /**
     * Get league divisions
     *
     * @return array
     */
    protected function getTiers($empty = true) {
        $data = [];
        if($empty) {
            $data[''] = '-';
        }
        $data += [
            'unranked' =>  t('Unranked'),
            'bronze'=>t('Bronze'),
            'silver' => t('Silver'),
            'gold' => t('Gold'),
            'platinum' => t('Platinum'),
            'diamond' => t('Diamond'),
            'master' => t('Master'),
            'challenger' => t('Challenger')
        ];
        return $data;
    }

    protected function getDivisions($empty = true) {
        $data = [];
        if($empty) {
            $data[''] = '-';
        }
        $data += [
            'I' =>  'I',
            'II'=> 'II',
            'III' => 'III',
            'IV' => 'IV',
            'V' => 'V',
        ];
        return $data;
    }

    /**
     * Get available roles
     *
     * @return array
     */
    protected function getRoles($prepare = false, $first_empty = false) {

        $roles = [
            'support',
            'ad',
            'mid',
            'jungle',
            'top'
        ];

        if($prepare) {

            if($first_empty) {
                $prepared_roles = ['' => '-'];
            } else {
                $prepared_roles = [];
            }
            foreach($roles AS $role) {
                $prepared_roles[$role] = ucfirst($role);
            }
            return $prepared_roles;

        }

        return $roles;

    }
    protected function getTypes($empty = true) {
        $data = [];
        if($empty) {
            $data[''] = '-';
        }
        $data += [
            'team' =>  'Team',
            'player' => 'Player',
        ];
        return $data;
    }
}