<?php
    $data = $variables['data'];
    $filter_form = $variables['filter_form'];
?>
<div id="findteam_form">
    <div class="team profile">
        <section class="section">
            <header><h2 class="title"><?php echo $variables['form_title']; ?></h2></header>
            <div class="profile-header">
                <?php echo $filter_form; ?>
            </div>
        </section>
    </div>
</div>

<div id="findteam">
    <?php if(count($data)): ?>
        <div class="team profile">
            <section class="section">
                <ul class="table roster expanded">
                    <?php foreach($data AS $find): ?>
                        <li>
                            <div class="row">
                                <div style="padding:10px;">
                                    <?php
                                    if($find['type'] == 'player') {
                                        include 'player.tpl.php';
                                    } else {
                                        include 'team.tpl.php';
                                    }
                                    ?>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </section>
        </div>
    <?php else: ?>
        <div class="team profile">
            <section class="section">
                <div class="profile-header">
                    <ul class="table roster expanded">
                        <li style="padding-top:10px;">
                            <?php
                            if(isset($_GET['sub']) && $_GET['sub'] == 'my') {
                                echo t("You don't have any application...");
                            } else {
                                echo t('No one is looking for a team with these parameters...');
                            }
                            ?>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    <?php endif; ?>
</div>
<div class="clear"></div>

<style>
    #findteam_form {
        float:left;
        width:300px;
    }

    #findteam {
        float:right;
        width:615px;
    }

    .find-image {
        float:left;
        width:120px;
    }

    .find-info {
        float:right;
        width:465px;
    }

    #findteam h2 {
        padding:0;
    }

    .level {
        color:#fff;
        font-size:12px;
        font-weight:bold;
    }



    .find-my {
        display:block;
        border-radius:5px;
        color:white;
        font-weight:bold;
        text-align:center;
        text-transform: uppercase;
        font-size:16px;
        padding:5px;
        margin-top:10px;
        position:relative;
        padding-top:6px;
        line-height:17px;
    }

    .find-my span{
        width:20px;
        height:20px;
        left: 10px;
        top:4px;
        position:absolute;
        display:block;
    }

    .find-my:hover {
        text-decoration:none;
    }


    .find-my.add:hover {
        background-color: #61a801;
        background: -webkit-linear-gradient(#61a801, #599803);
        background: linear-gradient(#61a801, #599803);
    }

    .find-my.add {
        background-color: #8cc400;
        background: -webkit-linear-gradient(#8cc400, #62a801);
        background: linear-gradient(#8cc400, #62a801);
        border:1px solid #549300;
        text-shadow: 1px 1px 5px #b1c69c;
    }
    .find-my.add span {
        background:url("/public/img/my-app-add.png") no-repeat center center;
    }

    .find-my.my:hover {
        background-color: #922241;
        background: -webkit-linear-gradient(#922241, #821e39);
        background: linear-gradient(#922241, #821e39);
    }

    .find-my.my {
        background-color: #c12853;
        background: -webkit-linear-gradient(#c12853, #922241);
        background: linear-gradient(#c12853, #922241);
        border:1px solid #671129;
        text-shadow: 1px 1px 5px #671129;
    }
    .find-my.my span {
        background:url("/public/img/my-app-my.png") no-repeat center center;
    }

    .find-my.team:hover {
        background-color: #0074cc;
        background: -webkit-linear-gradient(#0074cc, #0066b2);
        background: linear-gradient(#0074cc, #0066b2);
    }

    .find-my.team {
        background-color: #0092FF;
        background: -webkit-linear-gradient(#0092FF, #0074cc);
        background: linear-gradient(#0092FF, #0074cc);
        border:1px solid #003a66;
        text-shadow: 1px 1px 5px #003a66;
    }
    .find-my.team span {
        background:url("/public/img/my-app-team.png") no-repeat center center;
        background-size: 20px;
    }



    #findteam_form select {
        width:260px;
    }


    #findteam_form .form-item-div.markup {
        border-top: 1px dashed #666;
        margin-top: 10px;
    }

    #findteam_form #form-textarea-item-about {
        width:248px;
    }

    .manage_links {
        position:absolute;
        top:5px;
        right:5px;
    }

    .manage_links a{
        margin-left:3px;
    }
    .about-txt {
        overflow: hidden;

        max-height: 50px;
    }
    .read-more {
        text-transform: uppercase;
        color: #999999;
    }
    .highlight {
        transition: max-height 500ms ease;
        -webkit-transition:max-height 500ms ease;
        -moz-transition: max-height 500ms ease;
        -ms-transition: max-height 500ms ease;
        -o-transition: max-height 500ms ease;
        max-height: 800px;
    }


</style>

<script>
    function tier_change() {
        var tier = $('#form-select-item-tier').val();
        if(tier == 'master' || tier == 'challenger' || tier == 'unranked') {
            $('.form-field-division').hide();
        } else {
            $('.form-field-division').show();
        }
    }
    function type_change() {
        var type = $('#form-select-item-type').val();
        if(type == 'team') {
            $('.form-field-division').hide();
        } else {
            $('.form-field-division').show();
        }
    }
    $(document).ready(function() {
        $('#form-select-item-tier').change(function() {
           tier_change();
        });
        tier_change();
    });
    $(document).ready(function() {
        $('#form-select-item-type').change(function() {
            type_change();
        });
        type_change();
    });
    $(document).ready(function() {
        $(this).find('.read-more').click(function() {
            $(this).parent().find('.about-txt').toggleClass('highlight');
        });
    });
</script>