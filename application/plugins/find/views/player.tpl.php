<div class="find-image">
    <?php echo $find['image']; ?><br />
    <div class="division-info">
        <?php echo strtoupper($find['type']);?><br />
        <span class="tier">
            <?php echo strtoupper($find['tier']);?><br />
            <?php echo $find['division'];?>
        </span>
    </div>
</div>
<div class="find-info">
    <h2 class="profile-name"><a href="/user/<?=$find['username'];?>/"><?=$find['value'];?></a></h2>
    <h3><?php echo $find['region'];?></h3>
    <div class="find-created"><?php echo $find['created']; ?></div>
    <div class="find-about">
        <label><?php echo t('About'); ?></label>
        <div class="about-js"><div class="about-txt"><?php echo htmlspecialchars($find['about']); ?></div>
        <div class="read-more">Read more</div></div>
    </div>

    <div class="find-roles">
        <label><?php echo t('Roles played');?></label>
        <?php foreach($find['roles'] AS $role): ?>
            <div class="find-role">
                <img src="/public/img/icons/find/<?=$role['role'];?>.png" alt="" /> <br />
                <?=strtoupper($role['role']);?>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if(User::$user->uid == $find['uid'] || user_access('administer findteam')): ?>
        <div class="manage_links">
            <a href="?sub=edit&id=<?=$find['id'];?>"><img src="/public/img/icons/page_white_edit.png" alt="" /></a>
            <a class="confirm" href="?sub=delete&id=<?=$find['id'];?>"><img src="/public/img/icons/page_white_delete.png" alt="" /></a>
        </div>
    <?php endif; ?>
</div>

<style>
    .division-info {
        text-align: center;
    }

    .tier {
        font-size:14px;
        font-weight:bold;
        color:#ccc;
    }

    .find-about label,
    .find-roles label {
        display:block;
        font-weight:bold;
        margin-bottom:6px;
    }

    .find-about {
        width:220px;
        float:left;
        word-break: break-word;
        max-height:136px;
    }

    .find-roles {
        width:225px;
        float:left;
        margin-left:20px;
    }

    .find-role {
        float:left;
        width:43px;
        margin:1px;
        text-align:center;
        font-size:9px;
    }

    .find-role img {
        margin-bottom:3px;
    }

    .find-created {
        font-size: 10px;
        margin-bottom: 6px;
        color: #6E6E6E;
    }

</style>