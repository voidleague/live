<div id="recruitment">
    <div id="left">
        <h1 class="recruit-title"><span>Voidleague</span>Recruiting</h1>
        <p class="intro-text">
            We’re looking for humble but ambitious, razor-sharp gamers. We embrace those who see things differently, who aren't afraid to experiment, and who have a healthy disregard for the rules.
            Applicants should first learn about Riot and play League of Legends because we're looking for people who, like us, take play seriously and for whom it's never "just a game."
        </p>


        <table class="nostyle">
            <tr>
                <td><img src="/public/img/lol_icons/Ashe.png" alt="" width="100px"/><br />Content writers</td>
                <td><img src="/public/img/lol_icons/Blitz.png" alt="" width="100px"/><br />Cups administrators</td>
                <td><img src="/public/img/lol_icons/Darius.png" alt="" width="100px" /><br /> Streamers</td>
            </tr>
        </table>

        <p class="intro-footer">
           We are always looking for new talent! If you think you got a talent or a skill that you think we could use, please send us an application!
        </p>
        <h1 class="recruit-title2">Apply at <span>info@voidleague.com</span></h1>
    </div>
    <div id="right">
        <img src="/public/img/staff-shirt.png" alt="" />
    </div>
</div>