<div id="partners">
    <div id="left">
        <h1 class="recruit-title"><span>Voidleague</span>Partners</h1>
        <span class="h33">WHAT IS VOIDLEAGUE?</span>
        VoidLeague is an eSports organization that brings together PC gamers, to compete and socialize in an online community and live events for prizes.<br />
        <span class="h33">PARTNERS & SPONSORS</span>
        For any business inquiries, contact us at <a href="mailto:info@voidleague.com">info@voidleague.com</a>.<br /> We look forward to hearing from you.
        <span class="h33">MEDIA CONTACT</span>
        Anyone in the news media can contact us at <a href="mailto:info@voidleague.com">info@voidleague.com</a>.<br /> We look forward to hearing from you.
    </div>
    <div id="right">
        <img src="/public/img/partners_side.png" alt="" />
    </div>
</div>