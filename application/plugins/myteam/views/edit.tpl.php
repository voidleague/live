<?php $team = $this->team; ?>
<div id="left">
    <?php print $this->links(); ?>
    <div class="content myTeam-edit">
        <h1 class="title"><img src="/public/images/games/icons/<?php print $team->game;?>.jpg" alt="" width="16px" /> <?php print $team->name; ?> (<?php print $team->region;?> <?=$team->format;?>)</h1>
        <div class="team-photo">
            <h3><?php print t('Information'); ?></h3>
            <?php if(!empty($team->picture)): ?>
                <img src="/public/images/users_teams/<?php print $team->picture;?>" alt="" />
            <?php endif; ?>
            <?php print render_form($variables['photo']);?>
        </div>
        <div class="team-members">
            <h3><?php print t('Members'); ?></h3>
            <?php print render_form($variables['stuff']); ?>
        </div>
        <div class="clear"></div>
        <br /><br /><br/>
        <div class="new-member bx info">
            <h3><?php print t('Add new member'); ?></h3>
            <?php print render_form($variables['stuff_new']); ?>
        </div>

        <?php
        if(!empty($variables['delete_team'])) {
            print $variables['delete_team'];
        }
        ?>

    </div>
    
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>