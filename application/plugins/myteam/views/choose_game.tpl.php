<div id="left">
    <?php print $this->links(); ?>
    <div class="content myTeam-choose_game">
        <?php if(count($variables['available_teams']) > 0): ?>
            <div class="bx info"><?php print t('Please choose game for team'); ?></div>
            <?php foreach($variables['available_teams'] AS $game): ?>
                <a href="/myteam/create/<?php print $game; ?>" title="<?php print $game; ?>"><img src="/public/images/games/icons/<?php print $game; ?>.jpg" alt="<?php print $game; ?>"/ ></a>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="bx error"><?php print t('Sorry, but you can not create team'); ?></div>
        <?php endif; ?>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>