<div class="side side_style1">
    <div class="block">
        <div class="title"><h3><?php print t('Info'); ?></h3></div>
            <div class="content center">
                <?php print t('You can create team, invite your friends and play together in our tournaments and leagues.'); ?>
            </div>
    </div>
</div>