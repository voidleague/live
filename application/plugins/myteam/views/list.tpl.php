<div id="left">
    <?php print $this->links(); ?>
    <div class="content myTeam">
        <?php if(!empty($this->teams_raw)): ?>
            <?php foreach($this->teams_raw AS $team): ?>
                <div class="myteam-box">
                    <div class="myteam-box-photo">
                        <?php if(!empty($team->picture)): ?>
                            <img src="/public/images/users_teams/<?php print $team->picture; ?>" alt="" />
                        <?php endif; ?>
                    </div>
                    <div class="myteam-box-name">
                        <a href="/team/id/<?php print $team->slug; ?>-<?php print $team->id;?>"><?php print $team->name; ?></a>
                        <?php if(User::$user->uid == $team->captain): ?>
                            <a href="/myteam/edit/<?php print $team->id; ?>" class="myteam-edit" title="<?php print t('edit'); ?>"><img src="/public/images/edit_icon.png" alt=""/ > <?=t('edit');?></a>
                        <?php endif; ?>
                        <?php if(User::$user->uid != $team->captain): ?>
                            <a href="?leave_team=ok&team_id=<?=$team->id;?>" class="myteam-leave confirm"><?=t('Leave team');?></a>
                        <?php endif; ?>
                    </div>
                    <div class="myteam-box-rank"><img src="/public/images/games/icons/<?php print $team->game;?>.jpg" width="16px" /> <?php print $team->rank;?></div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <?php print t('You do not have any team'); ?>
        <?php endif; ?>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>