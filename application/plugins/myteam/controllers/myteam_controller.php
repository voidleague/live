<?php

use ClassLib\Teams;

class MyteamController extends AppController{
    function __construct() {
        $this->homepath = 'team_list';
        $this->title = t('My team');
        if(empty(User::$user->uid)) {
            $this->redirect('/');
        }
        
        // Get all user teams
        $teams = array();
        $teams_raw = array();
        $getTeams = db_query("SELECT users_teams.*, users_teams.uid AS captain FROM `users_teams_members`
                             JOIN `users_teams` ON users_teams.id = users_teams_members.team
                             WHERE users_teams_members.uid = '".User::$user->uid."' AND users_teams_members.status = 1");
        while($getTeam = db_fetch_object($getTeams)) {
            // get rank
            $team_rank = db_fetch_object(db_query("SELECT `rating`, `now` FROM `rank` WHERE `entity_id` = '".$getTeam->id."' AND `type` = 'team'"));

            if(empty($team_rank)) {
                $getTeam->rank = '<span class="points">(unranked)</span>';
            }
            else {
                $getTeam->rank =  t('Rank').': <strong>'.$team_rank->now.'</strong> <span class="points">('.$team_rank->rating.')</span>';
            }

            if(empty($getTeam->picture)) {
                $getTeam->picture = 'noimage.png';
            }
            $teams[$getTeam->game][] = $getTeam;
            $teams_raw[] = $getTeam;
        }
        $this->teams = $teams;
        $this->teams_raw = $teams_raw;
    }
    
    function links() {
        $this->links = array(
            'create' => t('Create team'),
            'team_list' => t('My teams'),
        );
        return parent::links();
    }
    
    function index() {
        return $this->team_list();
    }
    
    function team_list() {
        $params = get_query_parameters();
        if(isset($params->leave_team) && !empty($params->team_id)) {
            $this->leaveTeam($params->team_id);
        }
        return $this->view('list');
    }
    
    function sidebar() {
        return $this->view('sidebar');
    }

    function create() {
        $routes = arg();
        $available_teams = array();
        $all_games = array();
        // available_teams
        $getGames = db_query("SELECT `tag` FROM ".PREFIX."speles WHERE `create_team` = 1");


        while($getGame = db_fetch_object($getGames)) {
            $gameaccounts_count = db_result(db_query("SELECT count(*) FROM `gamesaccounts` WHERE `game` = '".$getGame->tag."'"));
            $formats = array('3on3','5on5');
            if(isset($this->teams[$getGame->tag]) && count($this->teams[$getGame->tag]) < ($gameaccounts_count*count($formats)) || !isset($this->teams[$getGame->tag])) {
                $available_teams[$getGame->tag] = $getGame->tag;
            }
            $all_games[$getGame->tag] = $getGame->tag;
        }
        
        if(isset($routes[2]) && in_array($routes[2],$all_games) && in_array($routes[2],$available_teams)) {
            // do nothing
        }
        else {
            return $this->view('choose_game',array('available_teams'=>$available_teams));
        }

        // create/save team
        if(isset($_POST['save']) && $_POST['save']) {
            $data = $this->security($_POST);
            $this->_create($data);
        }

        // Add region
        $regions = array();
        $getRegions = db_query("SELECT `name`,`value` FROM `gamesaccounts` WHERE `game` = '".$routes[2]."'");
        while($getRegion = db_fetch_object($getRegions)) {
            $regions[$getRegion->value] = $getRegion->name;
        }
        $form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('Name'),
            ),
            'tag' => array(
                '#type' => 'textfield',
                '#title' => t('TAG'),
            ),
            'region' => array(
                '#type' => 'select',
                '#title' => t('Region'),
                '#options' => $regions,
            ),
            'format' => array(
                '#type' => 'select',
                '#title' => t('Format'),
                '#options' => array('3on3'=>'3on3','5on5'=>'5on5'),
            ),
            'save' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );
        return $this->view('create',array('form'=>$form,'available_teams'=>$available_teams));
    }
    
    function edit() {
        $routes = arg();
        $query = get_query_parameters();
        $getTeam = db_fetch_object(db_query("SELECT users_teams.*, ".PREFIX."lietotaji.lietotajvards AS username, gamesaccounts.name AS region, gamesaccounts.value AS region_value
                                            FROM `users_teams`
                                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_teams.uid
                                            LEFT JOIN `gamesaccounts` ON gamesaccounts.value = users_teams.region
                                            WHERE users_teams.id = '".intval($routes[2])."'"));
        $this->team = $getTeam;
        if(isset($getTeam->uid) && $getTeam->uid != User::$user->uid || empty($getTeam->uid)) {
            $this->redirect('team/id/'.$getTeam->slug.'-'.$getTeam->id);
            //$this->setError(t('This is not your team!'));
            //return false;
        }

        if(isset($query->delete_team) && $query->delete_team == true) {
            db_query("DELETE FROM `users_teams_members` WHERE `team` = '".$getTeam->id."'");
            db_query("UPDATE `users_teams` SET `picture` = '', `uid` = 0, `name` = '*deleted*', `tag` = '*deleted*', `status` = 0 WHERE `id` = '".$getTeam->id."'");
            db_query("DELETE FROM `rank` WHERE `entity_id` = '".$getTeam->id."' AND `type` = 'team'");
            $this->setMessage(t('Team successfully deleted'));
            $this->redirect('myteam');
        }
        
        //photo
        if(isset($_POST['upload_photo']) && $_POST['upload_photo']) {
            $data = $this->security($_FILES);
            $post_data = $this->security($_POST);
            /*
             * Parbauda URL
             */
            if(parse_url($post_data->twitter, PHP_URL_HOST) != "twitter.com" && parse_url($post_data->twitter, PHP_URL_HOST)!= '') {
                $post_data->twitter = 'Not valid';
            }
            if(parse_url($post_data->facebook, PHP_URL_HOST) != "www.facebook.com" && parse_url($post_data->facebook, PHP_URL_HOST)!= '') {
                $post_data->facebook = 'Not valid';
            }
            /*
             * Saisina valid URL
             */
            if(parse_url($post_data->twitter, PHP_URL_HOST) == "twitter.com") {
                $post_data->twitter = substr(parse_url($post_data->twitter, PHP_URL_PATH),1);
            }
            if(parse_url($post_data->facebook, PHP_URL_HOST) == "www.facebook.com") {
                $post_data->facebook = substr(parse_url($post_data->facebook, PHP_URL_PATH),1);
            }

            db_query("UPDATE `users_teams` SET `country` = '".$post_data->country."' WHERE `id` = '".$getTeam->id."'");
            db_query("UPDATE `users_teams` SET `facebook` = '".$post_data->facebook."' WHERE `id` = '".$getTeam->id."'");
            db_query("UPDATE `users_teams` SET `twitter` = '".$post_data->twitter."' WHERE `id` = '".$getTeam->id."'");

            if(!empty($post_data->region)) {
                db_query("UPDATE `users_teams` SET `region` = '".$post_data->region."' WHERE `id` = '".$getTeam->id."'");
            }
            if(!empty($post_data->format)) {
                db_query("UPDATE `users_teams` SET `format` = '".$post_data->format."' WHERE `id` = '".$getTeam->id."'");
            }
            $this->photo_upload($data->photo);
        }

        if(empty($getTeam->region)) {
            // Add region
            $regions = array(''=>t('- Choose -'));
            $getRegions = db_query("SELECT `name`,`value` FROM `gamesaccounts` WHERE `game` = '".$getTeam->game."'");
            while($getRegion = db_fetch_object($getRegions)) {
                $regions[$getRegion->value] = $getRegion->name;
            }
            $region = array(
                'region' => array(
                    '#type' => 'select',
                    '#title' => t('Region'),
                    '#options' => $regions,
                )
            );
        }
        if(empty($getTeam->format)) {
            // Add region
            $formats = array(''=>t('- Choose -'),'3on3'=>'3on3','5on5'=>'5on5');
            $format = array(
                'format' => array(
                    '#type' => 'select',
                    '#title' => t('Format'),
                    '#options' => $formats,
                )
            );
        }
        $photo_form = array(
            'photo' => array(
                '#type' => 'filefield',
            ),
            'country' => array(
                '#type' => 'select',
                '#title' => t('Country'),
                '#default_value' => $getTeam->country,
                '#options' => countries(),
            ),
            'facebook' => array(
                '#type' => 'textfield',
                '#title' => t('Facebook'),
                '#description' => 'Enter your facebook username or direct link!',
                '#default_value' => $getTeam->facebook,
            ),
            'twitter' => array(
                '#type' => 'textfield',
                '#title' => t('Twitter'),
                '#description' => 'Enter your twitter username or direct link!',
                '#default_value' => $getTeam->twitter,
            ),
            'upload_photo' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );
        if(isset($region)) {
            $photo_form = $region+$photo_form;
        }
        if(isset($format)) {
            $photo_form = $format+$photo_form;
        }
        // get team stuff
        if(isset($_POST['save_stuff']) && $_POST['save_stuff']) {
            $change_captain = false;
            $data = $this->security($_POST);
            unset($data->save_stuff);
            foreach($data AS $gid=>$user_role) {
                if($change_captain && $user_role == 1) {
                    $user_role = 2;
                }
                if($user_role == 1) {
                    $change_captain = true;
                    $new_uid = db_result(db_query("SELECT `uid` FROM `users_gameaccount` WHERE `id` = '".$gid."'"));
                    db_query("UPDATE `users_teams` SET `uid` = '".$new_uid."' WHERE `id` = '".$getTeam->id."'");
                    db_query("UPDATE `users_teams_members` SET `role` = 2 WHERE `team` = '".$getTeam->id."' AND `uid` = '".User::$user->uid."'");
                }

                db_query("UPDATE `users_teams_members` SET `role` = '".$user_role."' WHERE `gid` = '".$gid."' AND `team` = '".$getTeam->id."'");
            }
            $this->setMessage(t('Members successfully saved'));
            $this->redirect('myteam/edit/'.$getTeam->id);
        }
        $stuff = array();
        $getStuffs = db_query("SELECT users_gameaccount.*, users_gameaccount.id AS gid, ".PREFIX."lietotaji.lietotajvards AS username, users_teams_members.uid,users_teams_members.role
                             FROM `users_teams_members`
                             LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_teams_members.uid
                             LEFT JOIN `users_gameaccount` ON users_gameaccount.uid = users_teams_members.uid
                             WHERE users_gameaccount.region = '".$getTeam->region_value."' AND users_teams_members.team = '".$getTeam->id."' AND users_teams_members.status = 1
                             ORDER BY users_teams_members.role ASC, users_teams_members.joined ASC");

        $query = get_query_parameters();
        if(!empty($query->delete_player) && is_numeric($query->delete_player) && $this->team->uid != $query->delete_player) {
            db_query("DELETE FROM `users_teams_members` WHERE `team` = '".$this->team->id."' AND `uid` = '".$query->delete_player."'");
            $this->setMessage(t('Member removed from team'));
            $this->redirect('myteam/edit/'.$this->team->id);
        }
        
        $stuff_form = array();
        while($getStuff = db_fetch_object($getStuffs)) {
            $stuff_form[$getStuff->gid] = array(
                '#type' => 'select',
                '#prefix' => $getStuff->value.' ('.getMember($getStuff->username).')',
                '#default_value' => ($getStuff->role),
                '#options' => array(1=>t('Captain'),2=>t('Co-captain'),3=>t('Player')),
                '#disabled' => ($getStuff->role == 1) ? TRUE : FALSE,
                '#sufix' => ($getStuff->role != 1) ? '<a href="/myteam/edit/'.$this->team->id.'?delete_player='.$getStuff->uid.'" class="confirm"><img src="/public/images/admin/delete.png"  alt="" /></a>' : '',
            );
        }
        
        $stuff_form['save_stuff'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
        );
        
        
        // New Member
        if(isset($_POST['invite_member']) && $_POST['invite_member']) {
            $data = $this->security($_POST);
            $data->region_value = $getTeam->region_value;
            $data->format = $getTeam->format;
            $this->invite($data);
        }
        
        $stuff_new = array(
            'username' => array(
                '#type' => 'textfield',
                '#title' => t('Username'),
                '#ajax' => 'getGameaccount',
                '#ajax_arguments'=> array($getTeam->region_value),
                '#description' => t('Start to type user gameaccount value to invite him to team. You can use gameaccounts only from<br /> <strong>'.$getTeam->game.' '.$getTeam->region.'</strong>'),
            ),
            'invite_member' => array(
                '#type' => 'submit',
                '#value' => t('Invite'),
            ),
        );

        $delete_team = '<div id="delete-my-team"><h3>'.t('Delete team').'</h3>'.t('You can delete your team, but there will be no chance to renew it after this operation').'<a href="/myteam/edit/'.$this->team->id.'?delete_team=true" class="confirm submit">'.t('Delete team').'</a></div>';
        return $this->view('edit',array('stuff'=>$stuff_form,'stuff_new'=>$stuff_new,'photo'=>$photo_form,'delete_team'=>$delete_team));
    }
    
    /*
     * Jasataisa validaacija (kopeejaa update/create)
     * izveidot komandu
     * labot komandu (+logo un sastavi)
     */
    private function _create($data) {
        $routes = Routes::$routes;

        try {
            $this->_create_validate($data);
            //insert
            $data->slug = slug($data->name);
            db_query("INSERT INTO `users_teams` (`country`,`format`,`region`,`slug`,`game`,`name`,`tag`,`created`,`uid`) VALUES ('".strtolower(User::$user->country)."','".$data->format."','".$data->region."','".$data->slug."','".$routes[2]."','".$data->name."','".$data->tag."','".time()."','".User::$user->uid."')");
            $team_id = mysql_insert_id();
            $gameaccount = db_result(db_query("SELECT `id` FROM `users_gameaccount` WHERE `uid` = '".User::$user->uid."' AND `region` = '".$data->region."'"));
            db_query("INSERT INTO `users_teams_members` (`gid`,`uid`,`team`,`status`,`role`,`joined`) VALUES ('".$gameaccount."','".User::$user->uid."','".$team_id."',1,1,NOW())");
            $this->setMessage(t('Team successfully created'));
            $this->redirect('myteam/edit/'.$team_id);
        }
        catch(Error $error) {
            $this->setError($error->getMessage());
        }
    }
    
    private function _update($data) {
        
    }
    
    private function _create_validate($data) {
        $preg_str_check = "#[^][ _A-Za-z0-9-]#";

        if(preg_match($preg_str_check, $data->tag) || preg_match($preg_str_check, $data->name)) {
            throw new Error(t('Name or Tag contains illegal characters'));
        }

        if(mb_strlen($data->name) < 3 || mb_strlen($data->tag) < 3) {
            throw new Error(t('Name or Tag need to be at least 3 symbols length'));
        }
        if(mb_strlen($data->name) > 24) {
            throw new Error(t('Name or Tag can not be longer than 24 symbols'));
        }

        if(mb_strlen($data->tag) > 6) {
            throw new Error(t('Name or Tag can not be longer than 6 symbols'));
        }

        if (preg_match('/voidleague/', $data->name) || preg_match('/voidleague/', $data->tag)) {
            throw new Error(t('Name voidleague ir forbidden'));
        }
        $nameTaken = db_result(db_query("SELECT count(*) FROM `users_teams` WHERE `name` = '".$data->name."'"));
        $tagTaken = db_result(db_query("SELECT count(*) FROM `users_teams` WHERE `tag` = '".$data->tag."'"));
        if($nameTaken) {
            throw new Error(t('Sorry, but team name !name as already taken',array('!name'=>'<strong>'.$data->name.'</strong>')));
        }
        if($tagTaken) {
            throw new Error(t('Sorry, but team tag !tag as already taken',array('!tag'=>'<strong>'.$data->tag.'</strong>')));
        }

        // Check if user has gameaccount in this region
        $gameaccountCheck = db_result(db_query("SELECT COUNT(*) FROM `users_gameaccount` WHERE `uid` = '".User::$user->uid."' AND `region` = '".$data->region."'"));
        if(!$gameaccountCheck) {
            throw new Error(t('You need to create gameaccount in this region first'));
        }

        // Check if user has no team in this region
        $routes = Routes::$routes;
        $game = mysql_real_escape_string($routes[2]);
        $sql = "SELECT
                    m.uid
                FROM
                    `users_teams_members` m
                LEFT JOIN `users_teams` t ON t.id = m.team
                WHERE
                    t.`game` = '".$game."' AND
                    t.`region` = '".$data->region."' AND
                    t.`format` = '".$data->format."' AND
                    t.`status` = 1 AND
                    m.`status` = 1 AND
                    m.`uid` = '".User::$user->uid."'";
        $region_team = db_result(db_query($sql));

        if(!empty($region_team)) {
            throw new Error(t('You already have team or you are in team who is in this region and format'));
        }

    }
    
    private function invite($data) {
        try {
            $this->invite_validate($data);
            db_query("INSERT INTO `users_teams_members` (`gid`,`uid`,`team`,`role`) VALUES ('".$this->new_member_gid."', '".$this->new_member_uid."','".$this->team->id."',3)");
            $this->sendMessage($this->new_member_uid,'Invite to team','You have been invited to join <a href="/team/id/'.$this->team->slug.'-'.$this->team->id.'">'.$this->team->name.'</a> team');
            $this->setMessage(t('Gameaccount !user has been successfully invited to !team',array('!user'=>$data->username,'!team'=>$this->team->name)));
        }
        catch(Error $error) {
            $this->setError($error->getMessage());
        }
    }
    
    private function invite_validate($data) {
        $gameaccount = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `value` = '".$data->username."' AND `region` = '".$data->region_value."'"));
        if(!$gameaccount) {
            throw new Error(t('Gameaccount !username was not found',array('!username'=>'<strong>'.$data->username.'</strong>')));
        }
        $userTeam = db_result(db_query("SELECT count(*)
                                       FROM `users_teams_members`
                                       LEFT JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                       WHERE users_teams.format = '".$data->format."' AND users_teams_members.gid = '".$gameaccount->id."' AND users_teams.game = '".$this->team->game."' AND users_teams_members.status = 1"));

        if($userTeam) {
            throw new Error(t('Gameaccount is in a team already!'));
        }
        
        $alreadyInvited = db_result(db_query("SELECT count(*)
                                       FROM `users_teams_members`
                                       LEFT JOIN `users_teams` ON users_teams.id = users_teams_members.team
                                       WHERE users_teams_members.gid = '".$gameaccount->id."' AND users_teams.game = '".$this->team->game."' AND users_teams_members.status = 0 AND users_teams_members.team = '".$this->team->id."'"));
        if($alreadyInvited) {
            throw new Error(t('You have already invited !username to this team',array('!username'=>$data->username)));
        }
        $this->new_member_gid = $gameaccount->id;
        $this->new_member_uid = $gameaccount->uid;
    }
    
    
    private function photo_upload($file) {
        include ROOT.'/application/controllers/IMGupload.php';  			
        $extension = strtolower(getExtension(stripslashes($file['name'])));
        $size = filesize($file['tmp_name']);

        if(!empty($file['name'])) {
            if(!$file['name']) {
                $this->setError(t('Please choose new photo'));
            }
            elseif($extension != ('jpg' || 'jpeg')) {
                $this->setError(t('Photo can be only in format: !formats',array('!formats'=>'<strong>.JPEG</strong')));
            }
            elseif($size > 1024*400) {
                $this->setError(t('Too big photo. Please resize it'));
            }
            else {
                // Delete old photo
                if(!empty($this->team->picture)) {
                    unlink(ROOT.'/public/images/users_teams/'.$this->team->picture);
                }

                // Add new photo
                $photo_name = time().'-'.$this->team->id.'.'.$extension;
                $image = new SimpleImage();
                $image->load($file['tmp_name']);
                // big image
                $image->resize(200,200);
                $image->save('public/images/users_teams/'.$photo_name);

                db_query("UPDATE `users_teams` SET `picture` = '".$photo_name."' WHERE `id` = '".$this->team->id."'");
                $this->setMessage(t('Team saved'));
                $this->redirect('myteam/edit/'.$this->team->id);
            }
        }
        else {
            $this->setMessage(t('Team saved'));
            $this->redirect('myteam/edit/'.$this->team->id);
        }
    }

    public function leaveTeam($team_id) {
        $leave = Teams::leaveTeam($team_id, User::$user->uid);
        if($leave == '0001') {
            $this->setError(t("Can't leave a team while it is in active tournament"));
        } else if($leave) {
            $this->setMessage(t('Successfully leaved team'));
            $this->redirect();
        }
    }
}

