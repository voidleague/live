<div id="left">
    <?php print $this->links(); ?>
    <div class="content myProfile-password">
        <div class="content">
            <h3><?php print t('Change password'); ?></h3>
            <div class="bx info">
                <?=t('You\'ll be throwed out of your account for security reasons if you change the password.');?>
            </div>
            <?php print render_form($variables['form']); ?>
        </div>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>