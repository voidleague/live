<div id="left">
    <?php print $this->links(); ?>
    <div class="content myProfile-photo">
        <div class="content">
            <h3><?php print t('Profile picture'); ?></h3>
            <?php if(!empty(User::$user->photo)): ?>
                <img src="/public/images/photo/<?php print User::$user->photo;?>" alt="" />
            <?php endif; ?>
            <br /><br />
            <?php print render_form($variables['form']); ?>
        </div>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>