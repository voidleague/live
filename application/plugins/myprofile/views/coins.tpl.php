<div id="left">
    <?php print $this->links(); ?>
    <div class="content myProfile-coins">
        <div id="coins_menu">
            <?php print $variables['coins_menu']; ?>
        </div>
        <div id="coins_content">
            <div class="coins_left"><?php print t('Coins left'); ?>: <strong style="color:#C94200"><?php print User::$user->coins; ?></strong></div>
            <br />
            <?php print $variables['content']; ?>
        </div>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>