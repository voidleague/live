<div id="left">
    <?php print $this->links(); ?>
    <div class="content myProfile">
        <?php print render_form($variables['form']); ?>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>

<script type="application/x-javascript">
    $('.tabs li:nth-child(1)').addClass('active');
</script>