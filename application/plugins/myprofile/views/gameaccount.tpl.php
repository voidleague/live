<div id="left">
    <?php print $this->links(); ?>
    <div class="content myProfile-gameaccount">
        <div id="gameaccount_menu">
            <?php print $variables['account_menu']; ?>
        </div>
        <div id="content_content">
            <?php print $variables['content']; ?>
        </div>
    </div>
</div>
<div id="right">
    <?php print $this->sidebar(); ?>
</div>