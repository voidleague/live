<div class="team profile">
    <div class="profile-header">
        <section class="section">
            <header>
                <h2 class="title"><?php print t('Gameaccounts');?></h2>
            </header>
                <table class="data">
                <tr class="row">
                    <th></th>
                    <th><?php print t('Game');?></th>
                    <th><?php print t('Type');?></th>
                    <th><?php print t('Value');?></th>
                    <th><?php print t('Created');?></th>
                </tr>
                <?php if(count($variables) > 0): ?>
                    <?php foreach($variables AS $gameaccount): ?>
                        <tr>
                        <td><a href="?tab=edit&amp;gameaccount=<?=$gameaccount->id;?>" class="edit"><img src="/public/images/admin/edit.png" alt="" /></a></td>
                        <td class="left"><?php print $gameaccount->game; ?></td>
                        <td class="left"><?php print $gameaccount->name; ?></td>
                        <td class="left"><?php print $gameaccount->value; ?></td>
                        <td class="left"><?php print date('d.m.Y',strtotime($gameaccount->created));?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr class="row">
                        <td colspan="5"><?php print t('This user has no GameAccounts added yet.');?></td>
                    </tr>
                <?php endif; ?>
                </table>
                <br />
                <a class="submit22" href="/myprofile/gameaccount?tab=add"><?php print t('Add account'); ?></a>
        </section>
    </div>
</div>