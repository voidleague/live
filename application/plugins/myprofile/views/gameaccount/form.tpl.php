<div class="team profile">
    <div class="profile-header">
        <section class="section">
            <header>
                <h2 class="title"><?php print t('Gameaccounts');?></h2>
            </header>
            <table class="data">
                <tr class="row">
                    <td><?php print render_form($variables['form']); ?></td>
                </tr>
            </table>
        </section>
        <?php if(!empty($variables['delete'])): ?>
            <div class="bx error" style="color:#000">
                <?=t('If you will delete your gameaccount you automatically will be deleted from all teams where your team account was');?>
                <?php print $variables['delete']; ?>
                <div class="clear"></div>
            </div>
        <?php endif; ?>
        <div class="clear"></div>
    </div>
</div>