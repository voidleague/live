<?php
use ORM;

class GameaccountController extends MyprofileController {
    private $api_key = '96583bdc-513b-4ee4-9565-33e4d290e3b6'; //unlimited key
    private $api_version = 'v1.4';
    public function accounts_list() {
        $gameaccounts = array();
        $getGameAccounts = db_query("SELECT users_gameaccount.*, gamesaccounts.name
                                        FROM `users_gameaccount`
                                        LEFT JOIN `gamesaccounts` ON gamesaccounts.value = users_gameaccount.region
                                        WHERE `uid` = '".User::$user->uid."' AND users_gameaccount.game = gamesaccounts.game");
        while($getGameAccount = db_fetch_object($getGameAccounts)) {
            $gameaccounts[] = $getGameAccount;
        }
        return $this->view('list',$gameaccounts);
    }
    public function add($id=null) {
        if($id) {
            $gameaccount = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `id` = '".$id."' AND `uid` = '".User::$user->uid."'"));
            if(empty($gameaccount)) {
                $this->setError(t('You do not have access to edit this gameaccount'));
                $this->redirect('myprofile/gameaccount');
            }
        }
        $games = array();
        $types = array();
        $getGames = db_query("SELECT `title`,`tag` FROM ".PREFIX."speles WHERE `status` = 1 ORDER BY `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $games[$getGame->tag] = htmlspecialchars($getGame->title);
        }
        $type_game = key($games);
        $getTypes = db_query("SELECT `value`,`name` FROM `gamesaccounts` WHERE `game` = '".$type_game."'");
        while($getType = db_fetch_object($getTypes)) {
            $types[$getType->value] = htmlspecialchars(($getType->name));
        }


        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            if($id) {
                $data->gid = $id;
                $data->game = $gameaccount->game;
                $data->region = $gameaccount->region;
                $this->_gameaccount_update($data);
            }
            else {
                $this->_gameaccount_create($data);
            }
        }
        $form = array(
            'game' => array(
                '#type' => 'select',
                '#title' => t('Game'),
                '#default_value'=> (isset($gameaccount)) ? $gameaccount->game : '',
                '#options' => $games,
                '#disabled' => (isset($gameaccount)) ? TRUE : FALSE,
            ),
            'region' => array(
                '#type' => 'select',
                '#title' => t('Server'),
                '#default_value' => (isset($gameaccount)) ? $gameaccount->region : '',
                '#options' => $types,
                '#disabled' => (isset($gameaccount)) ? TRUE : FALSE,
            ),
            'value' => array(
                '#type' => 'textfield',
                '#title' => t('Nick'),
                '#default_value' => isset($gameaccount) ? $gameaccount->value : '',
            ),
        );


        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
        );


        $delete = ($id) ? '<a href="?tab=delete&amp;gameaccount='.$id.'" class="confirm submit22">'.t('Delete').'</a>' : '';
        return $this->view('form',array('form'=>$form,'delete'=>$delete));
    }

    function edit() {
        $query = get_query_parameters();
        if(isset($query->gameaccount) && is_numeric($query->gameaccount)) {
            return $this->add($query->gameaccount);
        }
    }

    public function delete() {
        $query = get_query_parameters();
        if(isset($query->gameaccount) && is_numeric($query->gameaccount)) {
            $gameaccount = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `id` = '".$query->gameaccount."' AND `uid` = '".User::$user->uid."'"));
            if(empty($gameaccount)) {
                $this->setError(t('You do not have access to edit this gameaccount'));
                $this->redirect('myprofile/gameaccount');
            }
            else {
                db_query("DELETE FROM `users_teams_members` WHERE `uid` = '".User::$user->uid."' AND `gid` = '".$query->gameaccount."'");
                db_query("DELETE FROM `users_gameaccount` WHERE `id` = '".$query->gameaccount."'");
                $this->setMessage(t('Gameaccount successfully deleted'));
                $this->redirect('myprofile/gameaccount');
            }
        }
    }

    private function _gameaccount_create($data) {
        $taken = db_result(db_query("SELECT COUNT(*) FROM `users_gameaccount` WHERE `value` = '".$data->value."' AND `game` = '".$data->game."' AND `region` = '".$data->region."'"));
        $limit = db_result(db_query("SELECT COUNT(*) FROM `users_gameaccount` WHERE `game` = '".$data->game."' AND `region` = '".$data->region."' AND `uid` = '".User::$user->uid."'"));
        if(empty($data->value)) {
            $this->setError('Please fill value field');
        }
        elseif($limit) {
            $this->setError(t('You already have gameaccount on this region'));
        }
        elseif(mb_strlen($data->value) > 25) {
            $this->setError(t('Value can not be longer than 25 symbols'));
        }
        elseif(strlen($data->value) < 3) {
            $this->setError(t('Value need to be at least 3 symbols length'));
        }
        else {
            try {
                $data->unique_game_id = null;
                if($data->game == 'LOL') {
                    $data = $this->LOL_validate($data);
                }

                $this->gameaccount_validate($data);



                db_query("INSERT INTO `users_gameaccount` (`created`,`unique_game_id`,`game`,`uid`,`region`,`value`) VALUES (NOW(), '".$data->unique_game_id."','".$data->game."','".User::$user->uid."','".$data->region."','".$data->value."')");
                $gid = db_result(db_query("SELECT `id` FROM `users_gameaccount` WHERE `uid` = '".User::$user->uid."' ORDER BY `id` DESC LIMIT 1"));

                // get teams
                $getTeams = db_query("SELECT users_teams.id, users_teams_members.gid
                                        FROM `users_teams`
                                        LEFT JOIN `users_teams_members` ON users_teams_members.uid = users_teams.uid
                                        WHERE users_teams.uid = '".User::$user->uid."' AND users_teams.region = '".$data->region."'");
                while($team = db_fetch_object($getTeams)) {
                    if(empty($team->gid)) {
                        db_query("INSERT INTO `users_teams_members` (`gid`,`uid`,`team`,`joined`,`role`,`status`) VALUES ('".$gid."','".User::$user->uid."','".$team->id."', NOW(), 1, 1)");
                    }
                }

                $this->setMessage(t('Gameaccount successfully created'));
                $this->redirect('myprofile/gameaccount');

            } catch(Error $e) {
                $this->setError($e->getMessage());
            }
        }
    }


    private function _gameaccount_update($data) {
        $query = get_query_parameters();

        $limit = db_result(db_query("SELECT COUNT(*) FROM `users_gameaccount` WHERE `game` = '".$data->game."' AND `region` = '".$data->region."' AND `uid` = '".User::$user->uid."'"));
        if(empty($data->value)) {
            $this->setError('Please fill value field');
        }
        elseif(mb_strlen($data->value) > 25) {
            $this->setError(t('Value can not be longer than 25 symbols'));
        }
        elseif(strlen($data->value) < 3) {
            $this->setError(t('Value need to be at least 3 symbols length'));
        }
        else {
            try {
                $data->unique_game_id = null;
                if($data->game == 'LOL') {
                    $data = $this->LOL_validate($data);
                }

                // Check if user have team
                $this->gameaccount_validate($data);


                db_query("UPDATE
                            `users_gameaccount`
                          SET
                            `unique_game_id` = '".$data->unique_game_id."',
                            `value` = '".$data->value."'
                          WHERE
                            `uid` = '".User::$user->uid."' AND
                            `id` = '".$data->gid."'");
                $this->setMessage(t('Gameaccount successfully updated'));
                $this->redirect('myprofile/gameaccount');

            } catch(Error $e) {
                $this->setError($e->getMessage());
            }
        }
    }

    private function LOL_validate($data) {
        $summonerId_url = 'https://'.$data->region.'.api.pvp.net/api/lol/'.$data->region.'/'.$this->api_version.'/summoner/by-name/'.rawurlencode($data->value).'?api_key='.$this->api_key;
        $summoner_response = @file_get_contents($summonerId_url);
        $summoner_response = json_decode($summoner_response);

        if(User::$user->uid == 1) {
            dsm($summonerId_url);
        }
        if($summoner_response == false){
            //throw new Error(t('Sorry, but we could not find summoner with this summoner name'));
            $data->unique_game_id = '1337';

        } else {
            $summoner_response = end($summoner_response);
            $data->unique_game_id =  $summoner_response->id;
        }

        return $data;
    }

    /**
     * Validate gameaccount by rune page name
     *
     * @param $data
     */
    private function gameaccount_validate($data) {
        $taken = db_result(db_query("SELECT `id` FROM `users_gameaccount` WHERE `value` = '".$data->value."' AND `game` = '".$data->game."' AND `region` = '".$data->region."' AND `uid` != '".User::$user->uid."'"));
        if(!empty($taken)) {
            $summonerId_url = 'https://'.$data->region.'.api.pvp.net/api/lol/'.$data->region.'/'.$this->api_version.'/summoner/'.$data->unique_game_id.'/runes?api_key='.$this->api_key;
            $summoner_response = @file_get_contents($summonerId_url);
            $summoner_response = json_decode($summoner_response);
            $summoner_response = current($summoner_response);

            $verified = false;
            foreach($summoner_response->pages AS $rune_page) {
                if($rune_page->name == 'voidleague'.User::$user->uid) {
                    $verified = true;
                }
            }

            if(!$verified) {
                throw new Error('This summoner name is already taken! If this is your account you can just verify him.<br />If this is your account you can verify it by renaming one of your rune pages into <strong>"voidleague'.User::$user->uid.'"</strong> and try again.');
            } else {
                // Verified. Delete old gameaccount
                $illegal_gameaccount = ORM::for_table('users_gameaccount')->where(['value'=>$data->value,'game' => $data->game,'region'=>$data->region])->find_one();
                $gid = $illegal_gameaccount->id;
                $illegal_gameaccount->delete();

                // Delete from team members
                $team_member = ORM::for_table('users_teams_members')->where(['gid' => $gid])->find_one();
                if(!empty($team_member)) {
                    ORM::raw_execute("DELETE FROM `users_teams_members` WHERE `gid` = '".$gid."'");
                }
            }
        }

    }

}