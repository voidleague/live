<?php
class IngameController extends MyprofileController {
    function __construct() {
        $games = array();
        $get_games = db_query("SELECT `id`, `title`, `tag` FROM ".PREFIX."speles WHERE `game_nick` = 1");
        while($get_game = db_fetch_object($get_games)) {
            $games[$get_game->tag] = $get_game;
        }
        $this->games = $games;
    }

    public function index() {

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            $this->_save_ingame_nick($data);
        }

        $output = '<div class="bx info">'.t('You can add your in-game nicks so other gamers can easily find you').'</div>';
        $form = array();
        foreach($this->games AS $game) {
            $def_ingame = db_result(db_query("SELECT `nick` FROM `users_ingame_nick` WHERE `game` = '".$game->id."' AND `uid` = '".User::$user->uid."'"));
            $form[$game->tag.'#'.$game->id] = array(
                '#type' => 'textfield',
                '#title' => $game->title,
                '#default_value' => $def_ingame,
            );
        }
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
        );
        return $output . render_form($form);
    }

    private function _save_ingame_nick($data) {
        db_query("DELETE FROM `users_ingame_nick` WHERE `uid` = '".User::$user->uid."'");
        foreach($data AS $key=>$nick) {
            $game = end(explode('#',$key));
            db_query("INSERT INTO `users_ingame_nick` (`nick`,`game`,`uid`) VALUES ('".$nick."','".$game."','".User::$user->uid."')");
        }
        $this->setMessage(t('In-game nicks successfully changed'));
        $this->redirect();

    }
}