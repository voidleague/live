<?php

use Intervention\Image\ImageManagerStatic as Image;
use \ORM;
Image::configure(array('driver' => 'imagick'));

class MyprofileController extends AppController {
    
    
    function __construct() {
        if(empty(User::$user->uid)) {
            $this->redirect('/');
        }   
        $this->title = t('Profile');
        $this->homepath = 'info'; 
    }
    
    
    

    function links() {
        $this->links = array(
            'info' => t('Information'),
            //'coins' => t('Coins'),
            'photo' => t('Photo'),
            'gameaccount' => t('Gameaccounts'),
            'stream' => t('Stream'),
            'change_password' => t('Change password')
        );
        return parent::links();
    }
    
    function index() {
       return $this->info();
    }
    
    function info() {
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if(empty($data->skype)) {
                $this->setError(t('Please fill skype field'));
            }
            else {
                db_query("UPDATE ".PREFIX."lietotaji SET `dzimums` = '".$data->sex."',  `name` = '".$data->name."', `skype` = '".$data->skype."', `birthday` = '".$data->birthday."', `city` = '".$data->city."' WHERE `id` = '".User::$user->uid."'");
                $this->setMessage(t('Profile successfully updated'));
                $this->redirect('myprofile');
            }
        }

        $timezones = get_timezones();
        $information_form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('Name'),
                '#default_value' => User::$user->name,
            ),
            'birthday' => array(
                '#type' => 'date',
                '#title' => t('Birthday'),
                '#default_value' => (!empty(User::$user->birthday)) ? User::$user->birthday : date('d.m.Y'),
            ),
            'skype' => array(
                '#type' => 'textfield',
                '#title' => t('Skype'),
                '#default_value' => User::$user->skype,
            ),
            'sex' => array(
                '#type' => 'radios',
                '#title' => t('Sex'),
                '#options' => array(
                    1=>t('Male'),
                    2=>t('Female'),
                ),
                '#default_value' => User::$user->sex,
            ),
            'city' => array(
                '#type' => 'textfield',
                '#title' => t('City'),
                '#default_value' => User::$user->city,
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
            
        );
        return $this->view('information',array('form'=>$information_form));
    }


    public function ingame() {
        include ROOT.'/application/plugins/myprofile/controllers/ingame_controller.php';
        $ingame = new IngameController();
        $query = get_query_parameters();
        if(empty($query->index)) {
            $query->tab = 'index';
        }
        if(method_exists('IngameController',$query->tab)) {
            $content = $ingame->{$query->tab}();
            return $this->view('ingame',array('content'=>$content));
        }
    }

    public function gameaccount() {
        // create gameaccount menu
        $account_menu = array(
            'list' => t('List'),
            'add' => t('Add'),
        );
        $account_menu = render_links($account_menu);
        include ROOT.'/application/plugins/myprofile/controllers/gameaccount_controller.php';
        $gameaccount = new GameaccountController();
        $query = get_query_parameters();
        if(empty($query->tab) || !empty($query->tab) && !method_exists('GameaccountController',$query->tab)) {
            $query->tab = 'accounts_list';
        }
        //if(method_exists('GameaccountController',$query->tab)) {
            $content = $gameaccount->{$query->tab}();
            return $this->view('gameaccount',array('account_menu'=>$account_menu,'content'=>$content));
        //}
    }


    function coins() {

        // create coins menu
        $coins_menu = array(
            'sms' => t('SMS'),
            'paypal' => t('Paypal'),
            'ibank' => t('IBank'),
            'history' => t('History'),
        );
        $coins_menu = render_links($coins_menu);

        include ROOT.'/application/plugins/myprofile/controllers/coins_controller.php';
        $coins = new CoinsController();
        $query = get_query_parameters();
        if(empty($query->tab)) {
            $query->tab = 'sms';
        }
        if(method_exists('CoinsController',$query->tab)) {
            $content = $coins->{$query->tab}();
            return $this->view('coins',array('coins_menu'=>$coins_menu,'content'=>$content));
        }

    }

    public function stream() {
        $stream = db_fetch_object(db_query("SELECT * FROM `streams` WHERE `uid` = '".User::$user->uid."'"));
        if(isset($_POST['submit'])) {
            $data = $this->security($_POST);
            if(!empty($stream)) {
                $data->sid = $stream->id;
            }
            $this->stream_save($data);
        }

        $form = array(
            'link' => array(
                '#type' => 'textfield',
                '#title' => 'Link to twitch stream',
                '#default_value' => !empty($stream) ? $stream->link : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            )
        );
        $form = render_form($form);
        return $this->view('stream',array('form'=>$form));
    }

    private function stream_validate(&$data) {
        $link = 'twitch.tv';
        if(strpos($data->link, $link) == false) {
            throw new Error(t('Please enter valid link to twitch'));
        }

        include $_SERVER['DOCUMENT_ROOT'].'/application/plugins/stream/controllers/Twitch.php';

        $explode_twitch_username = explode('/',$data->link);
        $twitch_username = end($explode_twitch_username);
        $twitch = new Twitch();

        $stream_data = $twitch->getChannelStream($twitch_username);
        if(!empty($stream_data['stream'])) {
            $stream_data = $stream_data['stream'];
            $data->image = $stream_data['preview']['medium'];
            $data->viewers = $stream_data['viewers'];
            db_query("UPDATE `streams` SET `viewers` = '".$data->viewers."',`image` = '".$data->image."' WHERE `id` = '".$stream->id."'");
        }
        else {
            throw new Error('Stream is offline');
        }

    }

    private function stream_save($data) {
        if(!empty($data->link)) {
            try {
                $this->stream_validate($data);
                if(isset($data->sid)) {
                    db_query("UPDATE `streams` SET `viewers` = '".$data->viewers."', `image` = '".$data->image."', `link` = '".$data->link."' WHERE `uid` = '".User::$user->uid."'");
                }
                else {
                    $name = User::$user->username.' stream';
                    $slug = slug($name);
                    $data->streamer = User::$user->username;
                    db_query("INSERT INTO `streams` (`streamer`, `viewers`, `image`, `slug`,`link`,`uid`,`name`,`status`) VALUES ('".$data->streamer."','".$data->viewers."','".$data->image."','".$slug."','".$data->link."','".User::$user->uid."','".$name."',1)");
                }
                $this->setMessage(t('Stream successfully saved'));
                $this->redirect('myprofile/stream');
            }
            catch(Error $e) {
                $this->setError($e->getMessage());
            }
        }
        else {
            // delete if empty link
            db_query("DELETE FROM `streams` WHERE `uid` = '".User::$user->uid."'");
            $this->setMessage(t('Stream successfully saved'));
            $this->redirect('myprofile/stream');
        }
    }
    
    function sidebar() {
        return $this->view('sidebar');
    }
    
    public function photo() {
        if(isset($_POST['upload']) && $_POST['upload']) {
            $data = $this->security($_FILES);
            try {
                $this->_upload_photo($data);
                db_query("UPDATE ".PREFIX."lietotaji SET `xp` = xp+15, `photo` = '".$data->image."' WHERE `id` = '".User::$user->uid."'");
                $this->redirect('myprofile/photo');
            } catch(Error $e) {
                $this->setError($e->getMessage());
            }
        }
        $form = array(
            'photo' => array(
                '#type' => 'filefield',
                '#title' => t('Photo'),
            ),
            'upload' => array(
                '#type' => 'submit',
                '#value' => t('Upload'),
            )
        );
        return $this->view('photo',array('form'=>$form));
    }

    private function _upload_photo(&$data) {


        // Validate image
        $image = $data->photo;
        if(empty($image['name']) && empty($data->nid)) {
            throw new Error(t('Please add image'));
        }
        elseif(!empty($image['name'])) {
            $extension = strtolower(getExtension(stripslashes($image['name'])));
            $size = filesize($image['tmp_name']);

            if($extension != ('jpg' || 'jpeg' || 'png')) {
                throw new Error(t('Photo can be only in format: !formats',array('!formats'=>'<strong>JPG, PNG</strong>')));
            }
            if($size > 1024*10000) {
                throw new Error(t('Too big photo. Please resize it'));
            }

            $image_name = time().'.'.$extension;

            // Delete old photo
            if(User::$user->photo) {
                unlink(ROOT.'/public/images/photo/'.User::$user->photo);
            }


            Image::make($image['tmp_name'])->fit(80,80)->save('public/images/photo/small/'.$image_name);
            Image::make($image['tmp_name'])->fit(200,300)->save('public/images/photo/'.$image_name);
            $data->image = $image_name;
        }
    }


    public function change_password() {
        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            try {
                $this->_validate_password($data);
                db_query("UPDATE ".PREFIX."lietotaji SET `parole` = '".pw($data->new_password)."' WHERE `id` = '".User::$user->uid."' LIMIT 1");
                $this->setMessage(t('Password successfully changed. Please login with new password'));
                $this->redirect('?');
            } catch(Error $e) {
                $this->setError($e->getMessage());
            }
        }
        $form = [
            'current_password' => [
                '#type' => 'password',
                '#title' => t('Current password'),
            ],
            'new_password' => [
                '#type' => 'password',
                '#title' => t('New password'),
            ],
            'new_password2' => [
                '#type' => 'password',
                '#title' => t('Confirm new password')
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Change'),
            ]
        ];
        return $this->view('change_password',['form'=>$form]);
    }


    private function _validate_password($data) {
        if(empty($data->current_password) || empty($data->new_password) || empty($data->new_password2)) {
            throw new Error(t('Please fill all fields'));
        }
        if(pw($data->current_password) != User::$user->parole) {
            throw new Error(t('Current password is wrong'));
        }

        if(strlen($data->new_password) < 6) {
            throw new error(t('Password need to be at least !count characters long',array('!count'=>6)));
        }

        if($data->new_password != $data->new_password2) {
            throw new Error(t('New passwords need to match'));
        }
    }

    /**
     * Logout user and delete his cookie
     */
    public function logout() {
        ORM::raw_execute("UPDATE ".PREFIX."lietotaji SET `online` = '1245414972', `logoff` = :time WHERE `id` = :user_id",[
            'time' => time(),
            'user_id' => User::$user->uid
        ]);
        setcookie('AUTH','','0','/');
        header('location: /');
    }
}