<?php foreach($variables AS $faq): ?>
    <div class="faq-item">
        <div class="faq-question"><h3><?php print $faq->question; ?></h3></div>
        <div class="faq-answer"><?php print $faq->answer; ?></div>
    </div>
<?php endforeach; ?>