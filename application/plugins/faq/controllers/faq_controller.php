<?php
class FaqController extends AppController {
    function __construct() {
        $this->title = t('Frequently Asked Questions');
    }

    public function index() {
        $faqs = array();
        $getFAQs = db_query("SELECT * FROM `faq` ORDER BY `question` ASC");
        while($getFAQ = db_fetch_object($getFAQs)) {
            $faqs[] = $getFAQ;
        }
        return $this->view('index',$faqs);
    }
}