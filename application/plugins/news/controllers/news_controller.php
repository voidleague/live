<?php
use \ORM;

class NewsController extends AppController {

    public function index() {

    }

    /**
     * View news article
     * @return mixed|string
     */
    public function read() {
        if(!empty($this->args[2])) {
            $new = $this->load($this->args[2]);
        }

        $this->title = $new->title;
        $comments = new ClassLib\Comments('n', $new->nid);
        return $this->view('read', [
            'new' => $new,
            'comments' => ($new->comments) ? $comments->show(User::$user->uid) : '',
            'other_news' => $this->otherNews($new->nid)
        ]);
    }

    /**
     * Load other random news
     * @param $current_nid
     * @return array
     */
    protected function otherNews($current_nid) {
        $getOther_news = ORM::for_table(PREFIX.'news')
            ->where_not_equal('nid', $current_nid)
            ->limit(5)
            ->order_by_expr('RAND()')
            ->find_array();

        $max_title = 50;
        $other_news = [];
        foreach($getOther_news AS $other_new) {
            if(mb_strlen($other_new['title']) > $max_title) {
                $other_new['title'] = mb_substr($other_new['title'], 0, $max_title).'...';
            }
            $other_news[] = $other_new;
        }
        return $other_news;
    }

    /**
     * Load news article
     * @param $param
     * @return array|bool|object
     */
    protected function load($param) {
        $new_ids = explode('-', $param);
        $new_id = end($new_ids);
        if(is_numeric($new_id)) {
            $new = ORM::for_table(PREFIX.'news')
                ->where('nid', $new_id)
                ->find_array();
            if(!empty($new)) {
                return (object)$new[0];
            }
            return $new;
        }
        return false;
    }
}