<?php
/**
 * @var $new - opened article
 * @var $comments - comments
 * @var $other_news - Other random articles
 */
?>
<div id="left" class="current-article">
    <h1 class="title"><?=$new->title;?></h1>
    <div class="news-article-header">
        <div class="news-created"><?=laikaParveide($new->created);?></div>
        <div class="news-social">
            <div class="addthis">
                <?php  $new_url = 'http://'.$_SERVER['HTTP_HOST'].'/'.$new->slug.'-'.$new->nid; ?>
                <a href="http://twitter.com/share" class="twitter-share-button"><img src="/public/images/twitter.png" alt="" /></a>
                <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                <div id="fb-root"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://<?=$_SERVER['SERVER_NAME'].'/'.$new->slug.'-'.$new->nid;?>" send="false" layout="button_count" width="30" show_faces="no" font=""></fb:like></div>

                <script src="https://apis.google.com/js/platform.js" async defer></script>
                <div class="g-plus" data-action="share" data-annotation="bubble" data-href="<?=$new_url;?>"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="news-article-wrap">
        <?php echo $new->body; ?>
    </div>
    <?php echo $comments; ?>
</div>
<div id="right" class="other-articles">
    <?php foreach($other_news AS $other_new): ?>
        <div class="other-article">
            <a href="/news/read/<?=$other_new['slug'];?>-<?=$other_new['nid'];?>">
                <div class="article-image">
                    <img src="/public/img/news/full/<?=$other_new['image'];?>" alt="" width="350"/>
                </div>
                <div class="article-info">
                    <div class="article-title"><?=$other_new['title'];?></div>
                    <div class="article-date"><?=laikaParveide($other_new['created'])?></div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>

<style>
    .other-articles {
    }

    .other-articles .other-article {
        margin-bottom:10px;
        position:relative;
        width:250px;
        height:200px;
        background:#000;
        box-shadow: 2px 2px 5px #666;
    }

    .other-articles .other-article a {
        display:block;
        width:100%;
        height:100%;
        opacity:0.7;
        position:relative;
    }

    .other-articles .other-article a:hover {
        opacity:1;
    }

    .other-article .article-image {
        position:absolute;
        height:150px;
        width:250px;
        overflow:hidden;
    }

    .other-article .article-title {
        height: 50px;
        bottom: 0;
        position: absolute;
        margin-bottom: -15px;
        color: #fff;
        text-transform: uppercase;
        padding: 10px;
        overflow: hidden;
        font-weight: bold;
    }

    .other-article .article-date{
        position: absolute;
        bottom: 3px;
        right: 5px;
        font-size: 10px;
        color: #ccc;
    }
    h1.title {
        display:none;
    }

    #left h1.title {
        display:block;
    }

    .news-article-header {
        padding: 10px;
        box-shadow: 2px 2px 5px #666;
        height: 20px;
        margin-bottom: 40px;
    }

    .news-created {
        float:left;
        color:#666;
        margin-top:4px;
    }

    .news-social {
        float:right;
        width:259px;
    }

    #twitter-widget-0 {
        width: 77px !important;
    }
</style>