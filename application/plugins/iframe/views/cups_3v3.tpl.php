<base target="_parent" />
<a href="http://www.voidleague.com" target="_blank"><img src="/public/images/void_logo_iframe.png" alt="" /></a>
<div id="right">
    <div class="side side_style1 side_tournaments">
                    <div class="block">
                        <div class="title">
                            <!--<h3><?php print t('Opened & active cups'); ?></h3>-->
    </div>
    <div class="block_content">

        <?php
        $cups = db_query("SELECT * FROM `cups` WHERE `status` != 2 AND `published` = 1 AND `format` = '3on3' ORDER BY `start_time` ASC");
        $cups_i = 0;
        while($cup = db_fetch_object($cups)) {
            $cup_date = date('d.m.Y H:i',strtotime($cup->start_time));
            $entities = db_result(db_query("SELECT count(*) FROM `cups_teams` WHERE `status` = 0 AND `cid` = '".$cup->id."'"));
            $players = '<span class="players">'.$entities.'/'.$cup->teams.' | '.$cup->game.'</span>';
            print '<div class="side_tournament">';
            print '<h5><span class="cup_status status-'.$cup->status.'"></span><a href="/cups/id/'.$cup->slug.'-'.$cup->id.'"> '.$cup->name.'</a></h5>';
            print '<p class="registered">'.t('Cup').' <span>'.$cup_date.'</span></p>';
            print '<span class="user-count">'.t(ucfirst($cup->type)).': '.$entities.'/'.$cup->teams.'</span>';

            print '</div>';
            $cups_i++;
        }

        if(empty($cups_i)) {
            print '<p>'.t('There are no opened or active cups...').'</p><br />';
        }
        ?>
        <div class="hide_last_sepr"></div>
    </div>
    </div>
    </div>
    <div class="clear"></div>
</div>

<style>

body {
margin:0;
padding:0;

}
.side_tournament .registered span {
    font-weight: normal;
}
#slider #navigation .active, #slider #navigation .active:hover{
	background:url(../images/slider_item_a.png) repeat-x;
}

#slider #navigation .active .image{
	background:#000;
}

#slider #navigation .active h2, #slider #navigation .active p, #slider #navigation .active:hover h2, #slider #navigation .active:hover p{
	color:#fff;
}

/* ar sideStyle1*/


#right .side_tournaments .side_tournament{
	width:100%;
	float:left;
	padding-bottom:5px;
	margin-bottom:5px;
	border-bottom:1px dotted #dbdbdb;
}

#right .side_tournaments .side_tournament .type {
    width: 20px;
    float: left;
    text-align: center;
}

.clear {
    clear:both;
}

#right .side_tournaments .side_tournament h5{
    float: left;
    margin-top: 4px;
    margin-left: 3px;
}

#right .side_tournaments .side_tournament h5 a{
	color:#2694e2;
}

#right .side_tournaments .side_tournament h5 a strong{
	font-weight:bold;
}

#right .side_tournaments .side_tournament a.register{
	background:url(/public/themes/gamemaster/img/register_tournament.png) repeat-x;
	border-radius:3px;
	border:1px solid #e73a05;
	padding:0 8px;
	height:16px;
	line-height:16px;
	float:left;
	color:#fff;
	text-transform:uppercase;
	text-decoration:none;
	box-shadow:0 0 0 1px rgba(255,255,255,0.3) inset;
	font-size:11px;
	margin-left:10px;
	margin-top:8px;
}

#right .side_tournaments .side_tournament p.registered{
    margin-left: 3px;
    color: #000000;
    text-transform: uppercase;
    font-size: 11px;
    margin-top: 5px;
    float: left;
    font-weight: bold;
}

#right .side_tournaments .hide_last_sepr{
	height:1px;
	font-size:0;
	background:#fff;
	float:left;
	width:100%;
	margin-top:-6px;
}

.cup_status {
    display: block;
    float: left;
    width: 12px;
    height: 13px;
    background: url("/public/themes/gamemaster/img/cup_status.png") no-repeat -9px -8px;
    border: aliceblue;
}

.cup_status.status-1 {
    background: url("/public/themes/gamemaster/img/cup_status.png") no-repeat -30px -27px;
}

.side_tournament .user-count {
    color: gray;
    display: block;
    float: right;
    margin-top: 5px;
    width: 75px;
    font-size: 11px;
}

#right {
    width: 234px;
}

h5, p {
    margin:0;
    padding:0;
}

body {
    font-family: Verdana;
    font-size: 12px;
}

h5 {
    font-size: 100%;
    font-weight: 400;
}
</style>