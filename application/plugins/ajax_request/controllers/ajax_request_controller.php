<?php
class Ajax_requestController extends AppController {
    function getUsername() {
        $routes = arg();
        if(!empty($routes[2])) {
            $getUsernames = db_query("SELECT `lietotajvards` AS username FROM ".PREFIX."lietotaji WHERE lietotajvards LIKE '".mysql_real_escape_string($routes[2])."%' LIMIT 10");
            $users = array();
            while($getUser = db_fetch_object($getUsernames)) {
                $users[] = $getUser->username;
            }
            if(!empty($users)) {
                print json_encode($users);
            }
        }    
        die;
    }

    function getGameaccount() {
        $routes = arg();
        $query = get_query_parameters();
        if(!empty($routes[2]) && !empty($query->region)) {

            $getGameaccounts = db_query("SELECT `value` FROM `users_gameaccount` WHERE `value` LIKE '".mysql_real_escape_string($routes[2])."%' AND `region` = '".mysql_real_escape_string($query->region)."' LIMIT 10");
            $gameaccounts = array();
            while($getGameaccount = db_fetch_object($getGameaccounts)) {
                $gameaccounts[] = $getGameaccount->value;
            }
            if(!empty($gameaccounts)) {
                print json_encode($gameaccounts);
            }
        }
        die;
    }

    /**
     * Use request from plugin folder
     */
    public function index() {
        if(empty($_GET['module']) || empty($_GET['sub'])) die;
        $module = $_GET['module'];
        $sub = $_GET['sub'];

        if(file_exists(ROOT.'/application/plugins/'.$module.'/controllers/'.$module.'_controller.php')) {
            include_once ROOT.'/application/plugins/'.$module.'/controllers/'.$module.'_controller.php';
            $plugin_name = ucfirst($module).'Controller';
            $plugin = new $plugin_name;
            if(method_exists($plugin,$sub)) {
                $plugin->{$sub}();
            } else {
                die('r');
            }

        }
        die;
    }
}