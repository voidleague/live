<?php
class ChatController extends AppController{
    
    function __construct() {
        $this->title = t('Chat');
    }
    
    function index() {
        if(isset($_POST['message']) && $_POST['message']) {
            $data = $this->security($_POST);
            $this->save($data);
        }
        
        $countChat = db_result(db_query("SELECT count(*) FROM `chat`"));
        // create pager
        $pager = $this->pager($countChat,15);
        
        $messages = array();
        $getChat = db_query("SELECT chat.*, ".PREFIX."lietotaji.lietotajvards AS username, ".PREFIX."lietotaji.photo AS avatar, ".PREFIX."lietotaji.country AS username_country,
                                    r.color AS username_group_color
                            FROM `chat`
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = chat.uid
                            LEFT JOIN `users_roles` ur ON ".PREFIX."lietotaji.id = ur.uid
                            LEFT JOIN `role` r ON r.rid = ur.rid
                            ORDER BY `date` DESC
                            LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        while($chat = db_fetch_object($getChat)) {
            $chat->text = htmlspecialchars($chat->text);
            if(empty($chat->avatar)) {
                $chat->avatar = 'noimage.png';
            }
            
            //operations
            $chat->operations = '';
            if(access('a')) {
                $chat->operations = '<a href="/chat/delete/'.$chat->id.'" class="confirm"><img src="/public/images/admin/status_notok.png" alt="" /></a>';
            }
            $messages[] = $chat;
        }
        
        // create form
        $form = array(); // empty
        if(User::$user->uid > 0) {
            $form = array(
                'message' => array(
                    '#type' => 'textarea',
                ),
                'submit' => array(
                    '#type' => 'submit',
                    '#value' => t('Add'),
                ),
            );
        }
        return $this->view('chat',array('pager'=>$pager,'messages'=>$messages,'form'=>$form));
    }
    
    
    private function save($data) {
        db_query("INSERT INTO `chat` (`text`,`date`,`uid`) VALUES ('".$data->message."','".time()."','".User::$user->uid."')");
        $this->setMessage(t('Chat message added'));
        $this->redirect('chat');
    }
    
    function delete() {
        $routes = arg();
        if(access('a') && is_numeric($routes[2])) {
            db_query("DELETE FROM `chat` WHERE `id` = '".$routes[2]."'");
            $this->redirect('chat');
        }
        else {
            die;
        }
    }
}
