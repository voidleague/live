<?php if(User::$user->uid > 0): ?>
    <div id="chat-user">
        <a href="/user/<?php print User::$user->username;?>/"><img src="/public/images/photo/small/<?php print User::$user->photo;?>"/></a>
        <br />
        <?php print getMember(User::$user->username); ?>
    </div>
    <div id="chat-form"><?php print render_form($variables['form']); ?></div>
    <div class="clear"></div>
<?php endif; ?>

    <div id="chat-messages">
        <?php foreach($variables['messages'] AS $message): ?>
            <div class="chat-message-div">
                    <div class="chat-user">
                        <a href="/user/<?php print $message->username;?>/"><img width="80" height="80" class="photo" src="/public/images/photo/small/<?php print $message->avatar;?>"/></a>
                        <br />
                        <?php print getMember($message->username,['color' => $message->username_group_color,'country'=>$message->username_country]); ?><br />
                    </div>
                    <div class="chat-message">
                        <span class="chat-time"><?php print laikaParveide($message->date); ?></span><br />
                        <?php print $message->text; ?>
                    </div>
                    <?php print $message->operations; ?>
                <div class="clear"></div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php print $variables['pager']; ?>