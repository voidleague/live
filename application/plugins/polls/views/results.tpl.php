<?php
$poll = $variables['poll'];
$colors =  array('#0099C6','#3366CC','#DC3912','#FF9900','#990099','#109618','#FF66CC','#663300');
?>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1.0', {'packages':['corechart']});
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            <?php foreach($variables['poll']->answers AS $answer) { ?>
            ["<?php print $answer->answer;?>", <?php print $answer->votes;?>],
            <?php } ?>
        ]);

        // Set chart options
        <?php if($variables['show_comments']) :?>
            // Set chart options
            var options = {
                     'width':720,
                     'height':300,
					 'is3D':true
		    };
        <?php else: ?>
            var options = {
                'width':320,
                'height':200,
                'legend': 'none',
                'is3D':true,
                'colors': ['<?=implode("','", $colors)?>'],
                'width': 320,
                'height': 200,
                'chartArea': { left: 10, top: 10, width:"320", height:"200" }
            };
        <?php endif; ?>
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
<div id="chart_div"></div>
<?php if(!$variables['show_comments']): ?>
<div id="poll_answers_list">
    <?php
    $answer_i = 0;
    foreach($variables['poll']->answers AS $answer) {
        print '<div class="answer-inline"><span style="background:'.$colors[$answer_i].';"></span> '.$answer->answer.'</div>';
        $answer_i++;
    }
    ?>
</div>
<?php endif; ?>
<?php
if($variables['show_comments']) {
   print comments('p',$poll->id,'poll');
} else {
    print '<a style="float:right;" href="/polls/results/'.$poll->id.'">'.t('comments').' ('.$variables['comments'].')</a>';
}
?>