<?php
class PollsController extends AppController {
    function __construct() {
        $this->title = t('Polls');
    }
    public function index() {
        return $this->block(4);
    }

    public function results() {
        $args = arg();
        if(!empty($args[2]) && is_numeric($args[2])) {
            return $this->block($args[2],true);
        }
    }

    public function block($id=false,$comments=false) {
        $output = '';
        $where = ($id) ? 'WHERE `id` = '.$id : '';
        $poll = db_fetch_object(db_query("SELECT * FROM `polls` ".$where." ORDER BY `id` DESC LIMIT 1"));
        if(!empty($poll)) {
            $output .= '<h6>'.$poll->question.'</h6>';
            $voted = db_result(db_query("SELECT COUNT(*) FROM `polls_data` WHERE `uid` = '".User::$user->uid."' AND `poll_id` = '".$poll->id."'"));
            $answers = array();
            $getAnswers = db_query("SELECT * FROM `polls_answers` WHERE `poll_id` = '".$poll->id."' ORDER BY `id` ASC");
            while($getAnswer = db_fetch_object($getAnswers)) {
                $answers[] = $getAnswer;
            }
            $poll->answers = $answers;

            if($voted) {
                $comments_count = db_result(db_query("SELECT count(*) FROM ".PREFIX."comments WHERE type='p' AND nid = '".$poll->id."'"));
                return $output . $this->view('results',array('poll'=>$poll,'comments'=>$comments_count,'show_comments'=>$comments));
            }
            else {
                return $output . render_form($this->poll_form($poll));
            }
        } else {
            $this->redirect('/');
        }
    }

    public function poll_form($poll) {

        if(isset($_POST['answers']) && $_POST['answers']) {
            $data = $this->security($_POST);
            $data->poll_id = $poll->id;
            $this->poll_form_submit($data);
        }

        $poll_answers = array();
        foreach($poll->answers AS $answer) {
            $poll_answers[$answer->id] = $answer->answer;
        }

        $form = array(
            '#id' => 'poll',
            'answers' => array(
                '#type' => 'radios',
                '#options' => $poll_answers,
            )
        );
        return $form;
    }

    private function poll_form_submit($data) {
        db_query("INSERT INTO `polls_data` (`uid`,`poll_id`) VALUES ('".User::$user->uid."','".$data->poll_id."')");
        db_query("UPDATE `polls_answers` SET `votes`=votes+1 WHERE id = '".$data->answers."' LIMIT 1");
        $this->setMessage(t('Thanks for your vote!'));
        $this->redirect('?');
    }
}