<?php
class RankController extends AppController {

    function __construct() {
        $this->title = t('Rank');
        $games = array();
        $getGames = db_query("SELECT * FROM ".PREFIX."speles ORDER BY `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $games[$getGame->tag] = $getGame;
        }
        $this->games = $games;

        $this->homepath = 't5on5';
        $this->links = array(
            't5on5' => t('5on5'),
            't3on3' => t('3on3'),
        );
        if(empty(Routes::$routes[1])) {
            Routes::$routes[1] = 'undefined';
        }
    }


    public function index() {
        $query = get_query_parameters();
        if(empty($this->games[Routes::$routes[1]])) {
            return '<div class="bx error">'.t('Sorry, but we could not find this game').'</div>';
        }

        if(empty($query->tab)) {
            $query->tab = 't5on5';
        }
        $output = '';
        if(isset($this->links)) {
            $output .= render_links($this->links);
        }

        if(isset($this->links[$query->tab])) {

            $regions = array();
            $getRegions = db_query("SELECT * FROM `gamesaccounts` WHERE `game` = '".Routes::$routes[1]."'");
            while($getRegion = db_fetch_object($getRegions)) {
                $regions[$getRegion->value] = htmlspecialchars($getRegion->name);
            }
            $this->regions = $regions;
            if(!empty($this->regions) && empty($query->region)) {
                $this->default_region = key($this->regions);
            }
            else {
                $this->default_region = mysql_real_escape_string($query->region);
            }
            //format
            if(empty($query->tab) || !empty($query->tab) && ($query->tab != 't5on5' && $query->tab != 't3on3')) {
              $query->default_format = '5on5';
            }
            else {

              $this->default_format = substr($query->tab,1);
           }
          // Rank page
          $items = array();
          $entities_count = db_result(db_query("SELECT count(*)
                                FROM `rank`
                                INNER JOIN `users_teams` ON users_teams.id = rank.entity_id
                                WHERE rank.game = '".Routes::$routes[1]."'
                                AND users_teams.status = 1
                                AND rank.type = 'team' AND users_teams.region = '".$this->default_region."'
                                AND users_teams.format = '".$this->default_format."'"));
          $pager = $this->pager($entities_count,30);
          $entities = db_query("SELECT rank.*, (win/(win+lost+draw)) AS win_procents, (win+lost+draw) AS games_played,
                                users_teams.slug AS team_slug, users_teams.id AS team_id, users_teams.name AS team_name,users_teams.country
                                FROM `rank`
                                INNER JOIN `users_teams` ON users_teams.id = rank.entity_id
                                WHERE rank.game = '".Routes::$routes[1]."'
                                AND rank.type = 'team'
                                AND users_teams.status = 1
                                AND users_teams.region = '".$this->default_region."'
                                AND users_teams.format = '".$this->default_format."'
                                ORDER BY rank.now ASC LIMIT ".$this->pager->from.",".$this->pager->perPage);
          $col = 0;
          while($entity = db_fetch_object($entities)) {
            $col++;
            if($col % 2 == 0) $entity->col = 'odd'; else $entity->col = 'even';
            if($entity->now > $entity->old) {
              $entity->rank_status = 'rank_down';
            }
            elseif($entity->now < $entity->old) {
              $entity->rank_status = 'rank_up';
            }
            else {
              $entity->rank_status = 'rank_neutral';
            }

            // W%
            $entity->win_procents = round($entity->win_procents*100,2).'%';
            $entity->label = '<img src="/public/images/flags/'.$entity->country.'.png" alt="" /> <a href="/team/id/'.$entity->team_slug.'-'.$entity->team_id.'">'.htmlspecialchars($entity->team_name).'</a>';
            $items[] = $entity;
          }
          $game = $this->games[Routes::$routes[1]];

          $regions = $this->regions;

          $output .= $this->view('list',array('items'=>$items,'label_type'=>'Team','pager'=>$pager,'game'=>$game,'regions'=>$regions));


        }
        return $output;
        // '<div class="bx info">Wow! Izskatās, ka drīz pēc kāda brītiņa te būs jauna sadaļa. Aktīvi strādājam, lai pēc iespējas ātrāk Jūs to ieraudzītu :)</div>';
    }
}