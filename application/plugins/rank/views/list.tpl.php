<?php $items = $variables['items'];  ?>
<h1 class="title"><a href="/rank/<?=$variables['game']->tag;?>/"><?=$variables['game']->title;?> <?php print t('Rank'); ?></a></h1>

<?php if(!empty($variables['regions'])) print render_links($variables['regions'],'region',false,'regions'); ?>
<?php if(count($items) > 0): ?>
    <table class="ipbtable rank">
        <thead>
        <tr class="head">
            <td class="rank-now"></td>
            <td class="rank-now2">#</td>
            <td class="rank-rating"><?php print t('Rating'); ?></td>
            <td class="rank-user"><?php print t($variables['label_type']); ?></td>
            <td class="rank-last_played"><?php print t('Last game');?></td>
            <td class="rank-games_played"><?php print t('Total games');?></td>
            <td class="rank-win"><?php print t('Wins');?></td>
            <td class="rank-lost"><?php print t('Loses');?></td>
            <?php if(!empty($game->can_draw)):?>
                <td class="rank-draw"><?php print t('Draws');?></td>
            <?php endif;?>
            <td class="rank-win_procentes"><?php print t('Win');?> %</td>
            <td class="rank-p10">P10</td>
        </tr>
        <thead>
        <tbody>
        <?php foreach($items AS $item): ?>
            <tr class="rank-member <?=$item->col;?>">
                <td class="rank-now"><?php if($item->rank_status != 'rank_neutral'): ?><img src="/public/images/<?=$item->rank_status;?>.png" alt="" /><?php endif; ?></td>
                <td class="rank-now2"><?=$item->now;?></td>
                <td class="rank-rating"><?=$item->rating;?></td>
                <td class="rank-user"><?=$item->label;?></td>
                <td class="rank-last_played"><?=date('d.m.Y',$item->last_played);?></td>
                <td class="rank-games_played"><?=$item->games_played;?></td>
                <td class="rank-win"><?=$item->win;?></td>
                <td class="rank-lost"><?=$item->lost;?></td>
                <?php if(!empty($item->can_draw)):?>
                    <td class="rank-draw"><?=$item->draw;?></td>
                <?php endif; ?>
                <td class="rank-win_procentes"><?=$item->win_procents;?></td>
                <td class="rank-p10"><?=$item->p10;?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php print $variables['pager']; ?>
<?php else: ?>
    <div class="bx info"><?php print t('No one has played this game match'); ?></div>
<?php endif; ?>

