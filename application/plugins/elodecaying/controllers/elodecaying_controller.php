<?php

use ORM;

/**
 * Class Elo_decaying_controller
 *
 * Removes lp from team when it haven't played in any tournament for a month
 * -50lp for month
 */

class ElodecayingController extends AppController {
    /**
     * How much LP need to remove
     * @var int
     */
    private $lp = 25;

    public function index() {
        // Get teams who haven't played more than month
        $teams = $this->getTeams();

        foreach($teams AS $team) {
            ORM::raw_execute("UPDATE `rank` SET `rating`=`rating`-:lp, `last_decay` = NOW() WHERE `entity_id` = :entity_id AND `type` = :type",[
                'lp' => $this->lp,
                'entity_id' => $team['entity_id'],
                'type' => 'team'
            ]);
        }

        echo 'LP decaying successful';
        exit;
    }


    /**
     * Get all teams who haven't decayed and played in last month
     *
     * @return mixed
     */
    private function getTeams() {
        return ORM::for_table('rank')->raw_query("SELECT
                                  r.*,
                                  t.name AS team_name
                                FROM
                                  `rank` r
                                INNER JOIN `users_teams` t ON t.id = r.entity_id
                                WHERE
                                  r.`type` = :type AND
                                  r.`rating` > :rating AND
                                  r.`last_played` < :last_played AND
                                  (r.`last_decay` < :last_decay OR r.`last_decay` IS NULL) AND
                                  t.`status` = 1 AND
                                  r.`format` != ''",[
            'type' => 'team',
            'rating' => 1300,
            'last_played' => strtotime('-1 month',time()),
            'last_decay' => date('Y-m-d H:i:s',strtotime('-1 month',time()))
        ])->find_array();
    }
}