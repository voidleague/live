<?php
class RegisterController extends AppController{
    
    function __construct() {
        $routes = arg();
        if($routes[0] == 'register' && empty($routes[1])) {
            $this->redirect('/'); // just for now
        }
        if(!empty(User::$user->uid)) {
            $this->redirect('/');
        }
        $this->title = t('Register');
    }
    
    function index($tooltip=false) {
        if(isset($_POST['register']) && $_POST['register']) {
            $data = $this->security($_POST);
            unset($data->register);
            try {
                $this->validate($data);
                $this->do_register($data); 
            } catch (Exception $error) {
                $this->setError($error->getMessage());
            } 
        }
        
        $register_form = array(
            'username' => array(
                '#type' => 'textfield',
                '#title' => t('Username'),
            ),
            'email' => array(
                '#type' => 'textfield',
                '#title' => t('E-mail'),
            ),
            'email_validate' => array(
                '#type' => 'textfield',
                '#title' => t('Repeat e-mail')
            ),
            'password' => array(
                '#type' => 'password',
                '#title' => t('Password'),
            ),
            'password_validate' => array(
                '#type' => 'password',
                '#title' => t('Repeat password'),
            ),
            'captcha' => array(
                '#type' => 'textfield',
            ),
            'register' => array(
                '#type' => 'submit',
                '#value' => t('Register'),
            )
        );
        
        return $this->view('index',array('register_form'=>$register_form));
    }
    
    
    private function do_register($data) {
        $data->crypted_password = pw($data->password);
        $data->salt = bin2hex(make_salt());
        
        db_query("INSERT INTO `".PREFIX."lietotaji` (`activated`,`registrejies`, `country`,`ip`,`lietotajvards`,`parole`,`epasts`,`salt`)
                VALUES
                (1, '".time()."','".get_country()."','".ip_address()."','".$data->username."','".$data->crypted_password."','".$data->email."','".$data->salt."')");

        $this->setMessage(t('You have successfully registered! Now you can log in'));
    }
    
    
    private function validate($data) {
        if(!empty($data->captcha)) {
            throw new error('GO AWAY EVIL!');
        }
        if(empty($data->username) || empty($data->email) || empty($data->email_validate) || empty($data->password) || empty($data->password_validate)) {
            throw new error(t('Please fill all fields'));
        }
        if(mb_strlen($data->username) < 4) {
            throw new error(t('Username need to be at least !count characters long',array('!count'=>4)));
        }
        if(mb_strlen($data->username) > 15) {
            throw new error(t('Username maximum length is !count characters',array('!count'=>15)));
        }
        if(!preg_match("/^[a-z0-9._-]+$/i",$data->username)) {
            throw new error(t('Username can contain only latin characters'));
        }
        if(strlen($data->password) < 6) {
            throw new error(t('Password need to be at least !count characters long',array('!count'=>6)));
        }
        if($data->password != $data->password_validate) {
            throw new error(t("Passwords do not match"));
        }
        if($data->email != $data->email_validate) {
            throw new error(t("E-mails do not match"));
        }
        if(!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            throw new error(t('Need to enter valid e-mail'));   
        }

        // Check if username or email is taken
        $usernameTaken = db_result(db_query("SELECT count(*) FROM ".PREFIX."lietotaji WHERE `lietotajvards` = '".$data->username."'"));
        $emailTaken = db_result(db_query("SELECT count(*) FROM ".PREFIX."lietotaji WHERE `epasts` = '".$data->email."'"));
        if($usernameTaken) {
            throw new error(t('Username already registered'));
	}
        if($emailTaken) {
            throw new error(t('E-mail already registered'));
	    }
        $last_ip = db_result(db_query("SELECT `ip` FROM ".PREFIX."lietotaji ORDER BY `id` DESC LIMIT 1"));
        if($last_ip == ip_address()) {
            //throw new error(t('You can\'t create account. Please contact administration'));
        }

    }
    
    function activation() {
        $this->title = t('Account activation');
        $routes = arg();
        if(empty($routes[2])) {
            $this->redirect('/');
        }
        $validSalt = db_result(db_query("SELECT `id` FROM ".PREFIX."lietotaji WHERE `activated` = 0 AND `salt` = '".mysql_real_escape_string($routes[2])."'"));
        if($validSalt) {
            db_query("UPDATE ".PREFIX."lietotaji SET `activated` = 1 WHERE `id` = '".$validSalt."'");
            $this->setMessage(t('Your account has been activated successfully'));
        }
        else {
            $this->setError(t('Invalid activation link or your account has already been activated'));
        }
    }
    
    function ajax_validate() {
        $data = $this->security($_POST);
        try {
            $this->validate($data);
            $this->do_register($data);
            print 'status_ok';
        }
        catch (Exception $e) {
            print $e->getMessage();
        }
        die;
    }
}
