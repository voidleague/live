<?php
class element {

	// create input field
	public function input($name,$type, $required=FALSE, $value=FALSE, $options=FALSE) {
		$errors = array();
	  // check if required
	  if(((!isset($_POST[$name]) || $_POST[$name] == false) AND $required==1 AND $_POST)) {
		$errors[$name] =1;
		$_SESSION['reg_error'] = true;
	  }
	  
	  // set attributes
		if(isset($errors[$name])==1) {
		  $error_class = ' error';
		}
		else {
		  $error_class = '';
		}
		$class = $name.$error_class;
	  
	  // set values
		if(isset($_POST[$name]) && $type=='textarea') {
			$value = $_POST[$name];
		}
		elseif(isset($_POST[$name]) AND !is_array($_POST[$name])) {
		//elseif(isset($_POST[$name]) AND $_POST[$name] AND !is_array($_POST[$name])) {
			$value = strip_tags($_POST[$name]);
			
		}
			elseif($_POST) { //array :: name[subname]
			// get post name
			$array_name = explode('[',$name); //perm
			$array_name2 = substr($array_name[1],0,-1); // u
			
			if($_POST[$array_name[0]][$array_name2]) {
				$value = 'on';
			}

		}
	  
	  
	  $output = '';
	  
	  if($type=='text') {
		$output = '<input type="text" name="'.$name.'" id="'.$name.'" value="'.$value.'" class="'.$class.'" />';
	  }
	  elseif($type=='password') {
		$output = '<input type="password" name="'.$name.'" id="'.$name.'" class="'.$class.'"/>';
	  }
	  elseif($type=='select') {
		$output = '<select name="'.$name.'" class="'.$class.'" id="'.$name.'"><option value="">- - - - - - - -</option>';
		  foreach($options AS $v=>$title) {
		  if(isset($_POST[$name]) AND $v==$_POST[$name] OR $v==$value) {
			$selected = 'selected="selected"';
		  }
		  else {
			$selected = '';
		  }
			$output .= '<option value="'.$v.'" '.$selected.'>'.$title.'</option>';
		  }
		$output .= '</select>';
	  }
	  elseif($type=='textarea') {
		$output = '<textarea name="'.$name.'" class="'.$class.'" id="'.$name.'">'.$value.'</textarea>';
	  }
	  elseif($type=='checkbox') {
		$checked=false;
		if(isset($value) && $value=='on') {
		  $checked = 'checked="checked"';
		}
		$output = '<input type="checkbox" name="'.$name.'" '.$name.' id="'.$name.'" '.$checked.'/>';
	  }
	  elseif($type=='submit') {
		$output = '<input type="submit" name="'.$name.'" value="'.$value.'" class="'.$name.'" id="'.$name.'" />';
	  }
	  elseif($type=='radio') {
		$checked=false;
		foreach($options AS $val=>$title) {
			if($val==$value) {
				$checked = 'checked="checked"';
			}
			$output .= '<input type="radio" name="'.$name.'" value="'.$val.'" class="'.$name.'" '.$checked.'>'.$title.'</option>';
		}
	  }
	  elseif($type=='file') {
			if($name=='image') {
			$output .= '<img src="/thumb.php?img=public/img/interviews/'.$value.'&max_w=100&max_h=100&crop=1" alt="" /><br />';
			}
			$output .= '<input type="file" name="'.$name.'" class="'.$name.'" id="'.$name.'" />';
	  }
	  

	  return $output;
	}
	
	// --<-- Parbaudam $_POST array'ja drosibu. Aizvacam html tagus, ja ir cipars, tad intval, citadi escape_string.
	public function security($array) {
	
	unset($array['submit']);
	unset($array['delete']);
	
		$return_data = array();
		foreach($array AS $key=>$value) {
			if(is_numeric($value)) {
				$value=intval($value);
			}
			elseif(is_array($value)) {
				foreach($value AS $subkey=>$val) {
					$value[$subkey] = mysql_real_escape_string(strip_tags($val));
				}
			}
			else {
				$value=mysql_real_escape_string(strip_tags($value));
			}
			$return_data[$key] = $value;
		}
		return (object)$return_data;
	}
	
	
	
	
	public function twitter() {
		return '<a href="http://twitter.com/share" class="twitter-share-button" data-count="none"><img src="/public/images/twitter.png" alt="" /></a>
				<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
	}
	
	public function facebook() {
		return '<div id="fb-root"><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:like href="http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'" send="false" layout="button_count" width="30" show_faces="no" font=""></fb:like></div>';
	}
	
	// shows how much registered users have readed this page (news,interview,article,etc)
	public function views($article_id,$type) {
		$article_id = intval($article_id);
		$type = mysql_real_escape_string(strip_tags($type));
		$ip = $_SERVER['REMOTE_ADDR'];
		$readed = db_result(db_query("SELECT count(*) FROM items_views WHERE ip='".$ip."' AND nid = '".$article_id."' AND type = '".$type."'"));
		
		if($readed == 0) { // first time
			db_query("INSERT INTO items_views (`ip`,`nid`,`type`,`date`) VALUES ('".$ip."','".$article_id."','".$type."','".time()."')");
		}
		
		// get results
		$result = db_result(db_query("SELECT count(*) FROM items_views WHERE nid = '".$article_id."' AND type = '".$type."'"));
		return $result;
	}
	
	
	
	public function suggestUser() {
		$output = '<input id="inputString" class="suggestUser" name="suggestedUser" class="text" type="text" />
			<div class="suggestionsBox" id="suggestions" style="display: none;"><img src="/public/images/upArrow.png" class="upArrow" />
				<div class="suggestionList" id="autoSuggestionsList"></div>
			</div>';
		return $output;
	}
}

$element = new element;
