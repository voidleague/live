<?php
class Youtube
{
	private $videoId;
	
	public function load($url)
	{
		$findme = 'youtube.com/watch?v=';
		
		$pos = strpos($url, $findme);		
		if ($pos === false)
		{
			$this->error = 'Not a valid link';
			return false;
		}
		$pos += strlen($findme);
		
		$pos2 = strpos($url, '&', $pos);
		if ($pos2 === false)
		{
			$pos2 = strlen($url);
		}
		
		$this->videoId = substr($url, $pos, $pos2 - $pos);
		return $this;
	}

	public function loadFromEmbed($url)
	{
		$findme = 'youtube.com/v/';
		
		$pos = strpos($url, $findme);		
		if ($pos === false)
		{
			$this->error = 'Not a valid link';
			return false;
		}
		$pos += strlen($findme);
		
		$pos2 = strpos($url, '&', $pos);
		if ($pos2 === false)
		{
			$pos2 = strpos($url, '"', $pos);
			if ($pos2 === false)
			{
				$pos2 = strlen($url);
			}
		}
		
		$this->videoId = substr($url, $pos, $pos2 - $pos);
		return $this;	
	}
	
	private function hasVideoId()
	{
		if (!isset($this->videoId))
		{
			$this->error = 'Load video first';
			return false;
		}
		return true;		
	}
		
	public function getEmbed($html = false, $width = 425, $height = 344)
	{
		if (!$this->hasVideoId())
		{
			return false;
		}
		
		$link = '//www.youtube.com/v/' . $this->videoId;
		if (!$html)
		{
			return $link;
		}
		
		return	'<object width="' . $width . '" height="' . $height . '">' .
				'<param name="movie" value="' . $link . '"></param>' .
				'<param name="allowFullScreen" value="true"></param>' .
				'<param name="allowscriptaccess" value="always"></param>' .
				'<embed src="' . $link . '" type="application/x-shockwave-flash" '.
				'allowscriptaccess="always" allowfullscreen="true" ' .
				'width="' . $width . '" height="' . $height . '"></embed></object>';
	}
	
	public function getPhoto()
	{
		if (!$this->hasVideoId())
		{
			return false;
		}
		
		return '//i1.ytimg.com/vi/' . $this->videoId . '/0.jpg';
	}
	
	public function getThumbnail($num = 0)
	{
		if (!$this->hasVideoId())
		{
			return false;
		}
		
		if ($num < 1) $num = 'default';
		if ($num > 3) $num = 3;
		return '//i1.ytimg.com/vi/' . $this->videoId . '/' . $num . '.jpg';
	}
	
	public function getThumbnails()
	{
		if (!$this->hasVideoId())
		{
			return false;
		}
		
		$result = array();
		for ($i=0; $i<4; $i++)
		{
			$result[] = $this->getThumbnail($i);
		}
		return $result;
	}
}

class video {
    private $videoId;
    public function rating() {
        global $lietotajs;
        global $video;
        $voted = mysql_result(db_query("SELECT count(id) FROM ".PREFIX."videos_rating WHERE uid = '".$lietotajs['id']."' AND vid = '".$video->vid."'"),0);
        if($voted==0 AND $lietotajs['id']) {
            $output  = '<form method="POST">';
            $output .= '<input type="submit" name="vote_up" class="vote-up-button" value="1" readonly /> ';
            $output .= '<input type="submit" name="vote_down" class="vote-down-button" value="1" readonly /> ';
            $output .= '</form>';
            $output .= '<span class="video-desc">'._('You can rate this video').'</span>';
            
        }
        elseif(!$lietotajs['id']) {
            $output = '<span class="video-desc">'._('Need to login to rate video!').'</span>';
        }
        else {
            $output = '<span class="video-desc">'._('You have already rated this video!').'</span>';
        }
    
        return $output;
    }
    
    function stats() {
    global $video;
        if($video->rating > 0) {
           $class='up';
           $stats = '+'.$video->rating;
        }
        elseif($video->rating < 0) {
          $stats = $video->rating;
          $class='belove';
        }
        else {
          $stats=0;
        }
        $output = '<span class="icon"></span><span class="count '.$class.'">'.$stats.'</span>';
        return $output;
    }
    
    function edit() {
    global $lietotajs;
    global $video;
        if($lietotajs['id'] == $video->uid OR admin('a') OR admin('b')) {
            $output  = '<div id="video-edit">';
            $output .= '<a href="/video/edit/'.$video->vid.'/">'._('Edit video').'</a>';
            $output .= '</div>';
        }
        return $output;
    }
}
?>