<?php

class Ranking {
	public function _reorder($game,$type='user',$region = '',$format = '') {
        if($type == 'user') {
            // [DEV] nevajag palaist katru reizi pec rezultata
            db_query("UPDATE `rank` SET `old` = `now` WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format." AND `status` = 1'");
            db_query("SET @a = 0");
            db_query("UPDATE `rank` SET `now` = @a:=@a+1 WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1 ORDER BY `rating` DESC, `entity_id` ASC");
        }
        else {
            // [DEV] nevajag palaist katru reizi pec rezultata
            db_query("UPDATE `rank` SET `old` = `now` WHERE `game` = '".$game."'AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1");
            db_query("SET @a = 0");
            db_query("UPDATE `rank` SET `now` = @a:=@a+1 WHERE `game` = '".$game."' AND `type` = '".$type."' AND `region` = '".$region."' AND `format` = '".$format."' AND `status` = 1 ORDER BY `rating` DESC, `entity_id` ASC");
        }
	}
	
	// calculate rating
	private function _calculate($S1,$S2,$R1,$R2)  {
		//if (empty($S1) || empty($S2) || empty($R1) || empty($R2)) return null;
        if ($S1 != $S2) {
            if ($S1 > $S2) {
                $E = 60 - round(1 / (1 + pow(10, ( ($R2 - $R1) / 400 ))) * 60);
                $R['R3'] = $R1 + $E;
                $R['R4'] = $R2 - $E;
            } else {
                $E = 60 - round(1 / (1 + pow(10, ( ($R1 - $R2) / 400 ))) * 60);
                $R['R3'] = $R1 - $E;
                $R['R4'] = $R2 + $E;
            }
        } else {
            if ($R1 == $R2) {
                $R['R3'] = $R1;
                $R['R4'] = $R2;
            } else {
                if($R1 > $R2) {
                    $E = 60 - round(1 / (1 + pow(10,( ($R1 - $R2) / 400 ))) * 60) - (60 - round(1 / (1 + pow(10, ( ($R2-$R1)/400 ))) * 60));
                    $R['R3'] = $R1 - $E;
                    $R['R4'] = $R2 + $E;
                } else {
                    $E = 60 - round(1 / (1 + pow(10, ( ($R2-$R1) / 400 ))) * 60) - (60 - round(1 / (1 + pow(10, ( ($R1-$R2) / 400))) * 60));
                    $R['R3'] = $R1 + $E;
                    $R['R4'] = $R2-$E;
                }
            }
        }
        
        $R['S1'] = $S1;
        $R['S2'] = $S2;
        $R['R1'] = $R1;
        $R['R2'] = $R2;
        
        $R['P1'] = (($R['R3'] - $R['R1']) > 0) ? "+".($R['R3'] - $R['R1']) : ($R['R3'] - $R['R1']);
        $R['P2'] = (($R['R4'] - $R['R2']) > 0) ? "+".($R['R4'] - $R['R2']) : ($R['R4'] - $R['R2']);
    
        $R['P1'] = round($R['P1'] / 2, 0);
        $R['P2'] = round($R['P2'] / 2, 0);
    
        if($R['P1'] == 0 || $R['P2'] == 0) {
            $R['P1'] = 1;
            $R['P2'] = -1;
        }
        $R['P2'] = abs($R['P2']);
		
		
        return $R;
	}
	
	private function _check($entity_id,$game,$type='user') {
		$check = db_fetch_object(db_query("SELECT count(*) AS exist FROM `rank` WHERE `game` =  '".$game."' AND entity_id = '".$entity_id."' AND `type` = '".$type."'"));
		if(isset($check->exist) && $check->exist > 0) {
			return true;
		}
		return false;
	}
	
	private function _addFirstTime($entity_id,$game,$type='user') {
        // add region
        if($type == 'team') {
            $team = db_fetch_object(db_query("SELECT `region`,`uid`,`format` FROM `users_teams` WHERE `id` = '".$entity_id."'"));
            if(!empty($team)) {
              $region = $team->region;
              $format = $team->format;
              $status = ($team->uid > 0) ? 1 : 0;
            }
            else {
              $region = '';
              $format = '';
              $status = '';
            }
        }
        else {
            $region = db_result(db_query("SELECT `region` FROM `users_gameaccount` WHERE `id` = '".$entity_id."'"));
            $format = '1on1';
        }

		db_query("INSERT INTO `rank` (`status`, `format`,`region`,`entity_id`,`game`,`rating`,`type`) VALUES ('".$status."','".$format."','".$region."','".$entity_id."','".$game."',1500,'".$type."')");
	}
	private function _getAllData($entity_id,$game,$type='user') {
		return db_fetch_object(db_query("SELECT * FROM `rank` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."'"));
	}
	
	private function _p10($entity_id,$entity_id_score,$entity_id2,$entity_id2_score,$game,$type='user') {
		if($entity_id_score > $entity_id2_score) {
			$status = 'W';
		}
		elseif($entity_id_score < $entity_id2_score) {
			$status = 'L';
		}
		
		db_query("INSERT INTO `rank_p10` (`entity_id`,`game`,`status`,`type`) VALUES ('".$entity_id."','".$game."','".$status."','".$type."')");
		/*
        $p10_count = db_fetch_object(db_query("SELECT count(*) AS last_games FROM `rank_p10` WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."' AND `type` = '".$type."'"));
		if($p10_count->last_games > 12) {
			db_query("DELETE FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."' ORDER BY `id` ASC LIMIT 1");
		}
		*/
		
		// count p10
		$p10 = db_fetch_object(db_query("SELECT count(*), 
								(SELECT count(*) FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `status` = 'W' AND `type` = '".$type."') AS win,
								(SELECT count(*) FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `status` = 'L' AND `type` = '".$type."') AS lost
								FROM `rank_p10` WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."'"));
		$status = 1;
        if($type == 'team') {
          $uid = db_result(db_query("SELECT `uid` FROM `users_teams` WHERE `id` = '".$entity_id."'"));
          if(empty($uid)) {
            $status = 0;
          }
        }
		db_query("UPDATE `rank` SET `status` = '".$status."', `p10` = '".$p10->win."-".$p10->lost."' WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."'");
		$p10 = $p10->win.'-'.$p10->lost;
		return $p10;
	}
	
	public function now($entity_id,$game,$type='user') {
		$rank = db_fetch_object(db_query("SELECT `now` FROM `rank` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."'"));
		if(isset($rank->now)) {
			return $rank->now;
		}
		else {
			return '-';
		}
	}
	
	public function remove($entity_id,$entity_id_rating, $entity_id2, $entity_id2_rating, $game,$type='user') {
		$entity_id_rating = '+'.$entity_id_rating;
        db_query("DELETE FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id."' AND `type` = '".$type."' ORDER BY id DESC LIMIT 1");
        db_query("DELETE FROM `rank_p10` WHERE `game` = '".$game."' AND `entity_id` = '".$entity_id2."' AND `type` = '".$type."' ORDER BY id DESC LIMIT 1");
		db_query("UPDATE `rank` SET `rating` = `rating`".$entity_id2_rating." WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."' AND `type` = '".$type."'");
		db_query("UPDATE `rank` SET `rating` = `rating`".$entity_id_rating." WHERE `entity_id` = '".$entity_id2."' AND `game` = '".$game."' AND `type` = '".$type."'");
	}
	// Add rank to database
	public function add($entity_id,$entity_id_score,$entity_id2,$entity_id2_score,$game,$type='user',$created=false, $double) {
		$data = new stdClass();
		$data->entity_id1 = new stdClass();
		$data->entity_id2 = new stdClass();
		if(!$this->_check($entity_id,$game,$type)) {
			$this->_addFirstTime($entity_id,$game,$type);
		}
		if(!$this->_check($entity_id2,$game,$type)) {
			$this->_addFirstTime($entity_id2,$game,$type);
		}
		
		$data->entity_id1 = $this->_getAllData($entity_id,$game,$type);
		$data->entity_id2 = $this->_getAllData($entity_id2,$game,$type);
		
		// p10
		$data->entity_id1->p10 = $this->_p10($entity_id,$entity_id_score,$entity_id2,$entity_id2_score,$game,$type);
		$data->entity_id1->p10 = $this->_p10($entity_id2,$entity_id2_score,$entity_id,$entity_id_score,$game,$type);
		// draw
		if($entity_id_score == $entity_id2_score) {
			$data->entity_id1->draw = $data->entity_id1->draw+1;
			$data->entity_id2->draw = $data->entity_id2->draw+1;
		}
		else {
			$data->entity_id1->win = $data->entity_id1->win+1;
			$data->entity_id2->lost = $data->entity_id2->lost+1;
				
			$rating = $this->_calculate($entity_id_score,$entity_id2_score,$data->entity_id1->rating,$data->entity_id2->rating);

            // Loser bracket gets 50% off elo
            if ($double) {
                $rating['P1'] = round($rating['P1'] / 2, 0);
                $rating['P2'] = round($rating['P2'] / 2, 0);
            }
			$data->entity_id1->rating = intval($data->entity_id1->rating)+$rating['P1'];
			$data->entity_id2->rating = intval($data->entity_id2->rating)-$rating['P2'];
			
		}

        $last_played = (!empty($created) ? $created : time());
		db_query("UPDATE `rank` SET `rating` = '".$data->entity_id1->rating."', `last_played` = '".$last_played."', `win` = '".$data->entity_id1->win."',`lost` = '".$data->entity_id1->lost."',`draw` = '".$data->entity_id1->draw."' WHERE `entity_id` = '".$entity_id."' AND `game` = '".$game."' AND `type` = '".$type."'");
		db_query("UPDATE `rank` SET `rating` = '".$data->entity_id2->rating."', `last_played` = '".$last_played."', `win` = '".$data->entity_id2->win."',`lost` = '".$data->entity_id2->lost."',`draw` = '".$data->entity_id2->draw."' WHERE `entity_id` = '".$entity_id2."' AND `game` = '".$game."' AND `type` = '".$type."'");

		return array(
			'P1'=>'+'.$rating['P1'],
			'P2'=>'-'.$rating['P2'],
		);
	}
	
	
	
	public function getRating($entity_id,$game,$type='user') {
		$check = db_fetch_object(db_query("SELECT `rating` FROM `rank` WHERE `game` =  '".$game."' AND entity_id = '".$entity_id."' AND `type` = '".$type."'"));
        if(isset($check->rating)) {
			return $check->rating;
		}
		return 0;
	}
}
