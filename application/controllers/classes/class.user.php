<?php

class User {
  public static $user;
}

  function getMember($username, $options=false)
  {

  $username_display =  '<a href="/user/'.$username.'/" '.(!empty($options['color']) ? 'style="color:#'.$options['color'].'; font-weight:bold;"' : '').'>'.$username.'</a>';

    if(!empty($options['country'])) {
        $username_display = '<img src="/public/images/flags/'.strtolower($options['country']).'.png" alt="" /> '.$username_display;
    }
    return $username_display;
  }

  function getMemberPhoto($photo,$username,$width,$height,$crop=false)
  {
        if(!$photo)
        {
        $image = 'public/images/noimage.png';
        }
        else
        {
        $image = 'public/images/photo/small/'.$photo;
        }
        
  $output = '<img src="/'.$image.'" width="80" alt="'.$username.'" hspace="0" vspace="0" />';
  return $output;
  }


function send_mail($uid,$title,$body) {
  $title = mysql_real_escape_string($title);
  $body = mysql_real_escape_string($body);
  db_query("INSERT INTO ".PREFIX."vestules (kam_id,noka_id, vestules_nosaukums, vestules_teksts,vestules_laiks) VALUES ('".$uid."','0000','".$title."','".$body."','".time()."')");
  return ''; //def
}


function user_access($permission) {
    if(empty(User::$user->uid)) {
        return false;
    }
    if(admin() || !empty(User::$user->permissions[$permission])) {
        return true;
    }
    return false;
}

function user_role($role) {
    if(in_array($role,User::$user->roles)) {
        return true;
    }
    return false;
}
?>