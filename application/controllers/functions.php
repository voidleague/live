<?php

/**
 * Convert BR tags to nl
 *
 * @param string The string to convert
 * @return string The converted string
 */
function br2nl($string)
{
    //$string = preg_replace('/<br><br>/', "", $string);
    //$string = preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
    return $string;
}



function status($status) {
    $status = ($status == 1)? 'ok' : 'notok';
    return '<img src="/public/images/admin/status_'.$status.'.png" alt="" />';
}

function l($title,$src,$options=false) {
    $class = (!empty($options['attributes']['class'])) ? ' class="'.implode(' ',$options['attributes']['class']).'"' : '';
    return '<a href="/'.$src.'"'.$class.'>'.$title.'</a>';
}
function otherDiffDate($created, $out_in_array=false){

    $intervalo = date_diff(date_create(),date_create($created));
    $output = '';
    $years = $intervalo->format('%y');
    $months = $intervalo->format('%m');

    if(!empty($years)) {
        $output .= $intervalo->y.' '.t('year'). ($months > 1 ? 's' : '').', ';
    }
    if(!empty($months)) {
        $output .= $intervalo->m.' '.t('month'). ($months > 1 ? 's' : ''). ($years < 1 ? ', ' : '').' ';
    }
    $days = $intervalo->format('%d');
    if(!empty($days) && $years<1) {
        $output .= $intervalo->d.' '.t('day'). ($months > 1 ? 's' : '').' ';
    }

    return $output;

}


function time_left( $integer, $full = false ) {
    $return = "";
    if($integer<1) { $return = "Expired"; $integer = 0; }
    if($full) {
        $w = " week";
        $h = " hour";
        $d = " day";
        $m=" minute";
        $s=" second";
    }
    else {
        $w = "w";
        $h = "h";
        $d = "d";
        $m = "m";
        $s = "s";
    }
    $seconds = $integer;
    $minutes = floor( $seconds / 60 );
    if ( $seconds / 60 >= 1) {
        if ( $minutes / 60 >= 1 ) {
            # Hours
            $hours = floor( $minutes / 60 );
            if ( $hours / 24 >= 1) {
                #days
                $days=floor($hours/24);
                if ($days/7 >=1) {
                    #weeks
                    $weeks = floor($days/7);
                    if ( $weeks > 1 ) $return = "{$weeks}{$w}s";
                    else if ( $weeks == 1 ) $return = "{$weeks}$w";
                } #end of weeks
                $days = $days - ( floor ( $days / 7 ) ) * 7;
                if ( isset($weeks) && $weeks >= 1 && $days >= 1 ) $return="$return ";
                if ( $days >1) $return="$return {$days}{$d}s";
                elseif ( $days == 1 ) $return="$return {$days}$d";
            } #end of days
            $hours = $hours - ( floor ( $hours / 24 ) ) * 24;
            if ( isset($days) && $days >= 1 && $hours >=1 ) $return="$return ";
            if ( $hours >1) $return="$return {$hours}{$h}s";
            elseif ( $hours == 1 ) $return="$return {$hours}$h";
        } #end of Hours
        $minutes=$minutes-(floor($minutes/60))*60;
        if (isset($hours) && $hours >= 1 && $minutes >= 1) $return="$return";
        if ($minutes > 1) {
            if(!empty($return)) $return = $return.', ';
            $return = "$return {$minutes}{$m}s";
        }
        elseif ($minutes == 1) $return = "$return {$minutes}$m";
    } #end of minutes
    if( !$return ) { // if empty display seconds
        $seconds = $integer-(floor($integer/60))*60;
        if ( $minutes >= 1 && $seconds >=1 ) $return = "$return ";
        if ( $seconds >1 ) $return = "$return {$seconds}{$s}s";
        elseif ( $seconds =1 ) $return = "$return {$seconds}$s";
    }
    return $return;
}


function array_sort_custom($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

class error extends Exception {}

function countries() {
    $countries = array();
    $getCountries = db_query("SELECT * FROM `countries` ORDER BY `name` ASC");
    while($country = db_fetch_object($getCountries)) {
        $countries[$country->code] = $country->name;
    }
    return $countries;
}

function languages() {
    $languages = array();
    $getLanguages = db_query("SELECT * FROM `languages` WHERE `enabled` = 1 ORDER BY `weight` ASC");
    while($language = db_fetch_object($getLanguages)) {
	$languages[$language->language] = $language;
    }
    return $languages;
}

function get_timezones() {
    $timezones = array();
    $getTimezones = db_query("SELECT * FROM `timezones` ORDER BY `name` ASC");
    while($timezone = db_fetch_object($getTimezones)) {
        $timezones[$timezone->timezone] = $timezone->name;
    }
    return $timezones;
}

function highlight($highlight,$string) {
    return str_replace($highlight,'<span class="highlight">'.$highlight.'</span>',$string);
}

function getFileExtension($filename){
    return substr($filename, strrpos($filename, '.'));
}

function ip_address() {
  return $_SERVER['REMOTE_ADDR'];
}


function get_query_parameters() {
    $args = $_GET;
    unset($args['q'], $args['submit']);
    return (object)$args;
}

function render_links($links,$path=false,$primary=false,$class='', $params = false) {
    if($path==false){
        $path = 'tab';
    }

    $query = get_query_parameters();
    $output = '<ul class="menu tabs '.$class.'">';
    $link_i = 1;
    $args = arg();

    if($params) {
        $params = [];
        foreach($query AS $key=>$val) {
            $params[$key] = $key.'='.$val;
        }
        $params = '?'.implode('&amp;',$params);
    }
    foreach($links AS $link_src=>$link_title) {
        if($primary) {
            $active = (isset($args[1]) && $args[1] == $link_src || empty($args[1]) && $link_i == 1) ? 'active' : '';
            $src = '/'.arg(0).'/'.$link_src;
            $link = $src;

            if($params) {
                $link .= $params;
            }
        }
        else {
            $active = (isset($query->{$path}) && $query->{$path} == $link_src || empty($query->{$path}) && $link_i == 1) ? 'active' : '';
            $link = array();
            //$query->{$path} = $val;

            foreach($query AS $key=>$val) {
                $link[$key] = $key.'='.$val;
            }
            $link[$path] = $path.'='.$link_src;

            //$link[] = $path.'='.$link_src;

            $link = '?'.implode('&amp;',$link);
        }

	$output .= '<li class="link-'.$link_i.' '.$active.'"><a href="'.$link.'" class="'.$active.'" title="'.strip_tags($link_title).'">'.$link_title.'</a></li>';
	$link_i++;
    }
    $output .= '</ul>';
    return $output;
}

function render_form($form,$children=false,$params =[]) {

    if(empty($params['method'])) {
        $params['method'] = 'POST';
    }
	$output = '';
	if($children==false) {
        $form_id = (isset($form['#id'])) ? 'id="'.$form['#id'].'"' : '';
	    $output .= '<form action="'.(!empty($form['#action']) ? $form['#action'] : '').'" method="'.$params['method'].'" '.$form_id.' enctype="multipart/form-data" autocomplete="off">';
	}
	foreach($form AS $field_key=>$field) {
        $class = '';
        if(!empty($field['#attributes']['class'])) {
            $class = $field['#attributes']['class'];
        }
	    if(is_array($field)) {
            if($children) {
            $field_key = $children.'['.$field_key.']';
            }
            $wrapper_class = (isset($field['#type'])) ? $field['#type'] : '';
            if($wrapper_class == 'button') {
                $wrapper_class = 'buttons';
            }
            $output .= '<div class="form-item-div '.$wrapper_class.' form-field-'.$field_key.'">';


            $default_value = (isset($field['#default_value'])) ? $field['#default_value'] : '';
            if(isset($_POST[$field_key])) {
             $default_value = $_POST[$field_key];
            }
            if($field_key == 'email' || $field_key == 'password' || $field_key == 'keyword') {
                $default_value = htmlentities(str_replace('"','',$default_value));
            }

            if(isset($field['#prefix'])) {
                $output .= '<div class="field-prefix">'.$field['#prefix'].'</div>';
            }
            if(isset($field['#type']) && $field['#type'] == 'image') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
                if(!empty($field['#default_value'])) {
                    $output .= '<div class="form-item-default_value"><img src="/'.$field['#path'] .'/'. $field['#default_value'].'" alt="" /></div>';
                }
                $output .= '<div class="form-item-field">
                                <div class="custom_file_upload image">
                                    <input type="text" class="file image" name="'.$field_key.'">
                                    <div class="file_upload">
                                        <input type="file" name="'.$field_key.'" class="form-filefield form-image item-'.$field_key.'" id="form-image-item-'.$field_key.'" />
                                    </div>
                                </div>';
                $output .= '</div>';
            }
            if(isset($field['#type']) && $field['#type'] == 'filefield') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }

                $output .= '<div class="form-item-field">
                                <div class="custom_file_upload">
                                    <input type="text" class="file" name="file_info">
                                    <div class="file_upload">
                                        <input type="file" name="'.$field_key.'" class="form-filefield item-'.$field_key.'" id="form-fielfield-item-'.$field_key.'" />
                                    </div>
                                </div>
                            </div>';
                $output .= '</div>';
            }

        if(isset($field['#type']) && $field['#type'] == 'select') {
            $output .= '<div class="form-item">';
            if(isset($field['#title'])) {
                $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
            }
            $disabled = (isset($field['#disabled']) && $field['#disabled'] == TRUE) ? 'disabled="disabled"' : '';
            $output .= '<div class="form-item-select">
                    <select '.$disabled.' name="'.$field_key.'" class="form-select item-'.$field_key.'" id="form-select-item-'.$field_key.'">';
            foreach($field['#options'] AS $option_key=>$option_label) {
            $active = (isset($field['#default_value']) && $field['#default_value'] == $option_key) ? 'selected="selected"' : '';
            $output .= '<option '.$active.' value="'.$option_key.'">'.$option_label.'</option>';
            }
            $output .= '</select></div>';

            $output .= '</div>';
        }


        if(isset($field['#type']) && $field['#type'] == 'radios') {
            $output .= '<div class="form-item">';
            if(isset($field['#title'])) {
                $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
            }

            $output .= '<div class="form-item-radios">';

            foreach($field['#options'] AS $option_key=>$option_label) {
            $active = (isset($field['#default_value']) && $field['#default_value'] == $option_key || !isset($field['#default_value']) && $option_key == 0) ? 'checked="checked"' : '';
            $output .= '<div class="form-option"><input type="radio" name="'.$field_key.'" '.$active.' value="'.$option_key.'">'.$option_label.'</option></div>';
            }
            $output .= '</select></div>';

            $output .= '</div>';
        }

        if(isset($field['#type']) && $field['#type'] == 'markup') {
            $output .= '<div class="form-item">';
            if(isset($field['#title'])) {
                $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
            }
            $output .= '<div class="form-item-field">';
            $output .= $field['#markup'];
            $output .= '</div></div>';

        }

        if(isset($field['#type']) && $field['#type'] == 'textarea') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
                $output .= '<div class="form-item-area">
                                <textarea name="'.$field_key.'" class="form-textarea item-'.$field_key.'" id="form-textarea-item-'.$field_key.'">'.$default_value.'</textarea>
                            </div>';


            if(isset($field['#format']) && $field['#format'] == 'full_html') {

                $output .= add_js('jQuery(function() {$(\'textarea[name="'.$field_key.'"]\').cleditor(); });','inline');
            }

            if(isset($field['#format']) && $field['#format'] == 'filtered_html') {

                $output .= add_js('jQuery(function() {$("#form-textarea-item-'.$field_key.'").cleditor({

                controls:
              "bold italic underline strikethrough | image link unlink",

                }); });','inline');
            }

            if(isset($field['#format']) && $field['#format'] == 'comments') {
                $output .= add_js('jQuery(function() {$("#form-textarea-item-'.$field_key.'").cleditor({

                controls:
              "bold italic underline strikethrough | image link unlink | champions spells items",

                }); });','inline');
            }
                $output .= '</div>';
            }

        if(isset($field['#type']) && $field['#type'] == 'fieldset') {
                $output .= '<fieldset>';
                if(isset($field['#title'])) {
                    $output .= '<legend>'.$field['#title'].':</legend>';
                }
            $output .= render_form($field['#children'],$field_key);
                $output .= '</fieldset>';
        }

            if(isset($field['#type']) && $field['#type'] == 'textfield') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
            $disabled = (isset($field['#disabled']) && $field['#disabled'] == TRUE) ? 'disabled="disabled"' : '';
                $output .= '<div class="form-item-field">';
            if(isset($field['#ajax'])) {
            $output .= '<div class="ajax-'.$field['#ajax'].'">';
            }
                $output .= '<input type="text" name="'.$field_key.'" '.$disabled.' value="'.$default_value.'" class="form-field item-'.$field_key.'" id="form-field-item-'.$field_key.'" />';
            if(isset($field['#ajax'])) {
                if(isset($field['#ajax_arguments'])) {
                    $output .= '<div class="ajax_arguments">'.implode(',',$field['#ajax_arguments']).'</div>';
                }
                $output .= '<div class="ajax-content-'.$field['#ajax'].'" style="display:none;"></div>';
            }
            if(isset($field['#ajax'])) {
            $output .= '</div>';
            }
            $output .= '</div>';
                $output .= '</div>';
            }


            if(isset($field['#type']) && ($field['#type'] == 'date' || $field['#type'] == 'datetime')) {
            if(empty($field['#format'])) {
            $dateFormat = 'yy-mm-dd';
            }
            else {
            $dateFormat = $field['#format'];
            }
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
            $disabled = (isset($field['#disabled']) && $field['#disabled'] == TRUE) ? 'disabled="disabled"' : '';
                $output .= '<div class="form-item-field">';
                $output .= '<input type="text" name="'.$field_key.'" '.$disabled.' value="'.$default_value.'" class="form-field item-'.$field_key.'" id="form-field-item-'.$field_key.'" />';
            $output .= '</div>';
                $output .= '</div>';
            // add datepicker
            //$output .= '<script>jQuery(function($){ $("#form-field-item-'.$field_key.'").datepicker(); });</script>';
            if($field['#type'] == 'datetime') {
            $js = '$("#form-field-item-'.$field_key.'").datetimepicker({ dateFormat: "'.$dateFormat.'", timeFormat: "HH:mm", defaultDate: "'.$default_value.'", value: "'.$default_value.'" });';
            }
            else {
            $js = '$("#form-field-item-'.$field_key.'").datepicker({ dateFormat: "'.$dateFormat.'", defaultDate: "'.$default_value.'", value: "'.$default_value.'" });';
            }
            $output .= '
    <script type="text/javascript">
        (function($) {
            $(document).ready(function(){
                $("#form-field-item-'.$field_key.'").datetimepicker({ dateFormat: "'.$dateFormat.'", timeFormat: "HH:mm", defaultDate: "'.$default_value.'", value: "'.$default_value.'" });
            });
        })(jQuery);
    </script>';
            }


            if(isset($field['#type']) && $field['#type'] == 'password') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
            $disabled = (isset($field['#disabled']) && $field['#disabled'] == TRUE) ? 'disabled="disabled"' : '';
                $output .= '<div class="form-item-password">';
                $output .= '<input type="password" name="'.$field_key.'" '.$disabled.' value="'.$default_value.'" class="form-field item-'.$field_key.'" id="form-field-item-'.$field_key.'" />';
            $output .= '</div>';
                $output .= '</div>';
            }

            if(isset($field['#type']) && $field['#type'] == 'checkbox') {
            $checked = '';
            if(isset($_POST[$field_key]) || $field['#default_value'] == 1) {
            $checked = 'checked="checked"';
            }

                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
                $output .= '<div class="form-item-checkbox">
                                <input type="checkbox" name="'.$field_key.'" '.$checked.' class="form-checkbox item-'.$field_key.'" id="form-checkbox-item-'.$field_key.'" />
                            </div>';
                $output .= '</div>';
            }


            if(isset($field['#type']) && $field['#type'] == 'checkboxes') {
                $output .= '<div class="form-item">';
                if(isset($field['#title'])) {
                    $output .= '<div class="form-item-title">'.$field['#title'].':</div>';
                }
                $output .= '<div class="form-item-checkboxes">';
                foreach($field['#options'] AS $key=>$val) {
                    $checked = (in_array($key,$field['#default_value'])) ? 'checked="checked"' : '';
                    $output .= '<div class="form-item-checkbox">';
                    $output .= '<input type="checkbox" name="'.$field_key.'['.$key.']" '.$checked.' class="form-checkbox item-'.$field_key.'-'.$key.'" id="form-checkbox-item-'.$field_key.'-'.$key.'" /> <label>'.$val.'</label>';
                    $output .= '</div>';
                }

                $output .= '</div>';
                $output .= '</div>';
            }

            if(isset($field['#type']) && $field['#type'] == 'submit') {
                $output .= '<div class="form-submit"><input type="submit" name="'.$field_key.'" class="form-submit-button '.$class.'" value="'.$field['#value'].'" /></div>';
            }

            if(isset($field['#type']) && $field['#type'] == 'hidden') {
                $output .= '<input type="hidden" name="'.$field_key.'" class="form-submit-button" value="'.$field['#value'].'" />';
            }


            if(isset($field['#sufix'])) {
                $output .= '<div class="field-sufix">'.$field['#sufix'].'</div>';
            }

            if(isset($field['#description'])) {
                $output .= '<div class="form-item-description">'.$field['#description'].'</div>';
            }
            if(isset($field['#allowed'])) {
                $output .= '<div class="form-item-allowed">'.t('Allowed extensions').': '.implode(',',$field['#allowed']).'</div>';
            }

            if(isset($field['#type']) && $field['#type'] == 'button') {
                $output .= '<div class="form-button"><button type="submit" name="'.$field_key.'" class="form-submit-button '.$class.'">'.$field['#value'].'</button></div>';
            }

        $output .= '</div>';
        }
    }
	$output .= '<div class="clear"></div>';
	
	if($children==false) {
	    $output .= '</form>';
	}


	return $output;
}


function slug($str) 
{ 
  $a = array('?', '?', '?', '?', 'Ä', 'Å', 'Æ', '?', '?', 'É', '?', '?', '?', '?', '?', '?', '?', '?', '?', 'Ó', '?', 'Õ', 'Ö', 'Ø', '?', '?', '?', 'Ü', '?', 'ß', '?', '?', '?', '?', 'ä', 'å', 'æ', '?', '?', 'é', '?', '?', '?', '?', '?', '?', '?', '?', 'ó', '?', 'õ', 'ö', 'ø', '?', '?', '?', 'ü', '?', '?', 'Ā', 'ā', '?', '?', 'Ą', 'ą', 'Ć', 'ć', '?', '?', '?', '?', 'Č', 'č', '?', '?', '?', '?', 'Ē', 'ē', '?', '?', 'Ė', 'ė', 'Ę', 'ę', '?', '?', '?', '?', '?', '?', '?', '?', 'Ģ', 'ģ', '?', '?', '?', '?', '?', '?', 'Ī', 'ī', '?', '?', 'Į', 'į', '?', '?', '?', '?', '?', '?', 'Ķ', 'ķ', '?', '?', 'Ļ', 'ļ', '?', '?', '?', '?', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', '?', '?', '?', 'Ō', 'ō', '?', '?', '?', '?', '?', '?', '?', '?', 'Ŗ', 'ŗ', '?', '?', 'Ś', 'ś', '?', '?', '?', '?', 'Š', 'š', '?', '?', '?', '?', '?', '?', '?', '?', 'Ū', 'ū', '?', '?', '?', '?', '?', '?', 'Ų', 'ų', '?', '?', '?', '?', '?', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?', '?'); 
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
  $str = str_replace($a, $b, $str); 
  return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),array('', '-', ''), $str)); 
} 

function twitter_addTweet($title, $message) {
	$consumerKey    = 'lwESIN8Y7rcaULaWk9dA4A';
	$consumerSecret = 'DStRXsaM7evoX0ZcWKBODrhIodFEaCDVSlC7LafHM';
	$oAuthToken     = '169006359-5gVOpxcASrIZwRdDfnBH3JJbzW3Lm917XWumQB1k';
	$oAuthSecret    = 'Ga5eZvJ4hMyRY9FtfVkFsLggLKWUNGrLIyvTr50Es';
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/controllers/twitter/twitteroauth.php');
	$tweet = new TwitterOAuth($consumerKey, $consumerSecret, $oAuthToken, $oAuthSecret);
	// post to twitter
	$statusMessage = $title. ' -> ' . $message;
	$result = $tweet->post('statuses/update', array('status' => $statusMessage));
}

function user_load($uid) {
	return db_fetch_object(db_query("SELECT *, lietotajvards AS name FROM ".PREFIX."lietotaji WHERE `id` = '".$uid."'"));
}

function arg($index = NULL, $path = NULL) {
  static $static_fast;
  if (!isset($path) && isset($_GET['q'])) {
    $path = trim($_GET['q'], '/');
  }
  if (!isset($arguments[$path])) {
    $arguments[$path] = explode('/', $path);
  }
  if (!isset($index)) {
    return $arguments[$path];
  }
  if (isset($arguments[$path][$index])) {
    return $arguments[$path][$index];
  }
}

	class Routes {
		public static $routes;
	}
	Routes::$routes = arg();


function adminlog($admins,$darbiba)
{
$SQL = "INSERT INTO ".PREFIX."adminlogs (admins,darbiba,datums) VALUES ('".mysql_real_escape_string($admins)."','".mysql_real_escape_string($darbiba)."','".time()."')";
mysql_query($SQL) or die(mysql_error());
}

function watchdog($darbiba, $admin = FALSE, $type = FALSE)
{
$SQL = "INSERT INTO ".PREFIX."adminlogs (admins,darbiba,datums, log_type) VALUES ('".mysql_real_escape_string($admins)."','".mysql_real_escape_string($darbiba)."','".time()."','".$type."')";
mysql_query($SQL) or die(mysql_error());
}

// --------<------------ comment form global function
// --------<------------ comment form global function
// --------<------------ comment form global function
// --------<------------ comment form global function
// --------<------------ comment form global function
function comments($type,$id,$page)
{

if($page =='poll') {
    $page = 'polls/results';
}
global $ielogojies;
global $lietotajs;
echo '<div class="clear"></div>
<div id="pieraksti" style="margin-top:20px;"><h3>'.t('Comments').'</h3><div class="clear"></div>';

$admin_erase = false;
$comment_count = 0;

      // ---<--- Admin staff
    if(access('a') OR access('p')) $admin_erase = true;
    if(isset($_POST['comment']) AND $admin_erase)
    {
    
        foreach($_POST as $c=>$c2)
        {
          foreach($c2 as $comment_id=>$c3)
          {
          db_query("DELETE FROM ".PREFIX."comments WHERE type = '".$type."' AND id = '".intval($comment_id)."'");
          if($page=='video') {
              echo '<script>window.location="/'.$page.'/i/'.$id.'/";</script>';
          }
          else {
              echo '<script>window.location="/'.$page.'/'.$id.'/";</script>';
          }
          
          }
        }
    }
    // --->--- Admin staff  

    db_query("DELETE FROM ".PREFIX."comments WHERE `uid` = 792");
$comments_db = mysql_query("SELECT ".PREFIX."comments.id, ".PREFIX."comments.date,".PREFIX."comments.uid, ".PREFIX."comments.comment, ".PREFIX."lietotaji.lietotajvards
                            FROM ".PREFIX."comments
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."comments.uid=".PREFIX."lietotaji.id
                            WHERE ".PREFIX."comments.nid = '".intval($id)."' AND ".PREFIX."comments.type = '".$type."' 
                            ORDER BY ".PREFIX."comments.id ASC");


    if($admin_erase) echo '<form method="POST" onsubmit="return confirm(\''.t('Delete?').'\')">';
    while($comment = mysql_fetch_assoc($comments_db))
    {
    $comment_count++;
    echo '
      <span>#'.$comment_count.'</span>
      <p class="niks">'.getMember($comment['lietotajvards']).'</p> 
      <span>'.laikaParveide($comment['date']).'</span>';
      if($admin_erase) echo '<span style="float:right;"><input type="submit" class="delete_comment" value="'.t('Delete').'" name="comment['.$comment['id'].']" /></span>';
      echo '<p class="teksts">'.htmlspecialchars($comment['comment']).'</p>';
    echo "<div class='clear'></div>";

    }
    if($admin_erase) echo '</form>';
// ---<---- no comments 
if($comment_count == 0) echo '<span style="color: #666;">'.t('There are no comments. You can be first!').'</span>';
// --->---- no comments 

echo '<div style="clear:both;"></div><br /><div style="border-bottom: 2px solid gray; color: gray; height: 13px;">'.t('Add comment').'</div><br />';

// ------<----- comment form
if(User::$user->uid)
{
if(isset($_POST['piev_kom']) AND !empty($_POST['komentars']))
{
$komentars = mysql_real_escape_string($_POST['komentars']);
$ip = mysql_real_escape_string($_SERVER['REMOTE_ADDR']);

$spam_sec = db_result(db_query("SELECT count(*) FROM ".PREFIX."comments WHERE `nid` = '".$id."' AND `uid` = '".User::$user->uid."' AND `date` > ".(time()-40)));
if($spam_sec > 0) {
    print '<div class="bx error">SPAM DETECTED</div>';
}
else {
    mysql_query("INSERT INTO ".PREFIX."comments (nid,uid,comment,`date`,`type`) VALUES ('".$id."','".$lietotajs['id']."','".$komentars."','".time()."', '".$type."')") or die(mysql_error());

          if($page=='video') {
              echo '<script>window.location="/'.$page.'/i/'.$id.'/";</script>';
          }
          else {
              echo '<script>window.location="/'.$page.'/'.$id.'/";</script>';
          }
}
}
$_POST  = NULL;
echo '<div style="clear:both; height: 1px; width: 100%;" id="comcom"></div>';
echo '<form method="POST" id="comments">
<textarea name="komentars" id="komentars">
</textarea>
<br/>
<input type="submit" name="piev_kom" class="submit" value="'.t('Add').'" />
</form>';
}
else
{
echo '<div class="bx info">'.t('Need to').' <a href="/login/">'.t('login').'</a> '.t('for ability to comment').'</div>';
}
echo '</div>';


// ------>----- comment form

return;
}
// -------->------------ comment form global function
// -------->------------ comment form global function
// -------->------------ comment form global function
// -------->------------ comment form global function
// -------->------------ comment form global function
// -------->------------ comment form global function







function avatar($user_id, $user_avatar, $w,$h)
{
$avatar = 1;
return $avatar;
}
function report_admins($username,$f1,$f2,$f3,$league_id,$info,$user_id)
{
mysql_query("UPDATE ".PREFIX."ligas_komandas SET warning=warning+1 WHERE lietotajs = '".$user_id."' AND ligas_id = '".$league_id."'") or die(mysql_error());
mysql_query("INSERT INTO ".PREFIX."ligas_warnings_history (l_id, u_id, time,info) VALUES ('".$league_id."','".$user_id."','".time()."','".$info."')") or die(mysql_error());
echo '<div class="error">'.$info.'</div>';
return;          
}
// online

class usersOnline {

	var $timeout = 600;
	var $count = 0;
	var $error;
	var $i = 0;
	
	function usersOnline () {
		$this->timestamp = time();
		$this->ip = $this->ipCheck();
		$this->new_user();
		$this->delete_user();
		$this->count_users();
	}
	
	function ipCheck() {
	/*
	This function will try to find out if user is coming behind proxy server. Why is this important?
	If you have high traffic web site, it might happen that you receive lot of traffic
	from the same proxy server (like AOL). In that case, the script would count them all as 1 user.
	This function tryes to get real IP address.
	Note that getenv() function doesn't work when PHP is running as ISAPI module
	*/
		if (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		}
		elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_X_FORWARDED')) {
			$ip = getenv('HTTP_X_FORWARDED');
		}
		elseif (getenv('HTTP_FORWARDED_FOR')) {
			$ip = getenv('HTTP_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_FORWARDED')) {
			$ip = getenv('HTTP_FORWARDED');
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$ip = $_SERVER['REMOTE_ADDR'];
		return $ip;
	}
	
	function new_user() {
		$insert = mysql_query ("INSERT IGNORE INTO `bliezamv3_online` SET `timestamp` = $this->timestamp, `ip` = '$this->ip' ON DUPLICATE KEY UPDATE `timestamp` = $this->timestamp");

	}
	
	function delete_user() {
		$delete = mysql_query ("DELETE FROM bliezamv3_online WHERE timestamp < ($this->timestamp - $this->timeout)");
		if (!$delete) {
			$this->error[$this->i] = "Unable to delete visitors";
			$this->i ++;
		}
	}
	
	function count_users() {
		if (count($this->error) == 0) {
			$count = (int)mysql_result ( mysql_query("SELECT COUNT( DISTINCT `ip` ) FROM `bliezamv3_online`"), 0);
			return $count;
		}
	}

}
//yolo

$visitors_online = new usersOnline();


//admin access


function array_explode($array,$seperator,$names)
{
	$return = array();
	
	foreach($array as $row)
	{
		$row = explode($seperator,$row);
		
		$temp_array = array();
		
		foreach($row as $key=>$cell)
		{
			$temp_array[$names[$key]] = $cell;
		}
		
		$return[] = $temp_array;
		unset($temp_array);
	}
	return $return;
}


function access($flag)
{
global $lietotajs;

  if(admin()) {
  return true;
  }
  elseif(isset($lietotajs['flags'])) {
    return strstr($lietotajs['flags'], $flag);
  }
  else {
    return false;
  }
  
}
function access2($flag,$flags=false)
{
    return strstr($flags, $flag);
}

function laikaParveide($s) { //Padodam UNIX timestampu
if($s) {
        if(strtotime('today') < $s) {
       
                //if(date('H', $s) >= 12 && date('H', $s) <= 17) {
                //        return 'Šodien '.date('H:i', $s);
                //}
                if(date('H', $s) >= 18 && date('H', $s) <= 23) {
                       return t('Today').'  '.date('H:i', $s);
                }
                /* elseif(date('H', $s) >= 00 && date('H', $s) <= 04) {
                        return 'Šonakt '.date('H:i', $s);
                }
                elseif(date('H', $s) >= 05 && date('H', $s) <= 11) {
                        return 'Šorīt '.date('H:i', $s);
                }
                */
         return _('Today').' '.date('H:i', $s);
        }
        elseif((strtotime('today') - 86400) < $s) {
                return t('Yesterday').' '.date('H:i', $s);
        }
       // elseif((strtotime('today') - 172800) < $s) {
        //        return 'Aizvakar '.date('H:i', $s);
       // }
        else {
                $menesis=array('01'=>'jan', '02'=>'feb', '03'=>'mar', '04'=>'apr', '05'=>'may', '06'=>'jun', '07'=>'jul', '08'=>'aug', '09'=>'sep', '10'=>'oct', '11'=>'nov', '12'=>'dec');
                return date('j', $s).'. '.$menesis[date('m', $s)].(date('Y') > date('Y', $s) ? ' '.date('Y', $s) : '').' '.date('H:i', $s);
        }
}
else {
  return '-';
}
}

// meklejam un raadam lapas saturu
 function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
 }


function bb2html($text)
{
  $bbcode = array(
                ";)",
                ":)",
                ";(",
                ":D",
                ":d",
                ":p",
                ":P",
                ":@",
                ":(",
                ":S",
                ":s",
                "(party)",
                "(y)",
                "(n)"
                );
  $htmlcode = array(
                "<img src='/public/images/smiles/wink.gif' alt=';)' title=';)' width='16' />",
                "<img src='/public/images/smiles/smile.gif' alt=':)' title=':)' width='16' />",
                "<img src='/public/images/smiles/cry.gif' alt=';(' title=';(' width='16' />",
                "<img src='/public/images/smiles/laugh.gif' alt=':D' title=':D' width='16' />",
                "<img src='/public/images/smiles/laugh.gif' alt=':d' title=':d' width='16' />",
                "<img src='/public/images/smiles/tongue.gif' alt=':p' title=':p' width='1x' />",
                "<img src='/public/images/smiles/tongue.gif' alt=':P' title=':P' width='16' />",
                "<img src='/public/images/smiles/angry.gif' alt=':@' title=':@' width='16' />",
                "<img src='/public/images/smiles/sad.gif' alt=':(' title=':(' width='16' />",
                "<img src='/public/images/smiles/worried.gif' alt=':S' title=':S' width='16' />",
                "<img src='/public/images/smiles/worried.gif' alt=':s' title=':s' width='16' />",
                "<img src='/public/images/smiles/party.gif' alt=':s' title=':s' width='16' />",
                "<img src='/public/images/smiles/yes.gif' alt='(y)' title='(y)' width='16' />",
                "<img src='/public/images/smiles/no.gif' alt='(n)' title='(n)' width='16' />"
                );
  $newtext = str_replace($bbcode, $htmlcode, $text);
  //$newtext = nl2br($newtext);//second pass
  return $newtext;
}

function forum_text($str)
{

$str = preg_replace( "#\[url\](?:http:\/\/)?(.+?)\[/url\]#is", "<a href=\"http://$1\">$1</a>", $str ); 
$str = preg_replace( "#\[img\](?:http:\/\/)?(.+?)\[/img\]#is", "<br /><img src=\"http://$1\" /><br />", $str ); 
$str = preg_replace( "#\[b\](.+?)\[/b\]#is", "<strong>$1</strong>", $str ); 
$str = preg_replace( "#\[i\](.+?)\[/i\]#is", "<i>$1</i>", $str ); 
$str = preg_replace( "#\[u\](.+?)\[/u\]#is", "<u>$1</u>", $str ); 

return $str;
}



// (c) defektologos.lv
function plain2click($str){
	$str = preg_replace_callback("/(http(s)?\:\/\/|www\.)[a-z0-9\-\.]+\.[a-z]{2,4}(\/[a-z0-9\-\?=_&\+%#@\.\/]*)?/i", 'plain2clickCallback', $str);
	$str = preg_replace("/[a-z0-9\-\._]+@[a-z0-9\-\.]+\.[a-z]{2,4}/i", '<a href="mailto:$0">$0</a>', $str);
	return $str;
}
 
function plain2clickCallback($matches){
	$ymatches = array();
	$url = str_replace(array('http://', 'https://'), '', $matches[0]);
	return '<a rel="nofollow" href="http'.$matches[2].'://'.$url.'" onclick="window.open(this.href); return false;">'.$matches[0].'</a>';
}

    function trashu_laiks($laiks)
    {
    if($laiks == '00:00:00') return '00:00';
    $sekundes = substr($laiks, -2);
    $minutes = substr($laiks, 3, -3);
    $laiks = $minutes.':'.$sekundes;
    return $laiks;
    }
    

function turnira_nosaukums($n)
{
$n = preg_replace("/_/"," ",$n);
return $n;
}

function addCoins($uid,$value,$text) {
    $diffValue = mb_substr($value,0,1);
    if($diffValue != '+' && $diffValue != '-') {
	$value = '+'.$value;
    }
    db_query("INSERT INTO ".PREFIX."monetu_vesture (lietotajs, skaits, teksts, datums) VALUES ('".$uid."','".$value."','".$text."','".time()."')");
    db_query("UPDATE ".PREFIX."lietotaji SET `coins` = coins".$value." WHERE `id` = '".$uid."'");
}

function coins_info($user,$value,$text)
{
mysql_query("INSERT INTO ".PREFIX."monetu_vesture (lietotajs, skaits, teksts, datums) VALUES ('".$user."','".$value."','".$text."','".time()."')");
}

function coins_info2($user,$value,$text,$admins)
{
mysql_query("INSERT INTO ".PREFIX."monetu_vesture (admins, lietotajs, skaits, teksts, datums) VALUES ('".$admins."', '".$user."','".$value."','".$text."','".time()."')");
}

function rating_info($user,$value,$text,$admins)
{
mysql_query("INSERT INTO ".PREFIX."reitinga_vesture (admins, lietotajs, skaits, teksts, datums) VALUES ('".$admins."', '".$user."','".$value."','".$text."','".time()."')");
}

// globaali eskeipojam
function h($vards)
{
$vards = htmlspecialchars($vards);
return $vards;
}
function tukss($vards)
{
$vards = trim($vards);
return $vards;
}


function lietotajs($id)
{
    $username =  mysql_result(db_query("SELECT lietotajvards FROM ".PREFIX."lietotaji WHERE id = '".intval($id)."'"),0);
    return getMember($username);
    return 'lietotajs()';
}

    function cut($text,$cik_burti) {

if(strlen($text) > $cik_burti+3)
{
$text = substr($text, 0, $cik_burti);
}
else
{
$text = substr($text, 0, $cik_burti);
}
return $text;
}

function error($teksts)
{
$err = '<div style="margin-bottom: 5px; background: #ff0000; padding-top: 5px; min-height: 20px; vertical-align:middle; text-align:center; color: #fff; font-weight:bold">'.$teksts.'</div>';
return $err;
}
function ok($teksts)
{
$err = '<div style="margin-bottom: 5px; background: #339900; padding-top: 5px; min-height: 20px; vertical-align:middle; text-align:center; color: #fff; font-weight:bold">'.$teksts.'</div>';
return $err;
}

function datums($laiks)
{
$kad = date('Y-m-d H:i',$laiks);
return $kad;
}


function give_xp($uid,$xp)
{
     db_query("UPDATE ".PREFIX."lietotaji SET `xp` = xp+".$xp." WHERE `id` = '".$uid."'");
}

function user_lvl($uid,$xp=false) {
    if(isset($xp)) {
        $xp = (int)$xp;
        return db_result(db_query("SELECT `name` FROM `xp_level` WHERE `xp` <= '".$xp."' ORDER BY `xp` DESC LIMIT 1"));
    }
}


function next_lvl($user_id) 
{
    $user_xp = mysql_result(mysql_query("SELECT xp FROM ".PREFIX."lietotaji WHERE id = '".$user_id."'"),0,0);
    $user_level = db_result(mysql_query("SELECT xp FROM ".PREFIX."xp_levels WHERE xp >= '".$user_xp."' ORDER BY xp ASC LIMIT 1"),0,0);  
    return $user_level;
}

?>
