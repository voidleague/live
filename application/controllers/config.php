<?php
$db_host = 'localhost';
$db_user = 'bliezam_bliezaml';
$db_password = '12qwaszx!@QWASZX';
$db_name = 'bliezam_bliezam';
$connection = mysql_connect($db_host, $db_user, $db_password) or die(mysql_error());
mysql_select_db($db_name, $connection);
mysql_query("SET NAMES utf8");
define("PREFIX", "bliezamv3_");

// ORM connection
\ORM::configure('mysql:host=localhost;dbname='.$db_name);
\ORM::configure('username', $db_user);
\ORM::configure('password', $db_password);
\ORM::configure('driver_options', [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
]);

// Define locations
define('GLOBAL_MEMCACHED_HOST','localhost');
define('GLOBAL_MEMCACHED_PORT','11211');