<?php
define("BaseUrl", $_SERVER['DOCUMENT_ROOT'].'/');
define("ROOT",$_SERVER['DOCUMENT_ROOT']);
define("debug",false); // Show queries and times on bottom of the page
define("APP", (dirname(dirname(dirname(__FILE__)))).'/application/');
$messages = '';
$content = '';


// Autoloads
require APP.'vendor/autoload.php';
require APP.'void.classes/autoload.php';

//---------------------<---------------- Controllers folder
include APP."controllers/classes/idiorm.php";

include APP."controllers/config.php";


include APP."controllers/functions.php";
include APP."controllers/core.php";
include APP."controllers/AppController.php";

include APP."controllers/login.php";

include APP."controllers/classes/class.elements.php";

include APP."controllers/classes/class.user.php";

include APP.'controllers/classes/rank.class.php';
require_once APP.'controllers/htmlpurifier/HTMLPurifier.auto.php';





$routes = arg();
//---------------------<---------------- User logged_in data
User::$user = new StdClass;
User::$user->uid = 0;
$lietotajs = array('id' => 0);

if(isset($_COOKIE['AUTH'])) {

    $result = checktoken($_COOKIE['AUTH'], $user_data);
    if ($result == 7) {

        $lietotajs = db_fetch_assoc(db_query("
			SELECT ".PREFIX."lietotaji.*,
			".PREFIX."lietotaji.dzimums AS sex, ".PREFIX."lietotaji.lietotajvards AS username,
			(SELECT COUNT(*) FROM `messages` WHERE `to` = ".PREFIX."lietotaji.id AND `status` = 0 AND `reciever_deleted` = 0) AS new_messages
			FROM ".PREFIX."lietotaji
			WHERE ".PREFIX."lietotaji.id = '".$user_data[0]."'"
        ));


        db_query("UPDATE ".PREFIX."lietotaji SET online = '".time()."', ip = '".$_SERVER['REMOTE_ADDR']."' WHERE id = '".$lietotajs['id']."'");

        $user = (object)$lietotajs;
        $user->uid = $user->id;
        $user->username = $user->lietotajvards;
        unset($user->id,$user->lietotajvards);

        // Get user groups
        if(empty($_SESSION['user']['permissions'])) {
            $user->roles = [];
            $user->permissions = [];
            $groups = db_query("SELECT role.* FROM `users_roles` LEFT JOIN `role` ON role.rid = users_roles.rid WHERE `uid` = '".$user->uid."' ORDER BY role.weight ASC");
            while($group = db_fetch_object($groups)) {
                $user->roles[$group->rid] = $group->name;
                $permissions = db_query("SELECT `permission` FROM `role_permission` WHERE `rid` = '".$group->rid."' ORDER BY `permission` ASC");
                while($permission = db_fetch_object($permissions)) {
                    $user->permissions[$permission->permission] = $permission->permission;
                }
            }
            $_SESSION['user']['permissions'] = $user->permissions;
        }
        $user->permissions = $_SESSION['user']['permissions'];

        // add User scoupe
        User::$user = $user;

    }

}


// Get user timezone for the first time
if(empty($_SESSION['user_timezone'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
    $res = geoip_record_by_name ($ip);
    $geoip_data = geoip_record_by_name($ip);
    $timezone = geoip_time_zone_by_country_and_region($geoip_data['country_code'],$geoip_data['region']);
    $_SESSION['user_timezone'] = $timezone;
    $_SESSION['user_country'] = $geoip_data['country_code'];
}


date_default_timezone_set($_SESSION['user_timezone']);
//-------------------->----------------- User logged_in data

// << banned by IP
$banned = db_fetch_object(db_query("SELECT * FROM `ip_bans` WHERE `ip` = '".ip_address()."'"));
if(isset($banned->id) && User::$user->uid != 1 && $routes[0] != 'login') {
    if($banned->perm == 1) {
        $banned->date = 'permanent';
    }
    print '<h1>Banned!</h1>';
    print 'Reason: '.$banned->reason.'<br />';
    print 'Date: '.$banned->date;
    die;
}



// >> banned by IP

$language = 'en';

if(isset($routes[0]) AND $routes[0] == 'lang' && !empty($routes[1]) && in_array($routes[1],$available_languages)) {
    setcookie("lang", $routes[1], time()+99999, "/");
    header('Location: /');
    die;
}

// << search
$search_form = '<div id="block-search-block"><form method="POST" autocomplete="off"><input type="text" name="keyword" /><input type="submit" name="search" value="'.t('Search').'" /></form></div>';
if(isset($_POST['search']) && $_POST['search']) {
    header('Location: /search?what=users&keyword='.$_POST['keyword']);
    die;
}

// >> search

$scripts = array();
$styles = array();

//--------------------<----------------- Include page (content)
$page_verified = false;

if(isset($routes[0]) && $routes[0]) {
    $page =  ROOT.'/application/views/pages/'.$routes[0].'.php';
    $page2 =  ROOT.'/application/views/pages/'.$routes[0].'s.php';
    if($routes[0] == 'admin' && user_access('access administration theme')) {
        include ROOT.'/administration/main.php';
    }
    elseif(file_exists($page)) {
        include $page;
    }
    // Pagaidam kamer ir vecais kods
    elseif(file_exists($page2) && User::$user->uid != 1) {
        include $page2;
    }
    elseif(isset($routes[0]) && file_exists(ROOT.'/application/plugins/'.$routes[0].'/controllers/'.$routes[0].'_controller.php')) {

        $plugin = new AppController($routes[0]);

        $title = $plugin->title;
        $content  = $plugin->content;

        if(!empty($plugin->breadcrumb)) {
            $breadcrumb = $plugin->breadcrumb;
        }
        if(!empty($plugin->styles)) {
            $styles = $plugin->styles;
        }
        if(!empty($plugin->scripts)) {
            $scripts = $plugin->scripts;
        }

        if(!empty($content)) {
            $page_verified = true;
        }

    }
    else {

        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
        header("Status: 404 Not Found");
        $_SERVER['REDIRECT_STATUS'] = 404;


        $title = 'NOT FOUND';
        $content = '<div class="notfound" style="margin: 0px auto; width: 400px; height:300px;"><img src="/public/img/404-not-found.gif" alt="" /></div>';
    }
}
else {
    include APP."views/pages/frontpage/index.php";
}

//-------------------->----------------- Include page (content)
if(empty($routes[0])) {
    $is_front = true;
}
else {
    $is_front = false;
}





// registration form
if($lietotajs['id'] < 1) {
    include_once ROOT.'/application/plugins/register/controllers/register_controller.php';
    $register_form = new RegisterController;
    $register_form = '<div class="register-form-side"><div class="content">'.$register_form->index().'<div class="label">'.t('Register').'</div></div></div>';
}

// Guider
$guider = '
<div id="guider">
<div class="guider-content">
<ul>
<li>Create a gameaccount here -> <a href="http://www.voidleague.com/myprofile/gameaccount">http://www.voidleague.com/myprofile/gameaccount</a></li>
<li>Tell your teammates to register and also create gameaccounts on VoidLeague</li>
<li>Create team here -> <a href="http://www.voidleague.com/myteam">http://www.voidleague.com/myteam</a></li>
<li>Invite your teammates here by clicking on edit button and inviting them by gameaccount names -> <a href="http://www.voidleague.com/myteam">http://www.voidleague.com/myteam</a></li>
<li>When you have enough/all of your teammates registered for 3v3 or 5v5 tournament format -> Team captain and co-captain will have a join button on the chosen team format tournaments</li>
<li>15 minutes before the tournament start time there will be check-ins to accept the participation on the tournament page. Only team captain and co-captain can have the check-in option.</li>
</ul>
</div>
<div class="guider_button">'.t('How to sign up for tournaments?').'</div>
</div>';
// handle errors
if(!empty($_SESSION['messages'])) {
    foreach($_SESSION['messages'] AS $type=>$messages_list) {
        $messages .= '<div class="messages '.$type.'"><ul>';
        foreach($messages_list AS $message) {
            $messages .= '<li>'.$message.'</li>';
        }
        $messages .= '</ul></div>';
    }
    unset($_SESSION['messages']);
}

$body_classes = '';
if($is_front) {
    $body_classes = 'front';
}
if(!empty($routes[0]) && $page_verified) {
    $body_classes = ' page-'.$routes[0];
}
$scripts[] = add_js('../js/jquery.cleditor.min.js');

$scripts = implode('',$scripts);
$styles = implode('',$styles);
$logged_in = (User::$user->uid > 0) ? ' logged_in' : '';
$theme_type = (!empty($plugin->iframe)) ? 'theme_plain' : 'theme';
