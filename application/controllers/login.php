<?php
// vari
$domain_name = explode(".",$_SERVER['SERVER_NAME']);
if(isset($domain_name[2])) {
  $domain_name = $domain_name[0].'.'.$domain_name[1].'.'.$domain_name[2];
}	


$av='AUTH'; // cepuma nosaukums
$cookie_domain = 'http://'.$domain_name;//Tavas web-lapas domēns
$cookie_path = '/';//Tavas web-lapas daļa, kurā darbosies cookie (izlasi, kas tas ir dokumentācijā)
$remember = 60*60*24*29.5*12*24;// aptuveni gads
$user_data = Array();



// Sataisam super kruto paroles izveide + salts
function crypt_pass($pass, $salt=NULL) {
    $salt_len=7;
    $algo='sha512';
    if (!$salt||strlen($salt)<$salt_len)
    {
        $salt=uniqid($pass, TRUE);    // lenght=32
    }
$salt=substr($salt, 0, $salt_len);
$hashed=hash($algo, $salt.$pass);
    return $salt.$hashed;
}

function pw($pw){
  return md5(md5($pw) . crypt_pass($pw)); // izmantojam kaa md5($_POST['parole']);
}



// dabujam saltu no db, vieglak..
function get_salt($uname){
$q = mysql_result(mysql_query("SELECT salt FROM ".PREFIX."lietotaji WHERE id = '".intval($uname)."' AND activated = 1 LIMIT 1"),0,0);
return $q;
}

function get_uid($uname){
$q = mysql_query("SELECT id FROM ".PREFIX."lietotaji WHERE lietotajvards = '".mysql_real_escape_string($uname)."' AND activated = 1 LIMIT 1");
$q = mysql_fetch_assoc($q);
return $q['id'];
}

// izveidojam saltu genereshanu
function make_salt(){
	$salt='';
	for($x=0;$x<64;$x++){
		$salt.=chr(mt_rand(0,255)); //ģenerējam sāli ar garumu 512b
	}
	return $salt;
}


function check_submitted_pw($username,$password,$autorenew){
	global $remember;
				
	$authenticated = false;


    if(is_array($username)) {
        $username = end($username);
    }
	$uid=get_uid($username);

	//echo "|$uid|";
	if('' != $uid){
	
		$salt=get_salt($uid);
		//echo "|$salt|";
		if('' != $salt){
		
			$salt=pack('H*',$salt);
			$parole = pw($password);
			//$parbaudama_parole = hash('sha512',$parole . $salt,false);
			if(check_pw($uid,$parole)){

				$authenticated = maketoken($uid,$parole,$autorenew,time()+$remember,'');//izveidojam vērtību, kas derīga $remember sekundes
			}
			else {
				//echo "necheck pw";
			}
		}
		else {
			//echo "ne salt";
		}
	}
	else {
		//echo "ne uid";
	}
	return $authenticated;
}

function maketoken($uid,$parole,$autorenew,$expire,$info){
$key = pack('H*','86b6de905bbee18b3425f7e5974c1cad7e24ec06a707a2766c5988c20ebc7337');//šī ir slepenā šifrēšanas atslēga HEX formā, NOMAINĪT OBLIGĀTI!!!!
$hmac_key = pack('H*','8412789462918462123861789234612378964123763412781273467252347564735');//šī ir slepenā HMAC atslēga NOMAINĪT OBLIGĀTI!!!!
$td = mcrypt_module_open('rijndael-256', '', 'cbc', '');
srand();
$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);//ja tiek izmantots Linux/BSD* tad izmantojiet MCRYPT_DEV_URANDOM
mcrypt_generic_init($td, $key, $iv);
$data=$uid.','.bin2hex($parole) .','.intval($autorenew).','.intval($expire). ','.ip2long($_SERVER['REMOTE_ADDR']) . ',' . bin2hex($info);//info - papildus mainīgie utt, kas jāsaglabā no vienas lapas uz otru
$data=$data.',' . hash_hmac('sha512',$data,$hmac_key,false);//Pievienojam imitoaizsardzību
//echo "kukijaa baazamais: $data <br>";
$data = mcrypt_generic($td, $data);//šifrējam
$data = bin2hex($iv) . ',' . bin2hex($data);
mcrypt_generic_deinit($td);
mcrypt_module_close($td);
return $data;
}
 
function checktoken($data,&$mas){ // $_COOKIE,$userdata

	$key = pack('H*','86b6de905bbee18b3425f7e5974c1cad7e24ec06a707a2766c5988c20ebc7337');//šī ir slepenā šifrēšanas atslēga HEX formā, NOMAINĪT OBLIGĀTI!!!!
	$hmac_key = pack('H*','8412789462918462123861789234612378964123763412781273467252347564735');//šī ir slepenā HMAC atslēga. NOMAINĪT OBLIGĀTI!!!!
	$td = mcrypt_module_open('rijndael-256', '', 'cbc', '');
	srand();
	$mm = explode(',',$data);
	if(count($mm)!=2){
	return 0; //nav IV
	}
	$iv = pack('H*',$mm[0]);//ja tiek izmantots Linux/BSD* tad izmantojiet MCRYPT_DEV_URANDOM
	mcrypt_generic_init($td, $key, $iv);
	//echo bin2hex($iv)."<br>";
	$data=rtrim(mdecrypt_generic($td,pack('H*', $mm[1])), "\0");
	mcrypt_generic_deinit($td);
	mcrypt_module_close($td);
	$mas = explode(',',$data);
	
	

	//echo $data;
	if(count($mas)!=7){
		return 1; //par daudz lauku
	}
	if($mas[3]<time()){
		return 2; //sesijas timeout
	}
	if(hash_hmac('sha512',$mas[0].','.$mas[1].','.$mas[2].','.$mas[3].','.$mas[4] . ',' . $mas[5],$hmac_key,false)!=$mas[6]){
		return 3; //mainīti dati
	}
	if(!($salt=get_salt($mas[0]))){
		return 4;//nav tāds UID - ļoti savādi!!! Laikam UID izdzēsts no DB
	}
	if(!check_pw($mas[0],pack('H*',$mas[1]))){
	///print_r($mas);
		return 5;//nepareiza parole - ļoti savādi!!! Laikam nomainīta parole
	}
	$mas[5]=pack('H*',$mas[5]);
	if(ip2long($_SERVER['REMOTE_ADDR']) != $mas[4]){
		return 6; //dati OK, bet IP mainījusies.
	}
	return 7; //viss ir OK	
}



function check_pw($uid,$hash){
$uid2 = mysql_query("SELECT id FROM ".PREFIX."lietotaji WHERE id = '".intval($uid)."' AND parole = '".mysql_real_escape_string($hash)."'") or die(mysql_error());
$uid2 = mysql_fetch_assoc($uid2);
if($uid == $uid2['id'])
{
return true;
}
else
{
return false;
}
}
$routes = arg();
if(isset($_POST['ielogoties']) && $_POST['ielogoties'])
{

	if($result=check_submitted_pw($_POST['user'],$_POST['pass'],(isset($_POST['atcereties'])?'1':'0'))){
		if(isset($_POST['atcereties'])){
			setcookie($av,$result,mktime()+$remember,'/');//Permanentais
		}
		else {
	 		setcookie($av,$result,0,'/');//Līdz aizvēršanai
		}
		
	$ip = mysql_real_escape_string($_SERVER['REMOTE_ADDR']);
	mysql_query("UPDATE ".PREFIX."lietotaji SET ip = '$ip' WHERE lietotajvards = '".mysql_real_escape_string($_POST['user'])."'") or die(mysql_error());
	

			if($routes[0] == 'register' OR isset($routes[1]) && $routes[1] == 'BAD_LOGIN')
	{
	header('location: ?logged',true,301);
	}
	else
	{

	header('location: ?logged',true,301);
	}
		exit();
	}
	else{
		setcookie($av,'',1);//Dodam pavēli izdzēst cookie
		header('location: /login/bad-data/');
	}
}

?>