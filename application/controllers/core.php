<?php

function admin_menu($menu) {
    $arrow = '<span style="position:absolute;"><img src="/public/images/arrow_w.png" alt="" /></span>';
    $output = '<ul>';
    foreach($menu AS $path=>$item) {
        if($path != 'tree') {
            if(isset($item['tree'])) {
                $output .= '<li class="parent"><a href="#">'.ucfirst($path) . $arrow.'</a><div class="submenu">';
                $output .= admin_menu($item);
                $output .= '</div></li>';
            }
            else {
                if(isset($item['access']) && user_access($item['access']) || !isset($item['access'])) {
                    $output .= '<li><a href="/admin/'.$path.'">'.$item['title'].'</a></li>';
                }
            }
        }
    }

    $output .='</ul>';
    return $output;
}

function get_country($ip = false)
{
    if(!$ip) {
	  $ip = ip_address();
    }

    $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
    if($ip_data && $ip_data->geoplugin_countryCode != null)
    {
        $result = $ip_data->geoplugin_countryCode;
    }
    else {
      $ip_data = json_decode(file_get_contents("http://ip2country.hackers.lv/api/ip2country?ip=".$ip));
      $result = end($ip_data->c);
    }
    if(empty($result)) {
      $result = 'LV';
    }
    return $result;
}

function generate_key($length){
    $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
    $str = '';
    for($i=0; $i<$length; $i++){
        $str.= $chars[array_rand($chars)];
    }
    return $str;
}


function add_js($link,$type='file') {
	if($type == 'inline') {
	    return '<script type="text/javascript">'.$link.'</script>';
	}
	else {
	    return '<script src="/public/js/'.$link.'" type="text/javascript"></script>';
	}
}

function add_css($link) {
	return '<link href="/public/css/'.$link.'" rel="stylesheet" type="text/css" />';
}

function redirect($link,$timeout=NULL) {
	if(!$timeout) $timeout = 0;
	$red_after = $timeout.'000';
	print '<script>function redirect() {window.location="'.$link.'"; } setTimeout( "redirect();", '.$red_after.' ); </script>';
}

function set_message($text,$status = false) {
	if(empty($status)) {
		$status = 'info';
	}
	return '<div class="bx '.$status.'">'.$text.'</div>';
}

$queries_count = 0;

function queryy($sql)
{
global $queries_count;
global $lietotajs;

$result = mysql_query($sql);
if(!$result AND $lietotajs['id'] == 1) echo '<div class="bx error">'.mysql_error().'</div>'; 
$queries_count++;
return $result;
}



function path_to_theme($theme=NULL) {
  if(!$theme) {
    $theme = '_day'; // default theme
  }
  return '/public/themes/'.$theme.'/';
}


if(debug) {
  $_SESSION['debug'] = array();
  $_SESSION['debug']['queries'] = array();
}

function db_query($sql)
{
  if(debug) {
    $mtime = microtime();
    $mtime = explode(" ",$mtime);
    $mtime = $mtime[1] + $mtime[0];
    $starttime = $mtime;
  }

  $result = mysql_query($sql);
  if(!$result) {
    print '<div class="bx error">'.mysql_error().'<br />
	QUERY: '.$sql.'</div>';
  }
  else {
    if(debug) {
      $mtime = microtime();
      $mtime = explode(" ",$mtime);
      $mtime = $mtime[1] + $mtime[0];
      $endtime = $mtime;
      $totaltime = ($endtime - $starttime);


      if(debug) {
        $_SESSION['debug']['queries'][] = array(
          'data' => $sql,
          'time'=>$totaltime.' seconds',
        );
        $_SESSION['debug']['full_time'] = $_SESSION['debug']['full_time']+$totaltime;
    }
  }



  }


  return $result;
}

function db_fetch_assoc($sql) {
	return mysql_fetch_assoc($sql);
}

function db_fetch_object($sql) {
  return mysql_fetch_object($sql);
}

function db_fetch_array($sql) {
  return mysql_fetch_assoc($sql);
}

function db_result($result) {
  if ($result && mysql_num_rows($result) > 0) {
    $array = mysql_fetch_row($result);
    return $array[0];
  }
  return FALSE;
}

function module($module) {
    return $_SERVER['DOCUMENT_ROOT'].'/application/modules/'.$module.'/';
}


function dsm($object) {
    print '<pre>';
    print_r($object);
    print '</pre>';
    return;
}




function checkEmail($email) {
    if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    return false;
    }

        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
        if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
            return false;
        }
    }
    if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { 
        $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
            return false; // Not enough parts to domain
            }
        for ($i = 0; $i < sizeof($domain_array); $i++) {
            if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                return false;
            }
        }
    }
    return true;
}


/*
 * i18n
 */
function t($string,$replace=false) {

    return stripslashes(t_translate($string,$replace)); // not translated
    
}

function t_translate($string,$replace) {
    if(!empty($replace)) {
	foreach($replace AS $key=>$val) {
	    $string = str_replace($key,$val,$string);
	}
    }
    return $string;
}


function admin()
{
  global $lietotajs;
  $admins = [
      1, // Yugis
      702, // Normix
      776 // Alvis
  ];
  if(in_array($lietotajs['id'], $admins)) {
      return true;
  }
  return false;
}

function navigation() {

    $menus = ClassLib\Memcached::get('menu_top', 300);
    if($menus == false) {
        $menus = \ORM::for_table('menu')
            ->order_by_asc('level')
            ->order_by_asc('order')
            ->find_array();
        ClassLib\Memcached::set('menu_top', $menus);
    }
	$output = '<ul>';
	foreach($menus AS $menu) {
		if($menu['level'] == 1) {
			$have_childs = false;
			// get childs menu
			$childs = '';
			foreach($menus AS $submenu) {
				$active = '';
				if($submenu['level'] == 2) {
					if($submenu['parent'] == $menu['id']) {
						$blank = '';
						if($menu['blank'] == 1) {
							$blank = ' target="_blank"';
						}

						$childs .= '<li class="menu-'.$submenu['id'].'"><a href="'.h($submenu['link']).'"'.$blank.'>'.h($submenu['title']).'</a></li>';
						$have_childs = true;
					}
				}
			}

			// get parent menu
			$blank = '';
			if($menu['blank'] == 1) {
				$blank = ' target="_blank"';
			}

			// class .active
			// :: example :: http://www.test.com/dada/
			$menu_active = explode('/',$menu['link']);
            $args = arg();
			if(isset($args[0]) && isset($menu_active[1]) && $args[0] == $menu_active[1] || empty($args[0]) && $menu['link'] == 'http://www.voidleague.com') {
				$active = ' active';
			}


			if($have_childs) {
				$output .= '<li class="menu-'.$menu['id'] . $active.' dropdown"><a href="'.h($menu['link']).'"'.$blank.'>'.h($menu['title']).'</a>';
				$output .= '<div class="submenu"><ul>';
				$output .= $childs;
				$output .= '</ul><div class="submenu_bottom"></div></div></li>';
			}
			else {
				$home = '';
				if($menu['link'] == 'http://www.voidleague.com') {
					$home = 'home';
				}
				$output .= '<li class="menu-'.$menu['id'] . $active.' '.$home.'"><a href="'.h($menu['link']).'"'.$blank.'>'.h($menu['title']).'</a></li>';
			}

		}
	}

	$output .= '</ul>';

	return $output;

}


function loginBox() {
global $lietotajs;
$mail_c = '';
            if($lietotajs['id'])
            {
			
              if(User::$user->new_messages > 0)
              {
              $mail_c =  '<span style="color: #ff0000; font-weight:bold;">('.User::$user->new_messages.')</span>';
              }
print '<div id="logged_in">';
?>
	<div id="userbox">
		<img usemap="#userboxLinks" src="/public/themes/gamemaster/img/userbox_bg.png" alt=""/>
		<map id="userboxLinks" name="userboxLinks">
			<area shape="poly" coords="25,2,162,2,162,36,147,43,141,47,131,58,103,58,94,46,25,46" href="/myprofile/" title="My Profile" />
			<area shape="poly" coords="0,50,92,50,97,56,100,61,128,61,125,69,123,77,123,83,105,83,92,94,0,94" href="/myprofile/gameaccount/" title="Game accounts" />
			<area shape="poly" coords="166,2,316,2,316,66,244,66,231,51,203,49,192,43,179,38,165,38" href="/messages/" title="Messages" />
			<area shape="poly" coords="203,54,228,54,241,68,330,68,330,98,245,98,231,111,203,111,210,96,213,86,210,68,203,54,204,57" href="/myprofile/logout/" title="Logout" />
			<area shape="poly" coords="29,97,94,97,103,86,123,86,125,101,138,119,162,127,101,127,99,130,29,130" href="/myteam" title="My team"></a>
		</map> 
		<div id="userbox-picture"><a href="/user/<?php print $lietotajs['lietotajvards'];?>/" title="<?php print $lietotajs['lietotajvards'];?>"><?php print getMemberPhoto($lietotajs['photo'],$lietotajs['lietotajvards'],80,80,1); ?></a></div>
		<a class="link-1" href="/myprofile"><?php print t('Profile'); ?></a>
		<a class="link-2" href="/myprofile/gameaccount"><?php print t('Game accounts');?></a>
		<a class="link-3" href="/myteam"><?php print t('My team'); ?></a>
		<a class="link-4" href="/messages"><?php print t('Messages');?> <?php print $mail_c;?></a>
		<a class="link-5" href="/myprofile/logout"><?php print t('Logout'); ?></a>
		<?php if(user_access('access administration theme')): ?>
			<div class="link-admin"><a title="Administrācijas panelis" href="/admin/"><img src="/public/themes/gamemaster/img/administrator-icon.png" alt="Administrācija" /></a></div>
		<?php endif; ?>
	</div>

<?php    
        
print '</div>'; // logged_in        
            }
            else
            {
            /*--------------------------*/
            /* NOT-LOGGED-IN:START */
            /*--------------------------*/
            ?>      
<form method="post" action="">
<p class="label"><?php print t('Username'); ?>:</p><input type="text" name="user" class="text"/>
<p class="label"><?php print t('Password'); ?>:</p><input type="password" name="pass" class="text" />
<input type="submit" class="submit" value="<?php print t('Go'); ?>" name="ielogoties"/>
<p id="remember"><?php print t('Remember'); ?>
<span id="checkbox1Wrap" class="styledCheckboxWrap">
<input type="checkbox" id="checkbox1" name="atcereties" onclick="setCheckboxDisplay(this)" class="styledCheckbox" /></span>
<span></span></p>
<a href="/password/" class="reset-password"><?php print t('Reset password'); ?></a>
				
</span>
</form>
<?php
            
            /*--------------------------*/
            /* NOT-LOGGED-IN:END */
            /*--------------------------*/ 
            }

}



?>
