<?php
use \ORM;

$roles = array();
$getRoles = db_query("SELECT * FROM `role` ORDER BY `name` ASC");
while($getRole = db_fetch_object($getRoles)) {
    $getRole->members = array();
    $role_count = 0;
    $getRole->name = htmlspecialchars($getRole->name);
    $getRoleMembers = db_query("SELECT `photo`, `lietotajvards` AS `username`, country FROM `users_roles` LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_roles.uid WHERE `rid` = '".$getRole->rid."'");
    while($getRoleMember = db_fetch_object($getRoleMembers)) {
        $getRoleMember->label = '<img src="/public/images/flags/'.strtolower($getRoleMember->country).'.png" alt="" /> '.getMember($getRoleMember->username);
        if(empty($getRoleMember->photo)) {
            $getRoleMember->photo = 'noimage.png';
        }
        $getRole->members[] = $getRoleMember;
        $role_count++;
    }
    if(!empty($role_count)) {
        $roles[] = $getRole;
    }

}
/*
$admins = ORM::for_table(PREFIX.'lietotaji')
                        ->table_alias('u')
                        ->left_outer_join('users_roles','r.uid = u.id', 'r')
                        ->where('r.rid', $role)
                        ->find_array();
*/
?>

<?php if(User::$user->uid == 1):?>
    <?php foreach($admins AS $admin): ?>
        <div class="admin-item" style="background: url('/public/images/photo/<?=$admin['photo'];?>'); width:150px; height:150px;">



        </div>
    <?php endforeach; ?>
<?php endif; ?>

<div id="admin-front">
    <div id="support">
        <h3>Support</h3>
        <div id="support-image">
            <img src="/public/images/admin/support.png" alt="" />
        </div>
        <div id="support-info">
            <div id="phone"><span><?=_('Skype');?>:</span> lazdas2</div>
            <div id="skype"><span><?=_('Phone');?>:</span>22136489</div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="members-roles">
        <?php foreach($roles AS $role): ?>
            <div class="role">
                <h3><?php print $role->name;?></h3>
                <div class="role-members">
                    <?php foreach($role->members AS $member): ?>
                        <div class="role-member showAdmin"><img src="/public/images/photo/small/<?php print $member->photo;?>" alt="" /><br /><?php print $member->label;?></div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

