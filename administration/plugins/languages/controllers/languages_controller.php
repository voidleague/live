<?php
class LanguagesController extends AppController{
    
    function __construct() {
        $this->homepath = 'list';
        $this->languagesList = array();
        $getLanguages = db_query("SELECT * FROM `languages` ORDER BY `default` DESC");
        while($getLanguage = db_fetch_object($getLanguages)) {
            // Operations links
            $operations = '<a href="/admin/languages/edit/'.$getLanguage->language.'/">'.t('edit').'</a>';
            if(empty($getLanguage->default)) {
                $operations .= ' | <a href="/admin/languages/delete/'.$getLanguage->language.'/" class="confirm">'.t('delete').'</a>';
            }
            $getLanguage->operations = $operations;
            
            // Enabled status
            $enabled = ($getLanguage->enabled == 1)? 'ok' : 'notok';
            $getLanguage->enabled = '<img src="/public/images/admin/status_'.$enabled.'.png" alt="" />';
            $this->languagesList[$getLanguage->language] = $getLanguage;
        }
    }
    
    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add language'),
            'translate' => t('Translate'),
        );
        return parent::admin_links();
    }
    
    function admin_index() {
       return $this->admin_list();   
    }

    function admin_list() {
        return $this->view('list');
    }
    
    function admin_add($id=null) {
        // add result
        if(isset($_POST) && !empty($_POST)) {
            $this->data = $this->security($_POST);
            $this->data->language = $id;
            if($id) {
                $_POST['language_id'] = $id;
                $this->_update($this->data);
              
            }
            else {
                $this->_add($this->data);
            }
        }
        
        if($id) {
            $id = mysql_real_escape_string($id);
            $language = db_fetch_object(db_query("SELECT * FROM `languages` WHERE `language` = '".$id."'"));
        }
        $form['name'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title'  => t('Name in English'),
            '#default_value' => ($id) ? $language->name : '',
        );
        $form['native'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title'  => t('Name in Native'),
            '#default_value' => ($id) ? $language->native : '',
        );
        $form['code'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title'  => t('CODE'),
            '#default_value' => ($id) ? $language->language : '',
            '#disabled' => ($id) ? TRUE : FALSE,
        );
        $form['enabled'] = array(
            '#type' => 'checkbox',
            '#required' => TRUE,
            '#title'  => t('Enabled'),
            '#default_value' => ($id) ? $language->enabled : '',
        );
        $form['submit'] = array(
            '#type' => 'submit',
            '#required' => TRUE,
            '#value'  => t('Add'),
        );
        return render_form($form);
    }
    
    function admin_edit() {
        $routes = arg();
        $this->admin_add($routes[3]);
    }
    

    function admin_delete() {
        $lang = mysql_real_escape_string(arg(3));
        $exist = db_fetch_object(db_query("SELECT * FROM `languages` WHERE `language` = '".$lang."'"));
        if($exist->language == 'en' || $exist->default) {
            $this->setError(t("You can't delete english or default language",'error'));
        }
        else {
            db_query("DELETE FROM `languages` WHERE `language` = '".$lang."' AND `default` = 0");
            db_query("ALTER TABLE `languages_phrases` DROP `".$lang."`");
            $this->setMessage(t('Language successfully deleted'));
            redirect('/admin/languages/');
        }
    }
    
    
    private function _add($data) {
        $exist = db_result(db_query("SELECT count(*) FROM `languages` WHERE `language` = '".$data->code."'"));
        if(!$exist) {
            $data->enabled = (isset($data->enabled) && $data->enabled == 'on') ? 1 : 0;
            db_query("INSERT INTO `languages` (`name`,`native`,`language`,`enabled`) VALUES ('".$data->name."','".$data->native."','".$data->code."','".$data->enabled."')");
            db_query("ALTER TABLE `languages_phrases` ADD ".$data->code." VARCHAR(255)");
            $this->setMessage(t('Language successfully added'));
            redirect('/admin/languages/');
        }
        else {
            $this->setError(t('There are already language with that CODE'));
        }
    }
    
    private function _update($data) {
        $data->enabled = (isset($data->enabled) && $data->enabled == 'on') ? 1 : 0;
        db_query("UPDATE `languages` SET `name` = '".$data->name."',`native` = '".$data->native."', `enabled` = '".$data->enabled."' WHERE `language` = '".$data->language."'");
        $this->setMessage(t('Language successfully updated'));
        redirect('/admin/languages/');
    }
    
    
    function admin_translate() {
        
        if(isset($_POST) && $_POST) {
            $this->data = $this->security($_POST);
            $this->_translate($this->data);
            $this->setMessage(t('Translations saved'));
        }
        
        // Set English as first
        $lang_en = $this->languagesList['en'];
        unset($this->languagesList['en']);
        $this->languagesList = array('en'=>$lang_en)+$this->languagesList;
        
        
        $output = '<form method="POST"><table class="translate">';
        foreach($this->languagesList AS $language) {
            $output .= '<th>'.$language->native.'</th>';
        }
        $dbPhrases = db_query("SELECT * FROM `languages_phrases` ORDER BY `en` ASC");
        while($dbPhrase = db_fetch_object($dbPhrases)) {
            $dbPhrase = (array)$dbPhrase;
            $output .= '<tr>';
        foreach($this->languagesList AS $language) {
            if($language->language != 'en') {
                $output .= '<td class="left"><input type="text" name="'.$language->language.'['.$dbPhrase['id'].']" value="'.$dbPhrase[$language->language].'" /></td>';
            }
            else {
                $output .= '<td class="left"><strong>'.$dbPhrase[$language->language].'</strong></td>';
            }
        }
            $output .= '<tr/>';
        }
        $output .= '</table><input type="submit" name="save_translation" value="'.t('Save').'" /></form>';
        return $output;
    }
    
    private function _translate($data) {
        $translations = array_keys($data->lv);
        foreach($translations AS $translation) {
            $translation = intval($translation);
            db_query("UPDATE `languages_phrases` SET `lv` = '".$data->lv[$translation]."', `ru` = '".$data->ru[$translation]."' WHERE `id` = '".$translation."'");
        }
    }
}
