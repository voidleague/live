<table>
    <tr>
        <th>English name</th>
        <th>Native name</th>
        <th>Code</th>
        <th>Enabled</th>
        <th>Operations</th>
    </tr>
    <?php foreach($this->languagesList AS $language): ?>
        <tr>
            <td class="bold"><?php print $language->name; ?></td>
            <td><?php print $language->native; ?></td>
            <td><?php print $language->language; ?></td>
            <td><?php print $language->enabled; ?></td>
            <td><?php print $language->operations; ?></td>
        </tr>
    <?php endforeach; ?>
</table>