<table class="faqs">
    <tr>
        <th style="width:10px;">#FAQ ID</th>
        <th><?php print t('Question'); ?></th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php if(count($variables['faqs']) > 0): ?>
        <?php foreach($variables['faqs'] AS $faq): ?>
            <tr>
                <td class="bold"><?php print $faq->id; ?></td>
                <td class="left"><?php print $faq->question; ?></td>
                <td style="width:50px;"><?php print $faq->operations; ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr><td colspan="6">There are no faq questions. <a href="/admin/faq/add">Add new faq question</a></td></tr>
    <?php endif; ?>
</table>