<?php
class FaqController extends AppController {

    public function __construct() {
        $this->permissions = 'administer faq';
        $this->homepath = 'list';
    }
    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add question'),
        );
        return parent::admin_links();
    }

    public function admin_index() {
        return $this->admin_list();
    }

    public function admin_list() {
        $this->title = t('FAQ');
        $getFAQs = db_query("SELECT * FROM `faq` ORDER BY `question` ASC");
        $faqs = array();
        while($getFAQ = db_fetch_object($getFAQs)) {
            $operations = '<a href="/admin/faq/edit/'.$getFAQ->id.'/">'.t('edit').'</a> | <a href="/admin/faq/delete/'.$getFAQ->id.'/" class="confirm">'.t('delete').'</a>';
            $getFAQ->operations = $operations;
            $faqs[] = $getFAQ;
        }
        return $this->view('list',array('faqs'=>$faqs));
    }

    public function admin_edit() {
        $args = arg();
        if(!empty($args[3])) {
            return $this->admin_add($args[3]);
        }
    }

    public function admin_delete() {
        $args = arg();
        if(!empty($args[3])) {
            db_query("DELETE FROM `faq` WHERE `id` = '".intval($args[3])."'");
            $this->setMessage(t('FAQ successfully deleted'));
            $this->redirect('admin/faq');
        }
    }

    public function admin_add($id=false) {
        if($id) {
            $faq = db_fetch_object(db_query("SELECT * FROM `faq` WHERE `id` = '".intval($id)."'"));
            if(empty($faq)) {
                $this->redirect('admin/faq');
            }
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            $post = (object)$_POST;
            $config = HTMLPurifier_Config::createDefault();
            $text = trim(html_entity_decode($post->answer));
            $config->set('HTML.AllowedElements','p,a,img,span,br,div');
            $config->set('HTML.AllowedAttributes', 'span.style, img.src, a.href');
            $config->set('CSS.AllowedProperties', array('text-decoration' => true,'font-weight' => true,'font-style' => true));

            $purifier = new HTMLPurifier($config);
            $data->answer = $purifier->purify("".$text);

            if($id) {
                $data->id = $id;
            }
            $this->_faq_save($data);
        }

        $form = array(
            'question' => array(
                '#type' => 'textfield',
                '#title' => t('Question'),
                '#default_value' => ($id) ? $faq->question : ''
            ),
            'answer' => array(
                '#type' => 'textarea',
                '#format' => 'filtered_html',
                '#title' => t('Answer'),
                '#default_value' => ($id) ? $faq->answer : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );
        return render_form($form);
    }

    /**
     * @param $data
     * Check if there is $data->id
     */
    private function _faq_save($data) {
        if(!empty($data->id)) {
            db_query("UPDATE `faq` SET `question` = '".$data->question."', `answer` = '".$data->answer."' WHERE `id` = '".$data->id."'");
        }
        else {
            db_query("INSERT INTO `faq` (`question`,`answer`) VALUES ('".$data->question."','".$data->answer."')");
        }
        $this->setMessage(t('FAQ successfully saved'));
        $this->redirect('admin/faq');
    }

}