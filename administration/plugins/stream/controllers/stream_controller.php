<?php
class StreamController extends AppController{
    function __construct() {
        $this->homepath = 'list';
    }
    function admin_index() {
        return $this->stream_admin_list();
    }

    public function admin_list() {
        return $this->stream_admin_list();
    }

    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add stream'),
            'overlays' => t('Overlays'),
        );
        return parent::admin_links();
    }

    function stream_admin_list() {
        $this->title = t('Streams');
        $getStreamsCount = db_result(db_query("SELECT count(*) FROM `streams`"));
        // create pager
        $pager = $this->pager($getStreamsCount);

        $getStreams = db_query("SELECT * FROM `streams`
                        ORDER BY `id` DESC
                        LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $streams = array();
        while($getStream = db_fetch_object($getStreams)) {

            $status = ($getStream->status == 1)? 'ok' : 'notok';
            $getStream->status = '<img src="/public/images/admin/status_'.$status.'.png" alt="" />';
            // Operations links
            $operations = '<a href="/admin/stream/edit/'.$getStream->id.'/">'.t('edit').'</a> | <a href="/admin/stream/delete/'.$getStream->id.'/" class="confirm">'.t('delete').'</a>';
            $getStream->operations = $operations;
            $streams[] = $getStream;
        }
        return $this->view('list',array('streams'=>$streams,'pager'=>$pager));
    }

    public function admin_edit() {
        return $this->admin_add(Routes::$routes[3]);
    }
    public function admin_add($id=null) {
        // add result
        if(isset($_POST) && !empty($_POST)) {
            $this->data = $this->security($_POST);
            $this->data->stream_id = intval($id);

            if($id) {
                $this->_update($this->data);
            }
            else {
                $this->_add($this->data);
            }
        }

        if($id) {
            $stream = db_fetch_object(db_query("SELECT * FROM `streams` WHERE `id` = '".intval($id)."'"));
        }
        $form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#default_value' => ($id) ? $stream->name : '',
            ),
            'streamer' => array(
                '#type' => 'textfield',
                '#title' => t('Streamer'),
                '#default_value' => ($id) ? $stream->streamer : '',
            ),
            'link' => array(
                '#type' => 'textfield',
                '#title' => t('Link to twitch'),
                '#default_value' => ($id) ? $stream->link : '',
            ),/*
            'stream_date' => array(
                '#type' => 'datetime',
                '#title' => t('Stream date'),
                '#default_value' => ($id) ? $stream->stream_date : '',
            ),
            */
            'status' => array(
                '#type' => 'checkbox',
                '#title' => t('Published'),
                '#default_value' => ($id) ? $stream->status : '',
            ),
            'front' => array(
                '#type' => 'checkbox',
                '#title' => t('Show in front page'),
                '#default_value' => ($id) ? $stream->front : '',
            ),
            /*
            'description' => array(
                '#type' => 'textarea',
                '#title' => t('Description'),
                '#default_value' => ($id) ? $stream->description : '',
            ),
            */
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );
        return render_form($form);
    }

    private function _add($data) {
        if(empty($data->name) || empty($data->link)) {
            $this->setError(t('Please fill all fields'));
        }
        else {
            $data->slug = slug($data->name);
            $data->status = (isset($data->status) && $data->status == 'on') ? 1 : 0;
            $data->front = (isset($data->front) && $data->front == 'on') ? 1 : 0;
            db_query("INSERT INTO `streams` (`front`,`slug`,`streamer`, `name`,`link`,`status`) VALUES ('".$data->front."','".$data->slug."','".$data->streamer."','".$data->name."','".$data->link."','".$data->status."')");
            $this->setMessage(t('Stream successfully added'));
            $this->redirect('admin/stream');
        }
    }

    private function _update($data) {
        if(empty($data->name) || empty($data->link)) {
            $this->setError(t('Please fill all fields'));
        }
        else {
            $data->slug = slug($data->name);
            $data->status = (isset($data->status) && $data->status == 'on') ? 1 : 0;
            $data->front = (isset($data->front) && $data->front == 'on') ? 1 : 0;
            db_query("UPDATE `streams` SET `front` = '".$data->front."', `slug` = '".$data->slug."', `streamer` = '".$data->streamer."', `name` = '".$data->name."', `link` = '".$data->link."', `status` = '".$data->status."' WHERE `id` = '".$data->stream_id."'");
            $this->setMessage(t('Stream successfully updated'));
            $this->redirect('admin/stream');
        }
    }

    public function admin_delete() {
        $stream_id = arg(3);
        if(is_numeric($stream_id)) {
            db_query("DELETE FROM `streams` WHERE `id` = '".$stream_id."'");
            $this->setMessage(t('Stream sucessfully deleted'));
        }
        $this->redirect('admin/stream');
    }

    public function admin_overlays() {
        return $this->view('overlays');
    }

}