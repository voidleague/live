<table class="streams">
    <tr>
        <th style="width:10px;">#streamID</th>
        <th>Name</th>
        <th>Streamer</th>
        <th>Status</th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php if(count($variables['streams']) > 0): ?>
        <?php foreach($variables['streams'] AS $stream): ?>
            <tr>
                <td class="bold"><?php print $stream->id; ?></td>
                <td><?php print $stream->name; ?></td>
                <td><?php print $stream->streamer; ?></td>
                <td><?php print $stream->status; ?></td>
                <td><?php print $stream->operations; ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr><td colspan="6">There are no streams. <a href="/admin/stream/add">Add new stream</a></td></tr>
    <?php endif; ?>
</table>

<?php print $variables['pager']; ?>