<?php
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'imagick'));

class NewsController extends AppController {
    public function __construct() {
        $this->title = t('News');
        $this->permission = 'administer news';
        $this->homepath = 'list';
    }

    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add'),
        );
        return parent::admin_links();
    }

    public function admin_index() {
        return $this->admin_list();
    }

    public function admin_list() {
        $getNewsCount = db_query("SELECT COUNT(*) FROM `".PREFIX."news`");
        $pager = $this->pager($getNewsCount);
        $getNews = db_query("SELECT * FROM `".PREFIX."news` ORDER BY `updated` DESC LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $news = array();
        while($getNew = db_fetch_object($getNews)) {
            $status = ($getNew->status == 1)? 'ok' : 'notok';
            $getNew->status = '<img src="/public/images/admin/status_'.$status.'.png" alt="" />';
            $getNew->operations = '<a href="/admin/news/edit/'.$getNew->nid.'/">'.t('edit').'</a> | <a href="/admin/news/delete/'.$getNew->nid.'">'.t('delete').'</a>';
            $news[] = $getNew;
        }
        return $this->view('list',array('news'=>$news,'pager'=>$pager));
    }

    /**
     * News form
     */
    public function admin_delete() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            db_query("DELETE FROM ".PREFIX."news WHERE `nid` = '".$args[3]."'");
            $this->setMessage(t('New successfully deleted'));
            $this->redirect('admin/news');
        }
    }

    public function admin_edit() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            return $this->admin_add($args[3]);
        }
    }
    public function admin_add($id=false) {
        $new = false;
        if($id) {
            $new = db_fetch_object(db_query("SELECT * FROM `".PREFIX."news` WHERE `nid` = '".$id."'"));
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $this->_submit_news($_POST,$new);
        }

        $form = array(
            'nid' => array(
                '#type' => 'hidden',
                '#value' => ($id) ? $new->nid : 0,
            ),
            'title' => array(
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#default_value' => ($id) ? $new->title : '',
            ),
            'image' => array(
                '#type' => 'filefield',
                '#title' => t('Image'),
                '#default_value' => ($id) ? $new->image : '',
                '#description' => t('Allowed formats: !formats',array('!formats'=>'<strong>JPG, PNG')),
            ),
            'desc' => array(
                '#type' => 'textarea',
                '#title' => t('Description'),
                '#default_value' => ($id) ? $new->desc : '',
            ),
            'body' => array(
                '#type' => 'textarea',
                '#format' => 'full_html',
                '#title' => t('Body'),
                '#default_value' => ($id) ? $new->body : '',
            ),
            'status' => array(
                '#type' => 'checkbox',
                '#title' => t('Publish'),
                '#default_value' => ($id) ? $new->status : '',
            ),
            'comments' => array(
                '#type' => 'checkbox',
                '#title' => t('Comments'),
                '#default_value' => ($id) ? $new->comments : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            )
        );

        return render_form($form);
    }

    /**
     * Validate posted news data
     */
    private function _submit_news($data,$existing_data) {
        try {
            $this->_validate_news($data,$existing_data);
            $this->_save_news($data);
        }
        catch(Error $e) {
            $this->setError($e->getMessage());
        }
    }

    private function _validate_news(&$data,$existing_data) {
        $post = (object)$data;
        $image = $_FILES['image'];
        $data = $this->security($data);
        if(empty($data->title) || empty($data->body) || empty($data->desc)) {
            throw new Error(t('Please fill all fields'));
        }

        if($existing_data && empty($image['name'])) {
            $data->image = $existing_data->image;
        }
        // Validate image
        if(empty($image['name']) && empty($data->nid)) {
            throw new Error(t('Please add image'));
        }
        elseif(!empty($image['name'])) {
            $extension = strtolower(getExtension(stripslashes($image['name'])));
            $size = filesize($image['tmp_name']);

            if($extension != ('jpg' || 'jpeg' || 'png')) {
                throw new Error(t('Photo can be only in format: !formats',array('!formats'=>'<strong>JPG, PNG</strong>')));
            }
            if($size > 1024*1000) {
                throw new Error(t('Too big photo. Please resize it'));
            }

            $image_name = time().'.'.$extension;

            Image::make($image['tmp_name'])->resize(79,65)->save('public/img/news/thumb/'.$image_name);
            Image::make($image['tmp_name'])->resize(600,337)->save('public/img/news/full/'.$image_name);
            $data->image = $image_name;
        }



        // Clear body
        $config = HTMLPurifier_Config::createDefault();
        $desc = trim(html_entity_decode($post->desc));
        $body = trim(html_entity_decode($post->body));
        $purifier = new HTMLPurifier($config);
        $data->body = $purifier->purify($body);
        $data->desc = $purifier->purify($desc);

        $data->comments = (isset($data->comments)) ? 1 : 0;
        $data->status = (isset($data->status)) ? 1 : 0;
        $data->slug = slug($data->title);

        $data->body = mysql_real_escape_string($data->body);
        $data->desc = mysql_real_escape_string($data->desc);
    }

    public function _save_news($data) {
        if(empty($data->nid)) {
            db_query("INSERT INTO ".PREFIX."news (`uid`,`status`,`desc`,`title`,`body`,`created`,`updated`,`image`,`comments`,`slug`) VALUES (
                '".User::$user->uid."','".$data->status."','".$data->desc."','".$data->title."','".$data->body."','".time()."','".time()."','".$data->image."','".$data->comments."','".$data->slug."'
            ) ");
            $data->nid = db_result(db_query("SELECT `nid` FROM ".PREFIX."news ORDER BY `nid` DESC LIMIT 1"));
        }
        else {
            db_query("UPDATE ".PREFIX."news SET `status` = '".$data->status."',`desc` = '".$data->desc."', `title` = '".$data->title."', `body` = '".$data->body."', `updated` = '".time()."', `comments` = '".$data->comments."',`slug` = '".$data->slug."', `image` = '".$data->image."' WHERE `nid` = '".$data->nid."'");
        }
        $this->redirect('new/'.$data->slug.'-'.$data->nid);
    }
}