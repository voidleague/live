<table class="news">
    <tr>
        <th>Title</th>
        <th>Status</th>
        <th class="left">Last update</th>
        <th class="center"><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['news'] AS $new): ?>
        <tr>
            <td class="left"><a href="/new/<?php print $new->slug;?>-<?php print $new->nid;?>"><?php print $new->title; ?></a></td>
            <td><?php print $new->status; ?></td>
            <td class="left"><?php print date('d.m.Y H:i',$new->updated);?></td>
            <td><?php print $new->operations; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php print $variables['pager']; ?>