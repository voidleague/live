<table>
    <tr>
        <th><?php print t('Game'); ?></th>
        <th class="left"><?php print t('Label');?></th>
        <th><?php print t('Value');?></th>
        <th><?php print t('Status');?></th>
        <th><?php print t('Operations');?></th>
    </tr>
    <?php foreach($variables AS $account): ?>
        <tr>
            <td><img src="/public/images/games/icons/<?php print strtoupper($account->game);?>.jpg" alt="" /></td>
            <td class="left"><?php print $account->name;?></td>
            <td class="left"><?php print $account->value;?></td>
            <td><?php print $account->status;?></td>
            <td><?php print $account->operations;?></td>
        </tr>
    <?php endforeach; ?>
    <?php if(empty($variables)): ?>
        <tr>
            <td colspan="5"><?php print t('There are no available gameaccounts added.'); ?></td>
        </tr>
    <?php endif; ?>
</table>

<a href="/admin/games/gameaccounts?tab=add" class="submit22"><?php print t('Add gameaccount');?></a>
