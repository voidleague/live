<table>
    <tr>
        <th><?php print t('Photo'); ?></th>
        <th class="left"><?php print t('Title');?></th>
        <th><?php print t('Tag');?></th>
        <th><?php print t('Status');?></th>
        <th><?php print t('Operations');?></th>
    </tr>
    <?php foreach($variables AS $game): ?>
        <tr>
            <td><img src="/public/images/games/icons/<?php print strtoupper($game->tag);?>.jpg" alt="" /></td>
            <td class="left"><?php print $game->title;?></td>
            <td class="left"><?php print $game->tag;?></td>
            <td><?php print $game->status;?></td>
            <td><?php print $game->operations;?></td>
        </tr>
    <?php endforeach; ?>
</table>