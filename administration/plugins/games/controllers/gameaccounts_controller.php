<?php
class GameaccountsController extends AppController {
    public function gameaccounts_list() {
        $gamesaccounts = array();
        $getGamesaccounts = db_query("SELECT gamesaccounts.*, ".PREFIX."speles.title  AS game_title
                                     FROM `gamesaccounts`
                                     LEFT JOIN ".PREFIX."speles ON ".PREFIX."speles.tag = gamesaccounts.game");
        while($getGameaccount = db_fetch_object($getGamesaccounts)) {
            $getGameaccount->operations = l(t('edit'),'admin/games/gameaccounts?tab=edit&id='.$getGameaccount->id).' | '.l(t('delete'),'admin/games/gameaccounts?tab=delete&id='.$getGameaccount->id,array('attributes'=>array('class'=>array('confirm'))));
            $getGameaccount->status = status($getGameaccount->status);
            $gamesaccounts[] = $getGameaccount;
        }
        return $this->view('list',$gamesaccounts);
    }

    public function delete() {
        $query = get_query_parameters();
        if(isset($query->id) && is_numeric($query->id)) {
            db_query("DELETE FROM `gamesaccounts` WHERE `id` = '".$query->id."'");
            $this->setMessage(t('Gameaccount successfully deleted'));
            $this->redirect('admin/games/gameaccounts');
        }
    }

    public function add($id=false) {
        if($id && is_numeric($id)) {
            $game = db_fetch_object(db_query("SELECT * FROM `gamesaccounts` WHERE `id` = '".$id."'"));
        }
        $games = array();
        $getGames = db_query("SELECT * FROM ".PREFIX."speles WHERE `status` = 1 ORDER BY `status` DESC, `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $games[$getGame->tag] = $getGame->title;
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            $data->status = (isset($data->status) && $data->status == 'on') ? 1 : 0;
            if($id) {
                $data->gameaccountId = $id;
                $this->_gameaccount_update($data);
            }
            else {
                $this->_gameaccount_create($data);
            }
        }


        $form = array(
            'game' => array(
                '#type'=> 'select',
                '#options' => $games,
                '#default_value' => ($id) ? $game->game : '',
                '#title' => t('Game'),
            ),
            'name' => array(
                '#type'=> 'textfield',
                '#title' => t('Label'),
                '#default_value' => ($id) ? $game->name : '',
                '#description' => t('For example. "Nordic&East server"')
            ),
            'value' => array(
                '#type'=> 'textfield',
                '#title' => t('Value'),
                '#default_value' => ($id) ? $game->value : '',
                '#description' => t('For example: eune'),
            ),
            'status' => array(
                '#type'=> 'checkbox',
                '#title' => t('Published'),
                '#default_value' => ($id) ? $game->status : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            )

        );
        return render_form($form);
    }

    public function edit() {
        $query = get_query_parameters();
        $id = (isset($query->id)) ? $query->id : null;
        return $this->add($id);
    }

    private function _gameaccount_create($data) {
        db_query("INSERT INTO `gamesaccounts` (`game`,`name`,`value`,`status`) VALUES ('".$data->game."','".$data->name."','".$data->value."','".$data->status."')");
        $this->setMessage(t('Gameaccount successfully created'));
        $this->redirect('admin/games/gameaccounts');
    }

    private function _gameaccount_update($data) {
        db_query("UPDATE `gamesaccounts` SET `game` = '".$data->game."', `name` = '".$data->name."', `value` = '".$data->value."', `status` = '".$data->status."'");
        $this->setMessage(t('Gameaccount successfully updated'));
        $this->redirect('admin/games/gameaccounts');
    }
}