<?php
class GamesController extends AppController{
    public $permission = 'administer games';
    public $homepath = 'list';
    public $title = 'Games';
    public function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add'),
            'gameaccounts' => t('Gameaccounts'),
        );
        parent::admin_links();
    }

    public function admin_list() {
        return $this->admin_index();
    }

    public function admin_edit() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            return $this->admin_add($args[3]);
        }
        else {
            $this->setError('Invalid link');
            $this->redirect('admin/games');
        }
    }


    public function admin_index() {
        $games = array();
        $getGames = db_query("SELECT * FROM ".PREFIX."speles ORDER BY `status` DESC, `title` ASC");
        while($getGame = db_fetch_object($getGames)) {
            $getGame->operations = l(t('edit'),'admin/games/edit/'.$getGame->id);

            if($getGame->security != 1) {
                $getGame->operations .= ' | '.l(t('delete'),'admin/games/delete/'.$getGame->id,array('attributes'=>array('class'=>array('confirm'))));
            }
            $getGame->status = status($getGame->status);
            $games[] = $getGame;
        }
        return $this->view('list',$games);
    }

    public function admin_delete() {
        $args = arg();
        if(is_numeric($args[3])) {
            $game = db_fetch_object(db_query("SELECT * FROM ".PREFIX."speles WHERE `id` = '".$args[3]."'"));
            if($game->security) {
              $this->setError(t('This game is secured'));
            }
            else {
                db_query("DELETE FROM ".PREFIX."speles WHERE `id` = '".$args[3]."'");
                $this->setMessage(t('Game successfully deleted'));
            }
            $this->redirect('admin/games');
        }
    }

    public function admin_add($id=false) {
        $game = false;
        if(!empty($id) && is_numeric($id)) {
            $game = db_fetch_object(db_query("SELECT * FROM ".PREFIX."speles WHERE `id` = '".$id."'"));
            if(empty($game->id)) {
                $this->setError(t('We could not find this game'));
                $this->redirect('admin/games');
            }
        }

        $form = array(
            'photo' => array(
                '#type' => 'image',
                '#title' => t('Image'),
                '#default_value' => ($game) ? $game->photo : '',
                '#path' => 'public/images/games/icons',
                '#allowed' => array('jpg'),
            ),
            'title' => array(
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#default_value' => ($game) ? $game->title : '',
            ),
            'tag' => array(
                '#type' => 'textfield',
                '#title' => t('Tag'),
                '#default_value' => ($game) ? $game->tag : '',
            ),
            'create_team' => array(
                '#type' => 'checkbox',
                '#title' => t('Can create team'),
                '#default_value' => ($game) ? $game->create_team : '',
            ),
            'can_draw' => array(
                '#type' => 'checkbox',
                '#title' => t('Can be draw'),
                '#default_value' => ($game) ? $game->can_draw : '',
            ),
            'status' => array(
                '#type' => 'checkbox',
                '#title' => t('Status'),
                '#default_value' => ($game) ? $game->status : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            )
        );


        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = new stdClass();
            $data->values = $this->security($_POST);
            $data->form = $form;
            $data->game = $game;
            $data->values->status = (isset($data->values->status) && $data->values->status == 'on') ? 1 : 0;
            $data->values->can_draw = (isset($data->values->can_draw) && $data->values->can_draw == 'on') ? 1 : 0;
            $data->values->create_team = (isset($data->values->create_team) && $data->values->create_team == 'on') ? 1 : 0;
            $data->values->photo = $_FILES['photo'];
            if($game) {
                $this->_update_game($data);
            }
            else {
                $this->_create_game($data);
            }
        }
        return render_form($form);
    }

    private function _create_game($data) {
        $args = arg();
        try {
            $this->game_validate($data);
            if(!empty($data->values->photo['name'])) {
                include ROOT.'/application/controllers/IMGupload.php';
                $extension = strtolower(getExtension(stripslashes($data->values->photo['name'])));
                $file = $data->values->photo;
                $image = new SimpleImage();
                $image->load($file['tmp_name']);
                $image->resize(48,48);
                $image->save($data->form->photo['#destination'].'/'.$name.'.'.$extension);
            }

            db_query("INSERT INTO ".PREFIX."speles () VALUES ()");
            db_query("UPDATE ".PREFIX."speles SET `title` = '".$data->values->title."', `tag` = '".$data->values->tag."', `create_team` = '".$data->values->create_team."', `can_draw` = '".$data->values->can_draw."', `status` = '".$data->values->status."' WHERE `id` = '".intval($args[3])."'");
            $this->setMessage(t('Game successfully created'));
            $this->redirect('admin/games');
        }
        catch(Error $e) {
            print '<div class="bx error">'.$e->getMessage().'</div>';
        }
    }


    private function _update_game($data) {
        $args = arg();
        try {
            $this->game_validate($data);
            if(!empty($data->values->photo['name'])) {
                include ROOT.'/application/controllers/IMGupload.php';
                $extension = strtolower(getExtension(stripslashes($data->values->photo['name'])));
                $file = $data->values->photo;
                $image = new SimpleImage();
                $image->load($file['tmp_name']);
                $image->resize(48,48);
                $image->save($data->form->photo['#destination'].'/'.$name.'.'.$extension);
            }

            db_query("UPDATE ".PREFIX."speles SET `title` = '".$data->values->title."', `tag` = '".$data->values->tag."', `create_team` = '".$data->values->create_team."', `can_draw` = '".$data->values->can_draw."', `status` = '".$data->values->status."' WHERE `id` = '".intval($args[3])."'");
            $this->setMessage(t('Game successfully updated'));
            $this->redirect('admin/games');
        }
        catch(Error $e) {
            print '<div class="bx error">'.$e->getMessage().'</div>';
        }
    }


    private function game_validate($data) {
        if(empty($data->values->title)) {
            throw new Error(t('Please fill name field'));
        }
        if(empty($data->values->tag)) {
            throw new Error(t('Please fill tag field'));
        }
        if(empty($data->values->photo['name']) && empty($data->game->photo)) {
            throw new Error(t('Please choose image'));
        }
    }

    public function admin_gameaccounts() {
        include ROOT.'/administration/plugins/games/controllers/gameaccounts_controller.php';
        $gameaccount = new GameaccountsController();
        $query = get_query_parameters();
        if(empty($query->tab)) {
            $query->tab = 'gameaccounts_list';
        }
        if(method_exists('GameaccountsController',$query->tab)) {
            $content = $gameaccount->{$query->tab}();
            return $this->view('gameaccounts',array('content'=>$content));
        }
    }
}