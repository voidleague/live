<?php
use \ORM;

error_reporting(E_ALL);
ini_set("display_errors", 1);


class Rank_resetController extends AppController {

    public function admin_index() {
        /*
        $getRank = $this->getRank();
        foreach($getRank AS $rank) {
            if($rank['now']) {
                $this->saveRank($rank);
            }
        }
        */
        echo 'done';
    }

    protected function getRank() {
        return ORM::for_table('rank')
            ->where('type','team')
            ->find_array();
    }

    protected function saveRank($rank) {

        ORM::raw_execute("INSERT INTO
            `rank_history`
          SET
            `entity_id` = :entity_id,
            `entity_type` = :entity_type,
            `rank` = :rank,
            `rating` = :rating,
            `year` = :year
          ON DUPLICATE KEY UPDATE
            `rank` = :rank,
            `rating` = :rating
            ",[
                'entity_id' => $rank['entity_id'],
                'entity_type' => $rank['type'],
                'rank' => $rank['now'],
                'rating' => $rank['rating'],
                'year' => date('Y',strtotime("-1 year", time()))
        ]);


    }
}