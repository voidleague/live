<?php
use \ORM;
/**
 * Class RulesController
 * Rules template system with replace variables
 */
class RulesController extends AppController {
    /**
     * League Of Legends game ID
     * @var int
     */
    private $game = 13;

    public function __construct() {
        $this->title = t('Rules');
        $this->homepath = 'list';
        $this->permission = 'administer rules';
    }

    public function admin_index() {
        if(isset($_POST) && $_POST) {
            $this->save_rules($_POST);
        }

        $rules = [];
        $get_rules = ORM::for_table('cups_rules_templates')->where(['game'=>$this->game])->find_many();
        foreach($get_rules AS $get_rule) {
            $rules[$get_rule->game_type] = $get_rule->rules;
        }

        $form = [
            'description' => [
                '#type' => 'markup',
                '#markup' => '<br /><div class="bx info"><strong>Replacements:</strong><br />
                    %tournament_datetime - Tournamente datetime (For example 2014-10-21 16:00)<br />
                    %tournament_date - Tournamente date (For example 2014-10-21)<br />
                    %tournament_time - Tournament time. (For example 16:00)<br />
                    %region - Tournament time. (For example West)<br />
                    %timezone - Tournament timezone. (For example CET timezone)<br />
                    %tournament_default_time - Tournament unconverted time. (For example 16:00)<br />
                </div>',
            ],
            '3on3' => [
                '#type' => 'textarea',
                '#format' => 'full_html',
                '#title' => '3on3',
                '#default_value' => (!empty($rules['3on3']) ? $rules['3on3'] : '')
            ],
            'classic' => [
                '#type' => 'textarea',
                '#format' => 'full_html',
                '#title' => '5on5 Classic',
                '#default_value' => (!empty($rules['classic']) ? $rules['classic'] : '')
            ],
            'aram' => [
                '#type' => 'textarea',
                '#format' => 'full_html',
                '#title' => '5on5 ARAM',
                '#default_value' => (!empty($rules['aram']) ? $rules['aram'] : '')
            ],
            'exist' => [
                '#type' => 'hidden',
                '#value' => (!empty($rules) ? 1 : 0)
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Save')
            ],
        ];
        return render_form($form);
    }

    /**
     * Save rules
     *
     * @param $data
     */
    private function save_rules($data) {
        // Remove submit button
        unset($data['submit']);

        foreach($data AS $game_type => $rule) {
            ORM::raw_execute("INSERT INTO
                                `cups_rules_templates`
                              SET
                                `game` = :game,
                                `game_type` = :game_type,
                                `rules` = :rules,
                                `updated` = NOW()
                              ON DUPLICATE KEY UPDATE
                                `rules` = :rules,
                                `updated` = NOW()",[
                                'game' => $this->game,
                                'rules' => $rule,
                                'game_type' => $game_type
                              ]);
        }
        $this->setMessage(t('Rules successfully saved'));
    }
}