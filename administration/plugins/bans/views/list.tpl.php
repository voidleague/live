<table class="ip_bans">
    <tr>
        <th style="width:10px;">#bid</th>
        <th>IP</th>
        <th><?php print t('Banned till'); ?></th>
        <th><?php print t('Reason'); ?></th>
        <th><?php print t('Banned by'); ?></th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['bans'] AS $ban): ?>
        <tr>
            <td class="bold"><?php print $ban->id; ?></td>
            <td><?php print $ban->ip; ?></td>
            <td><?php print $ban->date; ?></td>
            <td><?php print $ban->reason; ?></td>
            <td><?php print $ban->banned_by; ?></td>
            <td><?php print $ban->operations; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php print $variables['pager']; ?>