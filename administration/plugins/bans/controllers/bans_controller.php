<?php
class BansController extends AppController {
    function __construct() {
        $this->homepath = 'list';
        if(!access('b') OR !admin()) {
            $this->setError(t("Access denied"));
        }
        return '';
    }
    
    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add ban'),
        );
        parent::admin_links();
    }
    
    function admin_index() {
        return $this->admin_list();
    }
    
    function admin_add($ban_id = false) {
        $query = get_query_parameters();
        if($ban_id) {
            $ban = db_fetch_object(db_query("SELECT * FROM `ip_bans` WHERE `id` = '".$ban_id."'"));
        }
        if(isset($ban->ip)) {
            $ip = $ban->ip;
        }
        elseif(isset($query->ip)) {
            $ip = $query->ip;
        }
        else {
            $ip = '';
        }
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            if($ban_id) {
                $data->ban_id = $ban_id;
            }
            $this->save($data);
        }
        $form = array(
            'ip' => array(
                '#type' => 'textfield',
                '#title' => t('IP'),
                '#default_value' => $ip,
            ),
            'time' => array(
                '#type' => 'date',
                '#default_value' => (isset($ban->date)) ? $ban->date : '',
                '#title' => t('Ban till'),
            ),
            'perm' => array(
                '#type' => 'checkbox',
                '#title' => t('Permanent'),
                '#default_value' => (isset($ban->perm)) ? $ban->perm : '',
            ),
            'reason' => array(
                '#type' => 'textarea',
                '#title' => t('Reason'),
                '#default_value' => (isset($ban->reason)) ? $ban->reason : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Submit'),
            )
        );
        
        return $this->view('form',array('form'=>$form));
    }
    
    function admin_edit() {
        $routes = arg();
        if(is_numeric($routes[3])) {
            return $this->admin_add($routes[3]);
        }
        
    }
    
    function admin_delete() {
        $routes = arg();
        if(is_numeric($routes[3])) {
            db_query("DELETE FROM `ip_bans` WHERE `id` = '".$routes[3]."'");
            $this->setMessage(t('Ban successfully deleted'));
            redirect('/admin/bans');
        }
    }
    
    
    
    function admin_list() {
        $getBansCount = db_result(db_query("SELECT count(*) FROM `ip_bans`"));
        // create pager
        $pager = $this->pager($getBansCount);
        
        $getBans = db_query("SELECT ip_bans.*,".PREFIX."lietotaji.lietotajvards AS username
                            FROM `ip_bans`
                            LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = ip_bans.uid
                            ORDER BY `date` DESC
                            LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $bans = array();
        while($getBan = db_fetch_object($getBans)) {
            $getBan->banned_by = getMember($getBan->username);
            
            // Operations links
            $operations = '<a href="/admin/bans/edit/'.$getBan->id.'/">'.t('edit').'</a>';
            if(empty($getLanguage->default)) {
                $operations .= ' | <a href="/admin/bans/delete/'.$getBan->id.'/" class="confirm">'.t('delete').'</a>';
            }
            $getBan->operations = $operations;
            
            //xss
            $getBan->reason = htmlspecialchars($getBan->reason);
            
            if($getBan->perm == 1) {
                $getBan->date = t('permament');
            }
            $bans[] = $getBan;
        }
        return $this->view('list',array('bans'=>$bans,'pager'=>$pager)); 
    }
    
    
    /*
     * Add or update ban
     */
    private function save($data) {
        if(isset($data->perm) && $data->perm == 'on') {
            $data->perm = 1;
        }
        else {
            $data->perm = NULL;
        }
        if(isset($data->ban_id)) {
            db_query("UPDATE `ip_bans` SET `date` = '".$data->time."', `perm` = '".$data->perm."', `reason` = '".$data->reason."' WHERE `id` = '".$data->ban_id."'");
        }
        else {
            db_query("INSERT INTO `ip_bans` (`date`,`perm`,`reason`,`ip`,`uid`,`created`) VALUES ('".$data->time."','".$data->perm."','".$data->reason."','".$data->ip."','".User::$user->uid."','".time()."')");
        }
        $this->setMessage(t('Ban successfully saved'));
        redirect('/admin/bans');
    }
}
