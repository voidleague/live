<?php

use \ORM;
class SyncController extends AppController{
    public $game = 'LOL';
    public $type = 'team';

    private function sync_manual($region, $format) {

        $teams_ids = [];
        $getTeams = ORM::for_table('rank')
            ->select('entity_id')
            ->where('type',$this->type)
            ->where('game', $this->game)
            ->where('region', $region)
            ->where('format', $format)
            ->find_array();

        foreach($getTeams AS $team_id) {
            $teams_ids[] = $team_id['entity_id'];
        }


        ORM::raw_execute("DELETE FROM
                            `rank`
                          WHERE
                            `entity_id` IN (".implode(',',$teams_ids).")");

        ORM::raw_execute("DELETE FROM
                            `rank_p10`
                          WHERE
                            `entity_id` IN (".implode(',',$teams_ids).")");


        $rank = new Ranking;

        $sql = "
        SELECT
          *
        FROM
          `matches`
        WHERE
          `entity_type` = '".$this->type."' AND
        (`winner_entity` IN(".implode(',',$teams_ids).") OR `looser_entity` IN(".implode(',',$teams_ids)."))
        ORDER BY `id`";

        $matches = db_query($sql);

        while($match = db_fetch_object($matches)) {
            $rank->add($match->winner_entity,$match->winner_score,$match->looser_entity,$match->looser_score,$this->game,$this->type, $match->created);
        }


        $rank->_reorder($this->game,$this->type,$region,$format);
    }


    public function admin_index() {

        //$this->sync_manual('na','3on3');
        /*
        db_query("DELETE FROM `rank` WHERE `type` = '".$this->type."'");
        db_query("DELETE FROM `rank_p10` WHERE `type` = '".$this->type."'");
        $matches = db_query("SELECT * FROM `matches` WHERE `entity_type` = '".$this->type."' ORDER BY `id` ASC");
        $rank = new Ranking;
        while($match = db_fetch_object($matches)) {
            $rank->add($match->winner_entity,$match->winner_score,$match->looser_entity,$match->looser_score,$this->game,$this->type, $match->created);
        }
        */
/*
        $rank = new Ranking;
        $rank->_reorder($this->game,$this->type,'eune','5on5');
        $rank->_reorder($this->game,$this->type,'eune','3on3');
        $rank->_reorder($this->game,$this->type,'euw','5on5');
        $rank->_reorder($this->game,$this->type,'euw','3on3');
        $rank->_reorder($this->game,$this->type,'na','3on3');
        */
        return 'Sync finished...';
    }

    public function admin_teams_delete() {
        $teams = db_query("SELECT * FROM `users_teams` ORDER BY `id` ASC");
        while($team = db_fetch_object($teams)) {
            // count rooster
            $roster = db_result(db_query("SELECT count(*) FROM `users_teams_members` WHERE `team` = '".$team->id."'"));
            if(empty($roster)) {
                db_query("UPDATE `users_teams` SET `picture` = '', `uid` = 0, `name` = '*deleted*', `tag` = '*deleted*', `status` = 0 WHERE `id` = '".$team->id."'");
                db_query("DELETE FROM `rank` WHERE `entity_id` = '".$team->id."' AND `type` = 'team'");
                dsm($team->name);
            }
        }
        return 'delete';
    }


    public function admin_update_captain_gameaccount() {
        $captains = db_query("SELECT  `uid` FROM `users_teams_members` WHERE `gid` = 0");
        while($captain = db_fetch_object($captains)) {
            $gameaccount = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `uid` = '".$captain->uid."'"));
            if(!empty($gameaccount)) {
                db_query("UPDATE `users_teams_members` SET `gid` = '".$gameaccount->id."' WHERE `uid` = '".$captain->uid."'");
                dsm($gameaccount);
            }
        }
    }

    public function admin_get_teams_regions() {
        $getTeams = db_query("SELECT * FROM `users_teams` WHERE `region` = '' AND `status` = 1 AND `game` = 'LOL'");
        $count = 0;
        while($team = db_fetch_object($getTeams)) {
            // get last cup
            $cid = db_result(db_query("SELECT `cid` FROM `cups_tree`
                                        LEFT JOIN `matches` ON matches.id = cups_tree.match_id
                                        WHERE (matches.winner_entity = '".$team->id."' OR matches.looser_entity = '".$team->id."') AND matches.entity_type = 'team'
                                        ORDER BY matches.id DESC"));

            if(!empty($cid)) {
                $count++;
            }
            $cup_name = db_result(db_query("SELECT `name` FROM `cups` WHERE `id` = '".$cid."'"));
            if(preg_match('/EAST/',$cup_name)) {
                $region = 'eune';
            }
            else {
                $region = 'euw';
            }
            db_query("UPDATE `users_teams` SET `region` = '".$region."' WHERE `id` = '".$team->id."'");
        }

        db_query("UPDATE `users_teams` SET `status` = 0, `name` = '*deleted*',`tag` = '*deleted*', `uid` = 0 WHERE `region` = '' AND `game` = 'LOL'");
        dsm($count);
        return 'op';
    }
}