<?php
class PollsController extends AppController {
    function __construct() {
        $this->title = t('Polls');
        $this->permission = 'administer polls';
        $this->homepath = 'list';
    }

    public function admin_index() {
        return $this->admin_list();
    }

    public function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add')
        );
        return parent::admin_links();
    }

    public function admin_list() {
        $output = '';
        $getPolls = db_query("SELECT * FROM `polls` ORDER BY `id` DESC");
        $output .= '<table><tr><th class="left">'.t('Question').'</th><th class="center">'.t('Published').'</th><th class="center">'.t('Operatons').'</th></tr>';
        while($poll = db_fetch_object($getPolls)) {
            $poll->status = ($poll->published == 1)? 'ok' : 'notok';
            $status = '<img src="/public/images/admin/status_'.$poll->status.'.png" alt="" />';

            $output .= '<tr><td class="left">'.$poll->question.'</td>
                        <td class="center">'.$status.'</td>
                        <td class="center"><a href="/admin/polls/edit/'.$poll->id.'">'.t('edit').'</a> | <a class="confirm" href="/admin/polls/delete/'.$poll->id.'">'.t('delete').'</a></td></tr>';
        }
        $output .= '</table>';
        return $output;
    }

    public function admin_edit() {
        $args = arg();
        if(is_numeric($args[3])) {
            return $this->admin_add($args[3]);
        }
    }

    public function admin_add($id=false) {
        if($id) {
            $poll = db_fetch_object(db_query("SELECT * FROM `polls` WHERE `id` = '".$id."'"));
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $this->save($_POST);
        }

        $form = array(
            'id' => array(
                '#type' => 'hidden',
                '#value' => ($id) ? $poll->id : FALSE,
            ),
            'question' => array(
                '#type' => 'textfield',
                '#default_value' => ($id) ? $poll->question : '',
                '#title' => t('Question'),
            ),
            'answers' => array(
                '#type' => 'fieldset',
                '#title' => t('Answers'),
                '#children' => array(),
            ),
            'published' => array(
                '#type' => 'checkbox',
                '#default_value' => ($id) ? $poll->published : '',
                '#title' => t('Published'),
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );

        // Get poll answers
        if(!$id) {
            $form['answers']['#children'][] = array(
                '#type' => 'textfield',
            );
            $form['answers']['#children'][] = array(
                '#type' => 'textfield',
            );
        }
        else {
            $poll_answers = db_query("SELECT * FROM `polls_answers` WHERE `poll_id` = '".$id."' ORDER BY `id` ASC");
            while($poll_answer = db_fetch_object($poll_answers)) {
                $form['answers']['#children'][$poll_answer->id] = array(
                    '#type' => 'textfield',
                    '#default_value' => $poll_answer->answer
                );
            }
        }

        // add answer
        $form['answers']['#children']['add_answer'] = array(
            '#type' => 'markup',
            '#markup' => '<a href="#" class="add-poll-answer">'.t('Add answer').'</a>',
        );
        return render_form($form);
    }


    private function save($data) {
        $data = $this->security($data);
        if(empty($data->question) || empty($data->answers)) {
            $this->setError(t('Please fill all fields'));
        }
        else {
            $data->published = (isset($data->published) && $data->published == 'on') ? 1 : 0;
            // Insert poll
            if(empty($data->id)) {
                db_query("INSERT INTO `polls` (`question`,`published`) VALUES ('".$data->question."','".$data->published."')");
                $data->id = db_result(db_query("SELECT `id` FROM `polls` ORDER BY `id` DESC"));
                foreach($data->answers AS $answer) {
                    if(!empty($answer)) {
                        db_query("INSERT INTO `polls_answers` (`poll_id`, `answer`) VALUES ('".$data->id."','".$answer."')");
                    }
                }
            }
            else {
            // Update poll
                db_query("UPDATE `polls` SET `question` = '".$data->question."', `published` = '".$data->published."' WHERE `id` = '".$data->id."'");
                $used_answers = array();
                foreach($data->answers AS $key => $answer) {
                    if(!empty($answer)) {
                        db_query("UPDATE `polls_answers` SET `answer` = '".$answer."' WHERE `poll_id` = '".$data->id."' AND `id` = '".$key."'");
                        $used_answers[] = $key;
                    }
                }

                // Deleted empty answers
                db_query("DELETE FROM `polls_answers` WHERE `id` NOT IN(".implode(',',$used_answers).") AND `poll_id` = '".$data->id."'");

                //Add new answers
                if(!empty($data->new_answers)) {
                    foreach($data->new_answers AS $answer) {
                        if(!empty($answer)) {
                            db_query("INSERT INTO `polls_answers` (`poll_id`, `answer`) VALUES ('".$data->id."','".$answer."')");
                        }
                    }
                }
            }

            $this->setMessage(t('Poll successfully saved'));
            $this->redirect('admin/polls');
        }
    }

    public function admin_delete() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            db_query("DELETE FROM `polls` WHERE `id` = '".$args[3]."'");
            db_query("DELETE FROM `polls_answers` WHERE `poll_id` = '".$args[3]."'");
            db_query("DELETE FROM `polls_data` WHERE `poll_id` = '".$args[3]."'");
             $this->setMessage(t('Poll successfully deleted'));
            $this->redirect('admin/polls');
        }
    }
}