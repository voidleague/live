<br />
<div class="bx info">
    You need google chrome extension to use riot application links!<br />
    Download: <a href="https://chrome.google.com/webstore/detail/voidleague-rito/nfhggggklphdkomdlnlojeofpcgfheom?hl=en-US" target="_blank">"VoidLeague RITO" chrome extension</a>
</div>

<table class="news">
    <tr>
        <th>Name</th>
        <th>Start date</th>
        <th class="center"><?php print t('Application'); ?></th>
    </tr>
    <?php foreach($cups AS $cup): ?>
        <tr>
            <td class="left"><a href="/cups/id/<?php print $cup['slug'];?>-<?php print $cup['id'];?>"><?php print $cup['name']; ?></a></td>
            <td class="left"><?php print date('d.m.Y H:i', strtotime($cup['start_time']));?></td>
            <td><a title="Send application to riot" href='<?=$cup['riot_submit'];?>' target="_blank"><img src="/public/img/riot_submit.png" alt="" /></a></td>
        </tr>
    <?php endforeach; ?>
</table>