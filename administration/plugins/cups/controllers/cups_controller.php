<?php

use \ORM;

class CupsController extends AppController{
    function admin_index() {
       return $this->cups_admin_list();   
    }


    function admin_links() {
        $this->links = array(
            'index' => t('Create cup'),
            'generations' => t('Generation'),
        );
        return parent::admin_links();
    }


    function cups_admin_list() {
        $this->title = t('Create cup');
        $games = db_query("SELECT * FROM ".PREFIX."speles ORDER BY `title` ASC");
        $output = '';
        while($game = db_fetch_object($games)) {
            if(user_access('administer '.$game->tag.' cups')) {
                $output .= '<div class="admin-cup block"><a href="/admin/cups/create/'.$game->tag.'"><img src="/public/images/games/icons/'.$game->tag.'.jpg" alt="" /></a></div>';
            }
        }
        return $output;
    }
    
    function admin_create($cup=false) {
        
        $this->title = t('Create cup');
        if(isset($_POST['delete'])) {
            db_query("DELETE FROM `cups` WHERE `id` = '".$cup->id."'");
            $this->setMessage(t('Cup successfully deleted'));
            redirect('/cups');
        }
        if(isset($_POST['submit'])) {
            $data = $this->security($_POST);
            $data = (object)$_POST;
            

            if(empty($data->name) || empty($data->start_time)) {
                //$this->setError(t('Please fill all fields'));
            }
            else {
                $data->checkin_date = date('Y-m-d H:i',(strtotime($data->start_time)-900));
                if($cup) {
                    $data->id = $cup->id;
                    $data->game = $cup->game;
                    if(empty($data->delay)) {
                      $data->delay = 0;
                    }
                    $this->_update_cup($data);
                }
                else {
                    $this->_create_cup($data);
                }
            }
        }
        
        $args = arg();
        $game = isset($cup->game)?$cup->game:$args[3];

        $format_options = array('1on1'=>'1on1','2on2'=>'2on2','3on3'=>'3on3',/*'4on4'=>'4on4',*/'5on5'=>'5on5');
        $part_options = array(4=>4,8=>8,16=>16,32=>32,64=>64);
        
        if(!empty($cup->type) && $cup->type == 'players') {
            $format_options = array('1on1'=>'1on1');
        }
        elseif(!empty($cup->type) && $cup->type == 'teams') {
            unset($format_options['1on1']);
        }
        if(!empty($cup->start_time)) {
            $cup->start_time = substr($cup->start_time,0,-3);
        }
        if(!empty($cup->checkin_date)) {
            $cup->checkin_date = substr($cup->checkin_date,0,-3);
        }

        $place3_status = (isset($cup->place3) && $cup->status == 0 || !empty($cup)) ? FALSE : TRUE;
        $disabled = FALSE;
        $form = array(
            'name' => array(
                '#title' => t('Name'),
                '#type' => 'textfield',
                '#default_value' => (!empty($cup->name)) ? $cup->name : '',
            ),
            'registration' => array(
                '#type' => 'select',
                '#default_value' => isset($cup->registration) ? $cup->registration : '',
                '#title' => t('Registration opened'),
                '#options' => array(0=>t('No'),1=>t('Yes')),
            ),
            'teams' => array(
                '#type'  => 'select',
                '#title' => t('Teams'),
                '#options' => $part_options,
                '#default_value' => (!empty($cup->teams)) ? $cup->teams : '',
            ),
            'format' => array(
                '#type' => 'select',
                '#title' => t('Format'),
                '#options' => $format_options,
                '#default_value' => (!empty($cup->format)) ? $cup->format : '',
            ),
            'place3' => array(
                '#type' => 'checkbox',
                '#title' => '3/4 place bracket',
                '#default_value' => !(empty($cup->place3)) ? $cup->place3 : '',
                '#disabled' => $disabled,
            ),
            'double_elimination' => [
                '#type' => 'checkbox',
                '#title' => 'double elimination',
                '#default_value' => (!empty($cup->double_elimination) ? 1 : ''),
            ],
            'delay' => array(
                '#title' => t('Delay'),
                '#type' => 'textfield',
                '#default_value' => (!empty($cup->delay)) ? $cup->delay : '',
            ),
            'start_time' => array(
                '#type' => 'date',
                '#title' => t('Start date'),
                '#default_value' => (!empty($cup->start_time)) ? $cup->start_time : '',
            ),
            'published' => array(
                '#type' => 'checkbox',
                '#title' => 'Published',
                '#default_value' => !(empty($cup->published)) ? $cup->published : '',
                '#disabled' => $place3_status,
            ),
            'only_invites' => array(
                '#type' => 'checkbox',
                '#title' => 'Invites only',
                '#default_value' => (isset($cup->only_invites) && $cup->only_invites == 'Y') ? 1 : '',
                '#disabled' => $place3_status,
            ),
            'other_cup' => array(
                '#type' => 'checkbox',
                '#title' => 'Other cup',
                '#default_value' => (isset($cup->other_cup) && $cup->other_cup == 'Y') ? 1 : '',
                '#disabled' => $place3_status,
            ),
            'competitive' => array(
                '#type' => 'checkbox',
                '#title' => 'Competitive',
                '#default_value' => (isset($cup->competitive) && $cup->competitive == 'Y') ? 1 : '',
                '#disabled' => $place3_status,
            ),
            'riot_event' => [
                '#title' => t('Riot event link'),
                '#type' => 'textfield',
                '#default_value' => (!empty($cup->riot_event)) ? $cup->riot_event : '',
            ],
            'admins' => array(
                '#type' => 'fieldset',
                '#title' => t('Admins'),
            ),
            'custom_rules' => [
                '#type' => 'textarea',
                '#format' => 'full_html',
                '#title' => 'Custom rules',
                '#default_value' => (!empty($cup->custom_rules) ? $cup->custom_rules : '')
            ],
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
            
            'delete' => array(
                '#type' => 'submit',
                '#value' => t('Delete'),
            ),
        );

        // admins list
        $admins = array();
        $default_admins = array();
        if(isset($cup->id)) {
            $getDefaultAdmins = db_query("SELECT * FROM `cups_admins` WHERE `cid` = '".$cup->id."'");
            while($getDefaultAdmin = db_fetch_object($getDefaultAdmins)) {
                $default_admins[$getDefaultAdmin->uid] = true;
            }
        }


        //$getGameAdmins = db_query("SELECT * FROM ".PREFIX."lietotaji AS u, u.id AS uid WHERE u.id != '".User::$user->uid."' ORDER BY `id` ASC limit 10");
        $getGameAdmins = db_query("SELECT *
                                    FROM ".PREFIX."lietotaji
                                    JOIN `users_roles` ON users_roles.uid = ".PREFIX."lietotaji.id
                                    JOIN `role_permission` ON role_permission.rid = users_roles.rid
                                    WHERE `permission` = 'administer ".$game." cups'
                                    ORDER BY ".PREFIX."lietotaji.id ASC");
        while($getGameAdmin = db_fetch_object($getGameAdmins)) {
            $admins[$getGameAdmin->id] = array(
                '#type' => 'checkbox',
                '#title' => $getGameAdmin->lietotajvards,
                '#default_value' => (isset($default_admins[$getGameAdmin->uid])) ? 1 : '',
            );
        }
        $form['admins']['#children'] = $admins;


        // Add region
        $regions = array();
        $getRegions = db_query("SELECT `name`,`value` FROM `gamesaccounts` WHERE `game` = '".$game."'");
        while($getRegion = db_fetch_object($getRegions)) {
            $regions[$getRegion->value] = $getRegion->name;
        }
        $region = array(
            'region' => array(
                '#type' => 'select',
                '#title' => t('Region'),
                '#default_value' => (isset($cup->id)) ? $cup->region : '',
                '#options' => $regions,
            ),
        );
        $form = $region+$form;

        return $this->view('form',array('form'=>$form));
    }
    
    function admin_edit() {
        $arg = arg();
        if(!empty($arg[3]) && is_numeric($arg[3])) {
            $cup = db_fetch_object(db_query("SELECT * FROM `cups` WHERE `id` = '".$arg[3]."'"));
            if(!empty($cup->id)) {
                return $this->admin_create($cup);
            }
        }
        return false;
    }
    
    function _create_cup($cup) {
        $arg = arg();

        if($cup->format == '1on1') {
            $cup->type = 'players';
        }
        else {
            $cup->type = 'teams';
        }

        $cup->game = $arg[3];
        $cup->slug = slug($cup->name);
        
        //$cup = $this->security($cup);
        $cup->place3 = (isset($cup->place3) && $cup->place3 == 'on') ? 1 : 0;
        $cup->published = (isset($cup->published) && $cup->published == 'on') ? 1 : 0;
        $cup->double = (isset($cup->double_elimination) && $cup->double_elimination == 'on') ? 1 : 0;


        db_query("INSERT INTO `cups` (`double_elimination`, `region`,`published`, `delay`,`place3`, `checkin_date`,`name`,`slug`,`game`,`type`,`format`,`teams`,`start_time`,`uid`) VALUES ('".$cup->double."', '".$cup->region."','".$cup->published."','".$cup->delay."','".$cup->place3."','".$cup->checkin_date."','".$cup->name."','".$cup->slug."','".$cup->game."','".$cup->type."','".$cup->format."','".$cup->teams."','".$cup->start_time."','".User::$user->uid."')");
        $cup->id = db_result(db_query("SELECT `id` FROM `cups` ORDER BY `id` DESC LIMIT 1"));


        $this->_cup_admins($cup);
        $this->setMessage(t('Cup sucessfully created'));
        redirect('/cups/id/'.$cup->slug.'-'.$cup->id);
    }
    
    function _update_cup($cup) {
        $cup->slug = slug($cup->name);

       // $cup = $this->security($cup);
        $cup->place3 = (isset($cup->place3) && $cup->place3 == 'on') ? 1 : 0;
        $cup->double = (isset($cup->double_elimination) && $cup->double_elimination == 'on') ? 1 : 0;
        $cup->published = (isset($cup->published) && $cup->published == 'on') ? 1 : 0;
        $cup->only_invites = (isset($cup->only_invites) && $cup->only_invites == 'on') ? 'Y' : 'N';
        $cup->other_cup = (isset($cup->other_cup) && $cup->other_cup == 'on') ? 'Y' : 'N';
        $cup->competitive = (isset($cup->competitive) && $cup->competitive == 'on') ? 'Y' : 'N';
        db_query("UPDATE `cups` SET `competitive` = '".$cup->competitive."', `other_cup` = '".$cup->other_cup."', `only_invites` = '".$cup->only_invites."', `custom_rules` = '".mysql_real_escape_string($cup->custom_rules)."', `double_elimination` = '".$cup->double."', `riot_event` = '".$cup->riot_event."', `region` = '".$cup->region."', `published` = '".$cup->published."', `delay` = '".$cup->delay."', `place3` = '".$cup->place3."', `checkin_date` = '".$cup->checkin_date."', `start_time` = '".$cup->start_time."', `name` = '".$cup->name."', `slug` = '".$cup->slug."', `format` = '".$cup->format."', `teams` = '".$cup->teams."' WHERE `id` = '".$cup->id."'");


        $this->_cup_admins($cup);
        $this->setMessage(t('Cup sucessfully saved'));
        redirect('/cups/id/'.$cup->slug.'-'.$cup->id);
    }

    private function _cup_admins($cup) {
        db_query("DELETE FROM `cups_admins` WHERE `cid` = '".$cup->id."'");
        //db_query("INSERT INTO `cups_admins` (`cid`,`uid`) VALUES ('".$cup->id."','".User::$user->uid."')");

        if(!empty($cup->admins)) {
            foreach($cup->admins AS $admin_id=>$on) {
                db_query("INSERT INTO `cups_admins` (`cid`,`uid`) VALUES ('".$cup->id."','".$admin_id."')");
            }
        }
    }

    public function admin_generate_tournaments()
    {
        if(isset($_POST) && $_POST) {
            $this->generateTournaments($_POST);
        }
        return $this->view('generate', [
            'form' => $this->generateTournamentsForm()
        ]);
    }

    private function generateTournaments($data)
    {

        $lastTournament = ORM::for_table('cups')
            ->where('region', $data['region'])
            ->where_like('name', '%'.$data['type'].'%')
            ->order_by_desc('id')
            ->find_one();

        switch($data['type']) {
            case 'tt':
                $format = '3on3';
                $game_type = '3on3';
                break;
            default:
                $format = '5on5';
                $game_type = $data['type'];
                break;
        }

        if($game_type != 'aram') {
            $competitive = 'Y';
        } else {
            $competitive = 'N';
        }

        $preparedTournaments = [];

        $last_tournament_id = explode(' ', explode('#', $lastTournament['name'])[1])[0];
        $last_tournament_date = $lastTournament['start_time']; //checkin_date

        for($i=1; $i <= $data['count']; $i++) {
            if($game_type == '3on3') {
                $days = 7;
            } else {
                $days = 14;
            }
            //echo $last_tournament_date."\n";
            $last_tournament_date = date('Y-m-d H:i:s', strtotime($last_tournament_date.' +'.$days.' DAY'));
            //echo $last_tournament_date;
            //die;
            $name = $this->generateTournamentsName($data['type'], $data['region'], $format, $i, $last_tournament_id);
            $preparedTournaments[] = [
                'region' => $data['region'],
                'name' => $name,
                'game_type' => $game_type,
                'slug' => slug($name),
                'format' => $format,
                'teams' => 64,
                'published' => 0,
                'place3' => 1,
                'competitive' => $competitive,
                'game' => 'LOL',
                'start_time' => $last_tournament_date,
                'checkin_date' => date('Y-m-d', strtotime($last_tournament_date.' -15 MINUTE'))
            ];

        }

        foreach($preparedTournaments AS $tournament) {

            $cup = ORM::for_table('cups')->create();

            foreach($tournament AS $field=>$value) {
                $cup->{$field} = $value;
            }

            $cup->save();

        }

        $this->setMessage('done');
    }

    private function generateTournamentsName($type, $region, $format, $id, $last_tournament_id)
    {
        $regions = [
            'euw' => 'West',
            'eune' => 'East'
        ];
        if($type == 'aram') {
            $name = 'VoidLeague 5on5 ARAM #'.($id+$last_tournament_id).' '.$regions[$region];
        } else if($type == 'classic') {
            $name = 'VoidLeague 5on5 Classic #'.($id+$last_tournament_id).' '.$regions[$region];
        } else {
            $name = 'VoidLeague 3on3 TT #'.($id+$last_tournament_id).' '.$regions[$region];
        }
        return $name;
    }

    private function generateTournamentsForm()
    {
        $regions = ClassLib\Cups::getRegions('LOL');
        $region_options = [];
        foreach($regions AS $region) {
            $region_options[$region['value']] = $region['name'];
        }

        $form = [
            'region' => [
                '#title' => t('Region'),
                '#type' => 'select',
                '#options' => $region_options
            ],
            'type' => [
                '#title' => t('Type'),
                '#type' => 'select',
                '#options' => [
                    'classic' => 'Classic',
                    'aram' => 'ARAM',
                    'tt' => 'TT'
                ]
            ],
            'count' => [
                '#title' => t('How much tournaments'),
                '#default_value' => 30,
                '#type' => 'textfield',
                '#size' => 4
            ],
            'submit' => [
                '#value' => t('Generate'),
                '#type' => 'submit'
            ]
        ];

        return render_form($form);
    }

    /**
     * @return mixed|string
     */
    public function admin_generations()
    {
        $getCups = ORM::for_table('cups')
            ->where('status', 0)
            ->where('riot_event', '')
            ->order_by_asc('start_time')
            ->find_array();

        $cups = [];
        foreach($getCups AS $cup) {
            $cup['riot_submit'] = $this->generateRiotApplicationLink($cup);
            $cups[] = $cup;
        }

        return $this->view('generations', [
            'cups' => $cups
        ]);
    }

    private function generateRiotApplicationLink($cup)
    {
        $url = 'http://events.' . $cup['region'] . '.leagueoflegends.com/events/new#';
        //$url = 'http://events.' . $cup['region'] . '.leagueoflegends.com/events/106087/edit#';

        if(preg_match('/ARAM/', $cup['name'])) {
            $gameMode = 'howling-abyss';
        } else if(preg_match('/TT/', $cup['name'])) {
            $gameMode = 'twisted-treeline';
        } else {
            $gameMode = 'summoners-rift';
        }

        $start_date = strtotime($cup['start_time'] .' midnight');

        $data = [
            'organizer' => [
                'name' => 'VoidLeague',
                'phone' => '37122136489',
                'organization' => 'VoidLeague'
            ],
            'event' => [
                'name' => $cup['name'],
                'description' => "All teams must check-in in 15 minutes before tournament starts in registration link. If team doesnt show up in 15 minutes after tournament start, opposing team gets default win. Admins can always prolong the match start if there is a good reason and if it wont take too long. Finals for 1st and 2nd, 3rd and 4th place are played best of three. Full rule set is available in each tournament link. For any questions feel free to ask us on our webpage chat or message administrators privately. VoidLeague.com",
                'homepage' => 'https://www.voidleague.com',
                'stream' => '',
                'startDate' => $start_date . '000',
                'startTime' => date('Hi', strtotime($cup['start_time'])),
                'endDate' => $start_date . '000',
                'endTime' => '2359',
                'registrationLink' => 'https://www.voidleague.com/cups/id/' . $cup['slug'] . '-' . $cup['id'],
                'entryCost' => 0,
                'regStartDate' => strtotime($cup['start_time']. '-5 DAY midnight').'000',
                'regStartTime' => '0000',
                'regEndDate' => $start_date . '000',
                'regEndTime' => '1530'
            ],
            'details' => [
                'type' => 'single-elim',
                'minTeams' => 32,
                'gameMode' => $gameMode,
                'bestOf' => 'mixed'
            ]
        ];



        $url .= json_encode($data);
        return $url;
    }

}