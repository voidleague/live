<fieldset class="exposed">
    <legend><?php print t('Search filters'); ?></legend>
    <?php print $variables['exposed_form']; ?>
</fieldset>
<table class="streams">
    <tr>
        <th style="width:10px;">#UID</th>
        <th>Username</th>
        <th>E-mail</th>
        <th>Active</th>
        <th><?php print t('Operations'); ?></th>
    </tr>
        <?php foreach($variables['users'] AS $user): ?>
            <tr>
                <td class="bold"><?php print $user->id; ?></td>
                <td><?php print getMember($user->username); ?></td>
                <td><?php print $user->mail; ?></td>
                <td><?php print $user->status; ?></td>
                <td><?php print $user->operations; ?></td>
            </tr>
        <?php endforeach; ?>
</table>

<?php print $variables['pager']; ?>