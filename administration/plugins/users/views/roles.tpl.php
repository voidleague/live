<a href="/admin/users/groups/add"><?php print t('Add group'); ?></a>
<table class="roles">
    <tr>
        <th style="width:10px;">#GID</th>
        <th>Name</th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['roles'] AS $role): ?>
        <tr>
            <td class="bold"><?php print $role->rid; ?></td>
            <td><?php print htmlspecialchars($role->name); ?></td>
            <td><?php print $role->operations; ?></td>
        </tr>
    <?php endforeach; ?>
</table>