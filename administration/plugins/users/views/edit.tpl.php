<?php $user = $variables['user']; ?>
<div class="float_left">
<h1 class="title"><img src="/public/images/flags/<?=strtolower($user->country);?>.png" /> <?php print getMember($user->username); ?></h1>
<?php print $variables['form']; ?>
</div>
<div class="float_right">
    <?php if(!empty($variables['badges_form'])): ?>
        <br />
        <h3>Badges</h3>
        <?=$variables['badges_form'];?>
    <?php endif; ?>
</div>