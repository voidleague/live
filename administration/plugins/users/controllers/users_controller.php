<?php
use ClassLib\Badges;

class UsersController extends AppController{
    private $permissions = array(
        'administer faq',
        'administer streams',
        'access administration theme',
        'administer news',
        'administer languages',
        'administer polls',
        'administer forum',
        'moderate forum',
        'administer custom pages',
        'administer menu',
        'administer bans',
        'administer coins',
        'administer users',
        'administer gameaccounts',
        'administer teams',
        'administer warnings',
        'administer findteam',
        'administer badges',
        'administer rules'
    );


    function __construct() {

        // add game access
        $games = db_query("SELECT `tag` FROM ".PREFIX."speles ORDER BY `title` ASC");
        while($game = db_fetch_object($games)) {
            $this->permissions[] = 'administer '.$game->tag.' cups';
        }
        // administer all games
        $this->permissions[] = 'administer all cups';

        $this->homepath = 'list';
    }
    function admin_index() {
        return $this->users_list();
    }

    public function admin_list() {
        return $this->users_list();
    }

    function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'permissions' => t('Permissions'),
            'groups' => t('Groups'),
        );
        return parent::admin_links();
    }


    function admin_groups() {
        $this->title = t('Users groups');
        $args = arg();
        if(!empty($args[3])) {
            if($args[3] == 'edit' && is_numeric($args[4])) {
                return $this->admin_groups_form($args[4]);
            }
            elseif($args[3] == 'add') {
                return $this->admin_groups_form();
            }
            elseif($args[3] == 'delete' && is_numeric($args[4])) {
                db_query("DELETE FROM `role` WHERE `rid` = '".$args[4]."'");
                $this->setMessage(t('Group successfully deleted'));
                $this->redirect('admin/users/groups');
            }
        }

        $getRoles = db_query("SELECT * FROM `role` ORDER BY `name` ASC");
        $roles = array();
        while($getRole = db_fetch_object($getRoles)) {
            $getRole->operations = ($getRole->rid != 1) ? '<a href="/admin/users/groups/edit/'.$getRole->rid.'/">'.t('edit').'</a> | <a href="/admin/users/groups/delete/'.$getRole->rid.'/" class="confirm">'.t('delete').'</a>' : '';
            $roles[] = $getRole;
        }
        return $this->view('roles',array('roles'=>$roles));
    }

    function admin_groups_form($rid=false) {
        if($rid) {
            $group = db_fetch_object(db_query("SELECT * FROM `role` WHERE `rid` = '".$rid."'"));
            if(empty($group)) {
                $this->setMessage(t('There are no role with this #rid'));
                $this->redirect('admin');
            }
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            if($rid) {
                $data->color = str_replace('#','',$data->color);
                db_query("UPDATE `role` SET `name` = '".$data->name."', `color` = '".$data->color."' WHERE `rid` = ".intval($rid)."");
                $this->setMessage('Group successfully updated');
                $this->redirect('admin/users/groups');
            }
            else {
                db_query("INSERT INTO `role` (`name`) VALUES ('".$data->name."')");
                $this->setMessage('Group successfully added');
                $this->redirect('admin/users/groups');
            }
        }
        $form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('Name'),
                '#default_value' => isset($group->name) ? $group->name : '',
            ),
            'color' => [
                '#type' => 'textfield',
                '#title' => t('HEX Color'),
                '#default_value' => isset($group->color) ? '#'.$group->color : '',
                '#description' => t('<a href="http://www.w3schools.com/html/html_colors.asp" target="_blank">http://www.w3schools.com/html/html_colors.asp</a>')
            ],
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Submit'),
            ),
        );
        return render_form($form);
    }

    function users_list() {
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            $this->redirect('admin/users?username='.$data->username.'&mail='.$data->mail.'&role='.$data->role);
        }
        $query = get_query_parameters();
        $exposed_filters = '';
        if(isset($query->username) && isset($query->mail)) {
            if(!empty($query->username)) {
                $query->username = mysql_real_escape_string($query->username);
                $exposed_filters .= "AND `lietotajvards` LIKE '%".$query->username."%'";
            }
            if(!empty($query->mail)) {
                $query->mail = mysql_real_escape_string($query->mail);
                $exposed_filters .= "AND `epasts` LIKE '%".$query->mail."%'";
            }
            if(!empty($query->role)) {
                $query->role = mysql_real_escape_string($query->role);
                $exposed_filters .= "AND `rid` = '".$query->role."'";
            }
        }
        $this->title = t('Users');
        $getUsersCount = db_result(db_query("SELECT COUNT(*) FROM ".PREFIX."lietotaji LEFT JOIN `users_roles` ON users_roles.uid = ".PREFIX."lietotaji.id WHERE `id` != 1 ".$exposed_filters));
        $pager = $this->pager($getUsersCount);
        $getUsers = db_query("SELECT *, lietotajvards AS username, epasts AS mail FROM ".PREFIX."lietotaji  LEFT JOIN `users_roles` ON users_roles.uid = ".PREFIX."lietotaji.id WHERE `id` != 11 ".$exposed_filters." ORDER BY `id` DESC LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $users = array();
        while($getUser = db_fetch_object($getUsers)) {
            $status = ($getUser->activated == 1)? 'ok' : 'notok';
            $getUser->status = '<img src="/public/images/admin/status_'.$status.'.png" alt="" />';
            $getUser->operations = '<a href="/admin/users/edit/'.$getUser->id.'/">'.t('edit').'</a>';

            $users[] = $getUser;
        }

        $roles = array(''=>'- Any -');
        $getRoles = db_query("SELECT * FROM `role` ORDER BY `name` ASC");
        while($getRole = db_fetch_object($getRoles)) {
            $roles[$getRole->rid] = $getRole->name;
        }
        // Exposed form
        $form = array(
            'username' => array(
                '#type' => 'textfield',
                '#title' => t('Username'),
                '#default_value' => (isset($query->username)) ? $query->username : '',
            ),
            'mail' => array(
                '#type' => 'textfield',
                '#title' => t('E-mail'),
                '#default_value' => (isset($query->mail)) ? $query->mail : '',
            ),
            'role' => array(
                '#type' => 'select',
                '#title' => t('Role'),
                '#options' => $roles,
                '#default_value' => (isset($query->role)) ? $query->role : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Search'),
            ),
        );

        $exposed_form = render_form($form);

        return $this->view('list',array('users'=>$users,'pager'=>$pager,'exposed_form'=>$exposed_form));
    }


    public function admin_edit() {
        $args = arg();
        if(empty($args[3]) || !is_numeric($args[3])) {
            $this->redirect('admin/users');
        }

        $user = db_fetch_object(db_query("SELECT *, id AS uid, lietotajvards AS username FROM ".PREFIX."lietotaji WHERE `id` = '".$args[3]."'"));
        if(empty($user)) {
            $this->redirect('admin/users');
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            $this->admin_edit_submit($data);
        }

        $roles = array();
        $getRoles = db_query("SELECT * FROM `role` ORDER BY `name` ASC");
        while($getRole = db_fetch_object($getRoles)) {
            $roles[$getRole->rid] = $getRole->name;
        }
        $getUserRoles = db_query("SELECT `rid` FROM `users_roles` WHERE `uid` = '".$user->uid."'");
        $userRoles = array();
        while($getUserRole = db_fetch_object($getUserRoles)) {
            $userRoles[] = $getUserRole->rid;
        }


        // Edit form
        $form = array(
            'activated' => array(
                '#type' => 'checkbox',
                '#title' => t('Activated'),
                '#default_value' => (!empty($user->activated)) ? $user->activated : '',
            ),
            'password' => array(
                '#type' => 'textfield',
                '#title' => t('New password'),
            ),
            'sex' => array(
                '#type' => 'radios',
                '#title' => t('Sex'),
                '#default_value' => (!empty($user->dzimums)) ? $user->dzimums : '',
                '#options' => array(0=>t('Male'),1=>t('Female')),
            ),
            'roles' => array(
                '#type' => 'checkboxes',
                '#title' => t('Roles'),
                '#options' => $roles,
                '#default_value' => $userRoles,
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Submit'),
            ),
        );

        $form = render_form($form);
        return $this->view('edit',[
            'user'=>$user,
            'form'=>$form,
            'badges_form' => (user_access('administer badges') ? Badges::entityForm($user->id, 'user') : '')
        ]);
    }

    private function admin_edit_submit($data) {
        $args = arg();
        $data->activated = (isset($data->activated) && $data->activated == 'on') ? 1 : 0;

        // roles
        db_query("DELETE FROM `users_roles` WHERE `uid` = '".$args[3]."'");

        foreach($data->roles AS $rid=>$rid_status) {
            if($rid_status == 'on') {
                db_query("INSERT INTO `users_roles` (`uid`,`rid`) VALUES ('".$args[3]."','".$rid."')");
            }
        }

        db_query("UPDATE ".PREFIX."lietotaji SET `activated` = '".$data->activated."',`dzimums` = '".$data->sex."' WHERE `id` = '".$args[3]."'");

        if(!empty($data->password)) {
            $data->password = pw($data->password);
            db_query("UPDATE ".PREFIX."lietotaji SET `parole` = '".$data->password."' WHERE `id` = '".$args[3]."'");
        }

        $this->setMessage(t('User successfully saved'));
        $this->redirect('admin/users');
    }

    public function admin_permissions() {
            if(isset($_POST) && $_POST) {
                $data = $this->security($_POST);
                $this->_permissions_update($data);
            }

        $roles = array();
        $getRoles = db_query("SELECT * FROM `role` ORDER BY `name` ASC");
        while($getRole = db_fetch_object($getRoles)) {
            $roles[$getRole->rid] = $getRole->name;
        }

        $output = '<form method="POST"><table class="permissions"><th></th>';
        foreach($roles AS $role) {
            $output .= '<th>'.htmlspecialchars($role).'</th>';
        }
        foreach($this->permissions AS $permission) {
            $output .= '<tr><td class="perm_name">'.$permission.'</td>';
            foreach($roles AS $rid=>$role) {
                $perm_name = str_replace(' ','-',$permission);
                $checked = ($this->_permissionValue($rid,$permission)) ? 'checked="checked"' : '';
                $output .= '<td><input type="checkbox" name="'.$perm_name.'['.$rid.']" '.$checked.'/></td>';
            }
            $output .= '<tr/>';
        }
        $output .= '</table><input type="submit" name="submit" value="'.t('Save').'" /></form>';
        return $output;
    }

    private function _permissionValue($rid,$permission) {
        return db_result(db_query("SELECT count(*) FROM `role_permission` WHERE `rid` = '".$rid."' AND `permission` = '".$permission."'"));
    }

    private function _permissions_update($data) {
        $insert_data = array();
        if(!empty($data)) {
            foreach($data AS $permission=>$rids) {
                $permission = str_replace('-',' ',$permission);
                foreach($rids AS $rid=>$on) {
                    $insert_data[] = "('".$rid."','".$permission."')";
                }
            }
            db_query("DELETE FROM `role_permission`");
            db_query("INSERT INTO `role_permission` (`rid`,`permission`) VALUES ".implode(',',$insert_data));
        }

        $this->setMessage(t('Permissions sucessfully updated'));
        $this->redirect('admin/users/permissions');
    }
}