<fieldset class="exposed">
    <legend><?php print t('Search filters'); ?></legend>
    <?php print $variables['exposed_form']; ?>
</fieldset>
<table class="streams">
    <tr>
        <th style="width:10px;">#TID</th>
        <th>Name</th>
        <th>Region</th>
        <th>Format</th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['teams'] AS $team): ?>
        <tr>
            <td class="bold"><?php print $team->id; ?></td>
            <td class="left"><img src="/public/images/flags/<?=strtolower($team->country);?>.png" alt="" /> <a href="/team/id/<?=$team->slug;?>-<?=$team->id;?>"><?=$team->name;?></a></td>
            <td><?=$team->region;?></td>
            <td><?=$team->format;?></td>
            <td><?=$team->operations;?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php print $variables['pager']; ?>