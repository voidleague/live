<? $team = $variables['team']; ?>
<div class="myTeam-edit">
    <h2><img src="/public/images/flags/<?=strtolower($team->country);?>.png" alt="" /> <?=$team->name;?><br />
    <span style="font-size:14px;"><?=$team->format;?> <?=$team->region;?></span></h2>
    <div class="float_left">
        <h3><?=t('Main information');?></h3>
        <img src="/public/images/users_teams/<?=$team->logo;?>.png" alt="" /><br />
        <a href="?delete_logo=1" class="confirm"><?=t('Delete logo');?></a>
        <?php echo render_form($variables['team_form']);?>
    </div>
    <div class="float_right team-members">
        <h3><?=t('Roster');?></h3>
        <?php echo render_form($variables['roster_form']); ?>
        <br /><br />
        <h3><?=t('Add gameaccount');?></h3>
        <?php echo render_form($variables['roster_add_form']);?>
        <?php if(!empty($variables['badges_form'])): ?>
            <br />
            <h3><?=t('Badges');?></h3>
            <?php echo $variables['badges_form'];?>
        <?php endif; ?>


    </div>
    <div class="clear"></div>
</div>