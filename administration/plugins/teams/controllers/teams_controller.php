<?php

use ClassLib\Badges;
class teamsController extends AppController {
    /**
     * @var $team
     */
    private $team;

    function __construct() {
        $this->permission = 'administer teams';
        $this->title = t('Teams');
        $this->homepath = 'list';
    }

    function admin_index() {
        return $this->teams_list();
    }

    public function admin_list() {
        return $this->teams_list();
    }

    function admin_links() {
        $this->links = array(
            'list' => t('List'),
        );
        return parent::admin_links();
    }

    /**
     * Get data for teams list
     */
    public function teams_list() {
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            $this->redirect('admin/teams?name='.$data->name.'&format='.$data->format.'&region='.$data->region);
        }
        $query = get_query_parameters();
        $exposed_filters = '';
        if(isset($query->name)) {
            if(!empty($query->name)) {
                $query->name = mysql_real_escape_string($query->name);
                $exposed_filters .= " AND `name` LIKE '%".$query->name."%'";
            }
        }
        if(!empty($query->region)) {
            $query->region = mysql_real_escape_string($query->region);
            $exposed_filters .= " AND `region` = '".$query->region."'";
        }
        if(!empty($query->format)) {
            $query->format = mysql_real_escape_string($query->format);
            $exposed_filters .= " AND `format` = '".$query->format."'";
        }

        $this->title = t('Teams');
        $getTeamsCount = db_result(db_query("SELECT COUNT(*) FROM `users_teams`  WHERE `status` = 1 ".$exposed_filters));
        $pager = $this->pager($getTeamsCount);
        $getTeams = db_query("SELECT * FROM `users_teams` WHERE `status` = 1 ".$exposed_filters." ORDER BY `name` ASC LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $teams = [];
        while($getTeam = db_fetch_object($getTeams)) {
            $getTeam->operations = '<a href="/admin/teams/edit/'.$getTeam->id.'/">'.t('edit').'</a>';
            $teams[] = $getTeam;
        }

        // Exposed form

        // gameaccounts from database
        $gameaccount_options = [''=>t('All')];
        $getGameaccounts = db_query("SELECT * FROM `gamesaccounts` ORDER BY `name` ASC");
        while($gameaccount = db_fetch_object($getGameaccounts)) {
            $gameaccount_options[$gameaccount->value] = $gameaccount->name;
        }


        $form = array(
            'name' => array(
                '#type' => 'textfield',
                '#title' => t('name'),
                '#default_value' => (isset($query->name)) ? $query->name : '',
            ),
            'region' => array(
                '#type' => 'select',
                '#title' => t('Region'),
                '#default_value' => (isset($query->region)) ? $query->region : '',
                '#options' => $gameaccount_options
            ),
            'format' => array(
                '#type' => 'select',
                '#title' => t('Format'),
                '#default_value' => (isset($query->format)) ? $query->format : '',
                '#options' => [''=>t('All'),'3on3'=>'3on3','5on5'=>'5on5']
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Search'),
            ),
        );

        $exposed_form = render_form($form);

        return $this->view('list',array('teams'=>$teams,'pager'=>$pager,'exposed_form'=>$exposed_form));
    }

    public function admin_edit() {
        $args = arg();
        $access = true;
        if(empty($args[3]) || !empty($args[3]) && !is_numeric($args[3])) {
            $access = false;
        }

        $team = db_fetch_object(db_query("SELECT * FROM `users_teams` WHERE `id` = '".$args[3]."'"));
        if(empty($team)) {
            $access = false;
        }

        if(!$access) {
            return error('Illegal team edit');
        }

        if(empty($team->logo)) {
            $team->logo = 'noimage';
        }

        $this->team = $team;


        // submit data

        // New Member
        if(isset($_POST['invite_member']) && $_POST['invite_member']) {
            $data = $this->security($_POST);
            $data->region_value = $team->region;
            $data->format = $team->format;
            $gameaccount = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `value` = '".$data->username."' AND `region` = '".$team->region."'"));

            if(!empty($gameaccount)) {
                $data->new_member_gid = $gameaccount->id;
                $data->new_member_uid = $gameaccount->uid;
                db_query("INSERT INTO `users_teams_members` (`joined`, `gid`,`uid`,`team`,`role`,`status`) VALUES (NOW(), '".$data->new_member_gid."', '".$data->new_member_uid."','".$team->id."',3, 1)");
                $this->setMessage(t('Gameaccount added'));
                $this->redirect('admin/teams/edit/'.$team->id);
            }
        }

        // get team stuff
        if(isset($_POST['save_stuff']) && $_POST['save_stuff']) {
            $change_captain = false;
            $data = $this->security($_POST);
            unset($data->save_stuff);
            foreach($data AS $gid=>$user_role) {
                if($change_captain && $user_role == 1) {
                    $user_role = 2;
                }
                if($user_role == 1) {
                    $change_captain = true;
                    $new_uid = db_result(db_query("SELECT `uid` FROM `users_gameaccount` WHERE `id` = '".$gid."'"));
                    db_query("UPDATE `users_teams` SET `uid` = '".$new_uid."' WHERE `id` = '".$team->id."'");
                    db_query("UPDATE `users_teams_members` SET `role` = 2 WHERE `team` = '".$team->id."' AND `uid` = '".$team->uid."'");
                }

                db_query("UPDATE `users_teams_members` SET `role` = '".$user_role."' WHERE `gid` = '".$gid."' AND `team` = '".$team->id."'");
            }
            $this->setMessage(t('Members successfully saved'));
            $this->redirect('admin/teams/edit/'.$team->id);
        }


        $query = get_query_parameters();
        if(!empty($query->delete_player) && is_numeric($query->delete_player)) {
            db_query("DELETE FROM `users_teams_members` WHERE `team` = '".$team->id."' AND `uid` = '".$query->delete_player."'");
            $this->setMessage(t('Member removed from team'));
            $this->redirect('admin/teams/edit/'.$this->team->id);
        }

        if(!empty($query->delete_logo)) {
            db_query("UPDATE `users_teams` SET `logo` = '' WHERE `id` = '".$team->id."' LIMIT 1");
            $this->setMessage(t('Logo deleted'));
            $this->redirect('admin/teams/edit/'.$team->id);
        }

         // save team data
        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            db_query("UPDATE `users_teams` SET `name` = '".$data->name."', `tag` = '".$data->tag."', `slug` = '".slug($data->name)."' WHERE `id` = '".$team->id."' LIMIT 1");
            $this->setMessage(t('Team main information saved'));
            $this->redirect('admin/teams/edit/'.$team->id);
        }

        // Prepare team edit form
        $team_form = [
            'name' => [
                '#type' => 'textfield',
                '#title' => t('Name'),
                '#default_value' => $team->name
            ],
            'tag' => [
                '#type' => 'textfield',
                '#title' => t('Tag'),
                '#default_value' => $team->tag,
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Save')
            ]
        ];

        // Prepare roster edit form
        $stuff = array();
        $getStuffs = db_query("SELECT users_gameaccount.*, users_gameaccount.id AS gid, ".PREFIX."lietotaji.lietotajvards AS username, users_teams_members.uid,users_teams_members.role
                             FROM `users_teams_members`
                             LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_teams_members.uid
                             LEFT JOIN `users_gameaccount` ON users_gameaccount.uid = users_teams_members.uid
                             WHERE users_gameaccount.region = '".$team->region."' AND users_teams_members.team = '".$team->id."' AND users_teams_members.status = 1
                             ORDER BY users_teams_members.role ASC, users_teams_members.joined ASC");

        $query = get_query_parameters();
        if(!empty($query->delete_player) && is_numeric($query->delete_player) && $this->team->uid != $query->delete_player) {
            db_query("DELETE FROM `users_teams_members` WHERE `team` = '".$this->team->id."' AND `uid` = '".$query->delete_player."'");
            $this->setMessage(t('Member removed from team'));
            $this->redirect('team/edit/'.$this->team->id);
        }

        $stuff_form = array();
        while($getStuff = db_fetch_object($getStuffs)) {
            $stuff_form[$getStuff->gid] = array(
                '#type' => 'select',
                '#prefix' => $getStuff->value.' ('.getMember($getStuff->username).')',
                '#default_value' => ($getStuff->role),
                '#options' => array(1=>t('Captain'),2=>t('Co-captain'),3=>t('Player')),
                '#disabled' => ($getStuff->role == 1) ? TRUE : FALSE,
                '#sufix' => ($getStuff->role != 1) ? '<a href="/admin/teams/edit/'.$this->team->id.'?delete_player='.$getStuff->uid.'" class="confirm"><img src="/public/images/admin/delete.png"  alt="" /></a>' : '',
            );
        }

        $stuff_form['save_stuff'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
        );


        $stuff_new = array(
            'username' => array(
                '#type' => 'textfield',
                '#title' => t('Username'),
                '#ajax' => 'getGameaccount',
                '#ajax_arguments'=> array($team->region),
            ),
            'invite_member' => array(
                '#type' => 'submit',
                '#value' => t('Invite'),
            ),
        );


        return $this->view('edit',[
            'team'=>$team,
            'team_form' => $team_form,
            'roster_form' => $stuff_form,
            'roster_add_form' => $stuff_new,
            'badges_form' => (user_access('administer badges') ? Badges::entityForm($team->id) : '')
        ]);

    }


}