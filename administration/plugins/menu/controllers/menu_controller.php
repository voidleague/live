<?php
class MenuController extends AppController {
    public function __construct() {
        $this->title = t('Menu');
        $this->homepath = 'list';
        $this->permission = 'administer menu';
    }

    public function admin_links() {
        $this->links = array(
            'list' => t('List'),
            'add' => t('Add item'),
        );
        return parent::admin_links();
    }

    public function admin_index() {
        return $this->admin_list();
    }

    public function admin_list() {
        $menus = array();
        $menu_links = db_query("SELECT * FROM menu ORDER BY `order` ASC");
        while($menu_link = db_fetch_object($menu_links)) {
            $operations = '<a href="/admin/menu/edit/'.$menu_link->id.'/">'.t('edit').'</a> | <a href="/admin/menu/delete/'.$menu_link->id.'/" class="confirm">'.t('delete').'</a>';
            $menu_link->operations = $operations;
            $menus[$menu_link->id] = $menu_link;
        }
        return $this->view('list',array('menu'=>$menus));
    }

    public function admin_add($id=null) {
        $parents = array();
        $parents_q = db_query("SELECT id, title FROM menu WHERE level = 1 ORDER BY title ASC");
        while($parent = db_fetch_object($parents_q)) {
            $parents[$parent->id] = $parent->title;
        }

        // save
        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            if($id) {
                $data->id = $id;
            }
            $this->save_menu_item($data);
        }

        if($id) {
            $menu = db_fetch_object(db_query("SELECT * FROM `menu` WHERE `id` = '".$id."'"));
        }
        $form = array(
            'title' => array(
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#default_value' => ($id) ? $menu->title : '',
            ),
            'link' => array(
                '#type' => 'textfield',
                '#title' => t('Link'),
                '#default_value' => ($id) ? $menu->link : '',
            ),
            'parent' => array(
                '#type' => 'select',
                '#title' => t('Parent'),
                '#default_value' => ($id) ? $menu->parent : '',
                '#options' => $parents,
            ),
            'order' => array(
                '#type' => 'textfield',
                '#title' => t('Order'),
                '#default_value' => ($id) ? $menu->order : '',
            ),
            'blank' => array(
                '#type' => 'checkbox',
                '#title' => t('Open in new window'),
                '#default_value' => ($id) ? $menu->blank : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            ),
        );
        return render_form($form);
    }

    public function admin_edit() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            return $this->admin_add($args[3]);
        }
    }

    public function admin_delete() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            db_query("DELETE FROM `menu` WHERE `id` = '".$args[3]."'");
            $this->setMessage(t('Menu item successfully deleted'));
            $this->redirect('admin/menu');
        }
    }

    private function save_menu_item($item) {
        $item->blank = (!empty($item->blank)) ? 1 : 0;
        if(!empty($item->id)) {
            db_query("UPDATE menu SET `blank` =  '".$item->blank."', title = '".$item->title."',link = '".$item->link."', parent = '".$item->parent."',`order` = '".$item->order."' WHERE id = '".$item->id."'");
        } else {
            db_query("INSERT INTO `menu` (`title`,`link`,`parent`,`level`,`order`,`blank`) VALUES ('".$item->title."','".$item->link."','".$item->parent."',1,'".$item->order."','".$item->blank."')");
        }

        $this->setMessage(t('Menu item successfully saved'));
        $this->redirect('admin/menu');
    }
}