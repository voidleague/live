<table class="menu">
    <tr>
        <th><?php print t('Title'); ?></th>
        <th><?php print t('Operations'); ?></th>
    </tr>
    <?php if(count($variables['menu']) > 0): ?>
        <?php foreach($variables['menu'] AS $menu): ?>
            <tr>
                <td class="left"><a href="<?php print $menu->link;?>"><?php print $menu->title; ?></a></td>
                <td style="width:50px;"><?php print $menu->operations; ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr><td colspan="6">There are no menu items. <a href="/admin/menu/add"><?php print t('Add new menu item');?></a></td></tr>
    <?php endif; ?>
</table>