<fieldset class="exposed">
    <legend><?php print t('Search filters'); ?></legend>
    <?php print $variables['exposed_form']; ?>
</fieldset>
<table class="streams">
    <tr>
        <th style="width:10px;">#GID</th>
        <th>Value</th>
        <th>Region</th>
        <th>User</th>
        <th style="width:150px" class="center"><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['gameaccounts'] AS $account): ?>
        <tr>
            <td class="bold"><?php print $account->id; ?></td>
            <td class="left"><?php print $account->value; ?></td>
            <td class="left"><?php print $account->region; ?></td>
            <td class="left"><?php print getMember($account->username); ?></td>
            <td style="width:150px"><?php print $account->operations; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php print $variables['pager']; ?>