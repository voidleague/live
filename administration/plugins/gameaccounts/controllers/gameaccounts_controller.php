<?php
class GameaccountsController extends AppController {
    private $api_key = 'bcaa7471-e051-49da-874d-1693b750f5a9';
    function __construct() {
        $this->permission = 'administer gameaccounts';
        $this->title = t('Gameaccounts');
        $this->homepath = 'list';
    }

    function admin_index() {
        return $this->gameaccounts_list();
    }

    public function admin_list() {
        return $this->gameaccounts_list();
    }

    function admin_links() {
        $this->links = array(
            'list' => t('List'),
        );
        return parent::admin_links();
    }


    function gameaccounts_list() {
        if(isset($_POST) && $_POST) {
            $data = $this->security($_POST);
            $this->redirect('admin/gameaccounts?value='.$data->value.'&region='.$data->region);
        }
        $query = get_query_parameters();
        $exposed_filters = '';
        if(isset($query->value) && isset($query->region)) {
            if(!empty($query->value)) {
                $query->value = mysql_real_escape_string($query->value);
                $exposed_filters .= "AND `value` LIKE '%".$query->value."%'";
            }
            if(!empty($query->region)) {
                $query->region = mysql_real_escape_string($query->region);
                $exposed_filters .= "AND `region` = '".$query->region."'";
            }
        }

        $getGameaccountsCount = db_result(db_query("SELECT COUNT(*) FROM `users_gameaccount` WHERE 1=1 ".$exposed_filters));
        $pager = $this->pager($getGameaccountsCount);
        $getAccounts = db_query("SELECT users_gameaccount.*, ".PREFIX."lietotaji.lietotajvards AS username
                                    FROM `users_gameaccount`
                                    LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = users_gameaccount.uid
                                    WHERE 1=1 ".$exposed_filters."
                                    ORDER BY `id` DESC LIMIT ".$this->pager->from.",".$this->pager->perPage."");
        $gameaccounts = array();
        while($getaccount = db_fetch_object($getAccounts)) {
            $getaccount->operations = '<a href="/admin/gameaccounts/edit/'.$getaccount->id.'/">'.t('edit').'</a> | <a href="/admin/gameaccounts/delete/'.$getaccount->id.'/" class="confirm">'.t('delete').'</a>';

            $gameaccounts[] = $getaccount;
        }

        // gameaccounts from database
        $gameaccount_options = [];
        $getGameaccounts = db_query("SELECT * FROM `gamesaccounts` ORDER BY `name` ASC");
        while($gameaccount = db_fetch_object($getGameaccounts)) {
            $gameaccount_options[$gameaccount->value] = $gameaccount->name;
        }

        // Exposed form
        $form = array(
            'value' => array(
                '#type' => 'textfield',
                '#title' => t('Value'),
                '#default_value' => (isset($query->value)) ? $query->value : '',
            ),
            'region' => array(
                '#type' => 'select',
                '#title' => t('Region'),
                '#default_value' => (isset($query->region)) ? $query->region : '',
                '#options' => $gameaccount_options
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Search'),
            ),
        );

        $exposed_form = render_form($form);
        return $this->view('list',array('gameaccounts'=>$gameaccounts,'pager'=>$pager,'exposed_form'=>$exposed_form));
    }


    public function admin_delete() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            db_query("DELETE FROM `users_gameaccount` WHERE `id` = '".$args[3]."'");
            $this->setMessage(t('Gameaccount successfully deleted'));
            $this->redirect('admin/gameaccounts');
        }
    }

    public function admin_add($gid=null) {
        if($gid) {
            $account = db_fetch_object(db_query("SELECT * FROM `users_gameaccount` WHERE `id` = '".$gid."'"));
        }


        $getTypes = db_query("SELECT `value`,`name` FROM `gamesaccounts`");
        $types = array();
        while($getType = db_fetch_object($getTypes)) {
            $types[$getType->value] = htmlspecialchars(($getType->name));
        }

        if(isset($_POST['submit']) && $_POST['submit']) {
            $data = $this->security($_POST);
            if($gid) {
                $data->gid = $gid;
            }
            $this->save_gameaccount($data);
        }

        $form = array(
            'value' => array(
                '#type' => 'textfield',
                '#title' => t('Value'),
                '#default_value' => ($gid) ? $account->value : '',
            ),
            'region' => array(
                '#type' => 'select',
                '#options' => $types,
                '#default_value' => ($gid) ? $account->region : '',
            ),
            'submit' => array(
                '#type' => 'submit',
                '#value' => t('Save'),
            )
        );
        return render_form($form);

    }

    public function admin_edit() {
        $args = arg();
        if(!empty($args[3]) && is_numeric($args[3])) {
            return $this->admin_add($args[3]);
        }
    }

    private function save_gameaccount($data) {
        $data = $this->LOL_validate($data);
        db_query("UPDATE `users_gameaccount` SET `unique_game_id` = '".$data->unique_game_id."', `value` = '".$data->value."', `region` = '".$data->region."' WHERE `id` = '".$data->gid."'");
        $this->setMessage(t('Gameaccount successfully saved'));
        $this->redirect('admin/gameaccounts');
    }


    private function LOL_validate($data) {
        $summonerId_url = 'http://prod.api.pvp.net/api/lol/'.$data->region.'/v1.2/summoner/by-name/'.rawurlencode($data->value).'?api_key='.$this->api_key;
        $summoner_response = @file_get_contents($summonerId_url);
        $summoner_response = json_decode($summoner_response);

        if($summoner_response == false){
            //throw new Error(t('Sorry, but we could not find summoner with this summoner name'));
            $data->unique_game_id = '1337';

        } else {
            $data->unique_game_id =  $summoner_response->id;
        }
        return $data;
    }


}