<?php

use \ORM;
use ClassLib\Badges;
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'imagick'));

class badgesController extends AppController {
    /**
     * Object of badge ORM
     *
     * @var
     */
    private $badge;

    function __construct() {
        $this->title = t('Badges');
        $this->permission = 'administer badges';
        $this->homepath = 'badges';

        $args = arg();
        if(!empty($args[3])) {
            $this->badge = $this->loadBadge($args[3]);
        }

        // save form data
        if(isset($_POST) && $_POST) {
            $saved = $this->saveData($_POST);
            if($saved) {
                $this->setMessage(t('Badge saved'));
                $this->redirect('admin/badges');
            }
        }

    }

    protected function loadBadge($id) {
        return ORM::for_table('badges')->where('deleted','N')->find_one($id);
    }

    public function admin_links() {
        $this->links = array(
            '' => t('List'),
            'add' => t('Add'),
        );
        return parent::admin_links();
    }

    public function admin_index() {
        $getBadges = ORM::for_table('badges')->where('deleted','N')->find_array();
        return $this->view('list',[
            'badges' => $getBadges
        ]);
    }

    public function admin_add() {
        return $this->form();
    }

    public function admin_edit() {
        return $this->form();
    }

    public function admin_delete() {
        if(!empty($this->badge)) {
            $this->badge->deleted = 'Y';
            $this->badge->save();

            $this->setMessage(t('Badge deleted'));
            $this->redirect('admin/badges');
        }
    }

    protected function form() {
        $form = [
            'title' => [
                '#type' => 'textfield',
                '#title' => t('Title'),
                '#default_value' => ($this->badge) ? $this->badge->title : ''
            ],
            'icon' => [
                '#type' => 'image',
                '#title' => t('Icon (64x64)'),
                '#default_value' => ($this->badge) ? $this->badge->icon : '',
                '#path' => ($this->badge) ? 'public/img/badges/' : ''
            ],
            'submit' => [
                '#type' => 'submit',
                '#value' => t('Save')
            ]
        ];
        return render_form($form);
    }

    protected function saveData($data) {
        try {
            $this->validateData($data);
            if (!empty($this->badge)) {
                $badge = $this->badge;
            } else {
                $badge = ORM::for_table('badges')->create();
            }
            $badge->title = $data->title;

            if(!empty($data->icon)) {
                $badge->icon = $data->icon;
            }
            if(empty($this->badge)) {
                $badge->set_expr('created','NOW()');
            }
            $badge->save();

        } catch (Error $e) {
            return $this->setMessage($e->getMessage,'error');
        }

        return $badge;
    }

    protected function validateData(&$data) {
        $data = $this->security($data);
        // Validate title
        if(empty($data->title)) {
            throw new Error(t('Please fill title field'));
        }

        // Validate image
        if(!empty($_FILES['icon'])) {
            $image = $_FILES['icon'];
            $extension = strtolower(getExtension(stripslashes($image['name'])));
            $size = filesize($image['tmp_name']);

            if ($extension != ('jpg' || 'jpeg' || 'png')) {
                throw new Error(t('Photo can be only in format: !formats', array('!formats' => '<strong>JPG, PNG</strong>')));
            }
            if ($size > 1024 * 10000) {
                throw new Error(t('Too big photo. Please resize it'));
            }
            $image_name = time() . '.' . $extension;

            Image::make($image['tmp_name'])->fit(64, 64)->save('public/img/badges/' . $image_name);
            $data->icon = $image_name;
        } else {
            $data->icon = null;
        }
    }

    public function admin_removeBadge() {
        Badges::removeBadge($_POST['entity_id'],$_POST['badge_id'],$_POST['type']);
        echo 'ok';
        die;
    }

    public function admin_addBadge() {
        echo Badges::addBadge($_POST['entity_id'],$_POST['badge_id'],$_POST['type']);
        die;
    }
}