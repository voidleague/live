<table class="badges">
    <tr>
        <th width="64"></th>
        <th><?=t('Title');?></th>
        <th width="150" class="center"><?php print t('Operations'); ?></th>
    </tr>
    <?php foreach($variables['badges'] AS $badge): ?>
        <tr>
            <td><img src="/public/img/badges/<?=$badge['icon'];?>" alt="" /></td>
            <td><?=$badge['title'];?></td>
            <td><a href="/admin/badges/edit/<?=$badge['id'];?>"><?=t('edit');?></a> | <a href="/admin/badges/delete/<?=$badge['id'];?>" class="confirm"><?=t('delete');?></a></td>
        </tr>
    <?php endforeach; ?>
</table>