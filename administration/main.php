<?php

// Admin menu
$admin_menu = array(
    'index' => array(
        'path' => 'admin',
        'title' => t('Main'),
    ),
    'administration' => array(
        'tree' => TRUE,
        /*
        'languages' => array(
            'title' => t('Languages'),
            'access' => 'administer languages',
        ),
        */
        'badges' => [
            'title' => t('Badges'),
            'access' => 'administer badges'
        ],
        'users' => array(
            'title' => t('Users'),
            'access' => 'administer users',
        ),
        'polls' => array(
            'title' => t('Polls'),
            'access' => 'administer polls',
        ),
        'rules' => [
            'title' => t('Rules'),
            'access' => 'administer rules',
        ],
    ),
    'content' => array(
        'tree' => TRUE,
        'news' => array(
            'title' => t('News'),
            'access' => 'administer news',
        ),
        'faq' => array(
            'title' => t('FAQ'),
            'access' => 'administer faq',
        ),
        /*
        'pages' => array(
            'title' => t('Content pages'),
            'access' => 'administer pages',
        ),
        */
        'menu' => array(
            'title' => t('Menu'),
            'access' => 'administer menu',
        ),
    ),
    'gameaccounts' => array(
        'title' => t('Gameaccounts'),
        'access' => 'administer gameaccounts',
    ),
    'cups' => array(
        'title' => t('Cups'),
        'access' => 'administer cups',
    ),
    'bans' => array(
        'title' => t('Bans'),
        'access' => 'administer bans',
    ),
    'stream' => array(
        'title' => t('Streams'),
        'access' => 'administer streams',
    ),
    'teams' => [
        'title' => t('Teams'),
        'access' => 'administer teams',
    ]
);

$content = '<div id="admin_navigation">'.admin_menu($admin_menu).'</div>';
$styles[] = add_css('admin.css');

if(isset($routes[1]) && file_exists(ROOT.'/administration/plugins/'.$routes[1].'/controllers/'.$routes[1].'_controller.php')) {
    include_once ROOT.'/administration/plugins/'.$routes[1].'/controllers/'.$routes[1].'_controller.php';
    $plugin_name = ucfirst($routes[1]).'Controller';
    if(empty($routes[2])) {
        $routes[2] = 'index';
    }
    $plugin = new $plugin_name;
    if(!empty($plugin->permission) && user_access($plugin->permission) || empty($plugin->permission)) {
        if(method_exists($plugin,'admin_'.$routes[2])) {
            if(isset($plugin->attached)) {
                foreach($plugin->attached AS $type=>$files) {
                    foreach($files AS $file) {
                        if($type == 'js') {
                            $scripts[] = add_js($file);
                        }
                        else {
                            $styles[] = add_css($file);
                        }
                    }
                }
            }

            $method_name = 'admin_'.$routes[2];
            $links_method = 'admin_links';

            $links = $plugin->admin_links();
            $content .= $links;
            $content .= $plugin->{$method_name}();


            $title = t('Admin');
        }
        else {
            $content = error('function admin_'.$routes[2].' doesn\'t exist in class '.$plugin_name);
        }
    }
    else {
        $content = error('permission denied');
    }
}
else {
    include_once ROOT.'/administration/plugins/front/controllers/front_controller.php';
    $plugin = new FrontController;
    $content .= $plugin->admin_index();
}

if(empty($title)) {
    $title = t('Administration');
}
$content = '<div id="admin">'. $content.'</div>';



