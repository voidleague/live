(function($) {
    $(document).ready(function() {

        $('.pair_bo3').tooltip();
        $('.cup-add-result').click(function(e) {
            e.preventDefault();
            $.parent = $(this).parents('.tree-pair');
            $.data = $(this).attr('class').split(' ');

            // Close others add result boxes
            $('#cup-add-result-box').remove();
            // Get add result box

            $.ajax({
                url: '/cups/add_result/?c='+$.data[1]+'&p='+$.data[2]+'&'+'r='+$.data[3]+'&'+'double='+$.data[4]
            }).done(function(result) {
                $(result).appendTo($.parent);
            });
        });


        // Close add results box
        $('.tree-pair').delegate('a.close-add-result-box','click',function(e) {
            e.preventDefault();
            $(this).parents('#cup-add-result-box').remove();
        });

        // Accept result
        $('.tree-pair').delegate('input.cup-accept-result','click',function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: '/cups/add_result_submit',
                data: $(this).parents('form').serialize(),
                success: function(data) {
                    if(data == 'ok') {
                        location.reload();
                    }
                    else {
                        alert(data);
                    }
                }
            });
        });

        // Remove result
        $('.tree-pair').on('click','a.cup-remove-result',function(e) {
            e.preventDefault();
            $.getdata = $(this).attr('class').split(' ');
            $.ajax({
                type: "POST",
                url: '/cups/remove_result_submit',
                data: {
                    cup_id: $.getdata[1].split('-')[1],
                    row: $.getdata[2].split('-')[1],
                    pos: $.getdata[3].split('-')[1],
                    pair: $.getdata[9],
                    double: $.getdata[8]
                },
                success: function(data) {
                    if(data == 'ok') {
                        location.reload();
                    }
                    else {
                        alert(data);
                    }
                }
            });
        });

        /**
         * Room
         */
        /*
         if ($('#cups-room')) {
         setInterval(function(){
         cup_room_online_refresh();
         },5000);

         setInterval(function(){
         cup_room_chat_refresh();
         },5000);

         // $(".cup-room-chat-content").scrollTop($(".cup-room-chat-content")[0].scrollHeight);
         }

         function cup_room_online_refresh() {
         var cup_id = $('input[name="cup_id"]').val();
         var room_entity = $('input[name="room_entity"]').val();
         $.ajax({
         type: "GET",
         url: '/cups/cups_room_online?cup_id='+cup_id+'&room_entity='+room_entity,
         success: function(data) {
         $('.cup-room-online-content').html(data);
         }
         });
         }

         function cup_room_chat_refresh() {
         var cup_id = $('input[name="cup_id"]').val();
         $.ajax({
         type: "GET",
         url: '/cups/cups_room_chat?cup_id='+cup_id,
         data: $(this).parents('form').serialize(),
         success: function(data) {
         $('.cup-room-chat-content').html(data);
         }
         });
         }

         $('#cup-room-chat .form-submit').click(function(e) {
         e.preventDefault();
         $.ajax({
         type: "POST",
         url: '/cups/cups_room_chat_post',
         data: $(this).parents('form').serialize(),
         success: function(data) {
         $('#cup-room-chat .item-text').val('');
         cup_room_chat_refresh();
         }
         });
         });
         */
    });
})(jQuery);


var Cups = new function() {
    this.cup_id = false;
    this.team_id = false;
    this.hash = false;
    this.ft = 1;
    this.updateTeamInfo = function (cup_id, team_id, hash) {
        this.cup_id = cup_id;
        this.team_id = team_id;
        this.hash = hash;
        Cups.getTeamInfo();
        setInterval(function() {
            Cups.getTeamInfo();
        }, 10000); //10 seconds
    };
    this.getTeamInfo = function() {
        $.get('/cups/getTeamInfo?hash='+this.hash+'&team_id='+this.team_id+'&cup_id='+this.cup_id+'&ft='+this.ft, function(data) {
            Cups.ft = 0;
            if(data != '') {
                Cups.updateData(data);
            }
        }, 'json');
    };
    this.updateData = function (data) {
        var box = $('#cup-team-info');
        box.find('.enemy-team').text(data.enemy_team);
        box.find('.time span').text(data.start_time);
        box.find('.pair span').text(data.pair);
        box.find('.enemy-captain span').text(data.enemy_captain);

        // Play notification
        console.log(data);
        if(data.sound_alert) {
            var audio = new Audio('/public/sounds/cup_notification.mp3');
            audio.play();
        }
    }
    this.gameFinished = function(cup_id, team_id, hash) {

    }
};