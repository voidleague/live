var customCommentsButtons = {

    addIcon: function(e, data) {
        $(data.popup).children("img")
            .unbind("click")
            .bind("click", function(e) {
                var editor = data.editor;
                var type = $(this).data('type');
                var id = $(this).data('id');
                var return_html = '{{'+type+':'+id+'}}';
                editor.execCommand(data.command, return_html, null, null);
                editor.hidePopups();
                editor.focus();
            });
    },

    getChampions: function(id) {
        var champions_icons = [
            '/public/img/news/full/1422027980.jpg'
        ];
        output = '';
        $(champions_icons).each(function(id, img) {
            output += '<img title="'+img+'" width="40" height="40" src="'+img+'" data-type="champion" data-id="'+id+'" />';
        });
        return output;
    },

    getSpells: function(id) {
        var spells_icons = [
            '/public/img/news/full/1422027980.jpg'
        ];
        output = '';
        $(spells_icons).each(function(id, img) {
            output += '<img title="'+img+'" width="40" height="40" src="'+img+'" data-type="spell" data-id="'+id+'" />';
        });
        return output;
    },

    getItems: function(id) {
        var items_icons = [
            '/public/img/news/full/1422027980.jpg'
        ];
        output = '';
        $(items_icons).each(function(id, img) {
            output += '<img title="'+img+'" width="40" height="40" src="'+img+'" data-type="item" data-id="'+id+'" />';
        });
        return output;
    }

};

$.cleditor.buttons.champions = {
    name: "champions",
    title: "Champions",
    command: "inserthtml",
    popupName: "Champions",
    popupClass: "cleditorPrompt",
    popupContent: customCommentsButtons.getChampions,
    buttonClick: customCommentsButtons.addIcon,
    css: {
        backgroundImage: 'url('+$.cleditor.imagesPath()+'cleditor_custom_buttons.png)',
        height: 23
    }
};

$.cleditor.buttons.spells = {
    name: "spells",
    title: "Spells",
    command: "inserthtml",
    popupName: "Spells",
    popupClass: "cleditorPrompt",
    popupContent: customCommentsButtons.getSpells,
    buttonClick: customCommentsButtons.addIcon,
    css: {
        backgroundImage: 'url('+$.cleditor.imagesPath()+'cleditor_custom_buttons.png)',
        backgroundPosition: '113px 0px',
        height: 23
    }
};

$.cleditor.buttons.items = {
    name: "items",
    title: "Items",
    command: "inserthtml",
    popupName: "Items",
    popupClass: "cleditorPrompt",
    popupContent: customCommentsButtons.getItems,
    buttonClick: customCommentsButtons.addIcon,
    css: {
        backgroundImage: 'url('+$.cleditor.imagesPath()+'cleditor_custom_buttons.png)',
        backgroundPosition: '148px 0px',
        height: 23
    }
};