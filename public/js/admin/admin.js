(function($) {
    $(document).ready(function() {
       $('.add-poll-answer').click(function(e) {
            e.preventDefault();
            $.input = '<div class="form-item-div textfield"><div class="form-item"><div class="form-item-field"><input type="text" name="new_answers[]" value="" class="form-field item-answers[]" id="form-field-item-answers[]"></div></div></div>';
           $($.input).insertAfter($('form fieldset .form-item-div.textfield:last'));
        });

       $('.existingBadges').on('click','a.removeBadge', function(e) {
           e.preventDefault();
           var cont = confirm('Are you sure?');
           var container = $(this).parent('span');
           if(cont) {
               $.post('/admin/badges/removeBadge', {
                   entity_id: $(this).data('entity_id'),
                   badge_id: $(this).data('badge_id'),
                   type: $(this).data('type')
               }).done(function () {
                   container.remove();
               });
           }
       });

        $('.addBadge').click(function(e) {
            e.preventDefault();
            var select = $('select[name="badge"]');
            var val = select.val();

            if(val != '') {
                $.post('/admin/badges/addBadge', {
                    entity_id: select.data('entity_id'),
                    badge_id: val,
                    type: select.data('type')
                }).done(function (badge) {
                    if (badge != '') {
                        $(badge).appendTo('.existingBadges');
                    }
                });
            }
        });
    });
})(jQuery);