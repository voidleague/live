/**
 * Name: Cooloader
 * Author: Mr. J
 * Contact: info@mr-j.it
 */
(function($){
	
	var methods = {
		
		/**
		 * Initialize Cooloader
		 */
		init : function(Opts){
			
			var Obj		= this;
			var Opts	= $.extend({

				direction	: 'up',					// Direction load: up, down, right, left
				duration	: 1,					// Effect Duration in seconds
				effect		: '',					// Effect Type: easeOutBounce, swing, easeInOutCubic etc..
				tStart		: 0,					// time Start YYYYMMDDHHIISS
				tEnd		: 0,					// time End	YYYYMMDDHHIISS
				pStart		: 0,					// Percentual Start
				pEnd		: 100,					// Percentual End
				imgB		: 'img/w-b.png',		// Background Image
				imgH		: 'img/w-h.png',		// Loading Image
				cW			: 50,					// Counter Width
				cH			: 1,					// Counter Height
				cMl			: 0,					// Counter MarginLeft
				cMt			: 0,					// Counter MarginTop
				cC			: '#000',				// Counter Color
				cLive		: false,				// Counter Live refresh every 30s
				tTextP		: '%P%',				// Percentual Text
				tTextD		: '-%D ',				// Time Text Day
				tTextH		: '%H:',				// Time Text Hours
				tTextI		: '%I:',				// Time Text Minutes
				tTextS		: '%S',					// Time Text Seconds
				tTextDesc	: '%S!!!',					// Time Text Seconds
				tGMT		: 0,					// GMT TimeZone
				callback	: function(){},			// CallBack Function when percentual stop
				loadLink	: ''					// Load Link when load is 100%

			}, Opts);
			
			// Create Internal Global Var
			if(!$.fn.cooloader.global)
				$.fn.cooloader.global = new Array();
			
			// Set Global Var
			$.fn.cooloader.global[Obj.attr('id')] = $.extend({
				
				tRtime		: '',
				tRanimate	: '',
				callback	: 0
				
			});
			
			// Create HTML and Load
			Obj.cooloader('create', Opts).cooloader('load', Opts);
			
			return Obj;
		},
		
		/**
		 * Create and inset HTML into Cooloader
		 */
		create : function(Opts)
		{
			var Obj = this;
			
			// Load Images
			Obj.append('<img src="' + Opts.imgB + '" style="display: none;" class="cl-img" />');
			Obj.append('<img src="' + Opts.imgH + '" style="display: none;" class="cl-img" />');
			
			// Load Counter DIV
			Obj.append('<div class="cl-c"></div>');
			
			// Load Counter DIV - Insert Line and Text Content
			Obj.children('.cl-c').append('<div class="cl-c-line"></div><div class="cl-c-per"></div><div class="cl-c-text"></div>');
			
			// Load Background DIV - Img
			Obj.append('<div class="cl-b"></div>');
			
			// Load Hover DIV - Img
			Obj.append('<div class="cl-h"></div>');
			
			return Obj;
		},
		
		/**
		 * OnLoad Image Cooloader Load CSS
		 */
		load : function(Opts)
		{
			var Obj		= this;
			var ObjImg	= Obj.children('.cl-img');
			var count	= 0;
			
			ObjImg.load(function(){
				
				count++;
				
				if(count >= ObjImg.length)
				{
					// Get Options to give animation
					var set = Obj.cooloader('getOptsFx', Opts);

					// Set Background CSS
					Obj.children('.cl-b').css({

						background	: 'url(' + Opts.imgB + ') no-repeat center center',
						position	: 'absolute',
						height		: ObjImg.height(),
						width		: ObjImg.width()

					});

					// Set Hover CSS
					Obj.children('.cl-h').css({

						background	: 'url(' + Opts.imgH + ') no-repeat ' + set.s.bg,
						position	: 'absolute',
						marginLeft	: set.s.ml,
						marginTop	: set.s.mt,
						height		: set.s.h,
						width		: set.s.w

					});

					// Set Counter CSS
					var ObjClC = Obj.children('.cl-c');

					ObjClC.children('.cl-c-line').css({

						background	: Opts.cC,
						height		: Opts.cH,
						width		: Opts.cW

					});
					
					if(Opts.direction == 'right' || Opts.direction == 'left')
					{
						ObjClC.children('.cl-c-line').css({ marginRight	: 5 });
						ObjClC.children('.cl-c-line').css({ 'float' : 'left' });
					}
					
					ObjClC.css({

						position	: 'absolute',
						marginLeft	: set.c.sL,
						marginTop	: set.c.sT

					});

					// Start Animation
					Obj.cooloader('animate', Opts, set);
				}
				
			});
			
			return Obj;
		},
		
		/**
		 * Load Animation
		 */
		animate : function(Opts, set)
		{
			var Obj		= this;
			var ObjImg	= Obj.children('.cl-img');
			
			// Set Hover Animation
			Obj.children('.cl-h').animate({

				marginLeft	: set.e.ml,
				marginTop	: set.e.mt,
				height		: set.e.h,
				width		: set.e.w

			}, (Opts.duration * 1000), Opts.effect);
			
			// Set Counter Animation
			var ObjClC = Obj.children('.cl-c');
			
			ObjClC.animate({

				marginLeft	: set.c.eL,
				marginTop	: set.c.eT

			}, {
				
				duration		: (Opts.duration * 1000),
				
				specialEasing	: {
					
					marginLeft		: Opts.effect,
					marginTop		: Opts.effect
					
				},
				
				step			: function(now, fx)
				{
					// Fix Now Value
					switch(Opts.direction)
					{
						case 'up':
						var perVar	= ObjImg.height();
						var prop	= 'marginTop';
						break;
						
						case 'down':
						now += Opts.cH;
						var perVar	= ObjImg.height();
						var prop	= 'marginTop';
						break;
						
						case 'right':
						now += Opts.cW;
						var perVar	= ObjImg.width();
						var prop	= 'marginLeft';
						break;
						
						case 'left':
						var perVar	= ObjImg.width();
						var prop	= 'marginLeft';
						break;
					}
					
					// Control Prop Value
					if(fx.prop == prop)
					{
						// Set Percentual
						var per = Math.round(now / perVar * 100);

						// Fix Percentual Value
						if(Opts.direction == 'up' || Opts.direction == 'left')
						{
							per = 100 - per;
						}

						// Write Percentual Value
						ObjClC.children('.cl-c-per').html(Opts.tTextP.replace(/%P/, per));
					}
				},
				
				complete		: function()
				{
					if($.fn.cooloader.global[Obj.attr('id')].callback == 0)
					{
						Opts.callback();
						
						$.fn.cooloader.global[Obj.attr('id')].callback = 1;
					}
					
					// Set HTML Time Left
					if(set.t && ObjClC.children('.cl-c-text').text().length < 1)
					{
						var timeHTML = '';

						timeHTML  = '<div class="cl-tTextD"></div>';
						timeHTML += '<div class="cl-tTextH"></div>';
						timeHTML += '<div class="cl-tTextI"></div>';
						timeHTML += '<div class="cl-tTextS"></div>';
						timeHTML += '<div class="cl-tTextDesc"></div>';

						ObjClC.children('.cl-c-text').css({ opacity : 0 });
						ObjClC.children('.cl-c-text').html(timeHTML);
						ObjClC.children('.cl-c-text').children().css({ opacity : 0 });
						ObjClC.children('.cl-c-text').css({ opacity : 1 });
						
						// Refresh HTML Time
						Obj.cooloader('tRefresh', Opts);
						
						ObjClC.children('.cl-c-text').children().each(function(i){

							$(this).delay(100 * i).animate({ opacity : 1 }, 1200);

						});
						
						if(Opts.cLive == true) Obj.cooloader('live', Opts);
					}
				}
				
			})
			
			return Obj;
		},
		
		/**
		 * Time Refresh
		 */
		tRefresh : function(Opts)
		{
			var Obj = this;
			var set = Obj.cooloader('getOptsFx', Opts);
			
			Obj.find('.cl-tTextD').html(Opts.tTextD.replace(/%D/, set.t.d));
			Obj.find('.cl-tTextH').html(Opts.tTextH.replace(/%H/, set.t.h));
			Obj.find('.cl-tTextI').html(Opts.tTextI.replace(/%I/, set.t.i));
			Obj.find('.cl-tTextS').html(Opts.tTextS.replace(/%S/, set.t.s));
			Obj.find('.cl-tTextDesc').html(Opts.tTextDesc);
			return Obj;
		},
		
		/**
		 * Load Live Time
		 */
		live : function(Opts)
		{
			var Obj = this;
			
			$.fn.cooloader.global[Obj.attr('id')].tRtime = setInterval(function(){
				
				Obj.cooloader('tRefresh', Opts);
				
			}, 1000);
			
			$.fn.cooloader.global[Obj.attr('id')].tRanimate = setInterval(function(){
				
				var set = Obj.cooloader('getOptsFx', Opts);
				
				Obj.cooloader('animate', Opts, set);
				
				if(set.pEnd >= 100)
				{
					clearInterval($.fn.cooloader.global[Obj.attr('id')].tRanimate);
					
					Obj.children('.cl-c').children().reverse().each(function(i){
						
						$(this).delay(200 * i).animate({ opacity : 0 });
						
						if(i >= Obj.children('.cl-c').children().length - 1)
						{
							setTimeout(function(){
								
								if(Opts.loadLink != '')
								{
									$('body').animate({ opacity : 0 }, function(){

										var Obj = $(this);

										Obj.load(Opts.loadLink, function(){

											Obj.animate({ opacity : 1 }, 1000);

										});

									});
								}
								
							}, 500 * (i + 1));
						}
						
					});
				}
				
			}, 5000);
			
			return Obj;
		},
		
		/**
		 * Get Time Data
		 */
		getTimeData : function(ts, Opts)
		{
			var Obj = this;
			
			if(ts == 'now')
			{
				var t = new Date();
				
				var time = {
					
					y : t.getFullYear(),
					m : t.getMonth() + 1,
					d : t.getDate(),
					h : t.getHours(),
					i : t.getMinutes(),
					s : t.getSeconds()
					
				};
				
			} else {
				
				var now = new Date();
				var GMT = ((now.getTimezoneOffset() / 60) + Opts.tGMT);
				
				var y = ts.substr(0, 4);
				var m = ts.substr(4, 2) - 1;
				var d = ts.substr(6, 2);
				var h = parseInt(ts.substr(8, 2)) + GMT;
				var i = ts.substr(10, 2);
				var s = ts.substr(12, 2);
				
				var t = new Date(y, m, d, h, i, s);
				
				var time = {
					
					y : t.getFullYear(),
					m : t.getMonth() + 1,
					d : t.getDate(),
					h : t.getHours(),
					i : t.getMinutes(),
					s : t.getSeconds()
					
				};
			}
			
			return time;
		},
		
		/**
		 * Set Opts about direction load
		 */
		getOptsFx : function(Opts)
		{
			var Obj		= this;
			var ObjImg	= Obj.children('.cl-img');
			var set;
			
			// Time Control Percentual
			if(Opts.tStart > 0 && Opts.tEnd > 0)
			{
				// Start TimeStamp
				var tS = Obj.cooloader('getTimeData', Opts.tStart, Opts);
				
				// End TimeStamp
				var tE = Obj.cooloader('getTimeData', Opts.tEnd, Opts);
				
				// Now TimeStamp
				var tN = Obj.cooloader('getTimeData', 'now', Opts);
				
				// Date Start
				var dS = new Date(tS.y, tS.m, tS.d, tS.h, tS.i, tS.s);
				
				// Date End
				var dE = new Date(tE.y, tE.m, tE.d, tE.h, tE.i, tE.s);
				
				// Date Now
				var dN = new Date(tN.y, tN.m, tN.d, tN.h, tN.i, tN.s);
				
				// Total Seconds
				var sT = (dE - dS) / 1000;
				
				// Actual Seconds
				var sN = (dN - dS) / 1000;
				
				// Seconds - Days
				var d = (sT - sN) / 86400;
				
				// Seconds - Hours
				var h = (sT - sN) / 3600;
				
				// Seconds - Minutes
				var i = (sT - sN) / 60;
				
				// Seconds
				var s = (sT - sN);
				
				// Round Time
				d < 1 ? d = 0 : d = Math.floor(d);
				d > 0 ? h = 24 - tN.h : h = Math.floor(h);
				h > 0 ? i = 60 - tN.i : i = Math.floor(i);
				i > 0 ? s = 60 - tN.s : s = Math.floor(s);
				
				// Fix Time Value
				if(h == 24 || h < 0) h = 0;
				if(i == 60 || i < 0) i = 0;
				if(i > 0 && d > 0) h -= 1;
				if(s == 60 || s < 0) s = 0;
				
				// Double Decimal
				if(h < 10) h = '0' + h;
				if(i < 10) i = '0' + i;
				if(s < 10) s = '0' + s;
				
				// Set Time Options
				var t = {
					
					d : d,
					h : h,
					i : i,
					s : s
					
				};
				
				// Percentual Start
				Opts.pStart	= 0;
				
				// Percentual End
				Opts.pEnd	= sN / sT * 100;
				
				// Percentual End Control Overflow
				if(Math.floor(Opts.pEnd) >= 100)
				{
					clearInterval($.fn.cooloader.global[Obj.attr('id')].tRtime);
					
					Opts.pEnd = 100;
				}
			}
			
			switch(Opts.direction)
			{
				case 'up':
				
				var mStart	= Opts.pStart * ObjImg.height() / 100;
				var mEnd	= Opts.pEnd * ObjImg.height() / 100;
				
				set = {
					s	: {

						'bg'	: 'center bottom',
						'h'		: mStart,
						'w'		: ObjImg.width(),
						'mt'	: ObjImg.height() - mStart,
						'ml'	: 0

					},
					e	: {

						'h'		: mEnd,
						'w'		: ObjImg.width(),
						'mt'	: ObjImg.height() - mEnd,
						'ml'	: 0

					},
					c	: {
						'sL'	: Opts.cMl,
						'eL'	: Opts.cMl,
						'sT'	: ObjImg.height() - mStart,
						'eT'	: ObjImg.height() - mEnd
					}
				};
				
				break;
				
				case 'down':
				
				var mStart	= Opts.pStart * ObjImg.height() / 100;
				var mEnd	= Opts.pEnd * ObjImg.height() / 100;
				
				set = {
					s	: {

						'bg'	: 'center top',
						'h'		: mStart,
						'w'		: ObjImg.width(),
						'mt'	: 0,
						'ml'	: 0

					},
					e	: {

						'h'		: mEnd,
						'w'		: ObjImg.width(),
						'mt'	: 0,
						'ml'	: 0

					},
					c	: {
						'sL'	: Opts.cMl,
						'eL'	: Opts.cMl,
						'sT'	: mStart - Opts.cH,
						'eT'	: mEnd - Opts.cH
					}
				};
				
				break;
				
				case 'right':
				
				var mStart	= Opts.pStart * ObjImg.width() / 100;
				var mEnd	= Opts.pEnd * ObjImg.width() / 100;
				
				set = {
					s	: {

						'bg'	: 'left center',
						'h'		: ObjImg.height(),
						'w'		: mStart,
						'mt'	: 0,
						'ml'	: 0

					},
					e	: {

						'h'		: ObjImg.height(),
						'w'		: mEnd,
						'mt'	: 0,
						'ml'	: 0

					},
					c	: {
						'sL'	: mStart - Opts.cW,
						'eL'	: mEnd - Opts.cW,
						'sT'	: Opts.cMt,
						'eT'	: Opts.cMt
					}
				};
				
				break;
				
				case 'left':
				
				var mStart	= Opts.pStart * ObjImg.width() / 100;
				var mEnd	= Opts.pEnd * ObjImg.width() / 100;
				
				set = {
					s	: {

						'bg'	: 'right center',
						'h'		: ObjImg.height(),
						'w'		: mStart,
						'mt'	: 0,
						'ml'	: ObjImg.width() - mStart

					},
					e	: {

						'h'		: ObjImg.height(),
						'w'		: mEnd,
						'mt'	: 0,
						'ml'	: ObjImg.width() - mEnd

					},
					c	: {
						'sL'	: ObjImg.width() - mStart,
						'eL'	: ObjImg.width() - mEnd,
						'sT'	: Opts.cMt,
						'eT'	: Opts.cMt
					}
				};
				
				break;
			}
			
			// Insert Time Value
			if(t) set.t = t;
			
			// Insert Percentual Value
			set.pStart	= Opts.pStart;
			set.pEnd	= Opts.pEnd;
			
			return set;
		}
		
	};
	
	/**
	 * Plugin
	 */
	$.fn.cooloader = function(method)
	{
		if(methods[method])
		{
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
			
		} else if (typeof method === 'object' || !method) {
			
			return methods.init.apply(this, arguments);
			
		} else {
			
			$.error('Method ' + method + ' don\'t exist on jQuery.cooloader');
		}
	}
	
	$.fn.reverse = [].reverse;
	
})(jQuery);