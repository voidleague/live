/**
 * open_popup
 */
var dialog;
function open_popup(url, title, w, h, popup_class){
    if (dialog && dialog.dialog) {
        dialog.empty().remove();
    }

    dialog=$('<div style="overflow: hidden;"><div id="popup_loading"><img src="/public/images/loading.gif" /></div><div id="popup_frame_wrap"></div></div>');
    $(dialog).dialog({
        dialogClass: 'dialogWithDropShadow'+(popup_class != '' ? ' '+popup_class : ''),
        bgiframe: true,
        width: w,
        title: title,
        overlay: {
            backgroundColor: '#000',
            opacity: 0.8
        },
        height: 120,
        modal: true,
        position:top,
        resizable: true,
        autoResize: true,
        open: function() {
            $('#popup_frame_wrap').css('height','100%').html('<iframe allowtransparencty="true" id="popup_iframe" height="100%" class="popup_iframe" src="'+url+'" width="100%" border="0" frameborder="no" style="border:none;"></iframe>');
            $('#popup_loading').remove();
            setTimeout(function() {
                var content_height = $('iframe').contents().height()+20;
                $('.ui-dialog, #popup_frame_wrap, .ui-dialog-content').height(content_height);
                $('iframe').height(content_height);
                $('.ui-dialog').css('top',parseInt($('.ui-dialog').css('top'))-(content_height/2));
            },300);
        }
    });
}

(function ($) {




    /*
     * w2.0
     */

    $(document).load(function() {
        $('.register-form-side #register_form .item-captcha').hide();
    });

    $(document).ready(function() {
        //updateClock();
        //setInterval('updateClock()', 1000 );
        $('.tooltip').tooltip();




        $('a[href$="?tab=room"]').click(function(e) {
        var stile = "top=10, left=10, width=800, height=600 status=no, menubar=no, toolbar=no scrollbar=no";
        window.open($(this).attr('href')+'&nolayout', "", stile);
        e.preventDefault();
    });


	//Cups
    /*
	if ($('.page-cups').length > 0) {
	    $('.choose-game a').click(function(e) {
		e.preventDefault();
		$('.choose-game li').removeClass('active');
		$(this).parent('li').addClass('active');
		
		// ajaxRequest
		$.get('/cups/cups_list/'+$(this).attr('href')+'&ajax=1',function(data) {
		    $('.cups-list ul').html(data);
		});
	    });
	}
	*/
	//getUsername
        $('.ajax-getUsername input').keyup(function() {
            if($(this).val() != '') {
                $.get('/ajax_request/getUsername/'+$(this).val()+'/', function(data) {
                    if(data.length > 0) {
                        var output = '';
                        data = $.parseJSON(data);
                        $(data).each(function(key,value) {
                            output += '<a href="#">'+value+'</a><br />';
                        });
                        $('.ajax-content-getUsername').html(output).show();
                    }
                    else {
                        $('.ajax-content-getUsername').hide();
                    }
                });
            }
        });


        //getGameaccount
        $('.ajax-getGameaccount input').keyup(function() {
            if($(this).val() != '') {
                $.get('/ajax_request/getGameaccount/'+$(this).val()+'/?region='+$(this).parents('.ajax-getGameaccount').find('.ajax_arguments').text(), function(data) {
                    if(data.length > 0) {
                        var output = '';
                        data = $.parseJSON(data);
                        $(data).each(function(key,value) {
                            output += '<a href="#">'+value+'</a><br />';
                        });
                        $('.ajax-content-getGameaccount').html(output).show();
                    }
                    else {
                        $('.ajax-content-getGameaccount').hide();
                    }
                });
            }
        });
	
	$('.ajax-content-getUsername').delegate('a','click',function(e) {
	    $('.ajax-getUsername input').val($(this).text());
	    $('.ajax-content-getUsername').hide()
	    e.preventDefault();
	});

    $('.ajax-content-getGameaccount').delegate('a','click',function(e) {
        $('.ajax-getGameaccount input').val($(this).text());
        $('.ajax-content-getGameaccount').hide()
        e.preventDefault();
    });
	
	//Registration form
    $('.register-form-side #register_form .item-captcha').hide();
	$('.register-form-side #register_form .form-submit-button').click(function(e) {
	    e.preventDefault();
	    $(this).val('Please wait...').attr('disabled','disabled');
	    $.post('/register/ajax_validate', $('.register-form-side #register_form form').serialize(), function(result) {
		if(result == 'status_ok') {
		    // just reload page for message
		    window.location = '/';
		}
		else {
		    $('.register-form-side #register_form .messages').remove();
		    $('.register-form-side #register_form').prepend('<div class="messages error"><ul><li>'+result+'</li></ul></div>');
		    $('.register-form-side #register_form .form-submit-button').removeAttr('disabled','disabled').val('Register');
		}
	    });
	});
	
	$('.register-form-side .label').click(function() {
	    if($('.register-form-side').hasClass('opened'))
	    {
		$('.register-form-side').animate({
		    top: '-=283px'
		}, 200, function() {
            $('.register-form-side').removeClass('opened');
            $('.register-form-side .messages').hide();
		});
	    }
	    else {
		$('.register-form-side').animate({
		    top: '+=283px'
		}, 200, function() {
            $('.register-form-side').addClass('opened');
		});
	    }
	    return false;
	});


        // Guider
        $('#guider .guider_button').click(function() {
            $.guider = $('#guider');
            if($.guider.hasClass('opened')) {
                $.guider.animate({
                    top: '-=320px'
                }, 200, function() {
                    $.guider.removeClass('opened');
                });
            }
            else {
                $.guider.animate({
                    top: '+=320px'
                }, 200, function() {
                    $.guider.addClass('opened');
                });
            }
            return false;
        });
    });
    
    
    $.fn.extend({
        form_defaults: function(value) {
            var input = $(this);
            if(input.attr('value') == ''){ input.attr('value', value); }
            input.focus(function(){ if( input.attr('value') == value ){ input.attr('value', ''); } });
            input.blur(function(){ if(input.attr('value') == ''){ input.attr('value', value); } });        
            input.closest('form').submit(function(e){ if( input.attr('value') == value ){ input.attr('value', ''); }  });  
        }
    });
    
    $(document).ready(function() {
      $('#top_line input.default_value').form_defaults('Meklēt...');
    });
	
	
	$(document).ready(function() {
		$('.form-filefield').on("change", function(){
			$(this).val();
			$(this).parents('.custom_file_upload').find('.file').val($(this).val());
		});
	});
	/**
	  *	Slideshow
	*/
	var slider_data=1;
	var slider_id = 1;
	  
	// onclick
	$(document).ready(function() {
		$('#slider #navigation .item').click(function() {
			$('#slider #navigation .item').removeClass('active');
			$(this).addClass('active');
			var slider_data = $(this).find('.front-new-id').text();
			var slider_image = $(this).find('.front-new-image').text();
			var slider_desc = $(this).find('.front-new-desc').text();
			var slider_title = $(this).find('h2').text();
			$('#slider #image img').attr('src','/public/img/news/full/'+slider_image);
			$('#slider #about p').html(slider_desc);
			$('#slider h1').html('<a href="/news/read/'+slider_data+'/">'+slider_title+'</a>');
		});
		
	    // Confirm
	    $('a.confirm').click(function() {
		if (typeof $(this).attr('title') == "undefined") {
		    var text = "Do you really want to delete?";
		}
		else {
		    var text = $(this).attr('title');
		}
		if (!confirm(text)) {
		    return false;
		}
	    });
	});
	
	
	
	/**
	  *	SuggestUser
	 */
	$(document).ready(function() {
		$('.suggestUser').keyup(function() {
			if($(this).val().length == 0) {
				$('#suggestions').hide();
			}
			else {
				$.post("/public/scripts/suggestion_user.php", {queryString: ""+$(this).val()+""}, function(data){
					if(data.length >0) {
						$('#suggestions').show();
						$('#autoSuggestionsList').html(data);
					}
				});
			}
		});
		
	});
	
	/*
	 * Messages v2.0
	 */
	$(document).ready(function() {
	    $("#checkAllMessages").click(function() {
		$(':checkbox').prop('checked',this.checked);
	    });
	});
	
	
})(jQuery)

function fillSuggestionUser(value) {
	jQuery('.suggestUser').val(value);
	jQuery('#suggestions').hide();
}

function MailSelect(a) {
    var theForm = document.Mail;
    for (i=0; i<theForm.elements.length; i++) {
        if (theForm.elements[i].name=='id[]')
            theForm.elements[i].checked = a;
    }
}


function MailDelete(selectedtype) {
  document.Mail.supporttype.value = selectedtype;
  document.Mail.submit();
}


function launchWindow(sUrl, iWidth, iHeight, sWindowName)
{
		//alert(sWindowName);
		window.open(sUrl, sWindowName, 'height=' + iHeight + ', width=' + iWidth,'location=no', 'menubar=no', 'status=no');
	//window.open(sUrl, sWindowName, 'height=' + iHeight + ', width=' + iWidth + ', left=' + iLeft + ', top=' + iTop + ', location=no, menubar=no, status=no, toolbar=no, scrollbars=yes, resizable=no');
	if (!oWin) {
		alert('Your browser blocked the popup you tried to open!');
		return;
	}

	if (oWin.opener == null)
		oWin.opener = window;

	oWin.moveTo(iLeft, iTop);
	oWin.resizeTo(iWidth + 10, iHeight + 59);
	oWin.focus();
}



function DraugiemSay( title, url, titlePrefix ){
 window.open(
  'http://www.draugiem.lv/say/ext/add.php?title=' + encodeURIComponent( title ) +
  '&link=' + encodeURIComponent( url ) +
  ( titlePrefix ? '&titlePrefix=' + encodeURIComponent( titlePrefix ) : '' ),
  '',
  'location=1,status=1,scrollbars=0,resizable=0,width=530,height=400'
 );
 return false;
}
  
function setCheckboxDisplay(el){
	var objWrap;
	objWrap = document.getElementById(el.id+"Wrap");
	if(el) {
	    if(el.checked) objWrap.className = "styledCheckboxWrap wrapChecked";
	    else  objWrap.className = "styledCheckboxWrap";
	}
}



$(function() {
  // --<-- forum
  
  $('.side_forum li a').click(function() {
  
     $('.side_forum li').removeClass('active');
     $(this).parents('li').addClass('active');
     var forum_class = $(this).attr('class');
     $('.side_forum .forum-item').hide();
     $('.side_forum #'+forum_class).show();
     
     return false;
  })
  
  // -->-- forum
  
  // --<-- statistics
  $('.side_statistic li a').click(function() {
     $('.side_statistic li').removeClass('active');
     $(this).parents('li').addClass('active');
     var statistics_class = $(this).attr('class');
     $('.side_statistic .stats').hide();
     $('.side_statistic #'+statistics_class).show();
     return false;
  })
  // -->-- statistics

  // --<-- submit poll
  $('.side_poll input:radio, #poll input:radio').click(function() {
    $("#poll").submit();
  });
  // -->-- submit poll
  

  // -->-- More news
  
  // --<-- More comments
  $('.load_more.comments').click(function() {
      $('#left #latest_comments2').slideDown(); 
      $('.load_more.comments').fadeOut();
   return false;
  });
  // -->-- More comments
  
  
  // --<-- league
  $('.league_open .register_league').click(function(){
  var answer = confirm('Are you sure?');
  return answer // answer is a boolean
});  
  // -->-- league


    /**
     * Forum stuff
     */
    $('#moderate-forum select').change(function () {
        var success = confirm('Are you sure?');
        if (success == true) {
            window.location = '?method='+$(this).val();
        }
    });

    if($('.forums-categories img').length > 0) {
        $(".forums-categories img").load(function(){
            if($(this).width() > 700){
                //$(this).append('<div style="background: #000; color: #fff; width: 100%; height: 14px; padding: 3px; font-size: 10px;">Bilde samazināta</div>');
                $(this).height(Math.round((700*$(this).height())/$(this).width())).width(690).wrap($('<a />').attr("href", $(this).attr("src")));
            }
        });
    }


    /**
     * Load more team matches
     */
    $('.loadMoreMatches').click(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/ajax_request/?module=team&sub=load_matches&team="+$(this).data('team_id')+"&offset="+$(this).attr('data-offset')
        }).done(function(data) {
            $('.table.matches a.row:last').after(data);
            var new_offset = parseInt($('.loadMoreMatches').attr('data-offset'))+10;
            $('.loadMoreMatches').attr('data-offset',new_offset);
            if(data == '') {
                $('.load_more').remove();
            }
        });
    });

    if($('.checkin-countdown').length > 0) {
        var prepared_countdown_time = $('.checkin-countdown').text();
        prepared_countdown_time = new Date($('.checkin-countdown').text());
        $('.checkin-countdown').countdown({
             date: $('.checkin-countdown').text(),
             render: function(data) {
                 $(this.el).html("Check-in starts in: " + data.days + " <span>days</span> " + data.hours + " <span>hrs</span> " + this.leadingZeros(data.min, 2) + " <span>min</span> " + this.leadingZeros(data.sec, 2) + " <span>sec</span>");
             },
             onEnd: function(data) {
                 $('.checkin-countdown').data('countdown').stop();
                 setTimeout(function() {
                     $('.checkin-button').removeClass('checkin-countdown').text('Check-in');
                 },1000);
             }
         });
    }

    $('.checkin-button').click(function(e) {
        if($(this).hasClass('checkin-countdown')) {
            e.preventDefault();
        }
    });
})