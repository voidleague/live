<?php
/* 
  All code coded by MarisL
  Email: marislazda@gmail.com
  (c) 2011. All rights reserved. Copy is illegal
*/

session_start();
include $_SERVER['DOCUMENT_ROOT'].'/application/controllers/bootstrap.php';

$query = (object)$_GET;
if(isset($_GET['key']) && $_GET['key'] == '12qwaszx') {


	include $_SERVER['DOCUMENT_ROOT'].'/application/controllers/classes/cron.class.php';   

    $cron = new cron();
    if(empty($query->tab)) {
        $cron->run();
    }
    else {
        // op moves 2014
        $file = ROOT.'/application/plugins/'.$query->tab.'/controllers/'.$query->tab.'_controller.php';
        if(file_exists($file)) {
            include_once $file;
            $plugin_name = ucfirst($query->tab).'Controller';
            $plugin = new $plugin_name;
            $plugin->cron();

        }
    }
}


