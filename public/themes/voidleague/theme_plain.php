<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><? print $title; ?> | VoidLeague</title>
    <meta name="description" content="<?php print $title;?>, League Of Legends tournaments, ladders, leagues, cups">
    <meta name="keywords" content="<?php print $title;?> League Of Legends tournaments, ladders, leagues, cups">
    <meta property="og:title" content="<?php print $title;?>" />
    <meta property="dr:say:title" content="<?php print $title;?>" />
    <link rel="shortcut icon" href="/public/themes/<?=$theme;?>/img/favicon.ico" type="image/ico" />
    <link rel="icon" href="/public/themes/<?=$theme;?>/img/favicon.ico" type="image/ico" />
    <link href="/public/themes/<?=$theme;?>/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/jquery.cleditor.css" rel="stylesheet" type="text/css" />
    <link href="/public/themes/<?=$theme;?>/css/main.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/print.css" type="text/css" media="print" />
    <?php
    //if(!function_exists('page')) {
    print '<link href="/public/themes/'.$theme.'/css/v2.0.css" rel="stylesheet" type="text/css" />';
    //}
    ?>
    <link href="/public/css/system.css" rel="stylesheet" type="text/css" />
    <!--[if IE]>
    <link href="/public/themes/<?=$theme;?>/css/IE.css" rel="stylesheet" type="text/css" />
    <![endif]-->

    <link rel="stylesheet" href="/public/misc/jquery-ui.css" />
    <script src="/public/misc/jquery-1.10.2.min.js"></script>
    <script src="/public/misc/jquery-ui.js"></script>
    <script type="text/javascript" src="/public/js/jquery-ui-timepicker-addon.js"></script>


    <script type="text/javascript" src="/public/js/main.js"></script>
</head>
<body class="nohtml">
    <?php
                print $messages;
                print $content;
    ?>
</body>
</html>
