<?php
if(isset($_GET['allow_void'])) {
    $currentCookieParams = session_get_cookie_params();
    $sidvalue = session_id();
    setcookie(
        'allow_void',//name
        $sidvalue,//value
        0,//expires at end of session
        $currentCookieParams['path'],//path
        $currentCookieParams['domain'],//domain
        true //secure
    );    print '<pre>'.print_r($_COOKIE, true).'</pre>';
    header('location: /');
} ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><? print $title; ?> | VoidLeague</title>
	<meta name="description" content="<?php print $title;?>, League Of Legends tournaments, ladders, leagues, cups">
	<meta name="keywords" content="<?php print $title;?> League Of Legends tournaments, ladders, leagues, cups">
	<meta property="og:title" content="<?php print $title;?>" />
	<meta property="dr:say:title" content="<?php print $title;?>" />
	<link rel="shortcut icon" href="/public/themes/<?=$theme;?>/img/favicon.ico" type="image/ico" />
	<link rel="icon" href="/public/themes/<?=$theme;?>/img/favicon.ico" type="image/ico" />
	<link href="/public/themes/<?=$theme;?>/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/jquery.cleditor.css" rel="stylesheet" type="text/css" />
    <link href="/public/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />

    <link href="/public/themes/<?=$theme;?>/css/main.css?date=16052015" rel="stylesheet" type="text/css" />
	<link href="/public/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/print.css" type="text/css" media="print" />
	<?php
	//if(!function_exists('page')) {
		print '<link href="/public/themes/'.$theme.'/css/v2.0.css?date=16052015" rel="stylesheet" type="text/css" />';
	//}
	?>
	<link href="/public/css/system.css" rel="stylesheet" type="text/css" />
	<!--[if IE]>
	<link href="/public/themes/<?=$theme;?>/css/IE.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	 <link rel="stylesheet" href="/public/misc/jquery-ui.css" />
	<script src="/public/misc/jquery-1.10.2.min.js"></script>
	<script src="/public/misc/jquery-ui.js"></script>
	<script type="text/javascript" src="/public/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/public/js/jquery.countdown.js"></script>
    <script type="text/javascript" src="/public/js/jquery.bxslider.min.js"></script>

	
	<script type="text/javascript" src="/public/js/main.js?date=2909"></script>
    <?php if(isset($routes[0]) && $routes[0] == 'admin'): ?>
        <script type="text/javascript" src="/public/js/admin/admin.js"></script>
    <?php endif; ?>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37706855-2']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>

    <script type="text/javascript">
        var currenttime = '<? echo date("F d, Y H:i:s", time())?>' //PHP method of getting server date
        ///////////Stop editting here/////////////////////////////////
        var montharray= new Array("January","February","March","April","May","June", "July","August","September","October","November","December");
        var serverdate=new Date(currenttime);
        function padlength(what){
            var output=(what.toString().length==1)? "0"+what : what;
            return output;
        }
        function displaytime(){
            serverdate.setSeconds(serverdate.getSeconds()+1);
            var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear();
            var timestring = padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+ ":" + padlength(serverdate.getSeconds());
            document.getElementById("clock").innerHTML = '<span>'+timestring+'</span>';
        }
        setInterval("displaytime()", 1000);
    </script>
    <?php print $styles; ?>
	<?php print $scripts; ?>

</head>


<body class="<?php print $body_classes; ?>">
<div id="bg_lol"></div>

<?php if (empty($_COOKIE['allow_void'])): ?>
<div id="waiting-for-riotgames-bg"></div>
<div id="waiting-for-riotgames">
    <div class="logos">
        <img class="void" src="/public/themes/voidleague/img/logo.png" alt="" height="70px" />
        <img class="lol" src="https://signup.eune.leagueoflegends.com/theme/signup_theme/img/signup_logo2.png" alt="" height="90px" />
        <div class="clear"></div>
    </div>
    <div class="info">
        <h1>Tournaments temporary stopped!</h1>
        <div class="description">
            <strong>Welcome, Summoners!</strong><br />
            Probably some of you don't know it yet, but recently Riot had changed their system of their event applications and other things related to that.
            Right now, our tournaments are accepted till august of 2016, but the problem isn't there. Problem is that events.leagueoflegends.com accept winners only from their own list that had registered for our event only via their website and that was a big surprise for us, because right now we are not able to submit results because of that.
            <br /><br />
            <strong>What we're doing to fix it? </strong><br />
            First of all, we can't do anything much there, but we have already contacted with RiotGames staff and right now we're waiting for a response from them.
            If you have used their support, you know that a response from them can take a while, so sit tight and let's hope for the best!
            <br /><br /><br />
            Respectfully, your VoidLeague.<br /><br /><br />
            <a class="twitter-timeline" href="https://twitter.com/VoidLeague" data-widget-id="612229159541149696">Tweets by @VoidLeague</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
    </div>
</div>

<style>
    h1 {
        font-size:25px;
        text-shadow: 2px 2px 3px #000;
        color:white;
    }

    .description {
        margin-top:10px;
        text-shadow: 2px 2px 3px #000;
        color:white;
    }

    .info {
        padding:20px;
    }

    .logos {
        text-align:center;
    }

    .logos .void {
        margin-top:19px;
    }

    .logos img {
        float:left;
        margin-left:10px;
        margin-right:10px;
    }
    .logos img.lol {
        margin-top:10px;
    }


    #waiting-for-riotgames {
        width:500px;
        margin: 0px auto;
        background:#000;
        opacity:0.8;
    }
</style>
<?php die; endif; ?>
	<?php if(isset($register_form)) {
        print $register_form; }
    else {
        echo $guider;
    } ?>
<div class="wrapper" id="wrapper">
	<div id="header">
		<div id="logo"><a href="/"></a></div>
		<div class="login profile<?php print $logged_in; ?>">
			<?php loginBox(); ?>
		</div>
	</div>

		<div id="navigation">
		<?php echo navigation(); ?>
		</div>
		
	<div id="wrapper_wrapper">
	<div id="chat-box">
        <span class="chat-button"><a href="/chat" class="submit22 left"><?php print t('Chat');?></a></span>
        <span class="chat-inline">

	<?php
	
			$shoutbox = db_fetch_object(db_query("SELECT chat.*, ".PREFIX."lietotaji.lietotajvards AS username FROM `chat`
							     LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = chat.uid
							     ORDER BY `date` DESC LIMIT 1"));
	echo getMember($shoutbox->username).' <span class="time">'.laikaParveide($shoutbox->date).'</span><span class="chat-text">
	"'.bb2html(h(cut($shoutbox->text,100))).'"</span>';
	
	?>
    </span>
        <div id="clock"></div>
    </div>
	<?php if(!$is_front): ?>
	<div id="content_page">
	<?php endif;?>
	<?php
	if(function_exists('page')) {
		print $messages;
		page();
	}
	else {
		print '<h1 class="title">'.$title.'</h1>';
        if(isset($breadcrumb)) {
            print '<div id="breadcrumb">'.implode(' → ',$breadcrumb).'</div>';
        }
		print $messages;
		print $content;
	}
	?>
	<?php if(!$is_front): ?>
	<div class="clear"></div>
	</div>
	<?php endif; ?>
	





<!--
<div style="clear:both; background:red; padding:20px; color:white; text-align:center;"><?php print($queries_count); ?> queries</div>
-->

<div class="clear"></div>
<div class="footer">
	<div class="wrapper">
		<p><strong></strong><a href="#"><span></span></a></p>
		<ul></ul>
	</div>
</div>
<div class="to_top"><a href="#"></a></div>
<!-- new footer -->
<div id="footer">
	<div id="footer-left"><a href="/"></a></div>
	<div id="footer-center"></div>
	<div id="footer-right">
		<div id="footer-menu1">
            <a href="/stream"><?php print t('Streams'); ?></a>
            <a href="/teamspeak"><?php print t('TeamSpeak'); ?></a>
            <a href="/recruitment"><?php print t('Recruitment'); ?></a>
			<a href="/partners"><?php print t('Partners'); ?></a>
			<a href="/stream"><?php print t('Streams'); ?></a>
			<a href="/contacts/"><?php print t('Contacts'); ?></a>
		</div>
		
		<div id="footer-menu2">
			<a href="/contacts/bug"><?php print t('Report about bug'); ?></a>
		</div>
		<div id="copyright">
			2013-<?php print date('Y');?> VoidLeague.com. <?php print t('All rights reserved. Any form of copying without reference strictly prohibited.'); ?>
		</div>
		
	</div>
</div>
</div>
</div>
<!-- //new footer -->
</div>
</body></html>
<?php
if(User::$user->uid == 1 && debug == true) {
  dsm($_SESSION['debug']);

}
?>


