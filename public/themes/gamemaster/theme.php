<?php
/*
*   ### Pieejamie uzstadijumi ###
*   $main_title - Browsera virsraksts
*   $title - Lapas virsraksts
*   page() - Lapas saturs
*   $languages - Valodas karogi ar iespeju mainit valodu
*   loginBox() - Lietotaja panelis
*/
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><? print $title; ?> | Bliezam.lv</title>
	<meta name="description" content="<?php print $title;?>">
	<meta name="keywords" content="EASPORTS,GAMES,PC,NHL,FIFA,LOL,tournaments,leagues,online,NBA,multiplayer">
	<meta property="og:title" content="<?php print $title;?>" />
	<meta property="dr:say:title" content="<?php print $title;?>" />
	<link rel="shortcut icon" href="/public/themes/gamemaster/images/favicon.gif" type="image/gif" />
	<link rel="icon" href="/public/themes/gamemaster/images/favicon.gif" type="image/gif" />
	<link href="/public/themes/gamemaster/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/public/themes/gamemaster/css/main.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/colorbox.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="/public/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
	<?php
	if(!function_exists('page')) {
		print '<link href="/public/themes/gamemaster/css/v2.0.css" rel="stylesheet" type="text/css" />';	
	}
	?>
	<link href="/public/css/system.css" rel="stylesheet" type="text/css" />
	<!--[if IE]>
	<link href="/public/themes/gamemaster/css/IE.css" rel="stylesheet" type="text/css" />
	<![endif]-->

	 <link rel="stylesheet" href="/public/misc/jquery-ui.css" />
	<script src="/public/misc/jquery-1.10.2.min.js"></script>
	<script src="/public/misc/jquery-ui.js"></script>
	<script type="text/javascript" src="/public/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/public/js/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="/public/js/jquery.bxslider.min.js"></script>

	<script type="text/javascript" src="/public/js/main.js"></script>
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-37706855-2']);
	  _gaq.push(['_trackPageview']);

	  (function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	<?php print $scripts; ?>
</head>
<!--
<?php if(empty($_SESSION['video'])) : ?>
	<a style="display:none;" href="http://youtube.googleapis.com/v/8W-zzZimK5Y?rel=0&amp;wmode=transparent&autoplay=1&controls=0&showinfo=0&vq=hd480" class="colorbox video_layer youtube cboxElement" >play me</a>
	<script>
	$(document).ready(function() {
		$('.video_layer').click();
	});
	$('.video_layer').colorbox({iframe:true, innerWidth:425, innerHeight:344});
	</script>
	<?php $_SESSION['video'] = true; ?>
<?php endif; ?>
-->

<body class="<?php print $body_classes; ?>">
<div id="bg_lol"></div>
	<?php if(isset($register_form)) { print $register_form; } ?>
<div id="top_line">
	<div class="wrapper">
	<!--<iframe src="http://www.draugiem.lv/user/381584/" height="0px" width="0px"></iframe>-->
		<ul>
            <li class="game_lol"><a title="League Of Legends" href="http://www.leagueoflegends.com" target="_blank"></a></li>
      <?php
	  /*
      $games_list = db_query("SELECT `tag`,`title` FROM ".PREFIX."speles ORDER BY `seciba` ASC");
      while($game = mysql_fetch_object($games_list)) {
        print '<li class="game_'.strtolower($game->tag).'"><a title="'.$game->title.' '.t('rank').'" href="/rank/'.$game->tag.'"></a></li>';
      }
	  */
      ?>
		</ul>
		<div id="your_ip"><?php print t('Your IP');?>: <span><?=$_SERVER['REMOTE_ADDR'];?></span></div>
	</div>
</div>

<div id="chat">
	<a id="button" href="/chat/"><?php print t('Chat'); ?></a>
</div>

<div class="wrapper" id="wrapper">
	<div id="header">
		<div id="logo"><a href="/"></a></div>
		<div class="login profile">
			<?php loginBox(); ?>
		</div>
			<?php print $search_form; ?>
	</div>
	<div id="language-switch">
		<?php print $lang_switcher; ?>
	</div>
		<div id="navigation">
		<?php navigation(); ?>
		</div>
		
	<div id="wrapper_wrapper">
	<div id="chat-box">		
	<?php
	
			$shoutbox = db_fetch_object(db_query("SELECT chat.*, ".PREFIX."lietotaji.lietotajvards AS username FROM `chat`
							     LEFT JOIN ".PREFIX."lietotaji ON ".PREFIX."lietotaji.id = chat.uid
							     ORDER BY `date` DESC LIMIT 1"));
	echo getMember($shoutbox->username).' <span class="time">'.laikaParveide($shoutbox->date).'</span><span class="chat-text">
	"'.bb2html(h(cut($shoutbox->text,160))).'"</span>';
	
	?></div>
	<?php if(!$is_front): ?>
	<div id="content_page">
	<?php endif;?>
	<?php
	if(function_exists('page')) {
		print $messages;
		page();
	}
	else {
		print '<h1 class="title">'.$title.'</h1>';
		print $messages;
		print $content;
	}
	?>
	<?php if(!$is_front): ?>
	<div class="clear"></div>
	</div>
	<?php endif; ?>
	





<!--
<div style="clear:both; background:red; padding:20px; color:white; text-align:center;"><?php print($queries_count); ?> queries</div>
-->

<div class="clear"></div>
<div class="footer">
	<div class="wrapper">
		<p><strong></strong><a href="#"><span></span></a></p>
		<ul></ul>
	</div>
</div>
<div class="to_top"><a href="#"></a></div>
<!-- new footer -->
<div id="footer">
	<div id="footer-left"><a href="/"></a></div>
	<div id="footer-center"></div>
	<div id="footer-right">
		<div id="footer-menu1">
            <a href="/stream"><?php print t('Streams'); ?></a>
			<a href="/content/partners/"><?php print t('Partners'); ?></a>
			<a href="/content/adverts/"><?php print t('Adverts'); ?></a>
			<a href="/contacts/"><?php print t('Contacts'); ?></a>
			<a href="/tournaments_admins/"><?php print t('Tournaments administration'); ?></a>
		</div>
		
		<div id="footer-menu2">
			<a href="/contacts/bug"><?php print t('Report about bug'); ?></a>
		</div>
		<div id="copyright">
			<?php print date('Y');?> Bliezam.lv. <?php print t('All rights reserved. Any form of copying without reference strictly prohibited.'); ?>
		</div>
		
	</div>
</div>
</div>
</div>
<!-- //new footer -->
</div>
</body></html>
<?php
if(User::$user->uid == 1 && debug == true) {
  dsm($_SESSION['debug']);

}
?>


